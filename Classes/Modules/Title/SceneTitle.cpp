#include "SceneTitle.h"

#include <algorithm>


#include <Modules/GameJam/SceneGameJam.h>




CSceneTitle::CSceneTitle() :CBaseWidget()
{
	
}

CSceneTitle::~CSceneTitle()
{

}

void CSceneTitle::Init()
{
	LoadFromXML("xml/screens/title/title.xml");

	g_SoundManager->Play("title");

	auto btnPlay = GetChildByTag<CButtonWidget>(100, true);
	
	btnPlay->SetButtonListener([&](CBaseWidget * sender, CButtonWidget_EventType type)
	{
		if (type == CButtonWidget_EventType::Event_Click)
		{
			auto queue = CActionQueue::Create();
			queue->AddAction(CShowAction::Create(GetChildByTag(999), 0.2f));
			queue->AddAction(CCallbackAction::Create([&](){
				g_SoundManager->Stop("title");
				CScenesManager::GetInstance()->SetScene("comics_start");
			}));
			AddChild(queue);			
		}
	});

	auto queue = CActionQueue::Create();
	queue->AddAction(CHideAction::Create(GetChildByTag(999), 0.2f));
	AddChild(queue);
}


