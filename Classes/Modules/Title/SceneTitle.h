#ifndef MODULE_TITLE
#define MODULE_TITLE

#include <Framework.h>
#include <optimus/optimus.h>



class CSceneTitle : public CBaseWidget
{
public:
	OPTIMUS_CREATE(CSceneTitle);
	virtual ~CSceneTitle();

	virtual void Init();	
private:
	CSceneTitle();
};


#endif //MODULE_TITLE

