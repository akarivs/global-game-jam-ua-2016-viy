#ifndef MODULE_COMICS_START
#define MODULE_COMICS_START

#include <Framework.h>
#include <optimus/optimus.h>



class CSceneComicsStart : public CBaseWidget
{
public:
	OPTIMUS_CREATE(CSceneComicsStart);
	virtual ~CSceneComicsStart();

	virtual void Init();	
	virtual ReturnCodes OnMouseUp(const Vector &mousePos, const int mouseButton);
private:
	CSceneComicsStart();
};


#endif //MODULE_COMICS_START

