#ifndef MODULE_COMICS_DEATH
#define MODULE_COMICS_DEATH

#include <Framework.h>
#include <optimus/optimus.h>



class CSceneComicsDeath : public CBaseWidget
{
public:
	OPTIMUS_CREATE(CSceneComicsDeath);
	virtual ~CSceneComicsDeath();

	virtual void Init();	
	virtual ReturnCodes OnMouseUp(const Vector &mousePos, const int mouseButton);
private:
	CSceneComicsDeath();

	std::string currentTrack;
};


#endif //MODULE_COMICS_DEATH

