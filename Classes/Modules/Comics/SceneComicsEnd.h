#ifndef MODULE_COMICS_END
#define MODULE_COMICS_END

#include <Framework.h>
#include <optimus/optimus.h>



class CSceneComicsEnd : public CBaseWidget
{
public:
	OPTIMUS_CREATE(CSceneComicsEnd);
	virtual ~CSceneComicsEnd();

	virtual void Init();	
	virtual ReturnCodes OnMouseUp(const Vector &mousePos, const int mouseButton);
private:
	CSceneComicsEnd();
};


#endif //MODULE_COMICS_END

