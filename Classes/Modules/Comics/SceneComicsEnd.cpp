#include "SceneComicsEnd.h"

#include <algorithm>







CSceneComicsEnd::CSceneComicsEnd() :CBaseWidget()
{
	
}

CSceneComicsEnd::~CSceneComicsEnd()
{

}

void CSceneComicsEnd::Init()
{
	LoadFromXML("xml/screens/comics/end.xml");

	



	{
		auto queue = CActionQueue::Create();
		queue->AddAction(CHideAction::Create(GetChildByTag(999), 0.2f));
		AddChild(queue);
	}


	{
		auto queue = CActionQueue::Create();
		queue->AddAction(CScaleToAction::Create(GetChildByTag(1000), 10.2f, Vector(1.0f, 1.0f)));
		AddChild(queue);
	}

	{
		GetChildByTag(2001, true)->SetOpacity(0);

		auto queue = CActionQueue::Create();
		queue->AddAction(CSleepAction::Create(2.0f));
		queue->AddAction(CShowAction::Create(GetChildByTag(2001, true), 0.5f));
		queue->AddAction(CCallbackAction::Create([&](){
			g_SoundManager->Play("end_comics");
		}));
		AddChild(queue);
	}
}

//////////////////////////////////////////////////////////////////////////
ReturnCodes CSceneComicsEnd::OnMouseUp(const Vector &mousePos, const int mouseButton)
{
	auto queue = CActionQueue::Create();
	queue->AddAction(CShowAction::Create(GetChildByTag(999), 0.2f));
	queue->AddAction(CCallbackAction::Create([&](){
		g_SoundManager->Stop("end_comics");
		g_GameApplication->StopApp();
	}));
	AddChild(queue);
	return ReturnCodes::Skip;
}