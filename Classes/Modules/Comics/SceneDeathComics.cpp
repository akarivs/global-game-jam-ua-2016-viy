#include "SceneDeathComics.h"

#include <algorithm>







CSceneComicsDeath::CSceneComicsDeath() :CBaseWidget()
{
	
}

CSceneComicsDeath::~CSceneComicsDeath()
{

}

void CSceneComicsDeath::Init()
{
	LoadFromXML("xml/screens/comics/death.xml");

	
	int track = g_Random->RandI(1, 3);

	currentTrack = "death_" + StringUtils::ToString(track);

	GetChildByTag(3000 + track, true)->SetVisible(true);

	{
		auto queue = CActionQueue::Create();
		queue->AddAction(CHideAction::Create(GetChildByTag(999), 0.2f));
		AddChild(queue);
	}


	{
		auto queue = CActionQueue::Create();
		queue->AddAction(CScaleToAction::Create(GetChildByTag(1000), 10.2f, Vector(1.0f, 1.0f)));
		AddChild(queue);
	}

	{
		GetChildByTag(2001, true)->SetOpacity(0);

		auto queue = CActionQueue::Create();
		queue->AddAction(CSleepAction::Create(0.4f));
		queue->AddAction(CShowAction::Create(GetChildByTag(2001, true), 0.5f));
		queue->AddAction(CCallbackAction::Create([&](){
			g_SoundManager->Play(currentTrack);
		}));
		AddChild(queue);
	}
}

//////////////////////////////////////////////////////////////////////////
ReturnCodes CSceneComicsDeath::OnMouseUp(const Vector &mousePos, const int mouseButton)
{
	auto queue = CActionQueue::Create();
	queue->AddAction(CShowAction::Create(GetChildByTag(999), 0.2f));
	queue->AddAction(CCallbackAction::Create([&](){
		g_SoundManager->Stop(currentTrack);
		CScenesManager::GetInstance()->SetScene("jam");
	}));
	AddChild(queue);


	return ReturnCodes::Skip;
}