#include "SceneSplash.h"

#include <algorithm>



CSceneSplash * _sceneSplash = NULL;
CSceneSplash * CSceneSplash::GetScene()
{
	return _sceneSplash;
}

CSceneSplash::CSceneSplash() :CBaseWidget()
{
	_sceneSplash = this;
	m_AnimTime = 0.6f;
	m_PauseTime = 2.3f;
	m_CurrTime = 0.0f;
	m_State = 0;
	m_IsEnd = false;
}

CSceneSplash::~CSceneSplash()
{

}

void CSceneSplash::Init()
{
	RemoveAllChilds();
	m_Splashes.clear();

	InitResources();
}

void CSceneSplash::InitResources()
{
	std::string gameDir = "Branch/";
	for (int i = 1; i <= 5; i++)
	{
		CImageWidget * splash = nullptr;
		std::string splashItem = gameDir + StringUtils::ToString(i) + ".jpg";

		if (g_FileSystem->IsFileExists(splashItem))
		{
			splash = CImageWidget::Create(splashItem);
			splash->SetOpacity(0.0f);
			m_Splashes.push_back(splash);
		}

		splashItem = gameDir + StringUtils::ToString(i) + ".png";
		if (g_FileSystem->IsFileExists(splashItem))
		{
			splash = CImageWidget::Create(splashItem);
			splash->SetOpacity(0.0f);
			m_Splashes.push_back(splash);
		}

		splashItem = gameDir + StringUtils::ToString(i) + ".jpeg";
		if (g_FileSystem->IsFileExists(splashItem))
		{
			splash = CImageWidget::Create(splashItem);
			splash->SetOpacity(0.0f);
			m_Splashes.push_back(splash);
		}
	}
	for (size_t i = 0; i < m_Splashes.size(); i++)
	{
		AddChild(m_Splashes[i]);
	}

	m_ActionsQueue = CActionQueue::Create();
	for (int i = 0; i < m_Splashes.size(); i++)
	{
		CShowAction * show = CShowAction::Create(m_Splashes[i], 1.0f);
		CSleepAction * pause = CSleepAction::Create(m_PauseTime);
		CHideAction * hide = CHideAction::Create(m_Splashes[i], 1.0f);

		m_ActionsQueue->AddActions({ show, pause, hide });
	}
	CCallbackAction * callback = CCallbackAction::Create(OPTIMUS_CALLBACK_0(&CSceneSplash::SwitchScene, this));
	m_ActionsQueue->AddAction(callback);
	AddChild(m_ActionsQueue);
}


ReturnCodes CSceneSplash::OnMouseUp(const Vector & mousePos, const int button)
{
	auto res = CBaseWidget::OnMouseUp(mousePos, button);
	if (res != ReturnCodes::Skip)return res;

	if (m_ActionsQueue)
	{
		m_ActionsQueue = m_ActionsQueue->SkipAction();
	}


	return ReturnCodes::Skip;
}

void CSceneSplash::SwitchScene()
{
	std::string scene = "";
	g_Config->GetValue("after_splash_scene", scene);
	CScenesManager::GetInstance()->SetScene(scene);
}

