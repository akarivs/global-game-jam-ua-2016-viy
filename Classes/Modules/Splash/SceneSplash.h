#ifndef COMMON_MODULE_SPLASH
#define COMMON_MODULE_SPLASH

#include <Framework.h>
#include <optimus/optimus.h>



class CSceneSplash : public CBaseWidget
{
public:
	OPTIMUS_CREATE(CSceneSplash);
	virtual ~CSceneSplash();
	static CSceneSplash * GetScene();

	virtual void Init();

	virtual ReturnCodes OnMouseUp(const Vector & mousePos, const int button);
	virtual void InitResources();
private:
	CSceneSplash();
	void SwitchScene();
	std::vector<CImageWidget*>m_Splashes;
	int m_CurrentSplash;
	float m_AnimTime;
	float m_PauseTime;
	float m_CurrTime;
	int m_State;
	bool m_IsEnd;

	CActionQueue * m_ActionsQueue;
};


#endif //COMMON_MODULE_SPLASH

