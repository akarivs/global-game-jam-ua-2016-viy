#include "WinWindow.h"

#include "../SceneGameJam.h"




//////////////////////////////////////////////////////////////////////////
CWinWindow::CWinWindow() :CWindowWidget()
{
	m_CanHide = false;
}

//////////////////////////////////////////////////////////////////////////
CWinWindow::~CWinWindow()
{

}

//////////////////////////////////////////////////////////////////////////
void CWinWindow::Init()
{
	RemoveAllChilds(true);

	CWindowWidget::Init();
	SetImmortal(true);

	SetCloseOnEsc(false);
	LoadFromXML("xml/screens/win.xml");
}

//////////////////////////////////////////////////////////////////////////
ReturnCodes CWinWindow::OnMouseUp(const Vector &mousePos, const int mouseButton)
{
	if (m_CanHide)
	{
		Hide();
		return ReturnCodes::Block;
	}

	return ReturnCodes::Skip;
}

//////////////////////////////////////////////////////////////////////////
void CWinWindow::OnShowStart()
{
	auto text = GetChildByTag<CTextWidget>(400, true);
	int level = CSceneGameJam::GetScene()->level + 1;

	text->SetText(g_LocaleManager->GetText("win_text_" + StringUtils::ToString(level)));
	
}

//////////////////////////////////////////////////////////////////////////
void CWinWindow::OnShowEnd()
{
	m_CanHide = true;
}

//////////////////////////////////////////////////////////////////////////
void CWinWindow::OnHideEnd()
{
	CSceneGameJam::GetScene()->NextLevel();	
}

