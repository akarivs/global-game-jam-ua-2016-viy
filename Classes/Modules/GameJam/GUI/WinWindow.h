#ifndef JAM_WIN_WINDOW
#define JAM_WIN_WINDOW

#include <Framework.h>
#include <optimus/optimus.h>

#include <functional>

using namespace optimus;


/** @brief Окно победы
*
* @author Vadim Simonov <akari.vs@gmail.com>
* @copyright 2013-2015 New Bridge Games
*
*/

class CWinWindow : public CWindowWidget
{
public:
	OPTIMUS_CREATE(CWinWindow);
	virtual ~CWinWindow();

	
	virtual void Init();		

	virtual ReturnCodes OnMouseUp(const Vector &mousePos, const int mouseButton);
	virtual void OnShowStart();
	virtual void OnShowEnd();
	virtual void OnHideEnd();
protected:
	
	CWinWindow();
	bool m_CanHide;
};
#endif 