#ifndef BASE_PERSON
#define BASE_PERSON

#include <Framework.h>
#include <optimus/optimus.h>

using namespace optimus;

/** @brief Base VIY person
*
* @author Vadim Simonov <akari.vs@gmail.com>
* @copyright 2013-2015 New Bridge Games
*
*/
class CBasePerson : public CBaseWidget
{
public:
	OPTIMUS_CREATE(CBasePerson);
	virtual ~CBasePerson();

	virtual void Init();

	virtual ReturnCodes OnMouseMove(const Vector &mousePos);
	virtual ReturnCodes OnMouseUp(const Vector &mousePos, const int mouseButton);	
	virtual ReturnCodes Update(const float dt);	

	bool isHero;

protected:
	CBasePerson();

	GETTER_SETTER(IntVector, m_fieldPos, FieldPos);

	virtual void MoveUp(const float dt);
	virtual void MoveLeft(const float dt);
	virtual void MoveRight(const float dt);
	virtual void MoveDown(const float dt);

	

	float m_Speed;

	bool m_IsCheckCollisions;
};
#endif ///BASE_PERSON

