#include "EnemyPerson.h"
#include "../SceneGameJam.h"


//////////////////////////////////////////////////////////////////////////	
CEnemyPerson::CEnemyPerson():CBasePerson()
{
	g_Config->GetValue("min_distance_to_hero", m_MinDistanceToHero);
	m_Speed = 250.0f + g_Random->RandF(0.0f, 10.0f);
	SetCatchTouch(true);

	m_IsCheckCollisions = true;
	isHero = false;
	m_Moving = false;

	heroOffsets.push_back(IntVector(-1, 0));
	heroOffsets.push_back(IntVector(1, 0));
	heroOffsets.push_back(IntVector(0, -1));
	heroOffsets.push_back(IntVector(0, 1));
	heroOffsets.push_back(IntVector(1, -1));
	heroOffsets.push_back(IntVector(1, 1));
	heroOffsets.push_back(IntVector(-1, 1));
	heroOffsets.push_back(IntVector(-1, -1));

	m_CurState = States::Idle;
}

//////////////////////////////////////////////////////////////////////////
CEnemyPerson::~CEnemyPerson()
{

}

//////////////////////////////////////////////////////////////////////////
void CEnemyPerson::HolyWater()
{
	ChangeState(States::HolyWater);
}


//////////////////////////////////////////////////////////////////////////
void CEnemyPerson::Ressurect()
{
	m_HolyWaterTimer = 0.0f;
	ChangeState(States::Run);	
}

//////////////////////////////////////////////////////////////////////////
void CEnemyPerson::Init()
{	
	
}

//////////////////////////////////////////////////////////////////////////
void CEnemyPerson::SetType(const int type)
{
	auto container = CBaseWidget::Create();
	
	m_Type = type; 

	auto attackCallback = [&]()
	{
		if (CSceneGameJam::GetScene()->hero->health > 0)
		{

			if (m_Type == 0)
			{
				g_SoundManager->Play("monster_1");
			}
			else if (m_Type == 1)
			{
				g_SoundManager->Play("monster_2");
			}
			else if (m_Type == 2)
			{
				g_SoundManager->Play("monster_3");
			}

			int rnd = g_Random->RandI(0, 2);
			if (rnd == 0)
			{
				g_SoundManager->Play("ouch_1");
			}
			else if (rnd == 1)
			{
				g_SoundManager->Play("ouch_2");
			}
			else if (rnd == 2)
			{
				g_SoundManager->Play("ouch_3");
			}

			CSceneGameJam::GetScene()->hero->health -= m_AP;
			if (CSceneGameJam::GetScene()->hero->health <= 0)
			{
				CSceneGameJam::GetScene()->hero->health = 0;
				CSceneGameJam::GetScene()->hero->Death();				
			}
		}		
	};

	m_Shadow = CImageWidget::Create("images/effects/shadow.png");
	m_Shadow->SetScaleX(1.2f);
	m_Shadow->SetScaleY(1.6f);
	AddChild(m_Shadow);

	if (type == 0)
	{
		m_Idle = CSpriteAnimationWidget::Create();
		m_Idle->InitAnimation("images/persons/monster_1/idle.png", 51, 0.05f, AnimationType::ForwardLoop, 80);
		m_Idle->Play();
		m_Idle->SetVisible(false);
		m_Idle->SetDisabled(true);
		container->AddChild(m_Idle);


		m_Run = CSpriteAnimationWidget::Create();
		m_Run->InitAnimation("images/persons/monster_1/run.png", 20, 0.05f, AnimationType::ForwardLoop, 1);
		m_Run->Play();
		m_Run->SetVisible(false);
		m_Run->SetDisabled(true);
		container->AddChild(m_Run);

		m_Attack = CSpriteAnimationWidget::Create();
		m_Attack->InitAnimation("images/persons/monster_1/attack.png", 41, 0.033f, AnimationType::ForwardLoop, 30);
		m_Attack->Play();
		m_Attack->SetVisible(false);
		m_Attack->SetDisabled(true);
		m_Attack->SetCallback(41, attackCallback);
		m_Attack->SetCallback(59, attackCallback);



		container->AddChild(m_Attack);

		m_HolyWater = CSpriteAnimationWidget::Create();
		m_HolyWater->InitAnimation("images/persons/monster_1/water.png", 21, 0.033f, AnimationType::ForwardLoop, 140);
		m_HolyWater->Play();
		m_HolyWater->SetVisible(false);
		m_HolyWater->SetDisabled(true);
		container->AddChild(m_HolyWater);

		g_Config->GetValue("holy_water_time_monster_1", m_HolyWaterMaxTime);
		g_Config->GetValue("attack_point_monster_1", m_AP);
		g_Config->GetValue("speed_monster_1", m_Speed);

		container->SetScale(0.75f);
	}
	else if (type == 1)
	{
		m_Idle = CSpriteAnimationWidget::Create();
		m_Idle->InitAnimation("images/persons/monster_2/idle.png", 41, 0.05f, AnimationType::ForwardLoop, 150);
		m_Idle->Play();
		m_Idle->SetVisible(false);
		m_Idle->SetDisabled(true);
		container->AddChild(m_Idle);


		m_Run = CSpriteAnimationWidget::Create();
		m_Run->InitAnimation("images/persons/monster_2/run.png", 40, 0.05f, AnimationType::ForwardLoop, 1);
		m_Run->Play();
		m_Run->SetVisible(false);
		m_Run->SetDisabled(true);
		container->AddChild(m_Run);

		m_Attack = CSpriteAnimationWidget::Create();
		m_Attack->InitAnimation("images/persons/monster_2/attack.png", 51, 0.023f, AnimationType::ForwardLoop, 50);
		m_Attack->Play();
		m_Attack->SetVisible(false);
		m_Attack->SetDisabled(true);
		m_Attack->SetCallback(77, attackCallback);		

		container->AddChild(m_Attack);

		m_HolyWater = CSpriteAnimationWidget::Create();
		m_HolyWater->InitAnimation("images/persons/monster_2/water.png", 31, 0.033f, AnimationType::ForwardLoop, 110);
		m_HolyWater->Play();
		m_HolyWater->SetVisible(false);
		m_HolyWater->SetDisabled(true);
		container->AddChild(m_HolyWater);

		g_Config->GetValue("holy_water_time_monster_2", m_HolyWaterMaxTime);
		g_Config->GetValue("attack_point_monster_2", m_AP);
		g_Config->GetValue("speed_monster_2", m_Speed);

		container->SetScale(0.56f);
	}
	else if (type == 2)
	{
		m_Idle = CSpriteAnimationWidget::Create();
		m_Idle->InitAnimation("images/persons/monster_3/idle.png", 51, 0.05f, AnimationType::ForwardLoop, 65);
		m_Idle->Play();
		m_Idle->SetVisible(false);
		m_Idle->SetDisabled(true);
		container->AddChild(m_Idle);


		m_Run = CSpriteAnimationWidget::Create();
		m_Run->InitAnimation("images/persons/monster_3/run.png", 60, 0.05f, AnimationType::ForwardLoop, 1);
		m_Run->Play();
		m_Run->SetVisible(false);
		m_Run->SetDisabled(true);
		container->AddChild(m_Run);

		m_Attack = CSpriteAnimationWidget::Create();
		m_Attack->InitAnimation("images/persons/monster_3/attack.png", 36, 0.015f, AnimationType::ForwardLoop, 115);
		m_Attack->Play();
		m_Attack->SetVisible(false);
		m_Attack->SetDisabled(true);
		m_Attack->SetCallback(138, attackCallback);

		container->AddChild(m_Attack);

		m_HolyWater = nullptr;

		g_Config->GetValue("holy_water_time_monster_3", m_HolyWaterMaxTime);
		g_Config->GetValue("attack_point_monster_3", m_AP);
		g_Config->GetValue("speed_monster_3", m_Speed);

		container->SetScale(0.66f);

		m_Shadow->SetScaleX(2.6f);
		m_Shadow->SetScaleY(3.3f);

		m_MinDistanceToHero *= 1.35f;
	}

	AddChild(container);
	m_CurAnim = nullptr;
	ChangeState(States::Run);
}


//////////////////////////////////////////////////////////////////////////
void CEnemyPerson::ChangeState(States state)
{
	if (m_CurState == state)return;	
	m_CurState = state;

	if (m_CurAnim)
	{
		m_CurAnim->SetVisible(false);
		m_CurAnim->SetDisabled(true);
	}

	if (state == States::Run)
	{
		m_CurAnim = m_Run;
	}
	else if (state == States::Attack)
	{
		m_CurAnim = m_Attack;
	}
	else if (state == States::HolyWater)
	{
		m_HolyWaterTimer = m_HolyWaterMaxTime;
		m_CurAnim = m_HolyWater;
	}
	else if (state == States::Idle)
	{
		m_CurAnim = m_Idle;
	}

	m_CurAnim->SetVisible(true);
	m_CurAnim->SetDisabled(false);
}


//////////////////////////////////////////////////////////////////////////
ReturnCodes CEnemyPerson::Update(const float dt)
{
	auto code = CBasePerson::Update(dt);
	

	auto hero = CSceneGameJam::GetScene()->hero;
	auto field = CSceneGameJam::GetScene()->field;
	auto enemies = CSceneGameJam::GetScene()->enemies;
	auto lines = CSceneGameJam::GetScene()->lines;
	auto circles = CSceneGameJam::GetScene()->circles;
	auto coffins = CSceneGameJam::GetScene()->coffinPoints;

	auto & cellMy = field->GetCell(GetPositionX(), GetPositionY());
	auto & cellHero = field->GetCell(hero->GetPositionX(), hero->GetPositionY());

	if (m_CurState != States::HolyWater)
	{
		float angle = MathUtils::AngleBetween(hero->GetPosition(), GetPosition());
		SetRotation(angle - 90.0f);

		m_Shadow->SetRotation(360.0f - (angle - 90.0f));
	}	

	if (m_CurState == States::Idle || m_CurState == States::Run)
	{
		if (m_Moving == false)
		{
			if (hero->inCircle)
			{
				ChangeState(States::Idle);
				return ReturnCodes::Skip;
			}
			else
			{
				ChangeState(States::Run);
			}


			float length = (hero->GetPosition() - GetPosition()).GetLength();
			if (length <= m_MinDistanceToHero)
			{
				ChangeState(States::Attack);
				return ReturnCodes::Skip;
			}


			bool find = false;

			int offset = -1;

			cellHero = field->GetCell(hero->GetPositionX(), hero->GetPositionY());
			for (int i = 0; i < heroOffsets.size(); i++)
			{
				auto cellHeroNew = field->m_Field[cellHero.x + heroOffsets[i].x][cellHero.y + heroOffsets[i].y];
				if (cellHeroNew.free == false)
				{
					continue;
				}
				
				bool lineOnCell = false;
				for (int j = 0; j < lines.size(); j++)
				{
					auto line = lines[j];
					if (line.pos.x == cellHeroNew.x && line.pos.y == cellHeroNew.y)
					{
						lineOnCell = true;
						break;
					}
				}
				if (lineOnCell)
				{
					continue;
				}

				bool circleOnCell = false;
				for (int j = 0; j < coffins.size(); j++)
				{
					auto coffin = coffins[j];
					if (coffin.x== cellHeroNew.x && coffin.y == cellHeroNew.y)
					{
						circleOnCell = true;
						break;
					}
				}
				if (circleOnCell)
				{
					continue;
				}


				bool used = false;
				for (int j = 0; j < enemies.size(); j++)
				{
					auto enemy = enemies[j];
					if (enemy == this)continue;
					if (enemy->moveTo.x == cellHero.x + heroOffsets[i].x && enemy->moveTo.y == cellHero.y + heroOffsets[i].y)
					{
						used = true;
						break;
					}
				}
				if (used == false)
				{
					offset = i;
					break;
				}
			}

			std::vector<Direction> dirVec;
			if (offset == -1)
			{
				dirVec = field->FindPath(cellMy.x, cellMy.y, cellHero.x, cellHero.y);
			}
			else
			{
				dirVec = field->FindPath(cellMy.x, cellMy.y, cellHero.x + heroOffsets[offset].x, cellHero.y + heroOffsets[offset].y);
			}

			if (dirVec.size() > 0)
			{
				auto queue = CActionQueue::Create();

				Vector pos;
				float length;

				auto direction = dirVec[0];
				if (direction == Up)
				{
					pos = field->GetCellPos(cellMy.x, cellMy.y + 1);
					moveTo = IntVector(cellMy.x, cellMy.y + 1);
					length = (pos - GetPosition()).GetLength();

					pos.x += g_Random->RandF(-2.0f, 2.0f);
				}
				else if (direction == Down)
				{
					pos = field->GetCellPos(cellMy.x, cellMy.y - 1);
					moveTo = IntVector(cellMy.x, cellMy.y - 1);
					length = (pos - GetPosition()).GetLength();

					pos.x += g_Random->RandF(-2.0f, 2.0f);
				}
				else if (direction == Left)
				{
					pos = field->GetCellPos(cellMy.x - 1, cellMy.y);
					length = (pos - GetPosition()).GetLength();
					moveTo = IntVector(cellMy.x - 1, cellMy.y);

					pos.y += g_Random->RandF(-2.0f, 2.0f);
				}
				else if (direction == Right)
				{
					pos = field->GetCellPos(cellMy.x + 1, cellMy.y);
					length = (pos - GetPosition()).GetLength();
					moveTo = IntVector(cellMy.x + 1, cellMy.y);

					pos.y += g_Random->RandF(-2.0f, 2.0f);
				}
				else if (direction == UpLeft)
				{
					pos = field->GetCellPos(cellMy.x - 1, cellMy.y + 1);
					length = (pos - GetPosition()).GetLength();
					moveTo = IntVector(cellMy.x - 1, cellMy.y + 1);
				}
				else if (direction == UpRight)
				{
					pos = field->GetCellPos(cellMy.x + 1, cellMy.y + 1);
					length = (pos - GetPosition()).GetLength();
					moveTo = IntVector(cellMy.x + 1, cellMy.y + 1);
				}
				else if (direction == DownLeft)
				{
					pos = field->GetCellPos(cellMy.x - 1, cellMy.y - 1);
					length = (pos - GetPosition()).GetLength();
					moveTo = IntVector(cellMy.x - 1, cellMy.y - 1);
				}
				else if (direction == DownRight)
				{
					pos = field->GetCellPos(cellMy.x + 1, cellMy.y - 1);
					length = (pos - GetPosition()).GetLength();
					moveTo = IntVector(cellMy.x + 1, cellMy.y - 1);
				}



				queue->AddAction(CMoveToAction::Create(this, length / m_Speed, pos));
				queue->AddAction(CCallbackAction::Create([&](){
					m_Moving = false;
				}));
				AddChild(queue);
				m_Moving = true;
			}
		}
	}
	else if (m_CurState == States::Attack)
	{
		if ((hero->GetPosition() - GetPosition()).GetLength() > m_MinDistanceToHero)
		{
			ChangeState(States::Run);
		}
		if (hero->inCircle)
		{
			ChangeState(States::Run);
		}
	}
	else if (m_CurState == States::HolyWater)
	{
		if (m_HolyWaterTimer >= 0.0f)
		{
			m_HolyWaterTimer -= dt;
			if (m_HolyWaterTimer <= 0.0f)
			{
				ChangeState(States::Run);
			}
		}
	}
	
	return ReturnCodes::Skip;
}

//////////////////////////////////////////////////////////////////////////
void CEnemyPerson::MoveUp(const float dt)
{	
	float newY = GetPositionY() - m_Speed * dt;
	
	auto field = CSceneGameJam::GetScene()->field;
	auto & cell = field->GetCell(GetPositionX(), newY);
	if (cell.free)
	{
		SetPositionY(GetPositionY() - m_Speed * dt);
	}	
}

//////////////////////////////////////////////////////////////////////////
void CEnemyPerson::MoveDown(const float dt)
{
	float newY = GetPositionY() + m_Speed * dt;

	auto field = CSceneGameJam::GetScene()->field;
	auto & cell = field->GetCell(GetPositionX(), newY);
	if (cell.free)
	{
		SetPositionY(GetPositionY() + m_Speed * dt);
	}	
}

//////////////////////////////////////////////////////////////////////////
void CEnemyPerson::MoveLeft(const float dt)
{
	float newX = GetPositionX() - m_Speed * dt;

	auto field = CSceneGameJam::GetScene()->field;
	auto & cell = field->GetCell(newX, GetPositionY());
	if (cell.free)
	{
		SetPositionX(GetPositionX() - m_Speed * dt);
	}	
}

//////////////////////////////////////////////////////////////////////////
void CEnemyPerson::MoveRight(const float dt)
{
	float newX = GetPositionX() + m_Speed * dt;

	auto field = CSceneGameJam::GetScene()->field;
	auto & cell = field->GetCell(newX, GetPositionY());
	if (cell.free)
	{
		SetPositionX(GetPositionX() + m_Speed * dt);
	}
}

