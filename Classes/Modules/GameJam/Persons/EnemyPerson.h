#ifndef ENEMY_PERSON
#define ENEMY_PERSON

#include <Framework.h>
#include <optimus/optimus.h>

#include "BasePerson.h"

using namespace optimus;

/** @brief Enemy VIY person
*
* @author Vadim Simonov <akari.vs@gmail.com>
* @copyright 2013-2015 New Bridge Games
*
*/
class CEnemyPerson : public CBasePerson
{
public:
	OPTIMUS_CREATE(CEnemyPerson);
	virtual ~CEnemyPerson();

	virtual void Init();
	void SetType(const int type);

	virtual ReturnCodes Update(const float dt);	
	void HolyWater();
	void Ressurect();

	IntVector moveTo;
	int m_Type;
protected:
	enum class States { Idle, Run, Attack, HolyWater };
	States m_CurState;

	void ChangeState(States state);

	CEnemyPerson();

	std::vector<IntVector> heroOffsets;

	CImageWidget * m_Shadow;
	

	virtual void MoveUp(const float dt);
	virtual void MoveLeft(const float dt);
	virtual void MoveRight(const float dt);
	virtual void MoveDown(const float dt);		

	bool m_Moving;
	float m_MinDistanceToHero;
	float m_HolyWaterMaxTime;
	float m_HolyWaterTimer;

	CSpriteAnimationWidget * m_CurAnim;

	CSpriteAnimationWidget * m_Idle;
	CSpriteAnimationWidget * m_Run;
	CSpriteAnimationWidget * m_Attack;
	CSpriteAnimationWidget * m_HolyWater;

	float m_AP;
};
#endif ///ENEMY_PERSON

