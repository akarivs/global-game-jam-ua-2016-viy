#include "HeroPerson.h"
#include "../SceneGameJam.h"
#include "../HolyWater.h"


//////////////////////////////////////////////////////////////////////////	
CHeroPerson::CHeroPerson() :CBasePerson()
{
	m_Speed = 250.0f;
	SetCatchTouch(true);

	m_IsCheckCollisions = true;
	isHero = true;

	m_SpeedForward = 260.0f;
	m_SpeedBackward = 180.0f;

	inCircle = false;

	m_Angle = 0.0f;
	health = 100.0f;

	chalk = 0;
	holyWater = 0;

	g_Config->GetValue("max_chalk", maxChalk);
	g_Config->GetValue("max_holy_water", maxHolyWater);
}

//////////////////////////////////////////////////////////////////////////
CHeroPerson::~CHeroPerson()
{

}

//////////////////////////////////////////////////////////////////////////
void CHeroPerson::Death()
{
	ChangeState(States::Death, true);
}


//////////////////////////////////////////////////////////////////////////
void CHeroPerson::Init()
{
	m_CurState = (States)-1;

	m_CurTorso = nullptr;
	m_CurLegs = nullptr;

	auto m_Shadow = CImageWidget::Create("images/effects/shadow.png");
	m_Shadow->SetScaleX(1.2f);
	m_Shadow->SetScaleY(1.6f);
	AddChild(m_Shadow);

	auto container = CBaseWidget::Create();
	container->SetScale(0.74f);

	m_TorsoIdle = CSpriteAnimationWidget::Create();

	m_TorsoIdle->AddChild(CImageWidget::Create("images/persons/hero/idle/legs.png"));
	m_TorsoIdle->AddChild(CImageWidget::Create("images/persons/hero/idle/body.png"));
	m_TorsoIdle->AddChild(CImageWidget::Create("images/persons/hero/idle/head.png"));

	m_TorsoIdle->SetVisible(false);
	m_TorsoIdle->SetDisabled(true);
	container->AddChild(m_TorsoIdle);

	

	m_LegsRun = CSpriteAnimationWidget::Create();
	m_LegsRun->InitAnimation("images/persons/hero/run/legs.png", 20, 0.05f, AnimationType::ForwardLoop, 1);
	m_LegsRun->SetVisible(false);
	m_LegsRun->SetDisabled(true);
	container->AddChild(m_LegsRun);

	m_TorsoRun = CSpriteAnimationWidget::Create();
	m_TorsoRun->InitAnimation("images/persons/hero/run/body.png", 20, 0.05f, AnimationType::ForwardLoop, 1);
	m_TorsoRun->SetVisible(false);
	m_TorsoRun->SetDisabled(true);
	container->AddChild(m_TorsoRun);


	m_LegsRunBack = CSpriteAnimationWidget::Create();
	m_LegsRunBack->InitAnimation("images/persons/hero/back/legs.png", 21, 0.065f, AnimationType::ForwardLoop, 30);
	m_LegsRunBack->SetVisible(false);
	m_LegsRunBack->SetDisabled(true);
	container->AddChild(m_LegsRunBack);

	m_TorsoRunBack = CSpriteAnimationWidget::Create();
	m_TorsoRunBack->InitAnimation("images/persons/hero/back/body.png", 21, 0.065f, AnimationType::ForwardLoop, 30);
	m_TorsoRunBack->SetVisible(false);
	m_TorsoRunBack->SetDisabled(true);
	container->AddChild(m_TorsoRunBack);


	m_DrawLine = CSpriteAnimationWidget::Create();
	m_DrawLine->InitAnimation("images/persons/hero/line.png", 35, 0.04f, AnimationType::ForwardOnce, 51);
	m_DrawLine->SetVisible(false);
	m_DrawLine->SetDisabled(true);
	m_DrawLine->SetCallback(85, [&](){
		ChangeState(States::Run, true);
	});
	container->AddChild(m_DrawLine);	

	m_ThrowBottle = CSpriteAnimationWidget::Create();
	m_ThrowBottle->InitAnimation("images/persons/hero/bottle.png", 21, 0.04f, AnimationType::ForwardOnce, 185);
	m_ThrowBottle->SetVisible(false);
	m_ThrowBottle->SetDisabled(true);
	m_ThrowBottle->SetCallback(190, [&](){
		CHolyWater * water = CHolyWater::Create();
		water->Shoot(MathUtils::ToRadian(360 - m_Angle), GetPosition());
		CSceneGameJam::GetScene()->m_ObjectsContainer->AddChild(water);
	});
	m_ThrowBottle->SetCallback(205, [&](){
		ChangeState(States::Run, true);
	});
	container->AddChild(m_ThrowBottle);

	m_TorsoPray = CSpriteAnimationWidget::Create();
	m_TorsoPray->InitAnimation("images/persons/hero/pray.png", 66, 0.04f, AnimationType::ForwardLoop, 85);
	m_TorsoPray->SetVisible(false);
	m_TorsoPray->SetDisabled(true);	
	container->AddChild(m_TorsoPray);

	m_TorsoDeath = CSpriteAnimationWidget::Create();
	m_TorsoDeath->InitAnimation("images/persons/hero/death.png", 37, 0.04f, AnimationType::ForwardOnce, 150);
	m_TorsoDeath->SetVisible(false);
	m_TorsoDeath->SetDisabled(true);
	m_TorsoDeath->SetCallback(186, [&](){
		CSceneGameJam::GetScene()->Lose();
	});
	container->AddChild(m_TorsoDeath);


	


	AddChild(container);

	m_LegsRun->SetLayer(0);
	m_LegsRunBack->SetLayer(0);

	m_TorsoRun->SetLayer(1);
	m_TorsoRunBack->SetLayer(1);
	m_DrawLine->SetLayer(1);

	ChangeState(States::Run);
}


//////////////////////////////////////////////////////////////////////////
void CHeroPerson::ChangeState(States state, bool immediately)
{
	if (m_CurState == state)return;
	if (m_CurState == States::DrawCircle || m_CurState == States::DrawLine)
	{
		if (immediately == false)
		{
			return;
		}
	}

	g_SoundManager->Stop("pray");

	m_CurState = state;

	if (m_CurTorso)
	{
		m_CurTorso->SetVisible(false);
		m_CurTorso->SetDisabled(true);
	}

	if (m_CurLegs)
	{
		m_CurLegs->SetVisible(false);
		m_CurLegs->SetDisabled(true);
	}

	m_CurTorso = nullptr;
	m_CurLegs = nullptr;

	if (state == States::Run)
	{
		m_CurTorso = m_TorsoRun;
		m_CurLegs = m_LegsRun;

		m_Speed = m_SpeedForward;		
	}
	else if (state == States::RunBack)
	{
		m_CurTorso = m_TorsoRunBack;
		m_CurLegs = m_LegsRunBack;

		m_Speed = m_SpeedBackward;
	}
	else if (state == States::DrawLine)
	{
		m_CurTorso = m_DrawLine;
	}
	else if (state == States::ThrowBottle)
	{
		m_CurTorso = m_ThrowBottle;
	}
	else if (state == States::Idle)
	{
		m_CurTorso = m_TorsoIdle;
	}
	else if (state == States::Pray)
	{
		m_CurTorso = m_TorsoPray;
		g_SoundManager->Play("pray");
	}
	else if (state == States::Death)
	{
		m_CurTorso = m_TorsoDeath;
	}

	if (m_CurTorso)
	{
		m_CurTorso->SetDisabled(false);
		m_CurTorso->SetVisible(true);
		m_CurTorso->Stop();
		m_CurTorso->Play();
	}

	if (m_CurLegs)
	{
		m_CurLegs->SetVisible(true);
		m_CurLegs->SetDisabled(false);
		m_CurLegs->Stop();
		m_CurLegs->Play();
	}	
}

//////////////////////////////////////////////////////////////////////////
ReturnCodes CHeroPerson::OnMouseMove(const Vector &mousePos)
{
	auto code = CBasePerson::OnMouseMove(mousePos);
	if (code != ReturnCodes::Skip)return code;


	return ReturnCodes::Skip;
}

//////////////////////////////////////////////////////////////////////////
ReturnCodes CHeroPerson::OnMouseUp(const Vector &mousePos, const int mouseButton)
{
	auto code = CBasePerson::OnMouseUp(mousePos, mouseButton);
	if (code != ReturnCodes::Skip)return code;

	if (mouseButton == (int)MouseButton::Right && m_CurState != States::DrawLine && m_CurState != States::DrawCircle)
	{
		if (chalk == 0)
		{
			//TODO: Message no chalk
			g_SoundManager->Play("no_more");
			return ReturnCodes::Skip;
		}



		auto scene = CSceneGameJam::GetScene();
		float angle = MathUtils::AngleBetween(mousePos, GetPosition());		

		bool success = false;
		
		auto & cell = scene->field->GetCell(GetPositionX(), GetPositionY());

		if (angle >= 45.0f && angle < 135.0f)
		{
			success = scene->DrawLine(cell.x, cell.y + 1);
			if (success)
			{
				ChangeState(States::DrawLine);
				m_CurTorso->SetRotation(0.0f);
			}
		}
		else if (angle >= 135.0f && angle < 225.0f)
		{
			success = scene->DrawLine(cell.x - 1, cell.y, true);
			if (success)
			{
				ChangeState(States::DrawLine);
				m_CurTorso->SetRotation(90.0f);
			}
		}
		else if (angle >= 225.0f && angle < 315.0f)
		{
			success = scene->DrawLine(cell.x, cell.y - 1);
			if (success)
			{
				ChangeState(States::DrawLine);
				m_CurTorso->SetRotation(180.0f);
			}
		}
		else
		{
			success = scene->DrawLine(cell.x + 1, cell.y, true);
			if (success)
			{
				ChangeState(States::DrawLine);
				m_CurTorso->SetRotation(-90.0f);
			}				
		}	

		if (success)
		{
			auto cellPos = scene->field->GetCellPos(cell.x, cell.y);
			SetPosition(cellPos);

			g_SoundManager->Play("chalk");

			chalk--;
			CSceneGameJam::GetScene()->UpdateIndicators();
		}
		else
		{
			g_SoundManager->Play("cant");
		}
	}
	else if (mouseButton == (int)MouseButton::Middle && m_CurState != States::DrawLine && m_CurState != States::DrawCircle)
	{
		if (chalk == 0)
		{
			g_SoundManager->Play("no_more");
			//TODO: Message no chalk
			return ReturnCodes::Skip;
		}
		

		auto scene = CSceneGameJam::GetScene();
		float angle = MathUtils::AngleBetween(mousePos, GetPosition());

		bool success = false;

		auto & cell = scene->field->GetCell(GetPositionX(), GetPositionY());

		
		success = scene->DrawCircle(cell.x, cell.y);		
		if (success)
		{		
			inCircle = true;
			float prevAngle = 0.0f;
			if (m_CurTorso)
			{
				prevAngle = m_CurTorso->GetRotation();
			}
			ChangeState(States::DrawLine);
			m_CurTorso->SetRotation(prevAngle);
			auto queue = CActionQueue::Create();
			queue->AddAction(CSleepAction::Create(0.15f));
			queue->AddAction(CRotateByAction::Create(m_CurTorso, 0.35f, 360.0f));
			AddChild(queue);

			auto cellPos = scene->field->GetCellPos(cell.x, cell.y);
			SetPosition(cellPos);

			chalk--;
			CSceneGameJam::GetScene()->UpdateIndicators();
			g_SoundManager->Play("chalk");
		}
		else
		{
			g_SoundManager->Play("cant_read");
		}
	}
	else if (mouseButton == (int)MouseButton::Left && m_CurState != States::DrawLine && m_CurState != States::DrawCircle)
	{
		if (holyWater == 0)
		{
			//TODO: Message no chalk
			return ReturnCodes::Skip;
		}
		holyWater--;
		CSceneGameJam::GetScene()->UpdateIndicators();

		auto scene = CSceneGameJam::GetScene();		
		float prevAngle = 0.0f;
		if (m_CurTorso)
		{
			prevAngle = m_CurTorso->GetRotation();
		}
		ChangeState(States::ThrowBottle);	
		m_CurTorso->SetRotation(prevAngle);
	}
	return ReturnCodes::Skip;
}

//////////////////////////////////////////////////////////////////////////
ReturnCodes CHeroPerson::Update(const float dt)
{
	auto code = CBaseWidget::Update(dt);

	m_IsMoveUp = false;
	m_IsMoveDown = false;
	m_IsMoveLeft = false;
	m_IsMoveRight = false;

	if (m_CurState == States::Run || m_CurState == States::RunBack || m_CurState == States::Idle || m_CurState == States::Pray)
	{
		if (g_Input->IsKeyDown(KEY_W))
		{
			m_IsMoveUp = true;
			MoveUp(dt);
		}
		else if (g_Input->IsKeyDown(KEY_S))
		{
			m_IsMoveDown = true;
			MoveDown(dt);
		}
		if (g_Input->IsKeyDown(KEY_A))
		{
			m_IsMoveLeft = true;
			MoveLeft(dt);
		}
		else if (g_Input->IsKeyDown(KEY_D))
		{
			m_IsMoveRight = true;
			MoveRight(dt);
		}

		if (m_IsMoveLeft || m_IsMoveRight || m_IsMoveUp || m_IsMoveDown)
		{
			UpdateCircleStatus();
			if (m_CurState != States::RunBack)
			{
				ChangeState(States::Run);
			}			
		}	
		else
		{
			if (inCircle)
			{
				inCircle = true;
				ChangeState(States::Pray);
				m_CurTorso->SetRotation(180);					
			}
			else
			{
				ChangeState(States::Idle);
			}			
		}


		Vector transPos;
		auto trans = m_Transform.GetAbsoluteTransform();
		transPos.x = trans.x;
		transPos.y = trans.y;
		float angle = MathUtils::AngleBetween(g_Input->GetMousePos(), transPos);

		m_Angle = angle;

		if (m_CurTorso && m_CurState != States::Pray)
		{
			m_CurTorso->SetRotation(angle - 90.0f);


			float dirAngle = angle - 90.0f;



			if (m_IsMoveUp)
			{
				if (dirAngle > -90.0f && dirAngle < 90.0f)
				{
					ChangeState(States::RunBack);
					m_CurLegs->SetRotation(0.0f);
				}
				else
				{
					ChangeState(States::Run);
					m_CurLegs->SetRotation(180.0f);
				}
			}
			else if (m_IsMoveDown)
			{
				if (dirAngle > 90.0f && dirAngle < 270.0f)
				{
					ChangeState(States::RunBack);
					m_CurLegs->SetRotation(180.0f);
				}
				else
				{
					ChangeState(States::Run);
					m_CurLegs->SetRotation(0.0f);
				}
			}
			else if (m_IsMoveLeft)
			{
				if ((dirAngle > 180.0f && dirAngle < 270.0f) || (dirAngle > -90.0f && dirAngle < 0.0f))
				{
					ChangeState(States::RunBack);
					m_CurLegs->SetRotation(270.0f);

				}
				else
				{
					ChangeState(States::Run);
					m_CurLegs->SetRotation(90.0f);
				}
			}
			else if (m_IsMoveRight)
			{
				if (dirAngle > 0.0f && dirAngle < 180.0f)
				{
					ChangeState(States::RunBack);
					m_CurLegs->SetRotation(90.0f);
				}
				else
				{
					ChangeState(States::Run);
					m_CurLegs->SetRotation(270.0f);
				}
			}
		}

		if (m_CurState == States::Pray)
		{
			auto scene = CSceneGameJam::GetScene();
			if (scene->prayCount > 0.0f)
			{
				scene->prayCount -= scene->prayPerSecond*dt;
				if (scene->prayCount <= 0.0f)
				{
					scene->Win();
				}
			}			
		}
	}
	return ReturnCodes::Skip;
}


//////////////////////////////////////////////////////////////////////////
void CHeroPerson::UpdateCircleStatus()
{
	inCircle = false;

	auto & cell = CSceneGameJam::GetScene()->field->GetCell(GetPositionX(), GetPositionY());
	auto circles = CSceneGameJam::GetScene()->circles;

	for (int i = 0; i < circles.size(); i++)
	{
		auto circle = circles[i];
		for (int j = 0; j < 9; j++)
		{
			auto el = circle->elements[j];
			if (el.x == cell.x && el.y == cell.y)
			{
				inCircle = true;				
				return;
			}
		}
	}
}


