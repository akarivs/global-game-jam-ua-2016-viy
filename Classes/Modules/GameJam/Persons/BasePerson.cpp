#include "BasePerson.h"
#include "../SceneGameJam.h"


//////////////////////////////////////////////////////////////////////////	
CBasePerson::CBasePerson() :CBaseWidget()
{
	m_Speed = 190.0f;
	SetCatchTouch(true);

	m_IsCheckCollisions = true;
	isHero = true;
}

//////////////////////////////////////////////////////////////////////////
CBasePerson::~CBasePerson()
{

}


//////////////////////////////////////////////////////////////////////////
void CBasePerson::Init()
{	

}


//////////////////////////////////////////////////////////////////////////
ReturnCodes CBasePerson::OnMouseMove(const Vector &mousePos)
{
	auto code = CBaseWidget::OnMouseMove(mousePos);
	if (code != ReturnCodes::Skip)return code;
	
	
	return ReturnCodes::Skip;
}

//////////////////////////////////////////////////////////////////////////
ReturnCodes CBasePerson::OnMouseUp(const Vector &mousePos, const int mouseButton)
{
	auto code = CBaseWidget::OnMouseUp(mousePos, mouseButton);
	if (code != ReturnCodes::Skip)return code;
	

	return ReturnCodes::Skip;
}

//////////////////////////////////////////////////////////////////////////
ReturnCodes CBasePerson::Update(const float dt)
{
	auto code = CBaseWidget::Update(dt);
	
	return ReturnCodes::Skip;
}

//////////////////////////////////////////////////////////////////////////
void CBasePerson::MoveUp(const float dt)
{	
	float newY = GetPositionY() - m_Speed * dt;
	
	auto field = CSceneGameJam::GetScene()->field;
	auto & cell = field->GetCell(GetPositionX(), newY);
	if (cell.free)
	{
		SetPositionY(GetPositionY() - m_Speed * dt);
	}	
}

//////////////////////////////////////////////////////////////////////////
void CBasePerson::MoveDown(const float dt)
{
	float newY = GetPositionY() + m_Speed * dt;

	auto field = CSceneGameJam::GetScene()->field;
	auto & cell = field->GetCell(GetPositionX(), newY);
	if (cell.free)
	{
		SetPositionY(GetPositionY() + m_Speed * dt);
	}	
}

//////////////////////////////////////////////////////////////////////////
void CBasePerson::MoveLeft(const float dt)
{
	float newX = GetPositionX() - m_Speed * dt;

	auto field = CSceneGameJam::GetScene()->field;
	auto & cell = field->GetCell(newX, GetPositionY());
	if (cell.free)
	{
		SetPositionX(GetPositionX() - m_Speed * dt);
	}	
}

//////////////////////////////////////////////////////////////////////////
void CBasePerson::MoveRight(const float dt)
{
	float newX = GetPositionX() + m_Speed * dt;

	auto field = CSceneGameJam::GetScene()->field;
	auto & cell = field->GetCell(newX, GetPositionY());
	if (cell.free)
	{
		SetPositionX(GetPositionX() + m_Speed * dt);
	}
}

