#ifndef HERO_PERSON
#define HERO_PERSON

#include <Framework.h>
#include <optimus/optimus.h>

#include "BasePerson.h"

using namespace optimus;

/** @brief Hero VIY person
*
* @author Vadim Simonov <akari.vs@gmail.com>
* @copyright 2013-2015 New Bridge Games
*
*/
class CHeroPerson : public CBasePerson
{
public:
	OPTIMUS_CREATE(CHeroPerson);
	virtual ~CHeroPerson();

	virtual void Init();

	void Death();

	virtual ReturnCodes OnMouseMove(const Vector &mousePos);
	virtual ReturnCodes OnMouseUp(const Vector &mousePos, const int mouseButton);	
	virtual ReturnCodes Update(const float dt);	

	bool isHero;
	bool inCircle;

	void UpdateCircleStatus();

	float health;

	int chalk;
	int holyWater;

	int maxChalk;
	int maxHolyWater;

protected:
	CHeroPerson();

private:
	enum class States {Idle,Run,RunBack,DrawLine,DrawCircle, ThrowBottle,Pray, Death};

	CSpriteAnimationWidget * m_CurTorso;
	CSpriteAnimationWidget * m_CurLegs;


	CSpriteAnimationWidget * m_TorsoRun;
	CSpriteAnimationWidget * m_TorsoRunBack;
	CSpriteAnimationWidget * m_LegsRun;
	CSpriteAnimationWidget * m_LegsRunBack;
	CSpriteAnimationWidget * m_DrawLine;
	CSpriteAnimationWidget * m_ThrowBottle;
	CSpriteAnimationWidget * m_TorsoIdle;
	CSpriteAnimationWidget * m_TorsoPray;
	CSpriteAnimationWidget * m_TorsoDeath;

	States m_CurState;

	void ChangeState(States state, bool immediately = false);

	bool m_IsMoveUp;
	bool m_IsMoveDown;
	bool m_IsMoveLeft;
	bool m_IsMoveRight;

	float m_SpeedForward;
	float m_SpeedBackward;

	float m_Angle;
};
#endif ///HERO_PERSON

