#ifndef MODULES_GAME_JAM_VIY
#define MODULES_GAME_JAM_VIY

#include <Framework.h>
#include <optimus/optimus.h>



using namespace optimus;

/** @brief Viy
*
* @author Vadim Simonov <akari.vs@gmail.com>
* @copyright 2013-2015 New Bridge Games
*
*/

class CViy : public CBaseWidget
{
public:
	OPTIMUS_CREATE(CViy);
	~CViy();
	virtual void Init();
	
	virtual ReturnCodes Update(const float dt);

	int x;
	int y;

private:
	CViy();

	CSpriteAnimationWidget * m_Idle;
	CSpriteAnimationWidget * m_Open;
	
	float timer;
	
};
#endif ///MODULES_GAME_JAM_VIY

