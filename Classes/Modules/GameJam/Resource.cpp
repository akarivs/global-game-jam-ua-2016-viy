#include "Resource.h"
#include "SceneGameJam.h"


//////////////////////////////////////////////////////////////////////////	
CResource::CResource() :CBaseWidget()
{

}

//////////////////////////////////////////////////////////////////////////
CResource::~CResource()
{

}

//////////////////////////////////////////////////////////////////////////
void CResource::Init()
{

}

//////////////////////////////////////////////////////////////////////////
void CResource::SetType(const int type)
{
	m_Type = type;
	auto img = CImageWidget::Create();
	img->SetOpacity(0.0f);
	img->SetScale(0.0f);

	if (type == 0)
	{
		img->SetTexture("images/hud/holy_water.png");
	}
	else if (type == 1)
	{
		img->SetTexture("images/hud/chalk.png");
	}

	AddChild(img);


	auto queue = CActionQueue::Create();
	queue->AddAction(CShowAction::Create(img, 0.1f, Tween::EASE_IN_SIN, 0.0f));
	queue->AddAction(CScaleToAction::Create(img, 0.2f, Vector(1.0f, 1.0f), Tween::EASE_IN_SIN));
	AddChild(queue);

}

//////////////////////////////////////////////////////////////////////////
ReturnCodes CResource::Update(const float dt)
{
	CBaseWidget::Update(dt);

	Vector posCenter = GetPosition();

	auto hero = CSceneGameJam::GetScene()->hero;
	auto pos = hero->GetPosition();


	float diff = (posCenter - pos).GetLength();

	if (diff < 50)
	{
		if (m_Type == 0)
		{
			hero->holyWater++;			
		}
		else if (m_Type == 1)
		{
			hero->chalk++;
		}
		CSceneGameJam::GetScene()->UpdateIndicators();		
		CSceneGameJam::GetScene()->field->FreeResourcePoint(this);
		Destroy();
	}
	return ReturnCodes::Skip;
}
