#ifndef MODULES_GAME_JAM
#define MODULES_GAME_JAM

#include <Framework.h>
#include <optimus/optimus.h>

#include "Persons/BasePerson.h"
#include "Persons/HeroPerson.h"
#include "Persons/EnemyPerson.h"
#include "Field.h"
#include "Resource.h"

using namespace optimus;

/** @brief Base NBG Scene
*
* @author Vadim Simonov <akari.vs@gmail.com>
* @copyright 2013-2015 New Bridge Games
*
*/

struct SLine
{
	CImageWidget * line;
	IntVector pos;
	float timer;
	float maxTimer;
};

struct SCircle
{
	CImageWidget * circle;
	IntVector pos;
	IntVector elements[9];
	float timer;
	float maxTimer;
	bool inAction;
};

class CSceneGameJam : public CBaseWidget
{
public:
	OPTIMUS_CREATE(CSceneGameJam);
	virtual ~CSceneGameJam();

	static CSceneGameJam * GetScene()
	{
		return self;
	}

	virtual void Init();
	virtual void OnHardRestart();

	bool DrawLine(const int x, const int y, const bool isVertical = false);
	bool DrawCircle(const int x, const int y);

	void Win();
	void Lose();	
	void NextLevel();

	virtual ReturnCodes OnMouseMove(const Vector &mousePos);
	virtual ReturnCodes OnKeyUp(const int key);
	virtual ReturnCodes Update(const float dt);
	virtual ReturnCodes AfterDraw(CRenderState & state);

	static int level;
	bool isWin;

	CField * field;
	CBaseWidget * m_SceneContainer;
	CBaseWidget * m_ObjectsContainer;

	CHeroPerson * hero;
	std::vector<CEnemyPerson *> enemies;
	std::vector<SLine> lines;
	std::vector<SCircle*> circles;
	std::vector<IntVector> coffinPoints;
	std::vector<CResource*> resources;
	IntVector coffinPoint;

	Vector fieldSize;

	bool showTutorial;

	float prayCount;
	float prayPerSecond;
	float levelMaxTime;	
	float levelTime;


	bool needToLoadNextLevel;

	void SetCoffinPoint(const int x, const int y);
	void UpdateIndicators();
private:
	CSceneGameJam();

	static CSceneGameJam * self;

	std::vector<IntVector> circleOffsets;

	CSliderWidget * m_PrayCount;
	CSliderWidget * m_Health;
	CImageWidget * m_ClockArrow;

	CTextWidget * m_WaterCount;
	CTextWidget * m_ChalkCount;
	

	bool m_IsStatic;


};
#endif ///MODULES_GAME_JAM

