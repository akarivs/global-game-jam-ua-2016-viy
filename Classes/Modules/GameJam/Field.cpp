#include "Field.h"
#include "SceneGameJam.h"


//////////////////////////////////////////////////////////////////////////	
CField::CField()
{

}

//////////////////////////////////////////////////////////////////////////
CField::~CField()
{

}

//////////////////////////////////////////////////////////////////////////
void CField::Init(Vector size, int level)
{
	sizeX = ceil(size.x / CELL_SIZE);
	sizeY = ceil(size.y / CELL_SIZE);

	fieldPixelSize = size;

	cellsCount = sizeX * sizeY;

	m_Field = new SFieldCell*[sizeX];
	m_FindField = new PathFinderCell*[sizeX];
	for (int i = 0; i < sizeX; i++)
	{
		m_Field[i] = new SFieldCell[sizeY];
		m_FindField[i] = new PathFinderCell[sizeY];
		for (int j = 0; j < sizeY; j++)
		{
			m_Field[i][j].free = true;
			m_Field[i][j].x = i;
			m_Field[i][j].y = j;
		}
	}

	for (int x = 0; x < sizeX; x++)
	{
		for (int y = 0; y < 3; y++)
		{
			m_Field[x][y].free = false;
		}
		if (level == 0)
		{
			for (int y = sizeY - 4; y < sizeY; y++)
			{
				m_Field[x][y].free = false;
			}
		}
		else if (level == 1)
		{
			for (int y = sizeY - 4; y < sizeY; y++)
			{
				m_Field[x][y].free = false;
			}
		}
		else
		{
			for (int y = sizeY - 3; y < sizeY; y++)
			{
				m_Field[x][y].free = false;
			}
		}

	}

	for (int y = 0; y < sizeY; y++)
	{
		for (int x = 0; x < 3; x++)
		{
			m_Field[x][y].free = false;
		}
		if (level == 0)
		{
			for (int x = sizeX - 4; x < sizeX; x++)
			{
				m_Field[x][y].free = false;
			}
		}
		else if (level == 1)
		{
			for (int x = sizeX - 4; x < sizeX; x++)
			{
				m_Field[x][y].free = false;
			}
		}
		else
		{
			for (int x = sizeX - 4; x < sizeX; x++)
			{
				m_Field[x][y].free = false;
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
void CField::LoadObstacles(const std::string obstacles)
{
	NBG::CXMLResource* xmlRes = (NBG::CXMLResource*)g_ResManager->GetResource(obstacles);
	pugi::xml_document* doc_window = xmlRes->GetXML();

	pugi::xml_node node_obstacles = doc_window->first_child();

	for (pugi::xml_node obstacle = node_obstacles.child("obstacle"); obstacle; obstacle = obstacle.next_sibling("obstacle"))
	{
		std::string texture = obstacle.attribute("texture").value();
		auto obstacleImage = CImageWidget::Create(texture);
		obstacleImage->SetHotSpot(HotSpot::HS_UL);

		int type = obstacle.attribute("type").as_int();

		int x = obstacle.attribute("x").as_int();
		int y = obstacle.attribute("y").as_int();

		float ox = obstacle.attribute("ox").as_float();
		float oy = obstacle.attribute("oy").as_float();

		int width = obstacle.attribute("width").as_int();
		int height = obstacle.attribute("height").as_int();

		auto pos = GetCellPos(x, y);
		pos.x -= 25.0f + ox;
		pos.y -= 25.0f + oy;
		obstacleImage->SetPosition(pos);

		for (int i = x; i < x + width; i++)
		{
			for (int j = y; j < y + height; j++)
			{
				m_Field[i][j].free = false;
			}
		}

		if (type == 1)
		{
			int coffinX = obstacle.attribute("pray_x").as_int();
			int coffinY = obstacle.attribute("pray_y").as_int();

			CSceneGameJam::GetScene()->SetCoffinPoint(x + coffinX, y + coffinY);
		}
		else if (type == 2)
		{
			viy = CViy::Create();
			viy->SetPosition(pos);
			CSceneGameJam::GetScene()->m_ObjectsContainer->AddChild(viy);
			continue;
		}

		CSceneGameJam::GetScene()->m_ObjectsContainer->AddChild(obstacleImage);
	}



	pugi::xml_node node_resources = doc_window->child("chalk_water");

	for (pugi::xml_node point = node_resources.child("point"); point; point = point.next_sibling("point"))
	{
		int x = point.attribute("x").as_int();
		int y = point.attribute("y").as_int();

		SResourcePoint resP;
		resP.res = nullptr;
		resP.x = x;
		resP.y = y;

		resourcesSpawnPoints.push_back(resP);
	}


	pugi::xml_node node_monsters = doc_window->child("monsters");

	auto scene = CSceneGameJam::GetScene();
	for (pugi::xml_node monster = node_monsters.child("monster"); monster; monster = monster.next_sibling("monster"))
	{
		int x = monster.attribute("x").as_int();
		int y = monster.attribute("y").as_int();
		int type = monster.attribute("type").as_int();


		enemySpawnPoints.push_back(IntVector(x, y));

		auto enemy = CEnemyPerson::Create();
		scene->m_ObjectsContainer->AddChild(enemy);

		enemy->SetPosition(GetCellPos(x, y));
		enemy->SetFieldPos(IntVector(x, y));

		enemy->moveTo = IntVector(x, y);
		enemy->SetType(type);

		enemy->SetLayer(2);

		scene->enemies.push_back(enemy);

	}



	g_ResManager->ReleaseResource(obstacles);
}

void CField::FreeResourcePoint(CResource * res)
{
	for (int i = 0; i < resourcesSpawnPoints.size(); i++)
	{
		if (resourcesSpawnPoints[i].res == res)
		{
			resourcesSpawnPoints[i].res = nullptr;
			break;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
Vector CField::GetCellPos(int x, int y)
{
	Vector ret = fieldPixelSize / 2 * -1.0f;
	ret.x += x * CELL_SIZE + 25.0f;
	ret.y += y * CELL_SIZE + 25.0f;
	return ret;
}


//////////////////////////////////////////////////////////////////////////
SFieldCell & CField::GetCell(float x, float y)
{
	SFieldCell cell;

	int cellX = (x + fieldPixelSize.x / 2.0) / CELL_SIZE;
	int cellY = (y + fieldPixelSize.y / 2.0) / CELL_SIZE;

	return m_Field[cellX][cellY];
}

//////////////////////////////////////////////////////////////////////////
bool CField::Step(int x, int y, Direction dir)
{
	if (x < 0 || x >= sizeX || y < 0 || y >= sizeY) return false;
	if (m_FindField[x][y].value == -2 || m_FindField[x][y].value == 0) return false;
	m_FindField[x][y].value = 0;

	steps++;

	if (x == endCell.x && y == endCell.y)
	{
		m_FindField[x][y].dir = dir;
		return true;
	}
	else
	{
		m_FindField[x][y].dir = dir;
		cellsToFindTemp.push_back(m_FindField[x][y]);
	}
	return false;
}

//////////////////////////////////////////////////////////////////////////
std::vector<Direction> CField::FindPath(int startX, int startY, int endX, int endY)
{
	cellsToFind.resize(0);

	std::vector<Direction> path;

	startCell.x = startX;
	startCell.y = startY;

	endCell.x = endX;
	endCell.y = endY;

	///Fill temp grid with available cells map
	for (int x = 0; x < sizeX; x++)
	{
		for (int y = 0; y < sizeY; y++)
		{
			bool free = m_Field[x][y].free;
			if (free)
			{
				m_FindField[x][y].value = -1;
			}
			else
			{
				m_FindField[x][y].value = -2;
			}
			m_FindField[x][y].x = x;
			m_FindField[x][y].y = y;
		}
	}

	auto scene = CSceneGameJam::GetScene();
	for (int i = 0; i < scene->enemies.size(); i++)
	{
		auto enemy = scene->enemies[i];
		m_FindField[enemy->moveTo.x][enemy->moveTo.y].value = -2;
	}

	for (int i = 0; i < scene->lines.size(); i++)
	{
		auto line = scene->lines[i];
		m_FindField[line.pos.x][line.pos.y].value = -2;
	}

	for (int i = 0; i < scene->circles.size(); i++)
	{
		auto circle = scene->circles[i];
		m_FindField[circle->pos.x][circle->pos.y].value = -2;
		for (int j = 0; j < 9; j++)
		{
			m_FindField[circle->elements[j].x][circle->elements[j].y].value = -2;
		}
	}


	if (m_FindField[endX][endY].value == -2)
	{
		return path;
	}

	///Try to find path



	cellsToFind.push_back(m_FindField[startX][startY]);

	bool findPath = false;

	steps = 0;
	while (steps < cellsCount)
	{
		cellsToFindTemp.clear();
		if (cellsToFind.size() == 0)
		{
			break;
		}



		for each(const PathFinderCell & cell in cellsToFind)
		{
			int x = cell.x;
			int y = cell.y;

			if (Step(x, y + 1, Up))
			{
				findPath = true;
				goto END;
			}
			if (Step(x + 1, y, Right))
			{
				findPath = true;
				goto END;
			}
			if (Step(x, y - 1, Down))
			{
				findPath = true;
				goto END;
			}
			if (Step(x - 1, y, Left))
			{
				findPath = true;
				goto END;
			}
			/*if (Step(x + 1, y + 1, UpRight))
			{
			findPath = true;
			goto END;
			}
			if (Step(x - 1, y + 1, UpLeft))
			{
			findPath = true;
			goto END;
			}
			if (Step(x + 1, y - 1, DownRight))
			{
			findPath = true;
			goto END;
			}
			if (Step(x - 1, y - 1, DownLeft))
			{
			findPath = true;
			goto END;
			}*/
		}
		cellsToFind.clear();
		cellsToFind.insert(cellsToFind.end(), cellsToFindTemp.begin(), cellsToFindTemp.end());
	}
END:
	///If path finded, then search for back path
	if (findPath)
	{
		int x = endX;
		int y = endY;
		while (!(x == startX && y == startY))
		{
			auto & cell = m_FindField[x][y];
			if (cell.dir == Down)
			{
				y++;
			}
			else if (cell.dir == DownLeft)
			{
				x++;
				y++;
			}
			else if (cell.dir == Left)
			{
				x++;
			}
			else if (cell.dir == UpLeft)
			{
				x++;
				y--;
			}
			else if (cell.dir == Up)
			{
				y--;
			}
			else if (cell.dir == UpRight)
			{
				x--;
				y--;
			}
			else if (cell.dir == Right)
			{
				x--;
			}
			else if (cell.dir == DownRight)
			{
				x--;
				y++;
			}
			path.push_back(cell.dir);
		}
	}
	std::reverse(path.begin(), path.end());
	return path;
}