#include "Viy.h"
#include "SceneGameJam.h"


//////////////////////////////////////////////////////////////////////////	
CViy::CViy():CBaseWidget()
{
	
}

//////////////////////////////////////////////////////////////////////////
CViy::~CViy()
{

}

//////////////////////////////////////////////////////////////////////////
void CViy::Init()
{
	auto container = CBaseWidget::Create();

	m_Idle = CSpriteAnimationWidget::Create();
	m_Idle->InitAnimation("images/persons/viy.png", 1, 0.05f, AnimationType::ForwardLoop, 1);
	m_Idle->Play();
	m_Idle->SetVisible(true);	
	container->AddChild(m_Idle);


	m_Open = CSpriteAnimationWidget::Create();
	m_Open->InitAnimation("images/persons/viy.png", 100, 0.05f, AnimationType::ForwardOnce, 1);
	m_Open->Play();
	m_Open->SetVisible(false);	
	m_Open->SetCallback(100, [&](){
		m_Open->Stop();
		m_Open->SetVisible(false);
		m_Idle->SetVisible(true);
	});
	container->AddChild(m_Open);

	AddChild(container);

	timer = 30.0f;
}

//////////////////////////////////////////////////////////////////////////
ReturnCodes CViy::Update(const float dt)
{
	CBaseWidget::Update(dt);

	if (timer > 0.0f)
	{
		timer -= dt;
		if (timer <= 0.0f)
		{

			g_SoundManager->Play("viy");

			m_Idle->SetVisible(false);
			m_Open->SetVisible(true);
			m_Open->Stop();
			m_Open->Play();
			timer = 30.0f;


			auto enemies = CSceneGameJam::GetScene()->enemies;
			for (int i = 0; i < enemies.size(); i++)
			{
				auto enemy = enemies[i];
				if (enemy->m_Type != 2)
				{
					enemy->Ressurect();
				}				
			}

			auto lines = CSceneGameJam::GetScene()->lines;
			for (int i = 0; i < lines.size(); i++)
			{
				lines[i].line->SetVisible(false);
			}


			auto circles = CSceneGameJam::GetScene()->circles;
			for (int i = 0; i < circles.size(); i++)
			{
				circles[i]->circle->SetVisible(false);
			}

			lines.clear();
			circles.clear();
		}
	}

	
	return ReturnCodes::Skip;
}
