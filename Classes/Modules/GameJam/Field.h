#ifndef MODULES_GAME_JAM_FIELD
#define MODULES_GAME_JAM_FIELD

#include <Framework.h>
#include <optimus/optimus.h>

#include "Persons/BasePerson.h"
#include <list>
#include <vector>

#include "Resource.h"
#include "Viy.h"

using namespace optimus;

/** @brief Base NBG Scene
*
* @author Vadim Simonov <akari.vs@gmail.com>
* @copyright 2013-2015 New Bridge Games
*
*/

enum Direction { Start, UpLeft, Up, UpRight, Right, DownRight, Down, DownLeft, Left };

struct PathFinderCell
{
	int x;
	int y;
	int value;
	Direction dir;
};

struct SFieldCell
{
	bool free;
	int x;
	int y;
};


struct SResourcePoint
{
	CResource * res;
	int x;
	int y;
};


class CField
{
public:
	CField();
	~CField();

	SFieldCell ** m_Field;
	PathFinderCell ** m_FindField;
	std::vector<PathFinderCell> cellsToFind;
	std::vector<PathFinderCell> cellsToFindTemp;

	std::vector<SResourcePoint> resourcesSpawnPoints;
	std::vector<IntVector> enemySpawnPoints;

	void Init(Vector size, int level);
	void LoadObstacles(const std::string obstacles);

	std::vector<Direction> FindPath(int startX, int startY, int endX, int endY);

	int sizeX;
	int sizeY;
	int cellsCount;

	Vector GetCellPos(int x, int y);
	SFieldCell & GetCell(float x, float y);

	void FreeResourcePoint(CResource * res);

	CViy * viy;
	
private:
	const int CELL_SIZE = 50;

	Vector fieldPixelSize;

	IntVector startCell;
	IntVector endCell;

	bool Step(int x, int y, Direction dir);	
	int steps;
};
#endif ///MODULES_GAME_JAM

