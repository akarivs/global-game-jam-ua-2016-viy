#ifndef MODULES_GAME_JAM_HOLY_WATER
#define MODULES_GAME_JAM_HOLY_WATER

#include <Framework.h>
#include <optimus/optimus.h>



using namespace optimus;

/** @brief Base NBG Scene
*
* @author Vadim Simonov <akari.vs@gmail.com>
* @copyright 2013-2015 New Bridge Games
*
*/

class CHolyWater : public CBaseWidget
{
public:
	OPTIMUS_CREATE(CHolyWater);	
	~CHolyWater();
	virtual void Init();

	void Shoot(const float angle, Vector position);


private:
	CHolyWater();
	float m_Radius;
	float m_Distance;
	float m_FlyTime;

	void AffectBottle();

	CImageWidget * m_Bottle;
	CEmmiterWidget * m_Effect;
	CEmmiterWidget * m_Effect2;
};
#endif ///MODULES_GAME_JAM_HOLY_WATER

