#ifndef MODULES_GAME_JAM_RESOURCE
#define MODULES_GAME_JAM_RESOURCE

#include <Framework.h>
#include <optimus/optimus.h>



using namespace optimus;

/** @brief Pick up resource
*
* @author Vadim Simonov <akari.vs@gmail.com>
* @copyright 2013-2015 New Bridge Games
*
*/

class CResource : public CBaseWidget
{
public:
	OPTIMUS_CREATE(CResource);
	~CResource();
	virtual void Init();
	
	void SetType(const int type); //0 - water, 1 - chalk

	virtual ReturnCodes Update(const float dt);

	int x;
	int y;

	int m_Type;
private:
	CResource();

	
};
#endif ///MODULES_GAME_JAM_RESOURCE

