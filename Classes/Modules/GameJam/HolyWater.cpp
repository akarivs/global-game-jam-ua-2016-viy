#include "HolyWater.h"
#include "SceneGameJam.h"


//////////////////////////////////////////////////////////////////////////	
CHolyWater::CHolyWater() :CBaseWidget()
{
	
}

//////////////////////////////////////////////////////////////////////////
CHolyWater::~CHolyWater()
{

}

//////////////////////////////////////////////////////////////////////////
void CHolyWater::Init()
{
	g_Config->GetValue("holy_water_radius",m_Radius);
	g_Config->GetValue("holy_water_distance", m_Distance);
	g_Config->GetValue("holy_water_fly_time", m_FlyTime);
	

	m_Bottle = CImageWidget::Create("images/misc/holy_water.png");
	m_Bottle->SetOpacity(0.0f);
	m_Bottle->SetScale(0.4f);

	AddChild(m_Bottle);

	m_Effect = CEmmiterWidget::Create("xml/effects/holy_water_circle.xml");
	AddChild(m_Effect);
	m_Effect2 = CEmmiterWidget::Create("xml/effects/holy_water_base.xml");
	AddChild(m_Effect2);

	
}

//////////////////////////////////////////////////////////////////////////
void CHolyWater::Shoot(const float angle, Vector position)
{	
	m_Bottle->SetPosition(position);

	Vector dir = Vector(cos(angle), -sin(angle)) * m_Distance;

	{
		auto queue = CActionQueue::Create();
		queue->AddAction(CMoveToAction::Create(m_Bottle, m_FlyTime, position+dir, Tween::EASE_IN_SIN));
		queue->AddAction(CCallbackAction::Create([&](){
			AffectBottle();
		}));
		AddChild(queue);
	}

	{
		auto queue = CActionQueue::Create();
		queue->AddAction(CShowAction::Create(m_Bottle, 0.2f,Tween::EASE_IN_SIN,0.0f));
		queue->AddAction(CScaleByAction::Create(m_Bottle, m_FlyTime/2.0f, Vector(0.5f,0.5f), Tween::EASE_IN_SIN));
		queue->AddAction(CScaleByAction::Create(m_Bottle, m_FlyTime / 2.0f, Vector(-0.4f, -0.4f), Tween::EASE_OUT_SIN));		
		AddChild(queue);	
	}
}

//////////////////////////////////////////////////////////////////////////
void CHolyWater::AffectBottle()
{

	g_SoundManager->Play("bottle");

	//Destroy();

	m_Effect->SetPosition(m_Bottle->GetPosition());
	m_Effect2->SetPosition(m_Bottle->GetPosition());

	m_Bottle->SetVisible(false);

	m_Effect2->Shoot(500);
	m_Effect->Play(0.5f);

	Vector posCenter = m_Bottle->GetPosition();
	

	

	auto &enemies = CSceneGameJam::GetScene()->enemies;
	for (int i = 0; i < enemies.size(); i++)
	{
		auto enemy = enemies[i];
		if (enemy->m_Type == 2)continue;
		auto pos = enemy->GetPosition();


		float diff = (posCenter - pos).GetLength();

		if (diff < m_Distance)
		{
			//affect enemy
			enemy->HolyWater();
		}
		
	}

}
