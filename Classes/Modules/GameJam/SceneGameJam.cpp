#include "SceneGameJam.h"

#include <algorithm>

#include "GUI/WinWindow.h"

int CSceneGameJam::level = 0;

CSceneGameJam * CSceneGameJam::self = nullptr;


//////////////////////////////////////////////////////////////////////////	
CSceneGameJam::CSceneGameJam() :CBaseWidget()
{
	self = this;

	circleOffsets.push_back(IntVector(-1, 0));
	circleOffsets.push_back(IntVector(1, 0));
	circleOffsets.push_back(IntVector(0, -1));
	circleOffsets.push_back(IntVector(0, 1));
	circleOffsets.push_back(IntVector(1, -1));
	circleOffsets.push_back(IntVector(1, 1));
	circleOffsets.push_back(IntVector(-1, 1));
	circleOffsets.push_back(IntVector(-1, -1));

	field = new CField();


	needToLoadNextLevel = false;
	isWin = false;

	showTutorial = false;
}

//////////////////////////////////////////////////////////////////////////
CSceneGameJam::~CSceneGameJam()
{

}

void CSceneGameJam::Lose()
{
	g_SoundManager->Stop("test");

	auto queue = CActionQueue::Create();
	queue->AddAction(CShowAction::Create(GetChildByTag(999), 0.2f));
	queue->AddAction(CCallbackAction::Create([&](){
		CScenesManager::GetInstance()->SetScene("comics_death");
	}));
	AddChild(queue);

	g_SoundManager->Play("lose");
}

void CSceneGameJam::NextLevel()
{
	auto queue = CActionQueue::Create();
	queue->AddAction(CShowAction::Create(GetChildByTag(999), 0.2f));
	queue->AddAction(CCallbackAction::Create([&](){
		needToLoadNextLevel = true;
	}));
	AddChild(queue);
}


void CSceneGameJam::OnHardRestart()
{
	enemies.clear();
	lines.clear();
	circles.clear();
	coffinPoints.clear();	
}

//////////////////////////////////////////////////////////////////////////
ReturnCodes CSceneGameJam::OnMouseMove(const Vector &mousePos)
{
	auto code = CBaseWidget::OnMouseMove(mousePos);
	if (code != ReturnCodes::Skip)return code;

	return ReturnCodes::Skip;
}

//////////////////////////////////////////////////////////////////////////
void CSceneGameJam::Init()
{
	field->enemySpawnPoints.clear();
	field->resourcesSpawnPoints.clear();

	g_SoundManager->Play("test");
	LoadFromXML("xml/screens/jam/jam.xml");

	prayCount = 100.0f;
	g_Config->GetValue("pray_per_second", prayPerSecond);

	m_SceneContainer = CBaseWidget::Create();
	if (level == 0)
	{
		m_SceneContainer->LoadFromXML("xml/locations/house.xml");
		m_ObjectsContainer = m_SceneContainer->GetChildByTag(100, true);

		field->Init(m_SceneContainer->GetSize(), level);
		field->LoadObstacles("xml/locations/house_config.xml");
	}
	else if (level == 1)
	{
		m_SceneContainer->LoadFromXML("xml/locations/street.xml");
		m_ObjectsContainer = m_SceneContainer->GetChildByTag(100, true);

		field->Init(m_SceneContainer->GetSize(), level);
		field->LoadObstacles("xml/locations/street_config.xml");
	}
	else if (level == 2)
	{
		m_SceneContainer->LoadFromXML("xml/locations/church.xml");
		m_ObjectsContainer = m_SceneContainer->GetChildByTag(100, true);

		field->Init(m_SceneContainer->GetSize(), level);
		field->LoadObstacles("xml/locations/church_config.xml");
	}

	g_Config->GetValue("time_level_" + StringUtils::ToString(level), levelMaxTime);
	levelTime = 0.0f;

	AddChild(m_SceneContainer);

	hero = CHeroPerson::Create();
	m_ObjectsContainer->AddChild(hero);
	hero->SetLayer(2);

	hero->SetPosition(field->GetCellPos(4, 4));
	hero->SetFieldPos(IntVector(16, 10));


	fieldSize = m_SceneContainer->GetSize();

	m_PrayCount = GetChildByTag<CSliderWidget>(201, true);
	m_Health = GetChildByTag<CSliderWidget>(200, true);
	m_ClockArrow = GetChildByTag<CImageWidget>(202, true);
	m_WaterCount = GetChildByTag<CTextWidget>(203, true);
	m_ChalkCount = GetChildByTag<CTextWidget>(204, true);

	UpdateIndicators();

	auto queue = CActionQueue::Create();
	queue->AddAction(CHideAction::Create(GetChildByTag(999), 0.2f));
	AddChild(queue);
}

//////////////////////////////////////////////////////////////////////////
void CSceneGameJam::UpdateIndicators()
{
	std::wstring waterStr = StringUtils::ToWString(hero->holyWater) + L"/" + StringUtils::ToWString(hero->maxHolyWater);
	std::wstring chalkStr = StringUtils::ToWString(hero->chalk) + L"/" + StringUtils::ToWString(hero->maxChalk);

	m_WaterCount->SetText(waterStr);
	m_ChalkCount->SetText(chalkStr);
}

//////////////////////////////////////////////////////////////////////////
bool CSceneGameJam::DrawLine(const int x, const int y, const bool isVertical)
{
	auto & cell = field->m_Field[x][y];
	if (cell.free == false)return false;

	auto pos = field->GetCellPos(x, y);
	CImageWidget * image;
	if (isVertical)
	{
		image = CImageWidget::Create("images/misc/line_90.png");
	}
	else
	{
		image = CImageWidget::Create("images/misc/line.png");
	}

	image->SetLayer(-600);
	image->SetPosition(pos);
	m_ObjectsContainer->AddChild(image);

	SLine line;
	line.line = image;
	line.pos.x = x;
	line.pos.y = y;
	g_Config->GetValue("line_timer", line.timer);
	line.maxTimer = line.timer;

	lines.push_back(line);
	return true;
}


//////////////////////////////////////////////////////////////////////////
void CSceneGameJam::SetCoffinPoint(const int x, const int y)
{
	///Search for obstacles 
	for (int i = 0; i < circleOffsets.size(); i++)
	{
		coffinPoints.push_back(IntVector(x + circleOffsets[i].x, y + circleOffsets[i].y));
	}
	coffinPoint.x = x;
	coffinPoint.y = y;

	auto pupitr = CImageWidget::Create("images/misc/pupitr.png");
	pupitr->SetPosition(field->GetCellPos(x, y));
	pupitr->SetPositionY(pupitr->GetPositionY() - 33.0f);
	pupitr->SetLayer(1);
	m_ObjectsContainer->AddChild(pupitr);

}

//////////////////////////////////////////////////////////////////////////
bool CSceneGameJam::DrawCircle(const int x, const int y)
{
	auto & cell = field->m_Field[x][y];
	if (!(x == coffinPoint.x && y == coffinPoint.y))return false;


	///Search for obstacles 
	for (int i = 0; i < circleOffsets.size(); i++)
	{
		auto cellHeroNew = field->m_Field[x + circleOffsets[i].x][y + circleOffsets[i].y];
		bool used = false;
		for (int j = 0; j < enemies.size(); j++)
		{
			auto enemy = enemies[j];
			if (enemy->moveTo.x == x + circleOffsets[i].x && enemy->moveTo.y == y + circleOffsets[i].y)
			{
				return false;
				break;
			}
		}
	}

	///If no obstacles - draw circle
	SCircle * circle = new SCircle();

	circle->elements[0].x = x;
	circle->elements[0].y = y;

	for (int i = 0; i < circleOffsets.size(); i++)
	{
		circle->elements[i + 1].x = x + circleOffsets[i].x;
		circle->elements[i + 1].y = y + circleOffsets[i].y;
	}

	auto pos = field->GetCellPos(x, y);
	CImageWidget * image;
	image = CImageWidget::Create("images/misc/circle.png");

	image->SetLayer(-600);
	image->SetPosition(pos);
	image->SetOpacity(0.0f);

	m_ObjectsContainer->AddChild(image);


	circle->circle = image;
	circle->pos.x = x;
	circle->pos.y = y;
	circle->inAction = true;
	g_Config->GetValue("circle_timer", circle->timer);
	circle->maxTimer = circle->timer;

	circles.push_back(circle);


	auto queue = CActionQueue::Create();
	queue->AddAction(CShowAction::Create(image, 1.0f, Tween::EASE_IN_SIN));
	queue->AddAction(CCallbackAction::Create(std::bind([&](SCircle * circle){
		circle->inAction = false;
	}, circle)));
	AddChild(queue);

	return true;
}

//////////////////////////////////////////////////////////////////////////
ReturnCodes CSceneGameJam::OnKeyUp(const int key)
{
	CBaseWidget::OnKeyUp(key);


	if (key == KEY_U)
	{
		Win();
	}
	else if (key == KEY_ESCAPE)
	{
		showTutorial = !showTutorial;
		GetChildByTag(9999, true)->SetVisible(showTutorial);		
	}

	return ReturnCodes::Skip;
}


//////////////////////////////////////////////////////////////////////////
ReturnCodes CSceneGameJam::Update(const float dt)
{
	if (showTutorial)return ReturnCodes::Skip;
	auto code = CBaseWidget::Update(dt);


	if (needToLoadNextLevel)
	{
		level++;
		if (level == 3)
		{
			g_SoundManager->Stop("test");
			CScenesManager::GetInstance()->SetScene("comics_end");
		}
		else
		{
			CScenesManager::GetInstance()->SetScene("jam");
		}		
	}

	if (isWin)
	{
		return ReturnCodes::Skip;
	}

	levelTime += dt;
	if (levelTime >= levelMaxTime)
	{
		Lose();
	}

	float percent = levelTime / levelMaxTime;
	if (percent > 1.0f)
	{
		percent = 1.0f;
	}
	m_ClockArrow->SetRotation(206.0f * percent);

	m_SceneContainer->SetPosition(-hero->GetPositionX(), -hero->GetPositionY());

	Vector pos = m_SceneContainer->GetPosition();
	Vector diff = fieldSize - Vector(1920.0f, 1080.0f);
	diff /= 2.0f;

	if (pos.x > diff.x)pos.x = diff.x;
	if (pos.x < -diff.x)pos.x = -diff.x;

	if (pos.y > diff.y)pos.y = diff.y;
	if (pos.y < -diff.y)pos.y = -diff.y;

	m_SceneContainer->SetPosition(pos);

	auto iter = lines.begin();
	while (iter != lines.end())
	{
		auto &line = *iter;
		if (line.timer > 0.0f)
		{
			line.timer -= dt;

			float percent = line.timer / line.maxTimer;
			if (percent < 0.0f)percent = 0.0f;

			line.line->SetOpacity(percent * 255.0f);

			if (line.timer <= 0.0f)
			{
				line.line->Destroy();
				iter = lines.erase(iter);
				continue;
			}
		}
		iter++;
	}


	auto iterCircle = circles.begin();
	while (iterCircle != circles.end())
	{
		auto &circle = *iterCircle;
		if (circle->inAction)
		{
			iterCircle++;
			continue;
		}
		if (circle->timer > 0.0f)
		{
			circle->timer -= dt;

			float percent = circle->timer / circle->maxTimer;
			if (percent < 0.0f)percent = 0.0f;

			circle->circle->SetOpacity(percent * 255.0f);

			if (circle->timer <= 0.0f)
			{
				circle->circle->Destroy();
				iterCircle = circles.erase(iterCircle);
				hero->UpdateCircleStatus();
				continue;
			}
		}
		iterCircle++;
	}

	m_PrayCount->SetPercent(prayCount / 100.0f);
	m_Health->SetPercent((hero->health / 100.0f));



	int waterOnScene = hero->holyWater;
	int chalkOnScene = hero->chalk;

	for (int i = 0; i < field->resourcesSpawnPoints.size(); i++)
	{
		auto res = field->resourcesSpawnPoints[i].res;
		if (res && res->m_Type == 0)
		{
			waterOnScene++;
		}
		else if (res && res->m_Type == 1)
		{
			chalkOnScene++;
		}

	}

	if (waterOnScene < hero->maxHolyWater)
	{
		if (g_Random->RandI(0, 2000) < 3)
		{
			std::vector<int>freeIndexes;
			for (int i = 0; i < field->resourcesSpawnPoints.size(); i++)
			{
				if (field->resourcesSpawnPoints[i].res == nullptr)
				{
					freeIndexes.push_back(i);
				}
			}
			if (freeIndexes.size() > 0)
			{
				std::random_shuffle(freeIndexes.begin(), freeIndexes.end());

				CResource * res = CResource::Create();
				res->SetType(0);
				m_ObjectsContainer->AddChild(res);

				auto & spawnPoint = field->resourcesSpawnPoints[freeIndexes[0]];
				spawnPoint.res = res;

				res->SetPosition(field->GetCellPos(spawnPoint.x, spawnPoint.y));
			}
		}
	}

	if (chalkOnScene < hero->maxChalk)
	{
		if (g_Random->RandI(0, 2000) < 3)
		{
			std::vector<int>freeIndexes;
			for (int i = 0; i < field->resourcesSpawnPoints.size(); i++)
			{
				if (field->resourcesSpawnPoints[i].res == nullptr)
				{
					freeIndexes.push_back(i);
				}
			}
			if (freeIndexes.size() > 0)
			{
				std::random_shuffle(freeIndexes.begin(), freeIndexes.end());

				CResource * res = CResource::Create();
				res->SetType(1);
				m_ObjectsContainer->AddChild(res);

				auto & spawnPoint = field->resourcesSpawnPoints[freeIndexes[0]];
				spawnPoint.res = res;

				res->SetPosition(field->GetCellPos(spawnPoint.x, spawnPoint.y));
			}
		}
	}

	return ReturnCodes::Skip;
}

//////////////////////////////////////////////////////////////////////////
ReturnCodes CSceneGameJam::AfterDraw(CRenderState & state)
{
	if (g_GameApplication->IsCheatsEnabled())
	{
		if (g_Input->IsKeyDown(KEY_G))
		{
			for (int i = 0; i < field->resourcesSpawnPoints.size(); i++)
			{
				auto point = field->resourcesSpawnPoints[i];
				Vector pos = field->GetCellPos(point.x, point.y);
				pos += m_SceneContainer->GetPosition();
				g_Render->DrawLine(pos + Vector(-20.0f, 0.0f), pos + Vector(20.0f, 0.0f), 20.0f, Color(255, 255, 255, 255));
			}

			for (int i = 0; i < field->enemySpawnPoints.size(); i++)
			{
				auto point = field->enemySpawnPoints[i];
				Vector pos = field->GetCellPos(point.x, point.y);
				pos += m_SceneContainer->GetPosition();
				g_Render->DrawLine(pos + Vector(-20.0f, 0.0f), pos + Vector(20.0f, 0.0f), 20.0f, Color(255, 255, 0, 0));
			}

			for (int i = 0; i < field->sizeX; i++)
			{
				for (int j = 0; j < field->sizeY; j++)
				{			
					if (field->m_Field[i][j].free == false)
					{
						Vector pos = field->GetCellPos(i, j);
						pos += m_SceneContainer->GetPosition();
						g_Render->DrawLine(pos + Vector(-25.0f, 0.0f), pos + Vector(25.0f, 0.0f), 25.0f, Color(120, 0, 255, 0));
					}					
				}
			}
		}
	}
	return ReturnCodes::Skip;
}

//////////////////////////////////////////////////////////////////////////
void CSceneGameJam::Win()
{
	this->isWin = true;

	auto win = CWinWindow::Create();
	win->Show();		
		
	g_SoundManager->Play("win");

	AddChild(win);
	win->BringForward();
}