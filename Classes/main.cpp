#include <Framework.h>
#include <optimus/optimus.h>

#include <Modules/GameJam/SceneGameJam.h>
#include <Modules/Splash/SceneSplash.h>
#include <Modules/Title/SceneTitle.h>
#include <Modules/Comics/SceneComicsStart.h>
#include <Modules/Comics/SceneDeathComics.h>
#include <Modules/Comics/SceneComicsEnd.h>

namespace NBG
{
	CGameApplication * g_GameApplication = nullptr;
	std::mutex * g_GlobalMutex = nullptr;
}

int main(int argc, char* args[])
{
	g_GlobalMutex = new std::mutex();
#ifdef NBG_WIN32
	HANDLE Test_Present = CreateMutex(NULL, false, L"NBG_JAM_GAME");
	if (GetLastError() == ERROR_ALREADY_EXISTS)
	{
		HWND hWnd = FindWindowA("NewBridgeGameEngineClass", 0);
		SetFocus(hWnd);
		UpdateWindow(hWnd);
		SetForegroundWindow(hWnd);
		SetActiveWindow(hWnd);
		ShowWindow(hWnd, SW_RESTORE);
		return 0;
	};
#endif

	NBG_Init();
#ifdef NBG_WIN32
	exit(1);
#endif
	return 1;
}

optimus::managers::CScenesManager * scenesManager = nullptr;

void App_OnInit()
{
	optimus::helpers::CLuaHelper::GetInstance()->Init();
	scenesManager = optimus::managers::CScenesManager::GetInstance();

	scenesManager->Register<CSceneGameJam>("jam");
	scenesManager->Register<CSceneSplash>("splash");
	scenesManager->Register<CSceneTitle>("title");
	scenesManager->Register<CSceneComicsStart>("comics_start");
	scenesManager->Register<CSceneComicsDeath>("comics_death");
	scenesManager->Register<CSceneComicsEnd>("comics_end");
	


	scenesManager->Init();
	scenesManager->GetCursor()->SetTexture("images/cursor.png");
	scenesManager->GetCursor()->SetScale(0.75f);


	//scenesManager->GetDebugLabel()->SetFont(g_FontsManager->GetFont("text"));
	optimus::ui::CWidgetsFactory::GetInstance();

	std::string startScene = "";
	g_Config->GetValue("start_scene", startScene);

	scenesManager->SetScene(startScene);
}

void App_OnUpdate()
{	
	if (scenesManager)scenesManager->Update(g_FrameTime);
}

void App_OnMouseMove()
{
	if (scenesManager)scenesManager->OnMouseMove(g_MousePos);
}

void App_OnMouseUp(int mouseButton)
{
	if (scenesManager)scenesManager->OnMouseUp(g_MousePos, mouseButton);
}

void App_OnMouseDown(int mouseButton)
{
	if (scenesManager)scenesManager->OnMouseDown(g_MousePos, mouseButton);
}

void App_OnKeyDown(int key)
{
	if (scenesManager)scenesManager->OnKeyDown(key);
}

void App_OnKeyUp(int key)
{
	if (scenesManager)scenesManager->OnKeyUp(key);
}

void App_OnDraw(CRenderState & state)
{
	if (scenesManager)scenesManager->Draw(state);
	/*
	for (int i = 0; i < 40; i++)
	{
		g_Render->DrawLine(Vector(i*50.0f - 960, 0.0f - 540), Vector(i*50.0f - 960, 1080.0f - 540), 2.0f, Color(255, 255, 255, 255));
		g_Render->DrawLine(Vector(0.0f - 960, i*50.0f - 540), Vector(1920.0f - 960, i*50.0f - 540), 2.0f, Color(255, 255, 255, 255));
	}*/
}

void App_AfterLoop()
{
	if (scenesManager)scenesManager->AfterLoop();
	optimus::pool::CUIPool::GetInstance()->UpdatePool();
}

void App_OnRenderChange(RenderChangeType type)
{
	if (scenesManager)scenesManager->OnRenderChange(type);
}

void App_OnFocusChange(FocusChangeType type)
{
	if (scenesManager)scenesManager->OnFocusChange(type);
}

void NBG_Init()
{
	g_GameApplication = new CGameApplication();
	g_GameApplication->
		SetFullscreenSize(1920, 1080)->
		SetLogicalSize(1920, 1080)->
		SetWindowedSize(1366, 768)->
		SetFontsPath("xml/fonts.xml")->
		SetStringsPath("xml/strings.xml")->
		SetConfigPath("xml/config.xml")->
		SetAtlasesPath("atlases");

#ifdef NBG_WIN32
	g_GameApplication->SetEditionsPath("edition.xml", "xml/editions");
#elif defined(NBG_ANDROID)
	g_GameApplication->SetEditionsPath("edition_android.xml", "xml/editions");
#elif defined(NBG_IOS)
	g_GameApplication->SetEditionsPath("edition_ios.xml", "xml/editions");
#elif defined(NBG_OSX)
	g_GameApplication->SetEditionsPath("edition_osx.xml", "xml/editions");
#endif


	g_GameApplication->Init();
	g_GameApplication->SetOnInitCallback(std::bind(App_OnInit));
	g_GameApplication->SetOnMouseMoveCallback(std::bind(App_OnMouseMove));
	g_GameApplication->SetOnMouseUpCallback(std::bind(App_OnMouseUp, std::placeholders::_1));
	g_GameApplication->SetOnMouseDownCallback(std::bind(App_OnMouseDown, std::placeholders::_1));
	g_GameApplication->SetOnKeyUpCallback(std::bind(App_OnKeyUp, std::placeholders::_1));
	g_GameApplication->SetOnKeyDownCallback(std::bind(App_OnKeyDown, std::placeholders::_1));
	g_GameApplication->SetOnRenderChangeCallback(std::bind(App_OnRenderChange, std::placeholders::_1));
	g_GameApplication->SetFocusChangeCallback(std::bind(App_OnFocusChange, std::placeholders::_1));
	g_GameApplication->SetUpdateCallback(std::bind(App_OnUpdate));
	g_GameApplication->SetDrawCallback(std::bind(App_OnDraw, std::placeholders::_1));
	g_GameApplication->SetAfterLoopCallback(std::bind(App_AfterLoop));

	bool useLog = false;
	g_Config->GetValue("use_log", useLog);

	if (useLog)
	{
		g_Log->Init("log.txt");
	}


	/*
	std::string module;
	g_Config->GetValue("game_type", module);
	if (module == "mahjong")
	{	
	}
	else if (module == "nonograms")
	{	
	}
	*/

	g_PlayersManager->InitPlayers<CBasePlayer>(8);

	g_GameApplication->Run();
}