def CheckSubdir(dir):     
	if dir[10:22] == "localization":
		return True
	if dir[10:22] == "xml\\editions":
		return True
	if dir[10:22] == "images\\fonts":
		return True
	if dir[10:19] == "xml\\fonts":
		return True
	if dir[10:21] == "xml\\screens":
		return True
	return False
	
def CheckSubdirAndFile(dir,file):     
	if file == "edition.xml":
		return True
	if file == "strings.xml":
		return True
	if file == "fonts.xml":
		return True
	if file == "sounds.xml":
		return True
	if file == "config.xml":
		return True
	return False