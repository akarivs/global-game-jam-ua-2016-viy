import os      
import shutil
from xml.dom import minidom
import struct
import binascii
from struct import *
import time
import sys

working_dir = "./"
dir = "audio/sounds/not_ready/"

def list_files(dir):     
	r = []
	subdirs = [x[0] for x in os.walk(dir)]
	for subdir in subdirs:	
		t = ""
		level = subdir.count('\\')
		for i in range(0,level):
			t += "\t"
		tChild = t+"\t"
		tChild2	= tChild+"\t"
		files = os.walk(subdir).next()[2]
		if (len(files) > 0):
			for file in files:				
				size = os.path.getsize(subdir + "/" + file)
				r.append([file, size])
	return r 

#pack game data into game.pack
res = list_files(working_dir)
resources = res

print(resources)
output = ""
for resource in resources: 
	output += '<sound id="wave2_'+resource[0][0:-4]+'" path="'+dir+resource[0]+'"  group="sounds" /> \n'
f = open('test.xml', 'w')
f.write(output)
f.close()