#ifndef NBG_CORE_STRUCTURES
#define NBG_CORE_STRUCTURES

#include <string>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/FRect.h>

#include <map>
#include <vector>

namespace NBG
{



	/// Структура описания одной текстуры в атласе.
	struct AtlasTextureDescription
	{
		std::string atlasID;
		std::string atlasName;
		NBG::FRect uv;
		NBG::Vector size;
		NBG::Vector baseSize;
		NBG::Vector cropSize;
		NBG::Vector offset;
		bool isRotated;
	};

	/// Настройки символов.
	struct CharOffsets
	{
		int xOffset;
		int yOffset;
		int xAdvance;
		NBG::FRect uv;
		unsigned short id;
		int arrayId;
		Vector size;
	};

	/// Пары для кернинга (смещения символов относительно друг друга)
	struct FontKernPair
	{
		int first;
		int second;
		int offset;
	};

	struct FontKernPairV2
	{
		int id;
		int offset;
	};

	/// Описание шрифта.
	struct FontDescription
	{
		std::vector<CharOffsets> chars;		
		FontKernPair * kernings;
		int charsCount;
		int kerningsCount;
		std::map<int, std::vector<FontKernPairV2> > kerningsMap;
		float characterPadding;
		int baseLine;
		int spaceSize;
		int lineHeight;
		std::string texture;
	};
}
#endif // NBG_CORE_STRUCTURES
