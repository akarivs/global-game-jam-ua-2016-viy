#ifndef NBG_UTILS_MATH
#define NBG_UTILS_MATH

#include <Framework/Datatypes/Vector.h>
#include <vector>

namespace NBG
{
	/** @brief Класс-помощник со статичными математическими функциями
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013 New Bridge Games
	*
	*/
	class MathUtils
	{
	public:
		/// @name Конструкторы
		MathUtils();
		~MathUtils();

		static void Init();
		static void RelativeRotate(float x1, float y1, float &x2, float &y2, float angle);
		static float AngleBetween(Vector start, Vector end, Vector referenceAngle = Vector(-1.0f, 0.0f));
		static float ToRadian(float degree);
		static float ToDegree(float radian);

		static float FastSin(float radian);
		static float FastCos(float radian);

		static bool IsLineIntersects(Vector a, Vector b, Vector c, Vector d, Vector * returnVec = NULL);		
		
		template <class Type = Vector>
		static bool IsPointInsidePoly(const Vector &point, const std::vector<Type> &poly)
		{
			static const int q_patt[2][2] = { { 0, 1 }, { 3, 2 } };

			if (poly.size() < 3) return false;

			auto end = poly.end();
			auto pred_pt = poly.back();
			pred_pt.x -= point.x;
			pred_pt.y -= point.y;

			int pred_q = q_patt[pred_pt.y < 0][pred_pt.x < 0];

			int w = 0;

			for (int i = 0; i < poly.size(); i++)
			{
				auto cur_pt = poly[i];

				cur_pt.x -= point.x;
				cur_pt.y -= point.y;

				int q = q_patt[cur_pt.y < 0][cur_pt.x < 0];

				switch (q - pred_q)
				{
				case -3:++w; break;
				case 3:--w; break;
				case -2:if (pred_pt.x*cur_pt.y >= pred_pt.y*cur_pt.x) ++w; break;
				case 2:if (!(pred_pt.x*cur_pt.y >= pred_pt.y*cur_pt.x)) --w; break;
				}

				pred_pt = cur_pt;
				pred_q = q;

			}
			return w != 0;
		}

		static bool IsPointInsideCircle(const Vector &point, const Vector &center, const float radius);
		static Vector GetQuadBezierPoint(const Vector &start, const Vector &middle, const Vector &end, const float t);


		static float PI;
	private:


	};

}




#endif // NBG_UTILS_MATH

