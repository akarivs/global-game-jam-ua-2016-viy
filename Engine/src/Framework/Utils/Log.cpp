#include "Log.h"

#include <Framework.h>
#include <cstdlib>

namespace NBG
{
	//////////////////////////////////////////////////////////////////////////
    CLog::CLog()
    {
		m_File = nullptr;
    }

	//////////////////////////////////////////////////////////////////////////
	CLog::~CLog()
    {
		if (m_File)
		{
			fclose(m_File);
			delete m_File;
		}        
    }

	//////////////////////////////////////////////////////////////////////////
    bool CLog::Init(const std::string path)
    {
		m_File = g_FileSystem->OpenFile(path,"wt");

		std::string header = "<html><head><title>New Bridge Games - Log File</title><style TYPE='text/css'>";
		std::string styleNormal = ".normal{color: #000000; font-size: 16px;} ";
		std::string styleInfo = ".info{color: #014e9b; font-size: 16px;} ";
		std::string styleError = ".error{color: #9b2201; font-size: 18px; font-weight: bold;} ";
		std::string styleSuccess = ".success{color: #327e04; font-size: 18px; font-weight: bold;} ";		
		std::string styleBlock = "div{border: 1px solid black;} ";

		WriteSimpleLine(header);
		WriteSimpleLine(styleNormal);
		WriteSimpleLine(styleInfo);
		WriteSimpleLine(styleError);
		WriteSimpleLine(styleSuccess);
		WriteSimpleLine("</style></head><body>");

		return m_File != nullptr;
    }

	//////////////////////////////////////////////////////////////////////////
	void CLog::WriteSimpleLine(const std::string line)
	{	
		if (m_File == nullptr)return;	
		fwrite(line.c_str(), line.size(), 1, m_File);
		fwrite("\n", 1, 1, m_File);
		fflush(m_File);
	}

	//////////////////////////////////////////////////////////////////////////
	void CLog::WriteLine(const std::string line)
	{
		CONSOLE("%s", line.c_str());
		if (m_File == nullptr)return;		
		std::string newLine = "<div class='normal'>\t" + line + "</div>";
		fwrite(newLine.c_str(), newLine.size(), 1, m_File);
		fwrite("\n", 1, 1, m_File);		
		fflush(m_File);		
	}

	//////////////////////////////////////////////////////////////////////////
	void CLog::WriteLineError(const std::string line)
	{
		CONSOLE_ERROR("%s", line.c_str());
		if (m_File == nullptr)return;		
		std::string newLine = "<div class='error'>Error:\t" + line + "</div>";
		fwrite(newLine.c_str(), newLine.size(), 1, m_File);
		fwrite("\n", 1, 1, m_File);
		fflush(m_File);		
	}

	//////////////////////////////////////////////////////////////////////////
	void CLog::WriteLineInfo(const std::string line)
	{
		CONSOLE_INFO("%s", line.c_str());
		if (m_File == nullptr)return;
		std::string newLine = "<div class='info'>Info:\t" + line + "</div>";
		fwrite(newLine.c_str(), newLine.size(), 1, m_File);
		fwrite("\n", 1, 1, m_File);
		fflush(m_File);
	}

	//////////////////////////////////////////////////////////////////////////
	void CLog::WriteLineSuccess(const std::string line)
	{
		CONSOLE_SUCCESS("%s", line.c_str());
		if (m_File == nullptr)return;
		std::string newLine = "<div class='success'>Success:\t" + line + "</div>";
		fwrite(newLine.c_str(), newLine.size(), 1, m_File);
		fwrite("\n", 1, 1, m_File);
		fflush(m_File);
	}
}

