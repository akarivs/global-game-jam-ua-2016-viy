#ifndef NBG_UTILS_STRING
#define NBG_UTILS_STRING

#include <string>
#include <sstream>
#include <vector>
#include <math.h>

#ifdef NBG_ANDROID
	#include <stdlib.h>
#endif

#include "../Datatypes.h"
#include "../Datatypes/Color.h"
#include <../external/xml/pugixml.hpp>

namespace NBG
{
	/** @brief Класс-помощник со статичными строковыми функциями
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013 New Bridge Games
	*
	*/
	class StringUtils
	{
	public:
		/// @name Конструкторы
		StringUtils();
		~StringUtils();

		///Замена подстроки в строке
		template <class Type>
		inline static Type StringReplace(Type str, Type what, Type to, bool replaceAll = false)
		{
			for(size_t index=0; index=str.find(what, index), index!=std::string::npos;)
			{
				str.replace(index, what.length(), to);
				if (!replaceAll)
				{
					return str;
				}
				index+=to.length();
			}
			return str;
		}

		///Конвертация элемента в string
		template <class Type>
		inline static std::string ToString(Type element)
		{
			std::string str;
			std::stringstream out;
			out << element;
			str = out.str();
			return str;
		}

		///Конвертация элемента в wstring
		template <class Type>
		inline static std::wstring ToWString(Type element)
		{
			std::wstring str;
			std::wstringstream out;
			out << element;
			str = out.str();
			return str;
		}

		inline static Color HexStringToColor(std::string string)
		{
			NBG_Assert(string.size() == 8, "Size not 8!");
			std::string a = string.substr(0, 2);
			std::string r = string.substr(2, 2);
			std::string g = string.substr(4, 2);
			std::string b = string.substr(6, 2);

			Color clr;
			int cl;

			std::stringstream streamA(a);
			streamA >> std::hex >> cl;
			clr.a = cl;

			std::stringstream streamR(r);
			streamR >> std::hex >> cl;
			clr.r = cl;

			std::stringstream streamG(g);
			streamG >> std::hex >> cl;
			clr.g = cl;

			std::stringstream streamB(b);
			streamB >> std::hex >> cl;
			clr.b = cl;

			return clr;
		}

		inline static Color HexWStringToColor(std::wstring string)
		{
			NBG_Assert(string.size() == 8, "Size not 8!");
			std::wstring a = string.substr(0, 2);
			std::wstring r = string.substr(2, 2);
			std::wstring g = string.substr(4, 2);
			std::wstring b = string.substr(6, 2);

			Color clr;
			int cl;

			std::wstringstream streamA(a);
			streamA >> std::hex >> cl;
			clr.a = cl;

			std::wstringstream streamR(r);			
			streamR >> std::hex >> cl;
			clr.r = cl;

			std::wstringstream streamG(g);
			streamG >> std::hex >> cl;
			clr.g = cl;

			std::wstringstream streamB(b);
			streamB >> std::hex >> cl;
			clr.b = cl;

			return clr;
		}


		///Конвертация элемента в int
#ifdef NBG_ANDROID			
		inline static int ToInt(const std::string &element)
		{
			if (element.size() == 0)return 0;
			return atoi(element.c_str());
		}

		inline static int ToInt(const std::wstring &element)
		{
			if (element.size() == 0)return 0;
			return atoi(pugi::as_utf8(element).c_str());
		}

		inline static int ToFloat(const std::string &element)
		{
			if (element.size() == 0)return 0;
			return atof(element.c_str());
		}

		inline static int ToFloat(const std::wstring &element)
		{
			if (element.size() == 0)return 0;
			return atof(pugi::as_utf8(element).c_str());
		}
#else
		template <class Type>
		inline static int ToInt(const Type &element)
		{	
			if (element.size() == 0)return 0;
			return std::stoi( element );
		}

		template <class Type>
		inline static float ToFloat(const Type &element)
		{			
			if (element.size() == 0)return 0.0f;
			return std::stof(element);
		}
#endif
		

		static std::string PathToId(const std::string path);
		///разбивает строку на массив по разделителю
		static std::vector<std::string> ExplodeString(const std::string& str, const char& ch);
		///конвертирует строку в HotSpot
		static HotSpot StringToHotSpot(const std::string &hotSpot);
		///Убирает extension из названия файла
		static std::string RemoveExtension(const std::string str);
		///Получает extension из названия файла
		static std::string GetExtension(const std::string str);
	private:


	};

}




#endif // NBG_UTILS_MATH

