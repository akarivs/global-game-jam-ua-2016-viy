#ifndef FRAMEWORK_UTILS_LOG
#define FRAMEWORK_UTILS_LOG

#include <stdarg.h>
#include <../External/xml/pugixml.hpp>

#include <map>

using namespace pugi;

namespace NBG
{

    /** @brief Класс, реализующий работу с лог-файлом
    *    
    * @author Vadim Simonov <akari.vs@gmail.com>
    * @copyright 2015 New Bridge Games
    *
    */
    class CLog
    {
    public:
        /// @name Конструкторы
		CLog();
		~CLog();

        /// @name Загрузка конфига
        /// Загрузка с XML файла
        bool Init(const std::string path);
        
        
		void WriteSimpleLine(const std::string text);
		void WriteLine(const std::string text);        
		void WriteLineError(const std::string text);
		void WriteLineInfo(const std::string text);
		void WriteLineSuccess(const std::string text);
    private:
		FILE * m_File;
    };
}
#endif ///FRAMEWORK_UTILS_CONFIG
