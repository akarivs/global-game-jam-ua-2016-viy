	#include "StencilHelper.h"

#include <Framework.h>
#include <Framework/Global/Render.h>

#ifdef NBG_ANDROID
#include <EGL/egl.h> // requires ndk r5 or newer
#include <GLES/gl.h>
#endif

namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CStencilHelper::CStencilHelper()
	{
		m_Rectangle.Init(4,6);
		unsigned short * index = m_Rectangle.GetIndexes();
		index[0] = 0;
		index[1] = 1;
		index[2] = 2;
		index[3] = 3;
		index[4] = 2;
		index[5] = 1;

		Vertex * vertex = m_Rectangle.GetVertexes();
		vertex[0].color = 0xFF000000;
		vertex[1].color = 0xFF000000;
		vertex[2].color = 0xFF000000;
		vertex[3].color = 0xFF000000;		
	}

	//////////////////////////////////////////////////////////////////////////
	CStencilHelper::~CStencilHelper()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CStencilHelper::Begin(const bool useAlphaTest)
	{
#if defined(NBG_ANDROID) || defined(NBG_OSX)
		g_Render->DrawBatch();
		g_Render->StencilEnable();		
		g_Render->StencilClear();				
		g_Render->StencilLevelSet(2);
		g_Render->StencilSetCmpFunc(CMP_ALWAYS);		
		g_Render->StencilSetPasOpFunc(OP_REPLACE);
		g_Render->StencilSetFailOpFunc(OP_KEEP);
		if (useAlphaTest)
		{
			glEnable(GL_ALPHA_TEST);
			glAlphaFunc(CMP_GREATEREQUAL, 0x00000009);
		}
#else
		g_Render->DrawBatch();
		g_Render->StencilEnable();
		g_Render->StencilClear();
		g_Render->StencilLevelSet(2);
		g_Render->StencilSetCmpFunc(CMP_NEVER);
		g_Render->StencilSetPasOpFunc(OP_KEEP);
		g_Render->StencilSetFailOpFunc(OP_REPLACE);
		if (useAlphaTest)
		{
#ifdef NBG_WIN32
			g_Render->GetDevice()->SetRenderState(D3DRS_ALPHAREF, (DWORD)0x00000009);
			g_Render->GetDevice()->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
			g_Render->GetDevice()->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);
#endif
		}
#endif
	}

	//////////////////////////////////////////////////////////////////////////
	void CStencilHelper::Switch()
	{
#if defined(NBG_ANDROID) || defined(NBG_OSX)
		g_Render->DrawBatch();
		g_Render->StencilLevelSet(2);
		g_Render->StencilSetCmpFunc(CMP_EQUAL);		
		g_Render->StencilSetPasOpFunc(OP_REPLACE);
		g_Render->StencilSetFailOpFunc(OP_KEEP);
#else
		g_Render->DrawBatch();
		g_Render->StencilLevelSet(1);
		g_Render->StencilSetCmpFunc(CMP_LESS);		
		g_Render->StencilSetPasOpFunc(OP_KEEP);
		g_Render->StencilSetFailOpFunc(OP_REPLACE);
#endif
	}

	//////////////////////////////////////////////////////////////////////////
	void CStencilHelper::End()
	{
		g_Render->DrawBatch();		
		g_Render->StencilDisable();
#ifdef NBG_WIN32
		g_Render->GetDevice()->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);		
#elif defined(NBG_ANDROID) || defined(NBG_OSX)
		glDisable(GL_ALPHA_TEST);		
#endif
	}

	//////////////////////////////////////////////////////////////////////////
	void CStencilHelper::DrawRectangle(Transform * transform, const Vector size)
	{		
		Transform trans = transform->GetAbsoluteTransform();
		Vertex * vertexes = m_Rectangle.GetVertexes();

		Vector s = size;
		s.x *= trans.scalex;
		s.y *= trans.scaley;
		s /= 2.0f;

		vertexes->x = trans.x - s.x;
		vertexes->y = (trans.y - s.y);
		vertexes++;
		vertexes->x = trans.x + s.x;
		vertexes->y = (trans.y - s.y);
		vertexes++;
		vertexes->x = trans.x - s.x;
		vertexes->y = (trans.y + s.y);
		vertexes++;
		vertexes->x = trans.x + s.x;
		vertexes->y = (trans.y + s.y);
		g_Render->DrawMesh(g_GameApplication->GetTextureManager()->GetDefaultTexture(),&m_Rectangle);
	}
}
