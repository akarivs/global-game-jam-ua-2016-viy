#ifndef _AK_FRAMEWORK_GAME_APPLICATION
#define _AK_FRAMEWORK_GAME_APPLICATION

#include <functional>

#include <Framework/Datatypes/Vector.h>
#include <Framework/Utils/Config.h>
#include <Framework/Utils/Log.h>


#include <Framework/Managers/FontsManager.h>
#include <Framework/Managers/LocalizationManager.h>
#include <Framework/Managers/PlayersManager.h>
#include <Framework/Managers/SoundManager.h>

#include <Framework/Global/Render.h>
#include <Framework/Global/Render/RenderState.h>
#include <Framework/Global/System.h>
#include <Framework/Global/Input.h>
#include <Framework/Global/Random.h>

#include <Framework/Global/OperatingSystem.h>
#include <Framework/Global/TimeManager.h>
#include <Framework/Global/TextureManager.h>
#include <Framework/Global/FileSystem.h>
#include <Framework/Global/ResourcesManager.h>

#include <Framework/Helpers/AtlasHelper.h>
#include <Framework/Helpers/EditionHelper.h>
#include <Framework/Helpers/BinaryHelper.h>
#include <Framework/Helpers/StencilHelper.h>

#include <../include/theora_player/TheoraPlayer.h>
#include <../external/theora_player/OpenAL_AudioInterface.h>



namespace NBG
{


	class CGameApplication
	{
	public:
		CGameApplication();
		~CGameApplication();


		///загрузка игровой логики и запуск игры
		bool Init();
		bool Run();
		void AfterRun();
		void StopApp();
		bool IsStopped();
		bool IsPaused();
		void Pause();
		void Resume();

		bool IsCheatsEnabled();

		bool IsFullscreen();

		void SetWindowTitle(const std::wstring &title);


		std::string GetStringsPath()
		{
			return m_StringsPath;
		}


		CGameApplication * SetFullscreenSize(const int w, const int h);
		CGameApplication * SetLogicalSize(const int w, const int h);
		CGameApplication * SetWindowedSize(const int w, const int h);
		CGameApplication * SetFullscreen(const bool fullscreen);
		CGameApplication * SetFontsPath(const std::string &fontsPath);
		CGameApplication * SetSoundsPath(const std::string &soundsPath);
		CGameApplication * SetStringsPath(const std::string &fontsPath);
		CGameApplication * SetConfigPath(const std::string &configPath);
		CGameApplication * SetAtlasesPath(const std::string &atlasesPath);
		CGameApplication * SetEditionsPath(const std::string &editionXML, const std::string &editionsFolder);

		NBG::CConfig			* GetConfig();
		NBG::CLog				* GetLog();
		CFontsManager			* GetFontsManager();
		CLocalizationManager	* GetLocalizationManager();
		CPlayersManager			* GetPlayersManager();
		TheoraVideoManager		* GetTheoraVideoManager();
		CSoundManager			* GetSoundManager();		
		COperatingSystem		* GetOperatingSystem();
		CTimeManager			* GetTimeManager();
		CTextureManager			* GetTextureManager();
		CFileSystem				* GetFileSystem();
		CAtlasHelper			* GetAtlasHelper();
		CResourcesManager       * GetResourcesManager();
		CEditionHelper		    * GetEditionHelper();
		CRender					* GetRender();
		CSystem					* GetSystem();
		CInput					* GetInput();
		CRandom					* GetRandom();		
		CBinaryHelper			* GetBinaryHelper();
		CStencilHelper			* GetStencilHelper();

		Vector GetLogicalSize()
		{
			return m_LogicalSize;
		}

		Vector & GetWindowSize();
		Vector & GetWindowedSize(){ return m_WindowedSize; }
		double GetFrameTime();
		void SetFrameTime(double time){ m_FrameTime = time; }
		double GetAppTime();
		void ResetAppTime();


		void OnKeyUp(const int key);
		void OnKeyDown(const int key);
		void OnMouseUp(const int button);
		void OnMouseDown(const int button);
		void OnMouseMove();
		void OnWheelMove(const int delta);

		void OnBeforeRenderChange();
		void OnAfterRenderChange();
		void Update();
		void Draw();

		void ShareContext();
		void UnShareContext();

		void OnLostFocus()
		{
			if (m_FocusChangeCallback)
			{
				m_FocusChangeCallback(FocusChangeType::Lost);
			}
		}

		void OnGetFocus()
		{
			if (m_FocusChangeCallback)
			{
				m_FocusChangeCallback(FocusChangeType::Get);
			}
		}


		void SetOnInitCallback(std::function<void(void)> callback){ m_OnInitCallback = callback; };
		void SetOnMouseMoveCallback(std::function<void(void)> callback){ m_OnMouseMoveCallback = callback; };
		void SetOnMouseDownCallback(std::function<void(int)> callback){ m_OnMouseDownCallback = callback; };
		void SetOnMouseUpCallback(std::function<void(int)> callback){ m_OnMouseUpCallback = callback; };
		void SetOnMouseWheelCallback(std::function<void(int)> callback){ m_OnMouseWheelCallback = callback; };
		void SetOnKeyDownCallback(std::function<void(int)> callback){ m_OnKeyDownCallback = callback; };
		void SetOnKeyUpCallback(std::function<void(int)> callback){ m_OnKeyUpCallback = callback; };
		void SetUpdateCallback(std::function<void(void)> callback){ m_OnUpdateCallback = callback; };
		void SetDrawCallback(std::function<void(CRenderState &)> callback){ m_OnDrawCallback = callback; };
		void SetAfterLoopCallback(std::function<void(void)> callback){ m_AfterLoopCallback = callback; };
		void SetOnRenderChangeCallback(std::function<void(RenderChangeType)> callback){ m_OnRenderChangeCallback = callback; };
		void SetFocusChangeCallback(std::function<void(FocusChangeType)> callback){ m_FocusChangeCallback = callback; };



	private:
		bool m_IsApplicationActive;
		bool m_IsInitialized;
		NBG::Transform m_SavedGlobalMatrix;

		std::string m_FontsPath;
		std::string m_StringsPath;
		std::string m_SoundsPath;
		std::string m_ConfigPath;
		std::string m_AtlasesPath;
		std::string m_EditionsXMLPath;
		std::string m_EditionsFolderPath;

		Vector m_FullscreenSize;
		Vector m_WindowedSize;
		Vector m_LogicalSize;
		Vector m_CurrentSize;
		bool m_IsFullscreen;
		bool m_IsStopped;
		bool m_IsPaused;

		double m_FrameTime;
		double m_AppTime;

		CConfig m_Config;
		CLog m_Log;
		CFontsManager m_FontsManager;
		CLocalizationManager m_LocalizationManager;
		CPlayersManager m_PlayersManager;
		TheoraVideoManager *m_TheoraVideoManager;
		CSoundManager m_SoundManager;
		IOperatingSystem * m_OperatingSystem;
		ITimeManager * m_TimeManager;
		ITextureManager * m_TextureManager;
		IFileSystem * m_FileSystem;
		CRender		m_Render;
		CSystem		m_System;
		CInput		m_Input;
		CRandom		m_Random;

		CRenderState m_RenderState;

		CAtlasHelper m_AtlasHelper;
		CEditionHelper m_EditionHelper;
		CResourcesManager m_ResourcesManager;
		CBinaryHelper	m_BinaryHelper;
		CStencilHelper	m_StencilHelper;	

		float m_UpdateLimit;
		double m_UpdateCounter;
		float m_DrawLimit;
		float m_DrawCounter;

		///Debug draw call limiter to check optimize draw calls
		GETTER_SETTER(int, m_DrawCallLimiter, DrawCallLimiter);
		
		int m_FramesCount;
		float m_FramesTimer;

		std::function<void(void)> m_OnInitCallback;
		std::function<void(void)> m_OnUpdateCallback;
		std::function<void(CRenderState &)> m_OnDrawCallback;
		std::function<void(void)> m_AfterLoopCallback;
		std::function<void(int)>  m_OnKeyDownCallback;
		std::function<void(int)>  m_OnKeyUpCallback;
		std::function<void(void)>  m_OnMouseMoveCallback;
		std::function<void(int)>  m_OnMouseDownCallback;
		std::function<void(int)>  m_OnMouseUpCallback;
		std::function<void(int)>  m_OnMouseWheelCallback;		
		std::function<void(RenderChangeType)>  m_OnRenderChangeCallback;
		std::function<void(FocusChangeType)> m_FocusChangeCallback;
	protected:

	};
}

#endif
