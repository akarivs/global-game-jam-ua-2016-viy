#ifndef NBG_CORE_OPERATING_SYSTEM_BASE
#define NBG_CORE_OPERATING_SYSTEM_BASE

#include <string>
#include <map>

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Vector.h>


enum class FocusChangeType
{
	Get,
	Lost
};

namespace NBG
{
	/** @brief Базовый интерфейсный класс для операционной системы.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013 New Bridge Games
	*
	*/
	class IOperatingSystem
	{
	public:
		/// Создание окна.
		virtual bool CreateAppWindow() = 0;
		/// Главный цикл приложения.
		virtual void MainLoop() = 0;

		/// Установка заголовка окна.
		virtual void SetTitle(const std::wstring title)
		{

		};		

		/// Установка полноэкранного режима.
		virtual void SetFullscreenMode(bool isFullscreen)
		{

		};

		/// Проверка возможности перевода в оконный режим
		virtual bool IsCanBeWindowed()
		{
			return true;
		}
		virtual bool IsCanBeWindowed(Vector size)
		{
			return true;
		}

		///Проверка нахождения окна в фокусе
		virtual bool IsFocused()
		{
			return true;
		}

	};
}
#endif //NBG_CORE_OPERATING_SYSTEM_BASE
