#ifndef NBG_CORE_OPERAING_SYSTEM_OSX
#define NBG_CORE_OPERAING_SYSTEM_OSX


#ifdef NBG_OSX


#include "../BaseOperatingSystem.h"

#include <SDL2/SDL.h>
#include <IOKit/pwr_mgt/IOPMLib.h>
#include <IOKit/IOMessage.h>


namespace NBG
{
   
	class COperatingSystem : public IOperatingSystem
	{
	public:
        /// @name Конструкторы
		COperatingSystem();
		~COperatingSystem();
        
		/// Создание окна.
		virtual bool CreateAppWindow();
		/// Установка заголовка окна.
		virtual void SetTitle(const std::wstring title);
		/// Главный цикл приложения.
		virtual void MainLoop();
		/// Установка полноэкранного режима.
		virtual void SetFullscreenMode(bool isFullscreen);
        
        void ShareContext();
        void UnShareContext();
        
        virtual bool IsFocused()
        {
            return m_IsFocused;
        }
        SDL_Window * m_Window;
    private:
        
        SDL_GLContext m_Context;
        SDL_GLContext m_ThreadContext;
        
        Vector GetAvailableWindowedSize();
        bool IsCanBeWindowed(Vector size);
        
        bool m_IsFocused;
        	};
}
#endif
#endif
