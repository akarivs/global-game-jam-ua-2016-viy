#ifdef NBG_OSX

#include "OperatingSystem.h"

#include "Framework.h"
#include <mach/mach.h>


#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

#include <SDL2/SDL.h>


namespace NBG
{

    
    
	COperatingSystem::COperatingSystem()
	{
        m_IsFocused = true;
	}


	bool COperatingSystem::CreateAppWindow()
	{
               
        if (SDL_Init(SDL_INIT_EVERYTHING) != 0){
            std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
            return 1;
        }
        
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
        
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
        
        SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 1);
        
    	
	
        std::wstring title = g_GameApplication->GetLocalizationManager()->GetText("common_window_title");
        bool isCE = false;
        g_Config->GetValue("ce", isCE);
        if (!isCE)
        {
            title = g_GameApplication->GetLocalizationManager()->GetText("common_window_title_se");
        }
        
        m_Window = SDL_CreateWindow(pugi::as_utf8(title).c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1024, 768, SDL_WINDOW_SHOWN);
        if (m_Window == NULL){
            std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
            SDL_Quit();
            return 1;
        }
       
        
       
        SDL_GL_SetAttribute(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, 1);
        m_ThreadContext = SDL_GL_CreateContext(m_Window);
        SDL_GL_SetSwapInterval(1);
        m_Context = SDL_GL_CreateContext(m_Window);
        SDL_GL_SetSwapInterval(1);
        
        
        
        

       
        
        
        return true;
	}
    
    void COperatingSystem::ShareContext()
    {
        SDL_GL_MakeCurrent(m_Window, m_ThreadContext);
    }
    
    void COperatingSystem::UnShareContext()
    {
        SDL_GL_MakeCurrent(m_Window, nullptr);
    }

	void COperatingSystem::SetTitle(const std::wstring title)
    {
        SDL_SetWindowTitle(m_Window, pugi::as_utf8(title).c_str());
    }
    
    //////////////////////////////////////////////////////////////////////////
    bool COperatingSystem::IsCanBeWindowed(Vector size)
    {
        SDL_DisplayMode md;
        SDL_GetDesktopDisplayMode(0, &md);
        if (md.w <= size.x + 50.0f || md.h <= size.y + 160.0f)
        {
            return false;
        }
        return true;
    }
    
    ///
    Vector COperatingSystem::GetAvailableWindowedSize()
    {
        Vector ret = g_GameApplication->GetWindowedSize();
        
        if (IsCanBeWindowed(g_GameApplication->GetWindowedSize()) == false)
        {
            if (IsCanBeWindowed(g_GameApplication->GetLogicalSize()) == false)
            {
                if (IsCanBeWindowed(Vector(1067, 600)) == false)
                {
                    if (IsCanBeWindowed(Vector(854, 480)) == false)
                    {
                        if (IsCanBeWindowed(Vector(800, 600)) == false)
                        {
                            if (IsCanBeWindowed(Vector(640, 480)) == false)
                            {								
                                ret.x = 320;
                                ret.y = 240;
                            }
                            else
                            {
                                ret.x = 640;
                                ret.y = 480;
                            }
                        }
                        else
                        {
                            ret.x = 800;
                            ret.y = 600;
                        }
                    }
                    else
                    {
                        ret.x = 854;
                        ret.y = 480;
                    }
                }
                else
                {
                    ret.x = 1067;
                    ret.y = 600;
                }
            }
            else
            {
                ret = g_GameApplication->GetLogicalSize();
            }
        }
        return ret;
    }

    
    void COperatingSystem::SetFullscreenMode(bool isFullscreen)
	{
        
        if (isFullscreen)
		{
            SDL_DisplayMode md;
            SDL_GetDesktopDisplayMode(0, &md);
            SDL_SetWindowSize(m_Window, md.w, md.h);
            SDL_SetWindowFullscreen(m_Window, SDL_WINDOW_FULLSCREEN);
            
            auto v = Vector(md.w,md.h);
            g_System->UpdateSize(v, g_GameApplication->GetWindowSize(),isFullscreen);
		}
		else
		{
            SDL_SetWindowFullscreen(m_Window, SDL_FALSE);
            
            
            auto newSize = GetAvailableWindowedSize();
            
            SDL_SetWindowSize(m_Window,newSize.x, newSize.y);
            SDL_SetWindowPosition(m_Window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
            
            g_System->UpdateSize(newSize, g_GameApplication->GetWindowSize(),isFullscreen);
            
            
            std::wstring title = g_GameApplication->GetLocalizationManager()->GetText("common_window_title");
            bool isCE = false;
            g_Config->GetValue("ce", isCE);
            if (!isCE)
            {
                title = g_GameApplication->GetLocalizationManager()->GetText("common_window_title_se");
            }

            SetTitle(title);
        }
        g_Render->Reset();
        SDL_ShowCursor(!g_PlayersManager->GetCurrentPlayer()->IsCustomCursor());
	}
    
	void COperatingSystem::MainLoop()
	{
        
        SDL_Event e;
        while(true)
		{
			if (g_GameApplication->IsStopped())
			{
				break;
			}
            while (SDL_PollEvent(&e))
            {
                //If user closes the window
                if (e.type == SDL_QUIT)
                {
                    g_GameApplication->StopApp();
                }
                else if (e.type == SDL_TEXTINPUT)
                {
                    std::wstring str = pugi::as_wide(&e.edit.text[0]);
                    g_Input->SetLastInputString(g_Input->GetLastInput() + str);
                }
                //If user presses any key
                else if (e.type == SDL_KEYDOWN)
                {
                    g_GameApplication->OnKeyDown(e.key.keysym.scancode);
                    g_Input->SetKeyDown(e.key.keysym.scancode, true);
                }
                else if (e.type == SDL_KEYUP)
                {
                    g_GameApplication->OnKeyUp(e.key.keysym.scancode);
                    g_Input->SetKeyDown(e.key.keysym.scancode, false);
                    g_Input->SetLastInputString(L"");
                }
                else if (e.type == SDL_MOUSEBUTTONDOWN)
                {
                    g_GameApplication->OnMouseDown(CInput::MOUSE_BUTTON_LEFT);
                }
                else if (e.type == SDL_MOUSEBUTTONUP)
                {
                    g_GameApplication->OnMouseUp(CInput::MOUSE_BUTTON_LEFT);
                }
                else if (e.type == SDL_MOUSEMOTION)
                {
                    g_Input->SetMousePos(e.motion.x, e.motion.y);
                    g_GameApplication->OnMouseMove();
                }
                else if ( e.type == SDL_WINDOWEVENT	)
                {
                 
                        if (e.window.event ==  SDL_WINDOWEVENT_FOCUS_GAINED)
                        {
                            g_GameApplication->Resume();
                            g_GameApplication->GetTimeManager()->Reset();
                            g_GameApplication->OnGetFocus();
                            m_IsFocused = true;
                        }
                        else if (e.window.event == SDL_WINDOWEVENT_FOCUS_LOST)
                        {
                            g_GameApplication->Pause();
                            g_GameApplication->OnLostFocus();
                            m_IsFocused = false;
                        }
                }
       
            }
            
			if (g_GameApplication->IsPaused())
			{
				g_System->Sleep(100);
				continue;
			}
			g_GameApplication->Update();
			g_GameApplication->Draw();
            
            SDL_GL_SwapWindow(m_Window);
		}
	}
}


#endif
