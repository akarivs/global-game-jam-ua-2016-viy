#ifndef NBG_CORE_OPERAING_SYSTEM_WIN
#define NBG_CORE_OPERAING_SYSTEM_WIN

#ifdef NBG_WIN32

#include <windows.h>

#include <Framework/Datatypes.h>

#include "../BaseOperatingSystem.h"

namespace NBG
{
	/** @brief Класс для операционной системы Windows.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013 New Bridge Games
	*
	*/
	class COperatingSystem : public IOperatingSystem
	{
	public:
		/// @name Конструкторы
		COperatingSystem();
		~COperatingSystem();

		/// Создание окна.
		virtual bool CreateAppWindow();
		/// Установка заголовка окна.
		virtual void SetTitle(const std::wstring title);
		/// Главный цикл приложения.
		virtual void MainLoop();
		/// Установка полноэкранного режима.
		virtual void SetFullscreenMode(bool isFullscreen);



		/// Установка Windows HINSTANCE
		void SetInstance(HINSTANCE _instance)
		{
			m_hInstance =  _instance;
		};

		HWND GetWindow()
		{
			return m_HWND;
		};

		/// Проверка возможности перевода в оконный режим
		virtual bool IsCanBeWindowed();
		virtual bool IsCanBeWindowed(Vector & resultSize);
		///Проверка нахождения окна в фокусе
		virtual bool IsFocused();
		

		
	private:
		virtual bool _IsCanBeWindowed(Vector size);
		/// Дескриптор окна.
		HWND m_HWND;

		/// Instance Windows приложения.
		HINSTANCE m_hInstance;
	};
}
#endif
#endif
