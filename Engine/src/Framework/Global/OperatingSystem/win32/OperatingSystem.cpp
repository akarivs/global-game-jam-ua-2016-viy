#ifdef NBG_WIN32

#include "OperatingSystem.h"
#include <Framework.h>
#include <windowsx.h>


#include "../../../Project.Win32/resource.h"


#include "DbgHelp.h"
#include <eh.h>
#include <Psapi.h>
#include <string>
#include <sstream>
#include <fstream>



namespace NBG
{
	bool __g_Win32_SkipMouseClick = false;
	//==============================================================================
	LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{	
		wchar_t buffer[1];
		BYTE lpKeyState[256];

		float wheelDelta;

		switch (message)
		{
		case WM_KEYDOWN:
			/*GetKeyboardState(lpKeyState);
			if (ToUnicode(wParam, HIWORD(lParam)&0xFF, lpKeyState, buffer, 1, 0) > 0)
			{			
				g_Input->SetLastInputChar(buffer[0]);
			}*/
			g_GameApplication->OnKeyDown((int)wParam);
			g_Input->SetKeyDown(wParam, true);			
			break;
		case WM_CHAR:

			break;
		case WM_SETCURSOR:
			if (LOWORD(lParam) == HTCLIENT) // ���� ��������� �������
			{
				if (g_PlayersManager->GetCurrentPlayer()->IsCustomCursor() == false)
				{
					SetCursor((HCURSOR)LoadCursor(NULL, IDC_ARROW));
					ULONG_PTR hc = GetClassLong(hWnd, GCL_HCURSOR);
				}
				else
				{
					SetCursor(NULL);  // �������� �������!
				}				
			}
			else // �� ��������� ������� - ����� ��� ��������� "��� ��������"...
			{
				ShowCursor(true);
				return DefWindowProc(hWnd, message, wParam, lParam);
			}
			break;
		case WM_MOUSEWHEEL:
			wheelDelta = GET_WHEEL_DELTA_WPARAM(wParam);
			wheelDelta /= 120;
			g_GameApplication->OnWheelMove(wheelDelta);			
			g_Input->OnZoom(wheelDelta > 0 ? 1.1f : 0.9f, g_MousePos.x, g_MousePos.y);
			break;
		case WM_MOUSEMOVE:			
			g_Input->SetMousePos((float)GET_X_LPARAM(lParam), (float)GET_Y_LPARAM(lParam));			
			g_GameApplication->OnMouseMove();
			break;
		case WM_LBUTTONDOWN:
			if (__g_Win32_SkipMouseClick == false)
				g_GameApplication->OnMouseDown(CInput::MOUSE_BUTTON_LEFT);			
			break;
		case WM_LBUTTONUP:
			if (__g_Win32_SkipMouseClick == false)
				g_GameApplication->OnMouseUp(CInput::MOUSE_BUTTON_LEFT);
			break;
		case WM_RBUTTONDOWN:
			if (__g_Win32_SkipMouseClick == false)
				g_GameApplication->OnMouseDown(CInput::MOUSE_BUTTON_RIGHT);
			break;
		case WM_RBUTTONUP:
			if (__g_Win32_SkipMouseClick == false)
				g_GameApplication->OnMouseUp(CInput::MOUSE_BUTTON_RIGHT);
			break;
		case WM_MBUTTONDOWN:
			if (__g_Win32_SkipMouseClick == false)
				g_GameApplication->OnMouseDown(CInput::MOUSE_BUTTON_MIDDLE);
			break;
		case WM_MBUTTONUP:
			if (__g_Win32_SkipMouseClick == false)
				g_GameApplication->OnMouseUp(CInput::MOUSE_BUTTON_MIDDLE);
			break;
		case WM_KEYUP:
			g_GameApplication->OnKeyUp((int)wParam);
			g_Input->SetKeyDown(wParam, false);		
			break;
		case WM_DISPLAYCHANGE:
			g_Render->Reset();
			g_GameApplication->SetFullscreen(g_GameApplication->IsFullscreen());
			break;
		case WM_NCLBUTTONDBLCLK:
			g_GameApplication->SetFullscreen(true);
			break;
		case WM_SYSCOMMAND:
			if (g_GameApplication->IsFullscreen())
			{
				switch (wParam)
				{
				case SC_SCREENSAVE:
					return 0;
				case SC_MONITORPOWER:
					return 0;
				}
			}			
			if (wParam == SC_KEYMENU && lParam == KEY_ENTER)
			{
				g_GameApplication->SetFullscreen(!g_GameApplication->IsFullscreen());
				g_PlayersManager->GetCurrentPlayer()->SetFullscreen(g_GameApplication->IsFullscreen());
			}
			return DefWindowProc(hWnd, message, wParam, lParam);
			break;
		case WM_ACTIVATEAPP:
			if(wParam == false)
			{		
				g_GameApplication->Pause();						
				g_Render->CheckDevice();	
				g_GameApplication->OnLostFocus();

				if (g_GameApplication->IsFullscreen())
				{
					ShowWindowAsync(hWnd, SW_MINIMIZE);
				}
			}
			else if(wParam == TRUE)
			{	
				__g_Win32_SkipMouseClick = true;
				g_GameApplication->Resume();					
				g_Render->CheckDevice();						
				g_GameApplication->GetTimeManager()->Reset();
				g_GameApplication->OnGetFocus();				
			}
			break;	
		case WM_ENTERSIZEMOVE:			
			g_GameApplication->Pause();
			g_SoundManager->OnLostFocus();
			g_Render->CheckDevice();			
			break;
		case WM_SIZE:
			if (wParam == SIZE_MAXIMIZED)
			{
				g_GameApplication->SetFullscreen(true);
			}			
			break;
		case WM_EXITSIZEMOVE:			
			g_GameApplication->Resume();
			g_SoundManager->OnGetFocus();
			g_Render->CheckDevice();									
			g_GameApplication->GetTimeManager()->Reset();
			return DefWindowProc(hWnd, message, wParam, lParam);
			break;
		case WM_CLOSE:			
			g_GameApplication->StopApp();
			break;
		case WM_DESTROY:
			g_GameApplication->StopApp();
			break;
		default:		
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		return false;
	}
	
	//==============================================================================
	COperatingSystem::COperatingSystem()
	{
	
	}

	//==============================================================================
	COperatingSystem::~COperatingSystem()
	{
		
	}	

	//==============================================================================
	bool COperatingSystem::CreateAppWindow()
	{
		HMODULE hUser32 = LoadLibrary(L"user32.dll");
		typedef BOOL(*SetProcessDPIAwareFunc)();
		SetProcessDPIAwareFunc setDPIAware =
			(SetProcessDPIAwareFunc)GetProcAddress(hUser32, "SetProcessDPIAware");



		OSVERSIONINFO osvi;
		ZeroMemory(&osvi, sizeof(OSVERSIONINFO));
		osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
		GetVersionEx(&osvi);
		if (osvi.dwMajorVersion >= 6)
		{
			if (setDPIAware)
			{
				setDPIAware();
			}
		}


		FreeLibrary(hUser32);


		// Register the window class.
		const wchar_t *CLASS_NAME  = new wchar_t();
		CLASS_NAME = L"NewBridgeGameEngineClass";

		m_hInstance = GetModuleHandle(NULL);

		RECT		WindowRect;										// Grabs Rectangle Upper Left / Lower Right Values

		WindowRect.left=(long)0;									// Set Left Value To 0
		WindowRect.right=(long)(int)g_System->GetSystemResolution().x;								// Set Right Value To Requested Width
		WindowRect.top=(long)0;										// Set Top Value To 0
		WindowRect.bottom=(long)(int)g_System->GetSystemResolution().y;								// Set Bottom Value To Requested Height

		bool fullScreen = g_GameApplication->IsFullscreen();		

		WNDCLASSEXW wc = { };

		wc.cbSize = sizeof(WNDCLASSEX);
		wc.lpfnWndProc   = WndProc;
		wc.style		 = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
		wc.hInstance     = m_hInstance;
		wc.lpszClassName = (LPCWSTR)CLASS_NAME;
		wc.cbClsExtra		= 0;
		wc.cbWndExtra		= 0;
		wc.hCursor			= LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground	= NULL;
		wc.hIcon = LoadIcon(m_hInstance, MAKEINTRESOURCE(IDI_ICON1));
		wc.hIconSm = LoadIcon(m_hInstance, MAKEINTRESOURCE(IDI_ICON2));
		

		DWORD exS = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		DWORD exWS = WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS |	WS_CLIPCHILDREN;
		exWS ^= WS_THICKFRAME;

		if (fullScreen)
		{
			exS = 0;
			exWS = WS_EX_TOPMOST | WS_POPUP;
		}

		AdjustWindowRectEx(&WindowRect, exWS, false, exS);	// Adjust Window To True Requested Size

		
		std::wstring name = g_GameApplication->GetLocalizationManager()->GetText("common_window_title");
		bool isCE = false;
		g_Config->GetValue("ce", isCE);
		if (!isCE)
		{
			name = g_GameApplication->GetLocalizationManager()->GetText("common_window_title_se");
		}		

		RegisterClassEx(&wc);
		HWND hwnd = CreateWindowExW(
			exS,                              // Optional window styles.
			(LPCWSTR)CLASS_NAME,             // Window class
			name.c_str(),     // Window text
			exWS,            // Window style
			// Size and position
			0, 0, WindowRect.right-WindowRect.left, WindowRect.bottom-WindowRect.top,
			NULL,       // Parent window
			NULL,       // Menu
			m_hInstance,  // Instance handle
			NULL        // Additional application data
			);

		if (hwnd == NULL)
		{
			return false;
		}
		m_HWND = hwnd;


		SetWindowPos( m_HWND, HWND_NOTOPMOST
			,(GetSystemMetrics(SM_CXSCREEN)/2.0f - WindowRect.right/2.0f) ,(GetSystemMetrics(SM_CYSCREEN)/2.0f - WindowRect.bottom/2.0f),
			WindowRect.right-WindowRect.left,
			WindowRect.bottom-WindowRect.top,
			SWP_SHOWWINDOW );

		ShowWindow(hwnd,SW_NORMAL);
		UpdateWindow(hwnd);

		ShowCursor(false);
		ShowCursor(true);

		return true;
	}

	//==============================================================================
	void COperatingSystem::SetTitle(const std::wstring title)
	{
		SetWindowTextW(m_HWND,(LPCWSTR)&title.c_str()[0]);
	}

	//////////////////////////////////////////////////////////////////////////
	bool COperatingSystem::IsCanBeWindowed()
	{
		Vector res;
		return IsCanBeWindowed(res);
	}

	//////////////////////////////////////////////////////////////////////////
	bool COperatingSystem::IsCanBeWindowed(Vector & resultSize)
	{
		if (_IsCanBeWindowed(g_GameApplication->GetWindowedSize()) == false)
		{
			if (_IsCanBeWindowed(g_GameApplication->GetLogicalSize()) == false)
			{
				if (_IsCanBeWindowed(Vector(1067, 600)) == false)
				{
					if (_IsCanBeWindowed(Vector(854, 480)) == false)
					{
						if (_IsCanBeWindowed(Vector(800, 600)) == false)
						{
							if (_IsCanBeWindowed(Vector(640, 480)) == false)
							{								
								return false;
							}
							else
							{
								resultSize.x = 640;
								resultSize.y = 480;
								return true;
							}
						}
						else
						{
							resultSize.x = 800;
							resultSize.y = 600;
							return true;
						}
					}
					else
					{
						resultSize.x = 854;
						resultSize.y = 480;
						return true;
					}
				}
				else
				{
					resultSize.x = 1067;
					resultSize.y = 600;
					return true;
				}
			}
			else
			{				
				resultSize.x = g_GameApplication->GetLogicalSize().x;
				resultSize.y = g_GameApplication->GetLogicalSize().y;
				return true;			
			}
		}
		else
		{
			resultSize.x = g_GameApplication->GetWindowedSize().x;
			resultSize.y = g_GameApplication->GetWindowedSize().y;
			return true;
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////
	bool COperatingSystem::_IsCanBeWindowed(Vector size)
	{
		HDC hDCScreen = GetDC(NULL);
		int screenX = GetDeviceCaps(hDCScreen, HORZRES);
		int screenY = GetDeviceCaps(hDCScreen, VERTRES);

		int dpiX = GetDeviceCaps(hDCScreen, LOGPIXELSX);
		int dpiY = GetDeviceCaps(hDCScreen, LOGPIXELSY);

		screenX /= ((float)dpiX / 96.0f);
		screenY /= ((float)dpiY / 96.0f);

		ReleaseDC(NULL, hDCScreen);
		

		if (size.x == -1 && size.y == -1)
		{
			size = g_GameApplication->GetWindowedSize(); 
		}


		if (screenX <= size.x + 50.0f || screenY <= size.y + 50.0f)
		{
			return false;
		}
		return true;
	}

	//==============================================================================
	void COperatingSystem::SetFullscreenMode(bool isFullscreen)
	{		
		if (isFullscreen)
		{
			SetThreadExecutionState(ES_CONTINUOUS | ES_SYSTEM_REQUIRED | ES_AWAYMODE_REQUIRED);
		}
		else
		{
			SetThreadExecutionState(ES_CONTINUOUS);
		}

		Vector windowSize;
		if (isFullscreen)
		{
			HDC hDCScreen = GetDC(NULL);
			int screenX = GetDeviceCaps(hDCScreen, HORZRES);
			int screenY = GetDeviceCaps(hDCScreen, VERTRES);

			int dpiX = GetDeviceCaps(hDCScreen, LOGPIXELSX);
			int dpiY = GetDeviceCaps(hDCScreen, LOGPIXELSY);
			ReleaseDC(NULL, hDCScreen);



			windowSize.x = screenX;
			windowSize.y = screenY;

		}
		else
		{
			windowSize = g_GameApplication->GetWindowSize();
		}
		g_System->UpdateSize(windowSize, g_GameApplication->GetWindowSize(),isFullscreen);
		
		if (isFullscreen)
		{
			g_Render->Reset();

			SetWindowLong(m_HWND, GWL_EXSTYLE, 0);
			SetWindowLong(m_HWND, GWL_STYLE, WS_EX_TOPMOST | WS_POPUP);

			RECT		WindowRect;										// Grabs Rectangle Upper Left / Lower Right Values
			WindowRect.left=(long)0;									// Set Left Value To 0
			WindowRect.right=(long)(int)g_System->GetSystemResolution().x;								// Set Right Value To Requested Width
			WindowRect.top=(long)0;										// Set Top Value To 0
			WindowRect.bottom=(long)(int)g_System->GetSystemResolution().y;					// Set Bottom Value To Requested Height			
			DWORD exS = 0;
			DWORD exWS = WS_EX_TOPMOST | WS_POPUP;

			int test = GetSystemMetrics(SM_CXSCREEN) / 2.0f - WindowRect.right / 2.0f;
			int test2 = GetSystemMetrics(SM_CYSCREEN) / 2.0f - WindowRect.bottom / 2.0f;
			AdjustWindowRectEx(&WindowRect, exWS, FALSE, exS);	// Adjust Window To True Requested Size			
			SetWindowPos(m_HWND, 0
				,0,0,
				windowSize.x,
				windowSize.y, SWP_SHOWWINDOW);
		}
		else
		{
			Vector resultRes;
			bool isCan = IsCanBeWindowed(resultRes);
			if (isCan)
			{
				windowSize = resultRes;
				g_System->UpdateSize(windowSize, g_GameApplication->GetWindowSize(), isFullscreen);
			}
			else
			{
				g_GameApplication->SetFullscreen(true);
				g_PlayersManager->GetCurrentPlayer()->SetFullscreen(true);
				SetThreadExecutionState(ES_CONTINUOUS | ES_SYSTEM_REQUIRED | ES_AWAYMODE_REQUIRED);
				return;
			}			

			g_Render->Reset();
			DWORD exS = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
			DWORD exWS = (WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN) &~(WS_THICKFRAME);

			SetWindowLong(m_HWND, GWL_EXSTYLE, WS_EX_APPWINDOW | WS_EX_WINDOWEDGE);
			SetWindowLong(m_HWND, GWL_STYLE, exWS);
			RECT		WindowRect;										// Grabs Rectangle Upper Left / Lower Right Values
			WindowRect.left = (long)0;									// Set Left Value To 0
			WindowRect.right = (long)(int)g_System->GetSystemResolution().x;								// Set Right Value To Requested Width
			WindowRect.top = (long)0;										// Set Top Value To 0
			WindowRect.bottom = (long)(int)g_System->GetSystemResolution().y;					// Set Bottom Value To Requested Height

			AdjustWindowRectEx(&WindowRect, exWS, FALSE, exS);	// Adjust Window To True Requested Size

			ShowWindow(m_HWND, SW_MINIMIZE);
			ShowWindow(m_HWND, SW_RESTORE);

			SetWindowPos(m_HWND, HWND_NOTOPMOST
				, (GetSystemMetrics(SM_CXSCREEN) / 2.0f - (WindowRect.right - WindowRect.left) / 2.0f), (GetSystemMetrics(SM_CYSCREEN) / 2.0f - (WindowRect.bottom - WindowRect.top) / 2.0f),
				WindowRect.right - WindowRect.left,
				WindowRect.bottom - WindowRect.top, SWP_SHOWWINDOW);

			RECT clienRect;
			GetClientRect(m_HWND, &clienRect);

			///Some magic to adjust window size
			Vector diff = g_System->GetSystemResolution() - Vector(clienRect.right - clienRect.left, clienRect.bottom - clienRect.top);
			SetWindowPos(m_HWND, HWND_NOTOPMOST
				, (GetSystemMetrics(SM_CXSCREEN) / 2.0f - (WindowRect.right - WindowRect.left) / 2.0f), (GetSystemMetrics(SM_CYSCREEN) / 2.0f - (WindowRect.bottom - WindowRect.top) / 2.0f),
				WindowRect.right - WindowRect.left + diff.x,
				WindowRect.bottom - WindowRect.top + diff.y, SWP_SHOWWINDOW);
			GetClientRect(m_HWND, &WindowRect);
			
		}

		DWORD err = GetLastError();
		HICON hIcon = LoadIcon(m_hInstance, MAKEINTRESOURCE(IDI_ICON1)); // ��������� ������ �� ��������
		SetClassLong(m_HWND, GCL_HICON, (LONG)hIcon); // ������������ ����� ����
		err = GetLastError();

		HICON hIconSm = LoadIcon(m_hInstance, MAKEINTRESOURCE(IDI_ICON2)); // ��������� ������ �� ��������
		SetClassLong(m_HWND, GCL_HICONSM, (LONG)hIconSm); // ������������ ����� ����
		err = GetLastError();

		
		HWND hWndFgnd = ::GetForegroundWindow();
		SetWindowPos(m_HWND, hWndFgnd, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_NOACTIVATE);
		SetWindowPos(hWndFgnd, m_HWND, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);

		SetFocus(m_HWND);
		UpdateWindow( m_HWND );
		SetForegroundWindow(m_HWND);
		SetActiveWindow(m_HWND);	
	}

	//==============================================================================
	void COperatingSystem::MainLoop()
	{	
		MSG msg;
		ZeroMemory(&msg, sizeof(msg));
		while(true)
		{
			if (g_GameApplication->IsStopped())
			{
				CONSOLE("Stopped");
				break;
			}
			if(PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
			{
				if (msg.message == WM_QUIT)
				{
					CONSOLE("WM_Quit");
				}
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			else
			{
				Sleep(1);
			}
			if (g_GameApplication->IsPaused())
			{
				Sleep(100);
				continue;
			}
			g_GameApplication->Update();
			g_GameApplication->Draw();

			__g_Win32_SkipMouseClick = false;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	bool COperatingSystem::IsFocused()
	{
		auto wnd = GetForegroundWindow();
		return m_HWND == wnd;
	}
}
#endif
