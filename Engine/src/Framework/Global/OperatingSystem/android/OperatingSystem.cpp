#ifdef NBG_ANDROID

#include "OperatingSystem.h"
#include <Framework.h>

#include <string>
#include <sstream>
#include <fstream>

#include <jni.h>
#include <android/native_window.h> // requires ndk r5 or newer
#include <android/native_window_jni.h> // requires ndk r5 or newer

#include <thread>
#include <mutex>

#include <GLES/gl.h>
#include <EGL/egl.h>

		

JNIEnv * COperatingSystem::AndroidEnv;
JavaVM * COperatingSystem::AndroidVM;
AAssetManager *COperatingSystem::AssetManager;
bool COperatingSystem::m_IsFocused;
std::string COperatingSystem::locale = "en";

enum EEventType
{
	E_MOUSE_MOVE,
	E_MOUSE_UP,
	E_MOUSE_DOWN,
	E_SET_WINDOW,
	E_FOCUS_LOST,
	E_FOCUS_GET,
	E_ZOOM,
};

struct SEvent
{
	EEventType type;
	float var1;
	float var2;	
	float zoom;
	int finger;
	ANativeWindow* window;
};

ANativeWindow* window;

std::wstring dataPathW = L"";

std::queue<SEvent>m_Events;
std::mutex mtx;

extern "C"
{
	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_Initialize(JNIEnv* env, jobject pointer,jobject assetManager, jstring dataDir, jstring locale)	
	{		
		env->GetJavaVM(&COperatingSystem::AndroidVM);
		COperatingSystem::AndroidVM->AttachCurrentThread(&COperatingSystem::AndroidEnv, 0);

		COperatingSystem::AndroidEnv = env;


		const char *nativeString = env->GetStringUTFChars(dataDir, 0);
		std::string dataPathS = nativeString;
		dataPathS += "/";
		env->ReleaseStringUTFChars(dataDir, nativeString);

		dataPathW = pugi::as_wide(dataPathS);

		const char *nativeStringLocale = env->GetStringUTFChars(locale, 0);
		std::string localeS = nativeStringLocale;		
		env->ReleaseStringUTFChars(locale, nativeStringLocale);

		COperatingSystem::locale = localeS;	
		CONSOLE("Locale is - %s", localeS.c_str());

		COperatingSystem::AssetManager = AAssetManager_fromJava(env, assetManager);		
		CONSOLE("JAVAVM inited - Data path is: %s", dataPathS.c_str());

COperatingSystem::m_IsFocused = true;
		NBG_Init();
	}

	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_TouchDown(JNIEnv* env, jobject pointer,float x, float y, int finger)	
	{			
		mtx.lock();
		SEvent evt;
		evt.type = E_MOUSE_DOWN;
		evt.var1 = x;
		evt.var2 = y;
		evt.finger = finger;
		m_Events.push(evt);		
		mtx.unlock();
	}

	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_OnScale(JNIEnv* env, jobject pointer, float scale, float x, float y)
	{
		mtx.lock();
		SEvent evt;
		evt.type = E_ZOOM;
		evt.var1 = x;
		evt.var2 = y;
		evt.zoom = scale;		
		m_Events.push(evt);
		mtx.unlock();
	}

	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_SetSurface(JNIEnv* env, jobject pointer,jobject surface)	
	{		
		mtx.lock();
		if (surface != 0) {
			SEvent evt;
			evt.type = E_SET_WINDOW;
			evt.window = ANativeWindow_fromSurface(env, surface);
			window = evt.window;
			m_Events.push(evt);
			CONSOLE("Got window %p", evt.window);			
		} else {
			CONSOLE("Releasing window");
			ANativeWindow_release(window);
		}
		mtx.unlock();
	}

	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_TouchUp(JNIEnv* env, jobject pointer, float x, float y, int finger)
	{			
		mtx.lock();
		SEvent evt;
		evt.type = E_MOUSE_UP;
		evt.var1 = x;
		evt.var2 = y;
		evt.finger = finger;
		m_Events.push(evt);		
		mtx.unlock();
	}

	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_TouchMove(JNIEnv* env, jobject pointer, float x, float y, int finger)
	{	
		mtx.lock();
		SEvent evt;
		evt.type = E_MOUSE_MOVE;
		evt.var1 = x;
		evt.var2 = y;
		evt.finger = finger;
		m_Events.push(evt);		
		mtx.unlock();
	}

	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_ChangeResolution(JNIEnv* env, jobject pointer,int w, int h)	
	{		
		mtx.lock();
		g_System->SetSystemResolution(w,h);
		Vector size = g_System->GetSystemResolution();
		Vector logicalSize = g_GameApplication->GetLogicalSize();
		g_System->UpdateSize(size,logicalSize,true);
		mtx.unlock();
	}

	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_Pause(JNIEnv* env, jobject pointer)	
	{
		g_GameApplication->Pause();
		g_SoundManager->OnLostFocus();	
		COperatingSystem::m_IsFocused = false;
	}

	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_Resume(JNIEnv* env, jobject pointer)	
	{		
		g_GameApplication->Resume();
		g_SoundManager->OnGetFocus();						
		g_GameApplication->GetTimeManager()->Reset();		
		COperatingSystem::m_IsFocused = true;
	}

	JNIEXPORT void JNICALL Java_org_nbg_main_NBGActivity_Loop(JNIEnv* env, jobject pointer)	
	{

	}
};


namespace NBG
{
	COperatingSystem::COperatingSystem()
	{

	}

	COperatingSystem::~COperatingSystem()
	{

	}

	bool COperatingSystem::CreateAppWindow()
	{
		return true;
	}

	void COperatingSystem::MainLoop()
	{
		std::thread loopThread(COperatingSystem::Loop);
		loopThread.detach();
	}

	//////////////////////////////////////////////////////////////////////////
	void COperatingSystem::Loop()
	{				
		g_GlobalMutex = new std::mutex();
		bool isInited = false;
		while(true)
		{
			if (g_GameApplication->IsStopped())
			{
				break;
			}
			if (g_GameApplication->IsPaused())
			{
				continue;
			}
			mtx.lock();
			while (m_Events.empty() == false)
			{

				auto event = m_Events.front();
				m_Events.pop();
				if (event.type == E_MOUSE_DOWN)
				{
					g_Input->SetMousePos(event.var1,event.var2);		
					g_Input->SetFingerDown(event.finger, event.var1, event.var2);
					g_GameApplication->OnMouseMove();
					g_GameApplication->OnMouseDown(CInput::MOUSE_BUTTON_LEFT);
				}
				else if (event.type == E_MOUSE_MOVE)
				{
					g_Input->SetMousePos(event.var1,event.var2);
					g_Input->SetFingerPos(event.finger, event.var1, event.var2);
					g_GameApplication->OnMouseMove();							
				}
				else if (event.type == E_ZOOM)
				{					
					Vector p(event.var1, event.var2);
					g_System->ConvertMousePos(p);
					g_Input->OnZoom(event.zoom, p.x,p.y);										
				}
				else if (event.type == E_MOUSE_UP)
				{					
					g_GameApplication->OnMouseUp(CInput::MOUSE_BUTTON_LEFT);						
					g_Input->SetFingerUp(event.finger);
				}
				else if (event.type == E_SET_WINDOW)
				{
					g_Render->SetWindow(event.window);
					g_Render->Init();		
					if (isInited == false)
					{
						g_System->SetSavePath(dataPathW);
						g_GameApplication->AfterRun();
						isInited = true;
					}					
					g_ResManager->ReloadTextures();
				}
			}
			mtx.unlock();
			if (isInited && g_GameApplication->IsPaused() == false)
			{				
				g_GameApplication->Update();
				g_GameApplication->Draw();
			}			
		}
	}
}
#endif
