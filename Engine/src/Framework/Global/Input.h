#ifndef NBG_FRAMEWORK_GLOBAL_INPUT
#define NBG_FRAMEWORK_GLOBAL_INPUT

#include <string>
#include <ostream>

#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/Events/EventDispatcher.h>

#ifdef NBG_WIN32
	#include "Input/win32/KeyCodes.h"
#elif defined(NBG_OSX)
	#include "Input/osx/KeyCodes.h"
#elif defined(NBG_IOS)
	#include "Input/ios/KeyCodes.h"
#elif defined(NBG_ANDROID)
	#include "Input/android/KeyCodes.h"
#endif


namespace NBG
{
/** @brief Класс, реализующий доступ к глобальным переменным инпута.
 *
 * @author Vadim Simonov <akari.vs@gmail.com>
 * @copyright 2013 New Bridge Games
 *
 */

enum class MouseButton
{
	Left,
	Middle,
	Right
};

enum class InputEvents
{
	Zoom,	
};


class CInput : public CEventDispatcher
{
public:
	CInput();
	~CInput();

	void SetMousePos(int x, int y, bool convert = true);
	Vector & GetMousePos();

	void SetLastInputChar(wchar_t ch)
	{
		m_LastInput = ch;
	}

	void SetLastInputString(std::wstring str)
	{
		m_LastInput = str;
	}


	std::wstring &GetLastInput()
	{
		return m_LastInput;
	}

	static const int MOUSE_BUTTON_LEFT = 0;
	static const int MOUSE_BUTTON_MIDDLE = 1;
	static const int MOUSE_BUTTON_RIGHT = 2;

	void SetKeyDown(const int id, const bool isDown);
	bool IsKeyDown(const int id);
	bool IsSystemKey(const int id);

	void SetFingerUp(const int finger)
	{
		if (finger < 0 || finger >= FINGERS_COUNT)return;
		m_Fingers[finger].active = false;
		m_ActiveFingers--;
	}

	void SetFingerDown(const int finger, const float x, const float y)
	{
		if (finger < 0 || finger >= FINGERS_COUNT)return;
		m_Fingers[finger].active = true;
		m_Fingers[finger].pos.x = x;
		m_Fingers[finger].pos.y = y;
		m_ActiveFingers++;
	}
	
	void SetFingerPos(const int finger, const float x, const float y)
	{
		if (finger < 0 || finger >= FINGERS_COUNT)return;
		m_Fingers[finger].active = true;
		m_Fingers[finger].pos.x = x;
		m_Fingers[finger].pos.y = y;
	}

	int GetActiveFingersCount()
	{
		return m_ActiveFingers;
	}

	void OnZoom(const float zoom, const float centerX, const float centerY);

private:		
	static const int FINGERS_COUNT = 10;
	std::wstring m_LastInput;
	Vector m_MousePos;
	bool m_Keys[256];
	struct SFinger
	{
		bool active;
		Vector pos;
	};
	SFinger m_Fingers[FINGERS_COUNT];
	int m_ActiveFingers;	
};
}
#endif // NBG_FRAMEWORK_GLOBAL_INPUT
