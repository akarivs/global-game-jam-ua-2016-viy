#ifdef NBG_ANDROID

#include "FileSystem.h"
#include <Framework.h>
#include <stdio.h>

#include <unistd.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>


namespace NBG
{
	CFileSystem::CFileSystem()
	{
	}

	CFileSystem::~CFileSystem()
	{
	}


	CFileSystem::FileContainer CFileSystem::ReadFile(const std::string &filename, const std::string &mode)
	{
		FileContainer fc;
		JNIEnv * env = COperatingSystem::AndroidEnv;
		AAssetManager* mgr = COperatingSystem::AssetManager;
		AAsset* asset = AAssetManager_open(mgr, g_EditionHelper->ConvertPath(filename).c_str(), AASSET_MODE_UNKNOWN);
		if (NULL == asset) {
			fc.status = FileNotFound;
			return fc;
		}
		fc.size = AAsset_getLength(asset);
		fc.data = (char*) malloc (sizeof(char)*fc.size);		
		AAsset_read (asset,fc.data,fc.size);		
		AAsset_close(asset);		
		fc.status = FileOk;
		fc.status = FileOk;
		return fc;		
	}

	FILE * CFileSystem::OpenFile(const std::string &filename, const std::string &mode)
	{
		FILE* file = fopen(filename.c_str(),mode.c_str());
		return file;
	}

	FILE * CFileSystem::OpenFile(const std::wstring &filename, const std::wstring &mode)
	{
		FILE* file = fopen(pugi::as_utf8(filename).c_str(), pugi::as_utf8(mode).c_str());
		return file;
	}

	void CFileSystem::SaveFile(const std::string &filename, const void * data, const int size)
	{
		FILE* file = fopen(filename.c_str(),"wb");
		if (!file)
		{
			return;
		}
		fwrite(data,size,1,file);
		fclose(file);
	}

	void CFileSystem::SaveFile(const std::wstring &filename, const void * data, const int size)
	{
		FILE* file = fopen(pugi::as_utf8(filename).c_str(), "wb");
		if (!file)
		{
			return;
		}
		fwrite(data, size, 1, file);
		fclose(file);
	}


	bool CFileSystem::DeleteFile(const std::string &filename)
	{
		return true;
	}

	bool CFileSystem::DeleteFile(const std::wstring &filename)
	{
		return true;
	}

	std::wstring CFileSystem::GetSaveDir()
	{
		return std::wstring(L"");
	}

	///Существует ли файл
	bool CFileSystem::IsFileExists(const std::string &filename)
	{
		auto file = ReadFile(filename,"r");
		return file.status == FileOk;
		return true;
	}

	///Существует ли директория
	bool CFileSystem::IsDirExists(const std::string &filename)
	{
		auto assetDir = AAssetManager_openDir(COperatingSystem::AssetManager, filename.c_str());
		bool r = AAssetDir_getNextFileName(assetDir) != NULL;
		AAssetDir_close(assetDir);
		return r;		
	}

	///Существует ли директория
	bool CFileSystem::IsDirExists(const std::wstring &filename)
	{
		auto assetDir = AAssetManager_openDir(COperatingSystem::AssetManager, pugi::as_utf8(filename.c_str()).c_str());
		bool r = AAssetDir_getNextFileName(assetDir) != NULL;
		AAssetDir_close(assetDir);
		return r;		
	}

	bool CFileSystem::CreateDirectory(const std::string &path)
	{
		return true;
	}

	bool CFileSystem::CreateDirectory(const std::wstring &path)
	{
		return true;
	}


	std::wstring CFileSystem::GetCurrentDirectory()
	{
		return std::wstring(L"");
	}

	///Установка текущей директории
	bool CFileSystem::SetCurrentDirectory(std::wstring dir)
	{
		return true;
	}


	///Получение директории процесса
	std::wstring CFileSystem::GetProcessDirectory()
	{
		return std::wstring(L"");
	}

	std::vector<std::string> CFileSystem::GetDirectoryContents(const std::string &path)
	{
		STRING_VECTOR vec;
		AAssetDir* assetDir = AAssetManager_openDir(COperatingSystem::AssetManager, path.c_str());
		const char* filename;
		while ((filename = AAssetDir_getNextFileName(assetDir)) != NULL)
		{
			vec.push_back(filename);			
		}
		AAssetDir_close(assetDir);
		return vec;
	}
}
#endif
