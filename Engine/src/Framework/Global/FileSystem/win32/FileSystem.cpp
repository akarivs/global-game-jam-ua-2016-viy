#ifdef NBG_WIN32

#include <Framework.h>


#include "windows.h"
#include "FileSystem.h"
#include <stdio.h>
#include <Shlobj.h>
#include "../../GamePack.h"


namespace NBG
{
	//==============================================================================
	CFileSystem::CFileSystem()
	{
		m_GamePack = new CGamePack();
	}

	//==============================================================================
	CFileSystem::~CFileSystem()
	{
	}

	//////////////////////////////////////////////////////////////////////////
	void CFileSystem::Init()
	{
		m_GamePack->Init();
	}

	//==============================================================================
	CFileSystem::FileContainer CFileSystem::ReadFile(const std::string &filename, const std::string &mode)
	{
		FileContainer packResult = m_GamePack->ReadFile(g_EditionHelper->ConvertPath(filename));
		if (packResult.status == FileOk)return packResult;

		FileContainer fc;
		fc.status = FileOk;
		fc.data = NULL;
		fc.size = 0;

		std::wstring name = g_System->GetAppPath() + pugi::as_wide(g_EditionHelper->ConvertPath(filename));
		FILE* file = _wfopen(name.c_str(), pugi::as_wide(mode).c_str());
		if (!file)
		{
			fc.status = FileNotFound;
			return fc;
		}
		fseek(file, 0, SEEK_END);
		fc.size = ftell(file);
		fseek(file, 0, SEEK_SET);
		char *buff = new char[fc.size];
		fread(buff, sizeof(char), fc.size, file);
		fclose(file);

		fc.data = buff;
		return fc;
	}

	//==============================================================================
	FILE * CFileSystem::OpenFile(const std::string &filename, const std::string &mode)
	{
		std::string fileCorrect = g_EditionHelper->ConvertPath(filename);
		FILE* file = fopen(fileCorrect.c_str(), mode.c_str());
		return file;
	}

	//==============================================================================
	FILE * CFileSystem::OpenFile(const std::wstring &filename, const std::wstring &mode)
	{		
		FILE* file = _wfopen(filename.c_str(), mode.c_str());
		return file;
	}

	//==============================================================================
	void CFileSystem::SaveFile(const std::string &filename, const void * data, const int size)
	{
		auto res = g_System->GetAppPath() + pugi::as_wide(filename);
		FILE* file = _wfopen(res.c_str(), L"wb");
		if (!file)
		{
			return;
		}
		fwrite(data, size, 1, file);
		fclose(file);
	}

	//==============================================================================
	void CFileSystem::SaveFile(const std::wstring &filename, const void * data, const int size)
	{
		auto res = filename;
		FILE* file = _wfopen(res.c_str(), L"wb");
		if (!file)
		{
			return;
		}
		fwrite(data, size, 1, file);
		fclose(file);
	}

	//==============================================================================
	bool CFileSystem::DeleteFile(const std::string &filename)
	{
		return (bool)::DeleteFileA(filename.c_str());
	}

	//==============================================================================
	bool CFileSystem::DeleteFile(const std::wstring &filename)
	{
		return (bool)::DeleteFileW(filename.c_str());
	}

	//==============================================================================
	std::wstring CFileSystem::GetSaveDir()
	{
		wchar_t data[256];
		SHGetSpecialFolderPath(0, &data[0], CSIDL_APPDATA, false);
		return std::wstring(data);
	}

	//==============================================================================	
	bool CFileSystem::IsFileExists(const std::string &filename)
	{
		auto fileFromPack = m_GamePack->ReadFile(g_EditionHelper->ConvertPath(filename));
		if (fileFromPack.status == FileOk)
		{
			return true;
		}
		std::wstring fileCorrect = g_System->GetAppPath() + pugi::as_wide(g_EditionHelper->ConvertPath(filename));
		FILE* file = _wfopen(fileCorrect.c_str(), L"r");
		if (!file)
		{
			return false;
		}
		fclose(file);
		return true;
	}	

	//==============================================================================
	bool CFileSystem::IsDirExists(const std::string &filename)
	{
		std::vector<std::string> dirContents;
		dirContents = m_GamePack->GetDirectoryContents(filename);
		if (dirContents.size() > 0)return true;

		std::wstring name = g_System->GetAppPath() + pugi::as_wide(filename);
		DWORD ftyp = GetFileAttributesW(name.c_str());
		if (ftyp == INVALID_FILE_ATTRIBUTES)
			return false;  //something is wrong with your path!
		if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
			return true;   // this is a directory!
		return false;    // this is not a directory!
	}

	//==============================================================================
	bool CFileSystem::IsDirExists(const std::wstring &filename)
	{	
		DWORD ftyp = GetFileAttributesW(filename.c_str());
		if (ftyp == INVALID_FILE_ATTRIBUTES)
			return false;  //something is wrong with your path!
		if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
			return true;   // this is a directory!
		return false;    // this is not a directory!
	}

	

	//==============================================================================
	bool CFileSystem::CreateDirectory(const std::string &path)
	{
		int res = ::CreateDirectoryA(path.c_str(), 0);
		if (res == 0)
		{
			return false;
		}
		return true;
	}

	//==============================================================================
	bool CFileSystem::CreateDirectory(const std::wstring &path)
	{
		int res = ::CreateDirectoryW(path.c_str(), 0);
		if (res == 0)
		{
			return false;
		}
		return true;
	}

	//==============================================================================
	std::wstring CFileSystem::GetCurrentDirectory()
	{
		wchar_t str[255];
		::GetCurrentDirectoryW(255, &str[0]);
		return std::wstring(str);
	}

	//==============================================================================
	bool CFileSystem::SetCurrentDirectory(std::wstring dir)
	{
		return (bool)::SetCurrentDirectoryW(dir.c_str());
	}

	//==============================================================================
	std::wstring CFileSystem::GetProcessDirectory()
	{
		wchar_t str[255];
		GetModuleFileNameW(NULL, &str[0], 255);
		return std::wstring(str);
	}

	//==============================================================================
	std::vector<std::string> CFileSystem::GetDirectoryContents(const std::string &path)
	{
		std::vector<std::string> dirContents;
		dirContents = m_GamePack->GetDirectoryContents(path);
		if (dirContents.size() > 0)return dirContents;

		WIN32_FIND_DATAW FindFileData;
		HANDLE hFind;
		//std::wstring curDir = GetCurrentDirectory();
		std::wstring name = g_System->GetAppPath() + pugi::as_wide(path) + L"\\*";
		/*if(!::SetCurrentDirectoryA(name.c_str()))
		{
		CONSOLE("Error: Not a directory - %s", path.c_str());
		return dirContents;
		}*/
		hFind = FindFirstFileW(name.c_str(), &FindFileData);
		if (hFind == INVALID_HANDLE_VALUE)
		{
			///Invalid file handle
			//SetCurrentDirectory(curDir);
			return dirContents;
		}
		else
		{
			while (FindNextFileW(hFind, &FindFileData))
			{
				std::wstring fileName = FindFileData.cFileName;
				if (fileName == L"." || fileName == L"..")continue;
				///File found
				dirContents.push_back(pugi::as_utf8(FindFileData.cFileName));
			}
			FindClose(hFind);

			if (GetLastError() == ERROR_NO_MORE_FILES)
			{
				///Search finished
			}
			else
			{
				///Unknown error
				//SetCurrentDirectory(curDir);
				return dirContents;
			}
		}
		//SetCurrentDirectory(curDir);
		return dirContents;
	}

	//////////////////////////////////////////////////////////////////////////	
	bool CFileSystem::CopyLocalFile(const std::string &filename, const std::wstring &destination)
	{
		std::wstring from = g_System->GetAppPath() + pugi::as_wide(g_EditionHelper->ConvertPath(filename));

		std::ifstream  src(from, std::ios::binary);
		std::ofstream  dst(destination, std::ios::binary);


		if (src.is_open() && dst.is_open())
		{
			dst << src.rdbuf();
		}
		else			
		{
			return false;
		}
		src.close();
		dst.close();
		return true;
	}
}
#endif
