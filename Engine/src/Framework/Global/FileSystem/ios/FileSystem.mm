#ifdef NBG_IOS
#include <Framework.h>

#import <CoreFoundation/CoreFoundation.h>
#import <Foundation/Foundation.h>

#include "FileSystem.h"
#include <stdio.h>


namespace NBG
{
    CFileSystem::CFileSystem()
    {
        static char path[1024];
        CFBundleRef mainBundle = CFBundleGetMainBundle();
        assert(mainBundle);
        
        CFURLRef mainBundleURL = CFBundleCopyBundleURL(mainBundle);
        assert(mainBundleURL);
        
        CFStringRef cfStringRef = CFURLCopyFileSystemPath( mainBundleURL, kCFURLPOSIXPathStyle);
        assert(cfStringRef);
        
        CFStringGetCString(cfStringRef, path, 1024, kCFStringEncodingASCII);
        CFRelease(mainBundleURL);
        CFRelease(cfStringRef);
        path[1023] = 0;
        
        strcat( path, "/" );
        
        m_AppDirectory = path;
        m_AppDirectory += "ResourcesIOS/data/";
    }
    
    CFileSystem::~CFileSystem()
    {
    }
    
    CFileSystem::FileContainer CFileSystem::ReadFile(const std::string &filename, const std::string &mode)
    {
        FileContainer fc;
        fc.status = FileOk;
        fc.data = NULL;
        fc.size = 0;
        
        std::string path= g_EditionHelper->ConvertPath(filename);
        if (filename.substr(0,1)=="/")path=filename;
        
        FILE* file = fopen(path.c_str(),mode.c_str());
        if (!file)
        {
            fc.status = FileNotFound;
            return fc;
        }
        fseek(file,0,SEEK_END);
        fc.size = ftell(file);
        fseek(file,0,SEEK_SET);
        char *buff = new char[fc.size];
        fread(buff,sizeof(char),fc.size,file);
        fclose(file);
        
        fc.data = buff;
        return fc;
    }
    
    FILE * CFileSystem::OpenFile(const std::string &filename, const std::string &mode)
    {
        std::string path= filename;
        if (filename.substr(0,1)=="/")path=filename;
        FILE* file = fopen(path.c_str(),mode.c_str());
        return file;
    }
    
    //==============================================================================
    FILE * CFileSystem::OpenFile(const std::wstring &filename, const std::wstring &mode)
    {
        FILE* file = fopen(pugi::as_utf8(filename.c_str()).c_str(), pugi::as_utf8(mode.c_str()).c_str());
        return file;
    }
    
    void CFileSystem::SaveFile(const std::string &filename, const void * data, const int size)
    {
        FILE* file = fopen(filename.c_str(),"wb");
        if (!file)
        {
            return;
        }
        fwrite(data,size,1,file);
        fclose(file);
    }
    
    //==============================================================================
    void CFileSystem::SaveFile(const std::wstring &filename, const void * data, const int size)
    {
        auto res = filename;
        auto s = pugi::as_utf8(res.c_str());
        FILE* file = fopen(s.c_str(), "wb");
        if (!file)
        {
            return;
        }
        fwrite(data, size, 1, file);
        fclose(file);
    }
    
    
    
    
    ///—Û˘ÂÒÚ‚ÛÂÚ ÎË Ù‡ÈÎ
    bool CFileSystem::IsFileExists(const std::string &filename)
    {
        
        std::string path= filename;
        if (filename.substr(0,1)=="/")path=filename;
        NSString *errorMessage = [NSString stringWithCString:path.c_str()
                                                    encoding:[NSString defaultCStringEncoding]];
        return [[NSFileManager defaultManager] fileExistsAtPath:errorMessage];
        FILE* file = fopen(filename.c_str(),"r");
        if (!file)
        {
            return false;
        }
        fclose(file);
        return true;
    }
    
    ///—Û˘ÂÒÚ‚ÛÂÚ ÎË ‰ËÂÍÚÓËˇ
    bool CFileSystem::IsDirExists(const std::string &filename)
    {
        std::string path= filename;
        if (filename.substr(0,1)=="/")path=filename;
        NSString *errorMessage = [NSString stringWithCString:path.c_str()
                                                    encoding:[NSString defaultCStringEncoding]];
        return [[NSFileManager defaultManager] fileExistsAtPath:errorMessage];
    }
    
    std::wstring CFileSystem::GetSaveDir()
    {
        static std::string buffer;
        NSString* ns_path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@""];
        buffer = [ns_path UTF8String];
        return pugi::as_wide(buffer);
    }
    
    
    bool CFileSystem::CreateDirectory(const std::string &path)
    {
        NSError* ptrError = nil;
        BOOL returnValue = [[NSFileManager defaultManager] createDirectoryAtPath:[NSString stringWithUTF8String:path.c_str()] withIntermediateDirectories:YES attributes:nil error:&ptrError];
        return returnValue;
    }
    
    std::wstring CFileSystem::GetCurrentDirectory()
    {
        static char cwd[1024];
        getcwd( cwd, sizeof( cwd ) - 1 );
        cwd[1023] = 0;
        return pugi::as_wide(cwd);
    }
    
    bool CFileSystem::SetCurrentDirectory(std::wstring dir)
    {
        return false;
    }
    
    std::vector<std::string> CFileSystem::GetDirectoryContents(const std::string &path)
    {
        std::vector<std::string> dirContents;
        
        NSString *nsPath = [NSString stringWithCString:path.c_str()
                                              encoding:[NSString defaultCStringEncoding]];
        
        NSFileManager *manager = [NSFileManager defaultManager];
        NSDirectoryEnumerator *direnum = [manager enumeratorAtPath:nsPath];
        NSString *filename;
        
        while ((filename = [direnum nextObject] )) {
            
            std::string bar([filename UTF8String]);
            dirContents.push_back(bar);
            
        }
        return dirContents;
    }
    
    std::wstring CFileSystem::GetProcessDirectory()
    {
        //return pugi::as_wide(SDL_GetBasePath());
        return L"";
    }
}
#endif
