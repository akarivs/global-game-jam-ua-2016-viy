#ifndef NBG_FRAMEWORK_GLOBAL_RENDER
#define NBG_FRAMEWORK_GLOBAL_RENDER

enum class RenderChangeType
{
	Before,
	After
};

#ifdef NBG_WIN32
	#include "Render/win32/Render.h"
	#include "Render/win32/RenderTarget.h"
#elif defined(NBG_OSX)
	#include "Render/osx/Render.h"
#elif defined(NBG_IOS)
    #include "Render/ios/Render.h"
#elif defined(NBG_ANDROID)
    #include "Render/android/Render.h"
#endif

#endif // NBG_FRAMEWORK_GLOBAL_RENDER
