#include "ResourcesManager.h"

#include <Framework.h>

#include <Framework/Global/Resources/TextureResource.h>
#include <Framework/Global/Resources/XMLResource.h>
#include <Framework/Global/Resources/OGGResource.h>
#include <Framework/Utils/StringUtils.h>
#include <Framework/Datatypes.h>

#include <algorithm>

namespace NBG
{
	//////////////////////////////////////////////////////////////////////////
	CResourcesManager::CResourcesManager()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	CResourcesManager::~CResourcesManager()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CResourcesManager::Init()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	ResourceType CResourcesManager::GetTypeByExtension(const std::string file_name)
	{
		ResourceType rc = RS_UNDEFINED;
		std::string ext = file_name;
		int pos = ext.rfind(".");
		if (pos != std::string::npos)
		{
			ext = ext.substr(pos+1);
			if (ext == "png" || ext == "jpg" || ext == "bmp" || ext == "dds")
			{
				rc = RS_TEXTURE;
			}
			else if (ext == "xml")
			{
				rc = RS_XML;
			}
			else if (ext == "ogg")
			{
				rc = RS_SOUND;
			}
		}
		return rc;
	}

	//////////////////////////////////////////////////////////////////////////
	IResource* CResourcesManager::GetResource(const std::string file_name)
	{
		std::string id = file_name;
		if (m_Resources.find(id) == m_Resources.end())
		{
			IResource * res = NULL;
			ResourceType rs = GetTypeByExtension(file_name);
			if (rs == RS_TEXTURE)
			{
				res = new CTextureResource();
			}
			else if (rs == RS_XML)
			{
				res = new CXMLResource();
			}
			else if (rs == RS_SOUND)
			{
				res = new COGGResource();

			}
			if (!res)
			{
				g_Log->WriteLineError("Resouce type don't registered - " + file_name);				
				return NULL;
			}

			if (!res->Load(id))
			{				
				g_Log->WriteLineError("No such file - " + file_name);				
			}
			m_Resources[id] = res;
			res->IncreaseRefsCount();			
			return m_Resources[id];
		}
		else
		{
			m_Resources[id]->IncreaseRefsCount();			
			return m_Resources[id];
		}		
	}

	//////////////////////////////////////////////////////////////////////////
	void CResourcesManager::AddResource(const std::string &name, IResource * resource)
	{
		m_Resources[name] = resource;
	}

	//////////////////////////////////////////////////////////////////////////
	void CResourcesManager::ReleaseResource(const std::string id)
	{
		if (m_Resources.find(id) != m_Resources.end())
		{
			m_Resources[id]->DecreaseRefsCount();            
		}
		else
		{
			g_Log->WriteLineError("Resource not found! - " + id);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CResourcesManager::ReleaseResource(IResource * resource)
	{
		resource->DecreaseRefsCount();
	}

	//////////////////////////////////////////////////////////////////////////
	void CResourcesManager::ReleaseUnusedResources()
	{
		std::map<std::string, IResource*>resourcesToDelete;
		m_ResourcesIter = m_Resources.begin();
		while (m_ResourcesIter != m_Resources.end())
		{
			IResource * res = m_ResourcesIter->second;
			if (res->GetRefsCount() == 0)
			{
				resourcesToDelete[m_ResourcesIter->first] = m_ResourcesIter->second;
			}
			m_ResourcesIter++;
		}
		for (m_ResourcesIter=resourcesToDelete.begin(); m_ResourcesIter!=resourcesToDelete.end(); m_ResourcesIter++)
		{			
			delete m_ResourcesIter->second;
			m_Resources.erase(m_ResourcesIter->first);
		}
	}
	
	struct SP
	{
		int size;
		std::string name;
		int refs;
	};
	
	bool sortObj(const SP a, const SP b)
	{
		return a.size<b.size;
	}
	//////////////////////////////////////////////////////////////////////////
	void CResourcesManager::DumpResources()
	{
		
		std::vector<SP>sizes;
		m_ResourcesIter = m_Resources.begin();
		int totalSize = 0;
		while (m_ResourcesIter != m_Resources.end())
		{
			SP sp;
			IResource * res = m_ResourcesIter->second;			
			int size = (int)((float)res->GetSize()/1024.0f);			
			if (GetTypeByExtension(res->GetPath()) == RS_TEXTURE)
			{
				CTextureResource * tes = CAST(CTextureResource*, res);
				size = tes->GetTextureWidth()*tes->GetTextureHeight()*4 / 1024;
			}			
			sp.name = res->GetPath();
			sp.size = size;
			sp.refs = res->GetRefsCount();
			sizes.push_back(sp);
			totalSize += size;
			m_ResourcesIter++;
		}

		std::sort(sizes.begin(), sizes.end(), sortObj);
		for (int i=0; i<sizes.size(); i++)
		{
			auto &sz = sizes[i];
			g_Log->WriteLine("Resource: " + sz.name);
			g_Log->WriteLine("Size: " + StringUtils::ToString(sz.size) + "kb. Refs: " + StringUtils::ToString(sz.refs));
		}
		
		g_Log->WriteLineInfo("-------------------------------------------");
		g_Log->WriteLineInfo("Total size: " + StringUtils::ToString((int)((float)totalSize / 1024.0f)) + "mb");
	}

	//////////////////////////////////////////////////////////////////////////
	void CResourcesManager::ReloadTextures()
	{
		m_ResourcesIter = m_Resources.begin();
		while (m_ResourcesIter != m_Resources.end())
		{
			auto res = m_ResourcesIter->second;
			if (GetTypeByExtension(res->GetPath()) == RS_TEXTURE)
			{
				auto tex = CAST(CTextureResource*,res);
				tex->Load(tex->GetPath());
			}			
			m_ResourcesIter++;
		}		
	}

	//////////////////////////////////////////////////////////////////////////
	void CResourcesManager::FreeAllResources()
	{
		m_Resources.clear();		
	}
}
