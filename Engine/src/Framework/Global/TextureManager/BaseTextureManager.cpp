#include "BaseTextureManager.h"
//#include <cstring>

extern "C" {
	#include <Framework/../../External/libjpeg/jpeglib.h>
}

#define STB_IMAGE_IMPLEMENTATION
#include <../External/stb_image.h>

namespace NBG
{
	namespace PNG_HELPER
	{
		//==========================================================================
		void PNGRead(png_structp pngStruct, png_bytep data, png_size_t length)
		{
			PNGBuffer* pngBuffer = (PNGBuffer*)(png_get_io_ptr(pngStruct));
			memcpy(data, pngBuffer->data + pngBuffer->position, length);
			pngBuffer->position += length;
		}

		//==========================================================================
		Image PNGDecode(png_bytep data, const bool SwapBGRA)
		{
			png_structp pngStruct = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
			png_infop   pngInfo = png_create_info_struct(pngStruct);
			PNGBuffer   pngBuffer = { data, 8 };

			png_set_read_fn(pngStruct, (void*)&pngBuffer, PNGRead);
			png_set_sig_bytes(pngStruct, 8);
			png_read_info(pngStruct, pngInfo);

			Image image;
			png_get_IHDR(pngStruct, pngInfo, &image.width, &image.height, &image.bitDepth, &image.colorType, 0, 0, 0);

			// преобразование цвета
			if (image.bitDepth == 16) png_set_strip_16(pngStruct);
			switch (image.colorType)
			{
			case PNG_COLOR_TYPE_GRAY:
				if (image.bitDepth < 8) png_set_expand_gray_1_2_4_to_8(pngStruct);
				png_set_gray_to_rgb(pngStruct);
				break;

			case PNG_COLOR_TYPE_GRAY_ALPHA:
				png_set_gray_to_rgb(pngStruct);
				break;

			case PNG_COLOR_TYPE_PALETTE:
				png_set_palette_to_rgb(pngStruct);
				break;
			}
			if (png_get_valid(pngStruct, pngInfo, PNG_INFO_tRNS) != 0) png_set_tRNS_to_alpha(pngStruct);

			png_read_update_info(pngStruct, pngInfo);
			if (SwapBGRA)
			{
				png_set_bgr(pngStruct);
			}
			png_get_IHDR(pngStruct, pngInfo, &image.width, &image.height, &image.bitDepth, &image.colorType, 0, 0, 0);

			// чтение изображения
			png_uint_32 bytesInRow = png_get_rowbytes(pngStruct, pngInfo);
			png_bytepp rowPointer = new png_bytep[image.height];
			image.data = new png_byte[bytesInRow * image.height];

			for (png_uint_32 i = 0; i < image.height; ++i) rowPointer[i] = image.data + i * bytesInRow;
			png_read_image(pngStruct, rowPointer);

			png_destroy_read_struct(&pngStruct, &pngInfo, 0);
			delete[] rowPointer;
			return image;
		}
	}
	namespace JPG_HELPER
	{
		const static JOCTET EOI_BUFFER[1] = { JPEG_EOI };
		struct my_source_mgr {
			struct jpeg_source_mgr pub;
			const JOCTET *data;
			size_t       len;
		};

		static void my_init_source(j_decompress_ptr cinfo) {}

		static boolean my_fill_input_buffer(j_decompress_ptr cinfo) {
			my_source_mgr* src = (my_source_mgr*)cinfo->src;
			// No more data.  Probably an incomplete image;  just output EOI.
			src->pub.next_input_byte = EOI_BUFFER;
			src->pub.bytes_in_buffer = 1;
			return TRUE;
		}
		static void my_skip_input_data(j_decompress_ptr cinfo, long num_bytes) {
			my_source_mgr* src = (my_source_mgr*)cinfo->src;
			if (src->pub.bytes_in_buffer < num_bytes) {
				// Skipping over all of remaining data;  output EOI.
				src->pub.next_input_byte = EOI_BUFFER;
				src->pub.bytes_in_buffer = 1;
			}
			else {
				// Skipping over only some of the remaining data.
				src->pub.next_input_byte += num_bytes;
				src->pub.bytes_in_buffer -= num_bytes;
			}
		}
		static void my_term_source(j_decompress_ptr cinfo) {}

		static void my_set_source_mgr(j_decompress_ptr cinfo, const char* data, size_t len) {
			my_source_mgr* src;
			if (cinfo->src == 0) { // if this is first time;  allocate memory
				cinfo->src = (struct jpeg_source_mgr *)(*cinfo->mem->alloc_small)
					((j_common_ptr)cinfo, JPOOL_PERMANENT, sizeof(my_source_mgr));
			}
			src = (my_source_mgr*)cinfo->src;
			src->pub.init_source = my_init_source;
			src->pub.fill_input_buffer = my_fill_input_buffer;
			src->pub.skip_input_data = my_skip_input_data;
			src->pub.resync_to_restart = jpeg_resync_to_restart; // default
			src->pub.term_source = my_term_source;
			// fill the buffers
			src->data = (const JOCTET *)data;
			src->len = len;
			src->pub.bytes_in_buffer = len;
			src->pub.next_input_byte = src->data;
		}

		struct my_error_mgr {
			struct jpeg_error_mgr pub;	/* "public" fields */

			jmp_buf setjmp_buffer;	/* for return to caller */
		};

		typedef struct my_error_mgr * my_error_ptr;

		/*
		* Here's the routine that will replace the standard error_exit method:
		*/

		METHODDEF(void)
			my_error_exit(j_common_ptr cinfo)
		{
			/* cinfo->err really points to a my_error_mgr struct, so coerce pointer */
			my_error_ptr myerr = (my_error_ptr)cinfo->err;

			/* Always display the message. */
			/* We could postpone this until after returning, if we chose. */
			(*cinfo->err->output_message) (cinfo);

			/* Return control to the setjmp point */
			longjmp(myerr->setjmp_buffer, 1);
		}
//ф-ция определяющая размер картинки который подойдет для OpenGL
static int reNpot(int w) {
    //поддерживает ли OpenGL текстуры размера не кратным двум
    //эту переменную конечно надо определять один раз при старте проги с помощью
    //String s = gl.glGetString(GL10.GL_EXTENSIONS);
    //NON_POWER_OF_TWO_SUPPORTED = s.contains("texture_2D_limited_npot") || s.contains("texture_npot") || s.contains("texture_non_power_of_two");
    bool NON_POWER_OF_TWO_SUPPORTED = false;
    if (NON_POWER_OF_TWO_SUPPORTED) {
        if (w % 2) w++;
    } else {
        if (w <= 4) w = 4;
        else if (w <= 8) w = 8;
        else if (w <= 16) w = 16;
        else if (w <= 32) w = 32;
        else if (w <= 64) w = 64;
        else if (w <= 128) w = 128;
        else if (w <= 256) w = 256;
        else if (w <= 512) w = 512;
        else if (w <= 1024) w = 1024;
        else if (w <= 2048) w = 2048;
        else if (w <= 4096) w = 4096;
    }
    return w;
}

		//==========================================================================
		Image Decode(char * data, size_t len)
		{
			struct jpeg_decompress_struct cinfo;
			struct my_error_mgr jerr;
			cinfo.err = jpeg_std_error(&jerr.pub);
			/* Now we can initialize the JPEG decompression object. */
			jpeg_create_decompress(&cinfo);
			my_set_source_mgr(&cinfo, data, len);

			// read info from header.
			jpeg_read_header(&cinfo, TRUE);
			jpeg_start_decompress(&cinfo);

			Image img;
			img.width = cinfo.image_width;
			img.height = cinfo.image_height;
			img.glWidth = reNpot(cinfo.image_width);
			img.glHeight = reNpot(cinfo.image_height);

			int row = img.glWidth * 3;
			img.buffer = new unsigned char[row * img.glHeight];
         
			//построчно читаем данные
		    unsigned char* line = (unsigned char*) (img.buffer);
		    while (cinfo.output_scanline < cinfo.output_height) {
		        jpeg_read_scanlines(&cinfo, &line, 1);
		        line += row;
		    }


			jpeg_finish_decompress(&cinfo);
			jpeg_destroy_decompress(&cinfo);
			return img;
		}
	}
}