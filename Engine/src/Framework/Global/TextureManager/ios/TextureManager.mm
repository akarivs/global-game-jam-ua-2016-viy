#ifdef NBG_IOS
#include "TextureManager.h"
#include <Framework.h>
#include <stdlib.h>

#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/EAGLDrawable.h>
#include <OpenGLES/ES2/gl.h>

#import <GLKit/GLKit.h>

#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>

namespace NBG
{
	CTextureManager::CTextureManager()
	{
        m_DefaultTexture = NULL;
	}
    
    void CTextureManager::InitDefaultTexture()
    {
        m_DefaultTexture = new Texture();
        std::vector<unsigned char> textureData;
        
        int w = 32;
        int h = 32;
		
		m_DefaultTexture->SetSize(w,h);
		      
        GLuint _textureId;
        // Bind the texture
        
        glActiveTexture(GL_TEXTURE0);
        glGenTextures(1, &_textureId);
        // Bind the texture object
        glBindTexture(GL_TEXTURE_2D, _textureId);
        
        char * data = new char[w*h*4];
        memset(data,w*h*4,255);
        // Load the texture
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA,
                     GL_UNSIGNED_BYTE, data);
        
        // Set the filtering mode
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
       
        m_DefaultTexture->SetTextureData(new GLuint(_textureId));
		textureData.clear();
        delete[] data;
        
    }

    void
    sTexture_SizeToPow2(int &width, int &height)
    {
        int w = 1, h = 1;
        
        while (w < width) {w = w << 1;}
        while (h < height) {h = h << 1;}
        
        width = w;
        height = h;
    }
	bool CTextureManager::LoadTexture(CTextureResource * res)
	{
        std::vector<unsigned char> textureData;
        char * data = NULL;
        unsigned int w = 0;
        unsigned int h = 0;
        int w2 = 0;
        int h2 = 0;
        NSData *imageD = NULL;
        UIImage* image = NULL;

        
        
		std::string extension = NBG::StringUtils::GetExtension(res->GetPath());
        if (extension == "png")
        {
            PNG_HELPER::Image image = PNG_HELPER::PNGDecode((png_bytep)(res->GetData()), false);
			if( image.colorType == PNG_COLOR_TYPE_RGB )
			{
                CONSOLE("This is PNG without alpha channel, fix it! %s",res->GetPath().c_str());
                res->SetTexture(m_DefaultTexture);
                res->SetTextureWidth(m_DefaultTexture->GetSize().x);
                res->SetTextureHeight(m_DefaultTexture->GetSize().y);
                delete[] image.data;
                return false;
			}
			w = image.width;
			h = image.height;
            res->SetTextureWidth(w);
            res->SetTextureHeight(h);
            data = (char*)image.data;
        }
        else if (extension == "jpg")
        {
            auto img = JPG_HELPER::Decode(res->GetData(), res->GetSize());
            w = img.glWidth;
            h = img.glHeight;
            w2 = img.width;
            h2 = img.height;
            res->SetTextureWidth(w);
            res->SetTextureHeight(h);
            data = (char*)img.buffer;
            
        }
        
		
		
        
        GLuint _textureId;
        // Bind the texture
        
        glActiveTexture(GL_TEXTURE0);
        glGenTextures(1, &_textureId);
        // Bind the texture object
        glBindTexture(GL_TEXTURE_2D, _textureId);
        
        
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );        
       
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        
        if (extension == "jpg")
        {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB,
                         GL_UNSIGNED_BYTE, data);
        }
        else
        {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            
            
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA,
                         GL_UNSIGNED_BYTE, data);
        }
        
        
        
                        
        Texture * _texture = new Texture();
        _texture->SetSize(w,h);
        _texture->SetTextureData(new GLuint(_textureId));
        if (extension == "jpg")
        {
            _texture->SetSize(w2,h2);
            free(data);
        }
        else
        {
            _texture->SetSize(w,h);
            delete[] data;
        }
        res->SetTexture(_texture);       	return true;
 
	}
    
    void CTextureManager::ReleaseTexture(NBG::Texture *texture)
    {
        glDeleteTextures(1,(GLuint*)texture->GetTextureData());
    }
    
    void CTextureManager::SetRepeatTexture(Texture * texture)
    {
        GLuint * textureData = (GLuint*)texture->GetTextureData();
        glBindTexture(GL_TEXTURE_2D, *textureData);
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }
    
    
}
#endif //NBG_WINDOWS
