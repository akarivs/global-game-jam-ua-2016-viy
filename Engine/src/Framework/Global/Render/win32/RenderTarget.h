#ifndef NBG_FRAMEWORK_GLOBAL_RENDER_TARGET_WIN32
#define NBG_FRAMEWORK_GLOBAL_RENDER_TARGET_WIN32

#ifdef NBG_WIN32
#include <Framework/Datatypes/Color.h>
#include <d3d9.h>
#include <d3dx9.h>


#include <string>
#include <ostream>

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Mesh.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/IntVector.h>

#include <Framework/Global/Resources/TextureResource.h>


namespace NBG
{
	/** @brief Класс для рендера в текстуру
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2015 New Bridge Games
	*
	*/
	class CRenderTarget
	{
	public:    
		CRenderTarget(const int w, const int h);
		~CRenderTarget();

		void Begin();
		void End();

		void Reset();
		CTextureResource * GetResource()
		{
			return m_TextureRes;
		}
	private:
		IntVector m_Size;
		CTextureResource * m_TextureRes;
		Texture * m_Texture;
		IDirect3DTexture9 * m_TextureD3D;
		IDirect3DSurface9* m_Surface;
		IDirect3DSurface9* m_OrigTarget;

	};
}
#endif
#endif // NBG_FRAMEWORK_GLOBAL_RENDER_WIN32
