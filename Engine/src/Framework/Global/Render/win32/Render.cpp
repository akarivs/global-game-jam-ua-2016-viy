#ifdef NBG_WIN32
#include "Render.h"

#include <DxErr.h>
#include <Framework.h>

namespace NBG
{	
	//////////////////////////////////////////////////////////////////////////
	CRender::CRender()
	{
		m_PrevAction = RA_None;
		m_BlendMode = BlendMode::BLEND;

		m_MatrixOffset.x = m_MatrixOffset.y = 0.0f;
		m_LineMesh.Init(4, 6);
		unsigned short * indexes = m_LineMesh.GetIndexes();
		indexes[0] = 0;
		indexes[1] = 1;
		indexes[2] = 2;
		indexes[3] = 3;
		indexes[4] = 2;
		indexes[5] = 1;

		m_VertexBuffer = NULL;
		m_IndexBuffer = NULL;
		m_IsTextureRepeat = false;
		m_Device = NULL;
	}

	//////////////////////////////////////////////////////////////////////////
	CRender::~CRender()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::Init()
	{
		m_D3D = Direct3DCreate9(D3D_SDK_VERSION);
		if (m_D3D == NULL)
		{			
			CONSOLE("Could not create Direct3D Object");
			NBG_Assert(0, "Could not create Direct3D Object");
			return;
		}
		m_PrevTexture = NULL;
		m_Device = NULL;

		m_MatrixView = new D3DXMATRIX();
		m_MatrixProjection = new D3DXMATRIX();
		m_MatrixWorld = new D3DXMATRIX();
		Reset();

		
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::Reset()
	{		
		g_GameApplication->OnBeforeRenderChange();
		if (m_VertexBuffer)
		{
			m_VertexBuffer->Release();
			m_VertexBuffer = 0;
		}

		if (m_IndexBuffer)
		{
			m_IndexBuffer->Release();
			m_IndexBuffer = 0;
		}
		Vector resolution = g_System->GetSystemResolution();

		D3DDISPLAYMODE d3ddm;
		m_D3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &d3ddm);


		D3DPRESENT_PARAMETERS   present_parameters;
		ZeroMemory(&present_parameters, sizeof(present_parameters));
		present_parameters.Windowed = !g_GameApplication->IsFullscreen();

		present_parameters.Windowed = true;

		if (present_parameters.Windowed)
		{
			present_parameters.PresentationInterval = D3DPRESENT_INTERVAL_ONE;
			present_parameters.SwapEffect = D3DSWAPEFFECT_DISCARD;
		}
		else
		{
			present_parameters.PresentationInterval = D3DPRESENT_INTERVAL_ONE;
			present_parameters.SwapEffect = D3DSWAPEFFECT_DISCARD;			
		}

		if (g_GameApplication->IsFullscreen())
		{
			resolution.x = GetSystemMetrics(SM_CXSCREEN);
			resolution.y = GetSystemMetrics(SM_CYSCREEN);
		}

		present_parameters.EnableAutoDepthStencil = true;
		present_parameters.AutoDepthStencilFormat = D3DFMT_D24S8;

		COperatingSystem * op = CAST(COperatingSystem*, g_GameApplication->GetOperatingSystem());

		DWORD  msqAAQuality = 0; // Non-maskable quality

		present_parameters.hDeviceWindow = op->GetWindow();
		present_parameters.BackBufferWidth = resolution.x;
		present_parameters.BackBufferHeight = resolution.y;
		present_parameters.BackBufferFormat = d3ddm.Format;		

		present_parameters.MultiSampleType = D3DMULTISAMPLE_NONE;

		/*
		// Set highest quality non-maskable AA available or none if not
		if (SUCCEEDED(m_D3D->CheckDeviceMultiSampleType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, D3DFMT_A8R8G8B8, TRUE, D3DMULTISAMPLE_NONMASKABLE, &msqAAQuality)))
		{
			// Set AA quality
			present_parameters.MultiSampleType = D3DMULTISAMPLE_NONMASKABLE;
			present_parameters.MultiSampleQuality = msqAAQuality - 1;
		}
		else
		{
			// No AA
			present_parameters.MultiSampleType = D3DMULTISAMPLE_NONE;
		}
		*/

		if (m_Device)
		{
			HRESULT hr = m_Device->Reset(&present_parameters);
			hr = m_Device->Reset(&present_parameters);
#ifdef NBG_DEBUG
			if (FAILED(hr)) 
			{
				fprintf(stderr, "Error: %s error description: %s\n",
					DXGetErrorStringA(hr), DXGetErrorDescriptionA(hr));
			}
#endif
		}
		else
		{
			HRESULT hr;
			DWORD m_d3d_dev_flag = 0;
			m_d3d_dev_flag |= D3DCREATE_HARDWARE_VERTEXPROCESSING;
			m_d3d_dev_flag |= D3DCREATE_MULTITHREADED;

			_D3DDEVTYPE m_d3d_dev_type = D3DDEVTYPE_HAL;

			g_Log->WriteLineInfo("Trying to create HARDWARE HAL d3d9");
			//CONSOLE("Trying to create HARDWARE HAL d3d9");
			hr = m_D3D->CreateDevice(D3DADAPTER_DEFAULT, m_d3d_dev_type, GetActiveWindow(), m_d3d_dev_flag, &present_parameters, &m_Device);

			if (FAILED(hr))
			{
				CONSOLE("Trying to create SOFTWARE HAL d3d9");
				m_d3d_dev_type = D3DDEVTYPE_HAL;
				m_d3d_dev_flag &= ~D3DCREATE_HARDWARE_VERTEXPROCESSING;
				m_d3d_dev_flag |= D3DCREATE_SOFTWARE_VERTEXPROCESSING;
				hr = m_D3D->CreateDevice(D3DADAPTER_DEFAULT, m_d3d_dev_type, GetActiveWindow(), m_d3d_dev_flag, &present_parameters, &m_Device);				
			}

			if (FAILED(hr))
			{
				CONSOLE("Trying to create MIXED HAL d3d9"); 
				m_d3d_dev_type = D3DDEVTYPE_HAL;
				m_d3d_dev_flag &= ~D3DCREATE_SOFTWARE_VERTEXPROCESSING;
				m_d3d_dev_flag |= D3DCREATE_MIXED_VERTEXPROCESSING;
				hr = m_D3D->CreateDevice(D3DADAPTER_DEFAULT, m_d3d_dev_type, GetActiveWindow(), m_d3d_dev_flag, &present_parameters, &m_Device);

				if (FAILED(hr))
				{
					CONSOLE("Trying to create MIXED REF d3d9");
					m_d3d_dev_type = D3DDEVTYPE_REF;
					hr = m_D3D->CreateDevice(D3DADAPTER_DEFAULT, m_d3d_dev_type, GetActiveWindow(), m_d3d_dev_flag, &present_parameters, &m_Device);
				}
			}

			if (FAILED(hr))
			{

				CONSOLE("Trying to create SOFTWARE REF d3d9");
				m_d3d_dev_flag &= ~D3DCREATE_MIXED_VERTEXPROCESSING;
				m_d3d_dev_flag |= D3DCREATE_SOFTWARE_VERTEXPROCESSING;

				m_d3d_dev_type = D3DDEVTYPE_REF;
				hr = m_D3D->CreateDevice(D3DADAPTER_DEFAULT, m_d3d_dev_type, GetActiveWindow(), m_d3d_dev_flag, &present_parameters, &m_Device);
			}

			if (FAILED(hr))
			{
				CONSOLE_ERROR("Cannot create any type of device!");
				return;
			}
		}

		m_Device->GetDeviceCaps(&m_Caps);

		HRESULT hr = m_Device->CreateVertexBuffer(MAX_OBJECTS*sizeof(Vertex) * 6,
			D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY,
			D3DFVF_CUSTOMVERTEX,
			D3DPOOL_DEFAULT,
			&m_VertexBuffer,
			NULL);
		hr = m_Device->CreateIndexBuffer(MAX_OBJECTS*sizeof(unsigned short) * 6, D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY , D3DFMT_INDEX16, D3DPOOL_DEFAULT, &m_IndexBuffer, NULL);


		D3DXMatrixIdentity(m_MatrixView);
		D3DXMatrixIdentity(m_MatrixWorld);

		Vector gameRes = g_System->GetGameResolution();
		Vector resScale = g_System->GetResolutionScale();
		D3DXMatrixOrthoLH(m_MatrixProjection, resolution.x, resolution.y, 0.0f, 1.0f);
		D3DXMatrixTranslation(m_MatrixWorld, 0.0f, 0.0f, 0.0f);
		m_MatrixOffset.y = (gameRes.y) / resScale.y;
		D3DXMatrixScaling(m_MatrixView, resScale.x, -resScale.y, 1.0f);

		m_WorldViewProjection = *m_MatrixWorld * *m_MatrixView * *m_MatrixProjection;

		m_Device->SetTransform(D3DTS_VIEW, m_MatrixView);
		m_Device->SetTransform(D3DTS_PROJECTION, m_MatrixProjection);
		m_Device->SetTransform(D3DTS_WORLD, m_MatrixWorld);

		ChangeState(RA_None);
		g_GameApplication->OnAfterRenderChange();

		m_CanDebugDrawCalls = g_GameApplication->IsCheatsEnabled();
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::CheckDevice()
	{
		if (m_Device == NULL)return;
		HRESULT hr = m_Device->TestCooperativeLevel();
		if (hr == D3DERR_DEVICELOST)
		{		
			Sleep(100);
			return;
		}
		else if (hr == D3DERR_DEVICENOTRESET)
		{
			Reset();
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::CreateOpaqueTextureFromTheoraFrame(unsigned char * data, int bpp, Vector size, Texture * texture)
	{
		IDirect3DTexture9* pTex = NULL;
		HRESULT hr = S_OK;
		if (texture->GetTextureData() == NULL)
		{
			hr = m_Device->CreateTexture(
				size.x,
				size.y,
				1,
				0,
				D3DFMT_A8R8G8B8,
				D3DPOOL_MANAGED,
				&pTex,
				NULL);
		}
		else
		{
			pTex = CAST(IDirect3DTexture9*, texture->GetTextureData());
		}

		D3DLOCKED_RECT lockedRect;
		hr = pTex->LockRect(0, &lockedRect, NULL, 0);

		if (SUCCEEDED(hr))
		{
			unsigned char*  src = data;
			unsigned char*  dst = (unsigned char*)lockedRect.pBits;
			int count = size.x * size.y * bpp;

			do
			{
				dst[0] = src[2];
				dst[1] = src[1];
				dst[2] = src[0];
				if (bpp == 4)
					dst[3] = src[3];
				else
					dst[3] = 255;

				dst += 4;
				src += bpp;
			} while (count -= bpp);
			hr = pTex->UnlockRect(0);
		}
		texture->SetTextureData(pTex);
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::CreateTransparentTextureFromTheoraFrame(unsigned char * data, int bpp, Vector size, Texture * texture)
	{
		IDirect3DTexture9* pTex = NULL;
		HRESULT hr = S_OK;
		if (texture->GetTextureData() == NULL)
		{
			hr = m_Device->CreateTexture(
				size.x / 2.0f,
				size.y,
				1,
				0,
				D3DFMT_A8R8G8B8,
				D3DPOOL_MANAGED,
				&pTex,
				NULL);
		}
		else
		{
			pTex = CAST(IDirect3DTexture9*, texture->GetTextureData());
		}

		D3DLOCKED_RECT lockedRect;
		hr = pTex->LockRect(0, &lockedRect, NULL, 0);

		if (SUCCEEDED(hr))
		{
			unsigned char*  src = data;
			unsigned char*  dst = (unsigned char*)lockedRect.pBits;
			int count = size.x * size.y * bpp;

			int i, j, k, x, y, w = size.x;
			int yPremult = 0;
			int sizeXDiv2 = size.x / 2.0f;
			for (y = 0; y < size.y; y++)
			{
				yPremult = (y)*w;
				for (x = 0; x < sizeXDiv2; x++)
				{
					i = (y*sizeXDiv2 + x) * 4;
					j = (yPremult + x) * 3;
					k = (yPremult + x + (sizeXDiv2)) * 3;
					dst[i] = src[j + 2];
					dst[i + 1] = src[j + 1];
					dst[i + 2] = src[j];
					dst[i + 3] = src[k];
				}
			}
			hr = pTex->UnlockRect(0);
		}
		texture->SetTextureData(pTex);
	}

	//////////////////////////////////////////////////////////////////////////
	char** CRender::GetAlphaFromRegion(Texture * texture, NBG::FRect rectangle)
	{
		char ** ret = NULL;
		int w = rectangle.right - rectangle.left + 1;
		int h = rectangle.bottom - rectangle.top + 1;

		ret = new char*[w];
		for (int i = 0; i < w; i++)
		{
			ret[i] = new char[h];
			for (int f = 0; f < h; f++)
			{
				ret[i][f] = 0;
			}
		}

		w = texture->GetSize().x;
		h = texture->GetSize().y;

		BYTE* PixelMap = NULL;
		RECT r;
		r.left = 0;
		r.top = 0;
		r.right = w;
		r.bottom = h;

		HRESULT hRes;
		IDirect3DTexture9 * directTexture = CAST(IDirect3DTexture9 *, texture->GetTextureData());

		D3DLOCKED_RECT RectLock;
		directTexture->LockRect(0, &RectLock, &r, D3DLOCK_DISCARD);

		PixelMap = (BYTE *)RectLock.pBits;

		char * pixel = NULL;

		int pitch = w * 4;
		int bpp = 4;

		int x1 = rectangle.left;
		int x2 = rectangle.right;
		int y1 = rectangle.top;
		int y2 = rectangle.bottom;
		int atlasPitch = w*h * 4;
		int p = 0;

		int key = 0;
		int pitchKey = 0;
		int pitchMultiply = 0;
		for (int y = y1; y < y2; y++)
		{
			pitchMultiply = pitch*(y);
			for (int x = x1; x < x2; x++)
			{
				if (x * 4 + (pitchMultiply) < atlasPitch)
				{
					key = pitch * (y - y1) + 4 * (x - x1);
					pitchKey = x * 4 + (pitchMultiply);
					if (PixelMap[pitchKey + 3] > 0)
					{
						ret[x - (int)rectangle.left][y - (int)rectangle.top] = 1;
					}
					else
					{
						ret[x - (int)rectangle.left][y - (int)rectangle.top] = 0;
					}
				}
			}
		}
		directTexture->UnlockRect(0);
		return ret;
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::ReleaseTexture(Texture * texture)
	{
		IDirect3DTexture9* _texture = (IDirect3DTexture9*)texture->GetTextureData();
		if (_texture)
		{
			_texture->Release();
		}
		texture->SetTextureData(NULL);
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::DrawtToBackbufer()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::Begin()
	{
		m_Device->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_STENCIL | D3DCLEAR_ZBUFFER, 0xFF000000, 0.0f, 0);
		m_Device->BeginScene();
		m_CurrentObject = 0;
		m_CurrentDrawPoly = 0;
		m_PolyCount = 0;
		m_VertexOffset = 0;
		m_VertexesCount = 0;
		m_IndexOffset = 0;
		m_IndexesCount = 0;

		m_VertexBuffer->Lock(0, sizeof(m_VertexDataPointer), (void**)&m_VertexDataPointer, D3DLOCK_DISCARD);
		m_IndexBuffer->Lock(0, 0, (void**)&m_IndexDataPointer, D3DLOCK_DISCARD);
		m_DrawCalls = 0;
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::FlushVertexBuffer(bool needToLockBufferAgain)
	{		
		if (m_CurrentDrawPoly == 0)
		{
			if (needToLockBufferAgain == false)
			{
				m_VertexBuffer->Unlock();
				m_IndexBuffer->Unlock();
			}			
			return;
		}
		m_DrawCalls++;
		ChangeState(RA_Batch);

		if (m_CurrentDrawPoly == 0)
		{
			if (needToLockBufferAgain == false)
			{
				m_VertexBuffer->Unlock();
				m_IndexBuffer->Unlock();
			}			
			return;
		}

		m_VertexBuffer->Unlock();
		m_IndexBuffer->Unlock();
		
		if (m_CanDebugDrawCalls)
		{
			if (g_GameApplication->GetDrawCallLimiter() != -1)
			{
				if (m_DrawCalls - 1 < g_GameApplication->GetDrawCallLimiter())
				{
					m_Device->SetStreamSource(0, m_VertexBuffer, 0, sizeof(D3DVERTEX));
					m_Device->SetIndices(m_IndexBuffer);
					m_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_VertexesCount, 0, m_CurrentDrawPoly);
				}
			}
			else
			{
				m_Device->SetStreamSource(0, m_VertexBuffer, 0, sizeof(D3DVERTEX));
				m_Device->SetIndices(m_IndexBuffer);
				m_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_VertexesCount, 0, m_CurrentDrawPoly);
			}
		}
		else
		{
			m_Device->SetStreamSource(0, m_VertexBuffer, 0, sizeof(D3DVERTEX));
			m_Device->SetIndices(m_IndexBuffer);
			m_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, m_VertexesCount, 0, m_CurrentDrawPoly);
		}
		m_CurrentObject = 0;
		m_CurrentDrawPoly = 0;
		m_VertexOffset = 0;
		m_IndexOffset = 0;
		m_IndexesCount = 0;
		m_VertexesCount = 0;
		if (needToLockBufferAgain)
		{
			m_VertexBuffer->Lock(0, sizeof(m_VertexDataPointer), (void**)&m_VertexDataPointer, D3DLOCK_DISCARD);
			m_IndexBuffer->Lock(0, 0, (void**)&m_IndexDataPointer, D3DLOCK_DISCARD);
		}		
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::DrawMesh(Texture  * texture, Mesh * mesh)
	{
		g_GlobalMutex->lock();
		IDirect3DTexture9* _texture = nullptr;
		HRESULT hr;
		if (texture)
		{
			_texture = (IDirect3DTexture9*)texture->GetTextureData();
		}

		vs = mesh->GetVertexesSize();
		vc = mesh->GetIndexesCount() / 3;
		m_VertexesCount += mesh->GetVertexCount();
		m_IndexesCount += mesh->GetIndexesCount();
		m_PolyCount += vc;
		m_CurrentObject += vc / 2;

		if (m_VertexesCount >= MAX_VERTEX || m_IndexesCount >= MAX_VERTEX)
		{
			FlushVertexBuffer();
			g_GlobalMutex->unlock();
			DrawMesh(texture, mesh);
			return;
		}

		if (m_CurrentObject == 0)
		{
			if (_texture)
			{
				hr = m_Device->SetTexture(0, _texture);
			}
			else
			{
				hr = m_Device->SetTexture(0, nullptr);
			}
		}
		if (m_PrevTexture != nullptr)
		{
			if (m_PrevTexture != _texture)
			{
				FlushVertexBuffer();
				m_Device->SetTexture(0, _texture);
				
				vs = mesh->GetVertexesSize();
				vc = mesh->GetIndexesCount() / 3;
				m_VertexesCount += mesh->GetVertexCount();
				m_IndexesCount += mesh->GetIndexesCount();
				m_PolyCount += vc;
				m_CurrentObject += vc / 2;
			}
		}
		else
		{
			if (m_PrevTexture != _texture)
			{
				FlushVertexBuffer();
				m_Device->SetTexture(0, _texture);

				vs = mesh->GetVertexesSize();
				vc = mesh->GetIndexesCount() / 3;
				m_VertexesCount += mesh->GetVertexCount();
				m_IndexesCount += mesh->GetIndexesCount();
				m_PolyCount += vc;
				m_CurrentObject += vc / 2;
				
			}
		}
		m_PrevTexture = _texture;


		memcpy((void*)(m_VertexDataPointer + (m_VertexOffset)), mesh->GetVertexes(), vs);

		m_CurrentDrawPoly += vc;
		m_VertexOffset += vs;

		int is = mesh->GetIndexesSize();
		int ic = mesh->GetIndexesCount();
		auto indexes = mesh->GetIndexes();
		int c = 0;
		while (c < ic)
		{
			m_IndexDataPointer[c] = indexes[c] + m_IndexOffset;
			++c;
		}
		m_IndexDataPointer += ic;
		m_IndexOffset += mesh->GetVertexCount();
		g_GlobalMutex->unlock();
	}
	/*void CRender::DrawMesh(Texture  * texture, Mesh * mesh)
	{
		g_GlobalMutex->lock();
		IDirect3DTexture9* _texture = nullptr;
		HRESULT hr;
		if (texture)
		{
			_texture = (IDirect3DTexture9*)texture->GetTextureData();
		}

		vs = mesh->GetVertexesSize();
		vc = mesh->GetIndexesCount() / 3;
		m_PolyCount += vc;
		m_CurrentObject += vc / 2;

		if (m_CurrentObject >= MAX_OBJECTS)
		{
			FlushVertexBuffer();
			g_GlobalMutex->unlock();
			DrawMesh(texture, mesh);			
			return;
		}

		if (m_CurrentObject == 0)
		{
			if (_texture)
			{
				hr = m_Device->SetTexture(0, _texture);
			}
			else
			{
				hr = m_Device->SetTexture(0, nullptr);
			}
		}
		if (m_PrevTexture != nullptr)
		{
			if (m_PrevTexture != _texture)
			{
				FlushVertexBuffer();
				m_Device->SetTexture(0, _texture);
			}
		}
		else
		{
			if (m_PrevTexture != _texture)
			{
				FlushVertexBuffer();
				m_Device->SetTexture(0, _texture);
			}
		}
		m_PrevTexture = _texture;

		
		memcpy((void*)(m_VertexDataPointer + (m_VertexOffset)), mesh->GetVertexes(), vs);		

		m_CurrentDrawPoly += vc;
		m_VertexOffset += vs;

		int is = mesh->GetIndexesSize();
		int ic = mesh->GetIndexesCount();
		auto indexes = mesh->GetIndexes();
		int c = 0;
		while (c < ic)
		{
			m_IndexDataPointer[c] = indexes[c] + m_IndexOffset;
			++c;
		}
		m_IndexDataPointer += ic;
		m_IndexOffset += mesh->GetVertexCount();
		g_GlobalMutex->unlock();
	}*/

	//////////////////////////////////////////////////////////////////////////
	void CRender::DrawLine(Vector ul, Vector dr, const float width, Color &color)
	{
		ChangeState(RA_Batch);
		SetBlendMode(BlendMode::BLEND);

		m_lineVector.x = 1.0f;
		m_lineVector.y = 1.0f;
		float DirX = (dr.x - ul.x), DirY = (dr.y - ul.y);
		if (DirX == 0)
		{
			if (DirY > 0)
			{
				m_lineVector.x = 1.0f;
			}
			else
			{
				m_lineVector.x = -1.0f;
			}
			m_lineVector.y = 0.0f;
		}
		else if (DirY == 0)
		{
			if (DirX > 0)
			{
				m_lineVector.y = -1.0f;
			}
			else
			{
				m_lineVector.y = 1.0f;
			}
			m_lineVector.x = 0.0f;
		}
		else
		{
			if (DirY < 0)
			{
				m_lineVector.x = -1.0f;
				m_lineVector.y = (DirX / DirY);
			}
			else if (DirY > 0)
			{
				m_lineVector.x = 1.0f;
				m_lineVector.y = -(DirX / DirY);
			}
		}
		m_lineVector.Normalize();
		m_lineVector.x *= width;
		m_lineVector.y *= width;


		Vertex * vertexes = m_LineMesh.GetVertexes();

		vertexes[0].x = ul.x - m_lineVector.x;
		vertexes[0].y = ul.y - m_lineVector.y;
		vertexes[1].x = ul.x + m_lineVector.x;
		vertexes[1].y = ul.y + m_lineVector.y;
		vertexes[2].x = dr.x - m_lineVector.x;
		vertexes[2].y = dr.y - m_lineVector.y;
		vertexes[3].x = dr.x + m_lineVector.x;
		vertexes[3].y = dr.y + m_lineVector.y;

		vertexes[0].u = 0; vertexes[0].v = 0;
		vertexes[1].u = 1; vertexes[1].v = 0;
		vertexes[2].u = 0; vertexes[2].v = 1;
		vertexes[3].u = 1; vertexes[3].v = 1;


		unsigned int clr = color.GetPackedColor();
		vertexes[0].color = vertexes[1].color = vertexes[2].color = vertexes[3].color = clr;
		DrawMesh(nullptr, &m_LineMesh);
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::ChangeState(RenderActions renderAction)
	{
		if (m_PrevAction == renderAction)return;
		m_PrevAction = renderAction;
		FlushVertexBuffer();

		switch (renderAction)
		{
		case RA_Line:
			m_Device->SetLight(0, m_LineLight);
			m_Device->LightEnable(0, true);
			m_Device->SetTexture(0, 0);

			m_Device->SetFVF(D3DFVF_XYZ); //set vertex format (NEW)

			m_Device->SetRenderState(D3DRS_LIGHTING, TRUE);
			m_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			m_Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			m_Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
			m_Device->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);


			m_Device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_ANISOTROPIC);
			m_Device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_ANISOTROPIC);
			m_Device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_ANISOTROPIC);
			break;
		case RA_Batch:			
			m_Device->SetTransform(D3DTS_VIEW, m_MatrixView);
			m_Device->SetTransform(D3DTS_PROJECTION, m_MatrixProjection);
			m_Device->SetTransform(D3DTS_WORLD, m_MatrixWorld);

			m_Device->SetRenderState(D3DRS_LIGHTING, FALSE);
			m_Device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
			m_Device->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);
			m_Device->SetRenderState(D3DRS_ALPHAREF, 0x00);
			m_Device->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
			m_Device->SetRenderState(D3DRS_CLIPPING, true);
			m_Device->SetRenderState(D3DRS_CLIPPLANEENABLE, false);
			m_Device->SetRenderState(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_ALPHA | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_RED);
			m_Device->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1);
			m_Device->SetRenderState(D3DRS_ENABLEADAPTIVETESSELLATION, false);
			m_Device->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
			m_Device->SetRenderState(D3DRS_INDEXEDVERTEXBLENDENABLE, true);
			m_Device->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
			m_Device->SetRenderState(D3DRS_WRAP0, 0);
			m_Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			m_Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

			m_Device->SetFVF(D3DFVF_CUSTOMVERTEX); //set vertex format (NEW)
			

			m_Device->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
			m_Device->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
			m_Device->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);

			m_Device->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
			m_Device->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
			m_Device->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);

			m_Device->SetTextureStageState(1, D3DTSS_ALPHAOP, D3DTOP_DISABLE);
			m_Device->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);


			m_Device->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
			m_Device->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

			
			m_Device->SetSamplerState(0, D3DSAMP_MAXMIPLEVEL, 0);
			m_Device->SetSamplerState(0, D3DSAMP_MAXANISOTROPY, 0);
			m_Device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
			m_Device->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
			m_Device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
			m_Device->SetSamplerState(0, D3DSAMP_MIPMAPLODBIAS, 0);
			m_Device->SetSamplerState(0, D3DSAMP_SRGBTEXTURE, 0);

			m_Device->SetRenderState(D3DRS_MULTISAMPLEANTIALIAS, false);
			m_Device->SetRenderState(D3DRS_ANTIALIASEDLINEENABLE, false);			
			break;
		case RA_3D:
			m_LineMaterial->Diffuse.a = 1.0f;
			m_LineMaterial->Diffuse.r = m_LineMaterial->Diffuse.g = m_LineMaterial->Diffuse.b = 1.0f;

			m_LineMaterial->Ambient.r = 1.0f;
			m_LineMaterial->Ambient.g = 1.0f;
			m_LineMaterial->Ambient.b = 1.0f;
			m_Device->SetMaterial(m_LineMaterial);
			m_Device->SetFVF(D3DFVF_CUSTOMVERTEX); //set vertex format (NEW)			
			break;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::DrawBatch()
	{
		FlushVertexBuffer(true);
		m_PrevTexture = NULL;
		_texture = NULL;
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::SetBlendMode(BlendMode mode)
	{
		if (mode == m_BlendMode)return;
		m_BlendMode = mode;
		FlushVertexBuffer();
		if (mode == BlendMode::BLEND)
		{
			m_Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			m_Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		}
		else if (mode == BlendMode::ADD)
		{
			m_Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			m_Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
		}
		else if (mode == BlendMode::MOD)
		{
			m_Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
			m_Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_INVDESTCOLOR);
		}
		else if (mode == BlendMode::SCREEN)
		{
			m_Device->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_DESTCOLOR);
			m_Device->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::SetTextureRepeat(const bool isRepeat)
	{
		if (isRepeat == m_IsTextureRepeat)return;
		m_IsTextureRepeat = isRepeat;
		FlushVertexBuffer();
		if (isRepeat)
		{
			m_Device->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
			m_Device->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
		}
		else
		{
			m_Device->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
			m_Device->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CRender::End(bool present)
	{
		FlushVertexBuffer(false);
		m_PrevTexture = NULL;
		_texture = NULL;
		m_Device->EndScene();
		if (present)
		{
			m_Device->Present(NULL, NULL, NULL, NULL);
		}		
	}
}
#endif