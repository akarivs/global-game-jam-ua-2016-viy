#ifdef NBG_WIN32
#include "RenderTarget.h"

#include <DxErr.h>
#include <Framework.h>

namespace NBG
{
	//////////////////////////////////////////////////////////////////////////
	CRenderTarget::CRenderTarget(const int w, const int h)
	{
		m_Surface = nullptr;
		m_OrigTarget = nullptr;


		auto device = g_Render->GetDevice();

		m_TextureRes = new CTextureResource();
		m_Texture = new Texture();
		auto hRes = D3DXCreateTexture(g_Render->GetDevice(), w, h, 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &m_TextureD3D);


		m_Texture->SetSize(w, h);
		m_Texture->SetTextureData(m_TextureD3D);

		m_TextureRes->SetTexture(m_Texture);
		m_TextureRes->SetTextureWidth(w);
		m_TextureRes->SetTextureHeight(h);
	}

	//////////////////////////////////////////////////////////////////////////
	CRenderTarget::~CRenderTarget()
	{
		delete m_Texture;
	}

	//////////////////////////////////////////////////////////////////////////
	void CRenderTarget::Begin()
	{
		auto device = g_Render->GetDevice();
		g_Render->DrawBatch();
		auto hr = device->GetRenderTarget(0, &m_OrigTarget);

		m_TextureD3D->GetSurfaceLevel(0, &m_Surface);
		hr = device->SetRenderTarget(0, m_Surface);
		device->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x00000000, 1, 0);
	}

	//////////////////////////////////////////////////////////////////////////
	void CRenderTarget::End()
	{
		g_Render->DrawBatch();
		auto device = g_Render->GetDevice();
		device->SetRenderTarget(0, m_OrigTarget);

		m_Texture->SetTextureData(m_TextureD3D);
	}
}
#endif