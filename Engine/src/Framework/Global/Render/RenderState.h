#ifndef NBG_FRAMEWORK_GLOBAL_RENDER_STATE
#define NBG_FRAMEWORK_GLOBAL_RENDER_STATE

#include <Framework/Datatypes/Matrix.h>
namespace NBG
{
	/** @brief Класс, текущего состояния рендера.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2015 New Bridge Games
	*
	*/
	class CRenderState
	{
	public:    
		CRenderState();
		~CRenderState();

		void Reset();

		void PushMatrix();
		void PopMatrix();
		void ApplyMatrix(Matrix &m);
		void SetMatrix(Matrix &m);

		struct MatrixStack
		{
			static const int maxSize = 128;
			Matrix stack[maxSize];
			int index;
			Matrix &back()
			{
				return stack[index];
			}
			Matrix *backP()
			{
				return &stack[index];
			}
			inline void push(Matrix &m)
			{
				index++;
				stack[index] = m;
			}
			inline void pop()
			{
				index--;
			}
			inline void reset()
			{
				index = 0;
			}
		};

		inline Matrix & GetMatrix()
		{
			return m_MatrixStack.back();
		}
		inline Matrix * GetMatrixPointer()
		{
			return &m_MatrixStack.back();
		}
	private:
		MatrixStack m_MatrixStack;
	};
}
#endif //NBG_FRAMEWORK_GLOBAL_RENDER_STATE
