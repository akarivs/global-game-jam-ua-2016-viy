#ifdef NBG_IOS
#include "Render.h"


#import <QuartzCore/QuartzCore.h>
#import <QuartzCore/CALayer.h>
#import <OpenGLES/EAGLDrawable.h>
#include <OpenGLES/ES1/gl.h>

#import <GLKit/GLKit.h>

#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>

#include <Framework.h>


namespace NBG
{
    namespace IOSRender
    {
        typedef struct {
            float Position[3];
            float Color[4];
            float Texture[2];
        } Vertex;
        

        
        GLuint vertexBuffer;
        GLuint indexBuffer;
        IOSRender::Vertex vertexes[CRender::MAX_OBJECTS*6];
        GLushort indices[CRender::MAX_OBJECTS*6];
        GLuint prevTextureId;
        char * vboBuffer;
        GLuint viewRenderbuffer, viewFramebuffer;
        GLuint depthRenderbuffer;
        EAGLContext * context;
        Matrix projectionM;
        
        GLuint _positionSlot;
        GLuint  _colorSlot;
        GLuint  _coordSlot;
        GLuint  _samplerSlot;
        GLuint  _projectionSlot;
        
        
              bool CreateFrameBuffer(CALayer * layer)
        {
            glGenFramebuffers(1, &viewFramebuffer);
            glGenRenderbuffers(1, &viewRenderbuffer);
            glBindFramebuffer(GL_FRAMEBUFFER, viewFramebuffer);
            glBindRenderbuffer(GL_RENDERBUFFER, viewRenderbuffer);
            [context renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)layer];
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, viewRenderbuffer);
            
            
            unsigned int status = glCheckFramebufferStatus(GL_FRAMEBUFFER);              if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
                NSLog(@"failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
                return NO;
            }
            return YES;
        }
    }
	//==============================================================================
	CRender::CRender()
	{
		m_PrevAction = RA_None;
        m_BlendMode = BlendMode::BLEND;

		m_MatrixOffset.x = m_MatrixOffset.y = 0.0f;
        
        m_LineMesh.Init(4,6);
		unsigned short * indexes = m_LineMesh.GetIndexes();
		indexes[0] = 0;
		indexes[1] = 1;
		indexes[2] = 2;
		indexes[3] = 3;
		indexes[4] = 2;
		indexes[5] = 1;
	}

	CRender::~CRender()
	{

	}
    
    void CRender::ShareContext()
    {
       /* EAGLContext *aContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2 sharegroup:IOSRender::context.sharegroup];
        
        if([EAGLContext setCurrentContext:(EAGLContext*)aContext] == NO) {
            NSLog(@"Failed to share context!");
            return;
        }*/

    }
    void CRender::UnShareContext()
    {
        
    }

	void CRender::Init(void * layer)
	{
        CALayer * _layer = (__bridge CALayer*)layer;
        
       
        IOSRender::context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1];
        if([EAGLContext setCurrentContext:(EAGLContext*)IOSRender::context] == NO) {
            NSLog(@"Failed to set current context!");
            return;
        }
        bool res = IOSRender::CreateFrameBuffer(_layer);
        if (!res)
        {
            NSLog(@"Could'nt create frame buffer!");
        }
        
        for (int i=0, j=0; i<MAX_OBJECTS; i+=6,j+=4)
        {
            IOSRender::indices[i] = j;
            IOSRender::indices[i+1] = j+1;
            IOSRender::indices[i+2] = j+2;
            IOSRender::indices[i+3] = j+3;
            IOSRender::indices[i+4] = j+2;
            IOSRender::indices[i+5] = j+1;
        }
       
        
        int width = g_System->GetSystemResolution().x;
        int height = g_System->GetSystemResolution().y;
        
        Vector winSize = Vector(width,height);
        Vector logical = g_GameApplication->GetLogicalSize();
        
        Vector gameRes = g_System->GetGameResolution();
        Vector resScale = g_System->GetResolutionScale();
        
        
        glViewport(0, 0, width, height);
        
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrthof(0.0f,width,height,0.0f, 0, 1.0f);
        
        
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glTranslatef(-0.5f, -(height)/resScale.y-0.5f, 0.0f);
        glScalef(resScale.x, resScale.y, 1.0f);
        
        
        CONSOLE("Window size: %d %d",width,height);
        CONSOLE("Game size: %f %f",gameRes.x, gameRes.y);
        CONSOLE("Logical size: %f %f",logical.x, logical.y);
        
		Reset();
	}
    
    bool CRender::CreateFrameBuffer()
    {
        return true;
    }

	void CRender::Reset()
	{		
		g_GameApplication->OnBeforeRenderChange();
		Vector resolution = g_System->GetSystemResolution();

		ChangeState(RA_None);
		g_GameApplication->OnAfterRenderChange();
	}



	void CRender::CreateOpaqueTextureFromTheoraFrame(unsigned char * data, int bpp, Vector size, Texture * texture)
	{
        glPixelStorei(GL_UNPACK_ALIGNMENT,4);
        glEnable(GL_TEXTURE_2D);
        GLuint text;
        if (texture->GetTextureData() == NULL)
        {
            glActiveTexture(GL_TEXTURE1);
            glGenTextures(1, &text);
            glBindTexture(GL_TEXTURE_2D, text);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, size.x,size.y,0, GL_RGB, GL_UNSIGNED_BYTE,data);
            
            texture->SetTextureData(new GLuint(text));
            return;
        }
        else
        {
            text = *(unsigned int*)texture->GetTextureData();
        }
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, text);
        glTexSubImage2D(GL_TEXTURE_2D, 0,0,0, size.x,size.y,GL_RGB, GL_UNSIGNED_BYTE,data);
        
        IOSRender::prevTextureId = -1;
	}

    void CRender::CreateTransparentTextureFromTheoraFrame(unsigned char * data, int bpp, Vector size, Texture * texture)
	{
        glPixelStorei(GL_UNPACK_ALIGNMENT,4);
        glEnable(GL_TEXTURE_2D);
        GLuint text;
        unsigned char * textureData = NULL;
        if (texture->GetTextureData() == NULL)
        {
            glActiveTexture(GL_TEXTURE1);
            glGenTextures(1, &text);
            glBindTexture(GL_TEXTURE_2D, text);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            
            textureData = new unsigned char[(int)(size.x/2.0f*size.y*4)];
            memset(textureData,0,size.x/2.0f*size.y*4);
            
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.x/2.0f,size.y,0, GL_RGBA, GL_UNSIGNED_BYTE,textureData);
            
            texture->SetTextureData(new GLuint(text));
            texture->SetUserData(textureData);
        }
        else
        {
            text = *(unsigned int*)texture->GetTextureData();
            textureData = (unsigned char*)texture->GetUserData();
        }
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, text);
        
        unsigned char*  src     = data;
        unsigned char*  dst     = textureData;
        
        int i,j,k,x,y,w=size.x;
        int yPremult = 0;
        int sizeXDiv2 = size.x/2.0f;
        for (y=0;y<size.y-2;y++)
        {
            yPremult = (y+2)*w;
            for (x=0;x<sizeXDiv2;x++)
            {
                i=(y*sizeXDiv2+x)*4;
                j=(yPremult+x+4)*3;
                k=(yPremult+x+(sizeXDiv2))*3;
                memcpy(&dst[i],&src[j],3);
                dst[i+3]=src[k];
            }
        }
        glTexSubImage2D(GL_TEXTURE_2D, 0,0,0, size.x/2.0f,size.y,GL_RGBA, GL_UNSIGNED_BYTE,dst);
        
        //IOSRender::prevTextureId = -1;
        
        
	}

	char** CRender::GetAlphaFromRegion(Texture * texture, NBG::FRect rectangle)
	{
        char ** ret = NULL;
        int w = rectangle.right - rectangle.left + 1;
        int h = rectangle.bottom - rectangle.top + 1 ;
        
        ret = new char*[w];
        for (int i=0; i<w; i++)
        {
            ret[i] = new char[h];
            for (int f=0; f<h; f++)
            {
                ret[i][f] = 0;
            }
        }
        
        w =  texture->GetSize().x;
        h =  texture->GetSize().y;
        
        GLuint textureId = -1;
        if (texture && texture->GetTextureData())
        {
            textureId = *(int*)texture->GetTextureData();
        }
        if (textureId == -1)return nullptr;
        
        glEnable(GL_TEXTURE_2D);
        glBindTexture( GL_TEXTURE_2D,textureId );
        GLubyte *pixels = new GLubyte[w*h*4];
        glBindTexture(GL_TEXTURE_2D, textureId);
        {
            GLuint framebuffer;
            glGenFramebuffers(1, &framebuffer);
            glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureId, 0);
            GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
            if (status != GL_FRAMEBUFFER_COMPLETE) {
                NSLog(@"Problem with OpenGL framebuffer after specifying color render buffer: %x", status);
            }
            glReadPixels(0, 0, w,
                         h,
                         GL_RGBA,
                         GL_UNSIGNED_BYTE, pixels);
            glBindFramebuffer(GL_FRAMEBUFFER, IOSRender::viewFramebuffer);
            glDeleteFramebuffers(1, &framebuffer);
        }
        glBindTexture(GL_TEXTURE_2D, 0);
        
        int x1 = rectangle.left;
        int x2 = rectangle.right;
        int y1 = rectangle.top;
        int y2 = rectangle.bottom;
        
        int pitch = w * 4;
        int pitchKey = 0;
        int pitchMultiply = 0;
        int key = 0;
        int atlasPitch = w*h * 4;
        
        for (int y = y1; y < y2; y++)
        {
            pitchMultiply = pitch*(y);
            for (int x = x1; x < x2; x++)
            {
                if (x * 4 + (pitchMultiply) < atlasPitch)
                {
                    key = pitch * (y - y1) + 4 * (x - x1);
                    pitchKey = x * 4 + (pitchMultiply);
                    
                    if (pixels[pitchKey + 3] > 0)
                    {
                        ret[x - (int)rectangle.left][y - (int)rectangle.top] = 1;
                    }
                    else
                    {
                        ret[x - (int)rectangle.left][y - (int)rectangle.top] = 0;
                    }
                }
            }
        }
        delete[] pixels;
        return ret;
	}


	void CRender::ReleaseTexture(Texture * texture)
	{
        texture->SetTextureData(NULL);
	}


	void CRender::Begin()
	{
        if (!IOSRender::context)return;
        
        [EAGLContext setCurrentContext:IOSRender::context];
        glBindFramebuffer(GL_FRAMEBUFFER, IOSRender::viewFramebuffer);

        
        glDepthMask(GL_FALSE);
        //glEnable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        
        glClearColor(0,0,0,0);
        glClearStencil(0x0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        
        
        
        IOSRender::prevTextureId = 0;
        
        Vector gameRes = g_System->GetGameResolution();
        Vector sysRes = g_System->GetSystemResolution() /2.0f;
        Vector resScale = g_System->GetResolutionScale();
        
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glTranslatef(sysRes.x, sysRes.y, 0.0f);
        glScalef(resScale.x, resScale.y, 1.0f);
        
        m_CurrentObject = 0;
        m_CurrentDrawPoly = 0;
        m_PolyCount = 0;
        m_VertexOffset = 0;
        m_IndexOffset = 0;	
        m_DrawCalls = 0;
        m_IndexCount = 0;
        
        m_VertexesCount = 0;
        m_IndexesCount = 0;

        
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	}

    #define BUFFER_OFFSET(i) ((char*)NULL + (i))
	void CRender::FlushVertexBuffer(bool needToLockBufferAgain)
	{
        if (m_CurrentObject == 0)return;
        m_DrawCalls++;
        ChangeState(RA_Batch);
        
        glEnable(GL_TEXTURE_2D);
        
        
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_COLOR_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        
        glVertexPointer(3, GL_FLOAT, sizeof(IOSRender::Vertex), &IOSRender::vertexes[0].Position[0]);
        glColorPointer(4, GL_FLOAT, sizeof(IOSRender::Vertex), &IOSRender::vertexes[0].Color[0]);
        glTexCoordPointer(2, GL_FLOAT, sizeof(IOSRender::Vertex), &IOSRender::vertexes[0].Texture[0]);
        glDrawElements(GL_TRIANGLES, m_IndexCount, GL_UNSIGNED_SHORT, &IOSRender::indices[0]);
        
        glDisableClientState(GL_VERTEX_ARRAY);
        glDisableClientState(GL_COLOR_ARRAY);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
        
        m_CurrentObject = 0;
        m_CurrentDrawPoly = 0;
        m_VertexOffset = 0;
        m_IndexOffset = 0;
        m_VertexesCount = 0;
        m_IndexesCount = 0;
        m_IndexCount = 0;
	}

    IOSRender::Vertex Vertices[4];
	void CRender::DrawMesh(Texture  * texture, Mesh * mesh)
	{
        GLuint textureId = -1;
        if (texture && texture->GetTextureData())
        {
            textureId = *(int*)texture->GetTextureData();
        }
        
        
        if (IOSRender::prevTextureId != 0)
        {
            
            if(IOSRender::prevTextureId != textureId)
            {
                FlushVertexBuffer();
                glBindTexture(GL_TEXTURE_2D,textureId);
            }
        }
        else
        {
            if (IOSRender::prevTextureId != textureId)
            {
                glBindTexture(GL_TEXTURE_2D,textureId);
            }
            
        }
        IOSRender::prevTextureId = textureId;
        
        vs = mesh->GetVertexesSize();
        vc = mesh->GetIndexesCount()/3.0f;
        m_VertexesCount += mesh->GetVertexCount();
        m_IndexesCount += mesh->GetIndexesCount();
        m_PolyCount += vc;
        
        
        m_CurrentObject += vc / 2;
        if (m_VertexesCount >= MAX_VERTEX || m_IndexesCount >= MAX_VERTEX)
        {
            FlushVertexBuffer();
            DrawMesh(texture, mesh);
            return;
        }
        
        memcpy(&IOSRender::vertexes[m_VertexOffset],mesh->GetVertexes(),vs);
        m_VertexOffset      +=mesh->GetVertexCount();
        
        int is = mesh->GetIndexesSize();
        int ic = mesh->GetIndexesCount();
        auto indexes = mesh->GetIndexes();
        int c = 0;
        while (c < ic)
        {
            IOSRender::indices[m_IndexCount + c] = indexes[c] + m_IndexOffset;
            ++c;
        }
        m_IndexCount += ic;
        m_IndexOffset += mesh->GetVertexCount();
	}
    
    void CRender::DrawLine(Vector ul, Vector dr, const float width, Color color)
	{
        ChangeState(RA_Batch);
        SetBlendMode(BlendMode::BLEND);
        
        m_lineVector.x = 1.0f;
        m_lineVector.y = 1.0f;
        float DirX = (dr.x - ul.x), DirY = (dr.y - ul.y);
        if (DirX == 0)
        {
            if (DirY > 0)
            {
                m_lineVector.x = 1.0f;
            }
            else
            {
                m_lineVector.x = -1.0f;
            }
            m_lineVector.y = 0.0f;
        }
        else if (DirY == 0)
        {
            if (DirX > 0)
            {
                m_lineVector.y = -1.0f;
            }
            else
            {
                m_lineVector.y = 1.0f;
            }
            m_lineVector.x = 0.0f;
        }
        else
        {
            if (DirY < 0)
            {
                m_lineVector.x = -1.0f;
                m_lineVector.y = (DirX / DirY);
            }
            else if (DirY > 0)
            {
                m_lineVector.x = 1.0f;
                m_lineVector.y = -(DirX / DirY);
            }
        }
        m_lineVector.Normalize();
        m_lineVector.x *= width;
        m_lineVector.y *= width;
        
        
        Vertex * vertexes = m_LineMesh.GetVertexes();
        
        vertexes[0].x = ul.x - m_lineVector.x;
        vertexes[0].y = ul.y - m_lineVector.y;
        vertexes[1].x = ul.x + m_lineVector.x;
        vertexes[1].y = ul.y + m_lineVector.y;
        vertexes[2].x = dr.x - m_lineVector.x;
        vertexes[2].y = dr.y - m_lineVector.y;
        vertexes[3].x = dr.x + m_lineVector.x;
        vertexes[3].y = dr.y + m_lineVector.y;
        
        vertexes[0].u = 0; vertexes[0].v = 0;
        vertexes[1].u = 1; vertexes[1].v = 0;
        vertexes[2].u = 0; vertexes[2].v = 1;
        vertexes[3].u = 1; vertexes[3].v = 1;
        
        color.Normalize();
        //unsigned int clr = color.GetPackedColor();
        vertexes[0].color = vertexes[1].color = vertexes[2].color = vertexes[3].color = color;
        DrawMesh(nullptr, &m_LineMesh);
	}


	void CRender::ChangeState(RenderActions renderAction)
	{
		if (m_PrevAction == renderAction)return;		
		m_PrevAction = renderAction;
		FlushVertexBuffer();

		switch (renderAction)
		{
		case RA_Line:
						break;
		case RA_Batch:
						break;
		case RA_3D:			
				
			break;
		}
	}

	void CRender::DrawBatch()
	{
		FlushVertexBuffer(false);
    }

	void CRender::SetBlendMode(BlendMode mode)
	{
		if (mode == m_BlendMode)return;
		m_BlendMode = mode;
		FlushVertexBuffer();
		if (mode == BlendMode::BLEND)
		{
            glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		}
		else if (mode == BlendMode::ADD)
		{
			glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		}
		else if (mode == BlendMode::MOD)
		{
            glBlendFunc(GL_ONE_MINUS_SRC_COLOR, GL_ONE);
		}
	}

	void CRender::End()
	{
        DrawBatch();
        glBindRenderbuffer(GL_RENDERBUFFER, IOSRender::viewRenderbuffer);
        [IOSRender::context presentRenderbuffer:GL_RENDERBUFFER];
	}
    
    /* TODO: implement stencil */
    void CRender::StencilEnable()
    {
        glEnable(GL_STENCIL_TEST);
        stencilFail = (StencilOperation)-1;
        stencilPass = (StencilOperation)-1;
        
    }
    void CRender::StencilDisable()
    {
        glDisable(GL_STENCIL_TEST);
    }
    void CRender::StencilClear(char bt)
    {
        glClear(GL_STENCIL_BUFFER_BIT);
        
    }
    void CRender::StencilLevelSet(char bt)
    {
        stencilLevel = bt;
    }
    void CRender::StencilSetCmpFunc(CompareType type)
    {
        glStencilFunc(type, (int)stencilLevel,0);
    }
    void CRender::StencilSetPasOpFunc(StencilOperation op)
    {
        stencilPass = op;
        if (stencilFail != -1)
        {
            glStencilOp(stencilFail,GL_KEEP, stencilPass);
        }
        
    }
    void CRender::StencilSetFailOpFunc(StencilOperation op)
    {
        stencilFail = op;
        if (stencilPass != -1)
        {
            glStencilOp(stencilFail,GL_KEEP, stencilPass);
        }
    }
}
#endif