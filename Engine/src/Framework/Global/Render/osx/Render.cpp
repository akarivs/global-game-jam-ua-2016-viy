#ifdef NBG_OSX
#include "Render.h"

#import <QuartzCore/QuartzCore.h>
//#import <GLKit/GLKit.h>

#include <Framework.h>


namespace NBG
{
    namespace AndroidRender
	{
		typedef struct {
			float Position[3];
			float Color[4];
			float Texture[2];
		} Vertex;
        
		GLuint vertexBuffer;
		GLuint indexBuffer;
		Vertex vertexes[CRender::MAX_OBJECTS*4];
		GLushort indices[CRender::MAX_OBJECTS*6];
		GLuint prevTextureId;
		char * vboBuffer;
		GLuint viewRenderbuffer, viewFramebuffer;
		GLuint depthRenderbuffer;
		Matrix projectionM;
		
		GLuint _positionSlot;
		GLuint  _colorSlot;
		GLuint  _coordSlot;
		GLuint  _samplerSlot;
		GLuint  _projectionSlot;
    }
	//==============================================================================
	CRender::CRender()
	{
		
		m_PrevAction = RA_None;
		m_BlendMode = BlendMode::BLEND;

		m_MatrixOffset.x = m_MatrixOffset.y = 0.0f;
        
        m_LineMesh.Init(4,6);
		unsigned short * indexes = m_LineMesh.GetIndexes();
		indexes[0] = 0;
		indexes[1] = 1;
		indexes[2] = 2;
		indexes[3] = 3;
		indexes[4] = 2;
		indexes[5] = 1;
	}

	CRender::~CRender()
	{

	}


	void CRender::Reset()
	{		
		g_GameApplication->OnBeforeRenderChange();
		Vector resolution = g_System->GetSystemResolution();

		ChangeState(RA_None);
		g_GameApplication->OnAfterRenderChange();
        
        glMatrixMode(GL_PROJECTION|GL_MODELVIEW);
        glLoadIdentity();
        
        Vector size = g_System->GetSystemResolution();
        Vector halfSize = size/2.0f;
        Vector scale = g_System->GetResolutionScale();
        
        glOrtho(-halfSize.x,halfSize.x,-halfSize.y,halfSize.y, 0, 1);
        glScalef(scale.x,-scale.y,1.0f);
        
        glViewport(0, 0, size.x, size.y);
        
        glEnable(GL_TEXTURE_2D);
        
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	}

	void CRender::ShareContext()
    {
        g_GameApplication->GetOperatingSystem()->ShareContext();
    }
    
    void CRender::UnShareContext()
    {
        g_GameApplication->GetOperatingSystem()->UnShareContext();
    }

	void CRender::CreateOpaqueTextureFromTheoraFrame(unsigned char * data, int bpp, Vector size, Texture * texture)
	{
        glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
        glEnable(GL_TEXTURE_2D);
        GLuint text;
        if (texture->GetTextureData() == NULL)
        {
            glActiveTexture(GL_TEXTURE1);
            glGenTextures(1, &text);
            glBindTexture(GL_TEXTURE_2D, text);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, size.x,size.y,0, GL_RGB, GL_UNSIGNED_BYTE,data);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            texture->SetTextureData(new GLuint(text));
            glActiveTexture(GL_TEXTURE0);
            return;
        }
        else
        {
            text = *(unsigned int*)texture->GetTextureData();
        }
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, text);
        glTexSubImage2D(GL_TEXTURE_2D, 0,0,0, size.x,size.y,GL_RGB, GL_UNSIGNED_BYTE,data);
        glActiveTexture(GL_TEXTURE0);
	}
    
    
  

    void CRender::CreateTransparentTextureFromTheoraFrame(unsigned char * data, int bpp, Vector size, Texture * texture)
	{
        glPixelStorei(GL_UNPACK_ALIGNMENT,4);
        glEnable(GL_TEXTURE_2D);
        GLuint text;
        unsigned char * textureData = NULL;
        if (texture->GetTextureData() == NULL)
        {
            glActiveTexture(GL_TEXTURE1);
            glGenTextures(1, &text);
            glBindTexture(GL_TEXTURE_2D, text);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            
            textureData = new unsigned char[(int)(size.x/2.0f*size.y*4)];
            memset(textureData,0,size.x/2.0f*size.y*4);
            
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.x/2.0f,size.y,0, GL_RGBA, GL_UNSIGNED_BYTE,textureData);
            
            texture->SetTextureData(new GLuint(text));
            texture->SetUserData(textureData);
        }
        else
        {
            text = *(unsigned int*)texture->GetTextureData();
            textureData = (unsigned char*)texture->GetUserData();
        }
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, text);
        
        unsigned char*  src     = data;
        unsigned char*  dst     = textureData;
        
        int i,j,k,x,y,w=size.x;
        int yPremult = 0;
        int sizeXDiv2 = size.x/2.0f;
        for (y=0;y<size.y;y++)
        {
            yPremult = (y)*w;
            for (x=0;x<sizeXDiv2;x++)
            {
                i=(y*sizeXDiv2+x)*4;
                j=(yPremult+x+4)*3;
                k=(yPremult+x+(sizeXDiv2))*3;
                memcpy(&dst[i],&src[j],3);
                dst[i+3]=src[k];
            }
        }
        glTexSubImage2D(GL_TEXTURE_2D, 0,0,0, size.x/2.0f,size.y,GL_RGBA, GL_UNSIGNED_BYTE,dst);
        glActiveTexture(GL_TEXTURE0);
	}

    ///////////////////////////////////////////////////////////////////////////
	char** CRender::GetAlphaFromRegion(Texture * texture, NBG::FRect rectangle)
	{
		char ** ret = NULL;
		int w = rectangle.right - rectangle.left + 1;
		int h = rectangle.bottom - rectangle.top + 1 ;

		ret = new char*[w];
		for (int i=0; i<w; i++)
		{
			ret[i] = new char[h];
			for (int f=0; f<h; f++)
			{
				ret[i][f] = 0;
			}
		}

		w =  texture->GetSize().x;
		h =  texture->GetSize().y;
        
        GLuint textureId = -1;
        if (texture && texture->GetTextureData())
        {
            textureId = *(int*)texture->GetTextureData();
        }
        if (textureId == -1)return nullptr;
        
        glEnable(GL_TEXTURE_2D);
        glBindTexture( GL_TEXTURE_2D,textureId );
        GLubyte *pixels = new GLubyte[w*h*4];
        glGetTexImage(GL_TEXTURE_2D, 0,GL_RGBA, GL_UNSIGNED_BYTE, pixels);
        
        int x1 = rectangle.left;
        int x2 = rectangle.right;
        int y1 = rectangle.top;
        int y2 = rectangle.bottom;
        
        int pitch = w * 4;
        int pitchKey = 0;
        int pitchMultiply = 0;
        int key = 0;
        int atlasPitch = w*h * 4;
        
        for (int y = y1; y < y2; y++)
        {
            pitchMultiply = pitch*(y);
            for (int x = x1; x < x2; x++)
            {
                if (x * 4 + (pitchMultiply) < atlasPitch)
                {
                    key = pitch * (y - y1) + 4 * (x - x1);
                    pitchKey = x * 4 + (pitchMultiply);

                    if (pixels[pitchKey + 3] > 0)
                    {
                        ret[x - (int)rectangle.left][y - (int)rectangle.top] = 1;
                    }
                    else
                    {
                        ret[x - (int)rectangle.left][y - (int)rectangle.top] = 0;
                    }
                }
            }
        }
        delete[] pixels;
		return ret;
	}


	void CRender::ReleaseTexture(Texture * texture)
	{
        texture->SetTextureData(NULL);
	}


	void CRender::Begin()
	{
        m_CurrentObject = 0;
		m_CurrentDrawPoly = 0;
		m_PolyCount = 0;
		m_VertexOffset = 0;
		m_IndexOffset = 0;
		m_DrawCalls = 0;
		
               glClear(GL_COLOR_BUFFER_BIT);
        
        
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
        
        m_CurrentObject = 0;
        m_DrawCalls = 0;
        m_PolyCount = 0;
        m_IndexCount = 0;
	}

	void CRender::FlushVertexBuffer(bool needToLockBufferAgain)
	{
		if (m_CurrentObject == 0)return;
		m_DrawCalls++;
		ChangeState(RA_Batch);
        
		glEnable(GL_TEXTURE_2D);
		//glEnable(GL_BLEND);
		//glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
        
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        
		glVertexPointer(3, GL_FLOAT, sizeof(AndroidRender::Vertex), &AndroidRender::vertexes[0].Position[0]);
		glColorPointer(4, GL_FLOAT, sizeof(AndroidRender::Vertex), &AndroidRender::vertexes[0].Color[0]);
		glTexCoordPointer(2, GL_FLOAT, sizeof(AndroidRender::Vertex), &AndroidRender::vertexes[0].Texture[0]);
		glDrawElements(GL_TRIANGLES, m_IndexCount, GL_UNSIGNED_SHORT, &AndroidRender::indices[0]);
        
		m_CurrentObject = 0;
		m_CurrentDrawPoly = 0;
		m_VertexOffset = 0;
		m_IndexOffset = 0;
		m_IndexCount = 0;
	}

	void CRender::DrawMesh(Texture  * texture, Mesh * mesh)
	{
        g_GlobalMutex->lock();

		GLuint textureId = -1;
		if (texture && texture->GetTextureData())
		{
			textureId = *(int*)texture->GetTextureData();
		}
        

        
		if (AndroidRender::prevTextureId != 0)
		{
            
			if(AndroidRender::prevTextureId != textureId)
			{
				FlushVertexBuffer();
				glBindTexture(GL_TEXTURE_2D,textureId);
			}
		}
		else
		{
			if (AndroidRender::prevTextureId != textureId)
			{
				glBindTexture(GL_TEXTURE_2D,textureId);
			}
            
		}
		AndroidRender::prevTextureId = textureId;
        
        if (textureId != -1)
        {
            glEnable(GL_TEXTURE_2D);
        }
        else
        {
            glDisable(GL_TEXTURE_2D);
        }
		
		vs = mesh->GetVertexesSize();
		vc = mesh->GetIndexesCount()/3.0f;
		m_PolyCount += vc;
		memcpy(&AndroidRender::vertexes[m_VertexOffset],mesh->GetVertexes(),vs);
        
		m_CurrentObject += vc / 2;
		if (m_CurrentObject >= MAX_OBJECTS)
		{
			FlushVertexBuffer();
			m_PolyCount += vc;
			memcpy(&AndroidRender::vertexes[m_VertexOffset], mesh->GetVertexes(), vs);
			m_CurrentObject += vc / 2;
		}
		
		m_VertexOffset      +=mesh->GetVertexCount();
		
        
		/*int is = mesh->GetIndexesSize();
         int ic = mesh->GetIndexesCount();
         
         memcpy(&AndroidRender::indices[m_IndexCount],mesh->GetIndexes(),is);
         
         for (int i=0; i<ic; i++)
         {
         AndroidRender::indices[m_IndexCount+i] += m_IndexOffset;
         }
         m_IndexCount += ic;
         m_IndexOffset += mesh->GetVertexCount();
         */
        
		int is = mesh->GetIndexesSize();
		int ic = mesh->GetIndexesCount();
		auto indexes = mesh->GetIndexes();
		//memcpy(m_IndexDataPointer,mesh->GetIndexes(),is);
		int c = 0;
		while (c < ic)
		{
			AndroidRender::indices[m_IndexCount + c] = indexes[c] + m_IndexOffset;
			++c;
		}
		m_IndexCount += ic;
		m_IndexOffset += mesh->GetVertexCount();
        
        g_GlobalMutex->unlock();
	}

    
    
    void CRender::DrawLine(Vector ul, Vector dr, const float width, Color color)
	{
		ChangeState(RA_Batch);
		SetBlendMode(BlendMode::BLEND);
        
		m_lineVector.x = 1.0f;
		m_lineVector.y = 1.0f;
		float DirX = (dr.x - ul.x), DirY = (dr.y - ul.y);
		if (DirX == 0)
		{
			if (DirY > 0)
			{
				m_lineVector.x = 1.0f;
			}
			else
			{
				m_lineVector.x = -1.0f;
			}
			m_lineVector.y = 0.0f;
		}
		else if (DirY == 0)
		{
			if (DirX > 0)
			{
				m_lineVector.y = -1.0f;
			}
			else
			{
				m_lineVector.y = 1.0f;
			}
			m_lineVector.x = 0.0f;
		}
		else
		{
			if (DirY < 0)
			{
				m_lineVector.x = -1.0f;
				m_lineVector.y = (DirX / DirY);
			}
			else if (DirY > 0)
			{
				m_lineVector.x = 1.0f;
				m_lineVector.y = -(DirX / DirY);
			}
		}
		m_lineVector.Normalize();
		m_lineVector.x *= width;
		m_lineVector.y *= width;
        
        
		Vertex * vertexes = m_LineMesh.GetVertexes();
        
		vertexes[0].x = ul.x - m_lineVector.x;
		vertexes[0].y = ul.y - m_lineVector.y;
		vertexes[1].x = ul.x + m_lineVector.x;
		vertexes[1].y = ul.y + m_lineVector.y;
		vertexes[2].x = dr.x - m_lineVector.x;
		vertexes[2].y = dr.y - m_lineVector.y;
		vertexes[3].x = dr.x + m_lineVector.x;
		vertexes[3].y = dr.y + m_lineVector.y;
        
		vertexes[0].u = 0; vertexes[0].v = 0;
		vertexes[1].u = 1; vertexes[1].v = 0;
		vertexes[2].u = 0; vertexes[2].v = 1;
		vertexes[3].u = 1; vertexes[3].v = 1;
        
        color.Normalize();
		//unsigned int clr = color.GetPackedColor();
		vertexes[0].color = vertexes[1].color = vertexes[2].color = vertexes[3].color = color;
		DrawMesh(nullptr, &m_LineMesh);
	}

	void CRender::ChangeState(RenderActions renderAction)
	{
		if (m_PrevAction == renderAction)return;		
		m_PrevAction = renderAction;
		FlushVertexBuffer();

		switch (renderAction)
		{
		case RA_Line:
						break;
		case RA_Batch:
						break;
		case RA_3D:			
				
			break;
		}
	}

	void CRender::DrawBatch()
	{
		FlushVertexBuffer(true);
		//m_PrevTexture = NULL;
    }

	void CRender::SetBlendMode(BlendMode mode)
	{
		if (mode == m_BlendMode)return;
		m_BlendMode = mode;
		FlushVertexBuffer();
		if (mode == BlendMode::BLEND)
		{
            glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		}
		else if (mode == BlendMode::ADD)
		{
			glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		}
		else if (mode == BlendMode::MOD)
		{
			glBlendFunc(GL_ONE_MINUS_DST_COLOR,GL_ONE);
		}
        else if (mode == BlendMode::SCREEN)
		{
			glBlendFunc(GL_DST_COLOR, GL_ONE);
		}
    }

	void CRender::End()
	{
		FlushVertexBuffer(false);		
	}
    
    /* TODO: implement stencil */
    
    /*
     void StencilEnable()							{m_Device->SetRenderState(D3DRS_STENCILENABLE, true);}
     void StencilDisable()							{m_Device->SetRenderState(D3DRS_STENCILENABLE, false);}
     void StencilClear(char bt = 0)					{m_Device->Clear(0, NULL, D3DCLEAR_STENCIL, 0xffffffff,1.0f,0);}
     void StencilLevelSet(char bt)					{m_Device->SetRenderState(D3DRS_STENCILREF, bt);}
     void StencilSetCmpFunc(CompareType type)		{m_Device->SetRenderState(D3DRS_STENCILFUNC, type);}
     void StencilSetPasOpFunc(StencilOperation op)	{m_Device->SetRenderState(D3DRS_STENCILPASS, op);}
     void StencilSetFailOpFunc(StencilOperation op)	{m_Device->SetRenderState(D3DRS_STENCILFAIL, op);}
     
     */
    void CRender::StencilEnable()
    {
        glEnable(GL_STENCIL_TEST);
		stencilFailSet = false;
		stencilPassSet = false;
        
    }
    void CRender::StencilDisable()
    {
        glDisable(GL_STENCIL_TEST);
    }
    void CRender::StencilClear(char bt)
    {
        glClearStencil(0x1);
        glClear(GL_STENCIL_BUFFER_BIT);
    }
    void CRender::StencilLevelSet(char bt)
    {
        stencilLevel = bt;
    }
    void CRender::StencilSetCmpFunc(CompareType type)
    {
        glStencilFunc(type, (int)stencilLevel,1);
    }
    void CRender::StencilSetPasOpFunc(StencilOperation op)
    {
        stencilPass = op;
		stencilPassSet = true;
		if (stencilFailSet)
        {
            glStencilOp(stencilFail,GL_KEEP, stencilPass);
        }
        
    }
    void CRender::StencilSetFailOpFunc(StencilOperation op)
    {
        stencilFail = op;
		stencilFailSet = true;
        if (stencilPassSet)
        {
			glStencilOp(stencilFail, GL_KEEP, stencilPass);
        }
    }
}
#endif