#ifdef NBG_WIN32
#include <windows.h>

#include "System.h" 
#include <Framework.h>
#include <Shlobj.h>
#include <psapi.h>

#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")



namespace NBG
{
	TCHAR* GetThisPath(TCHAR* dest, size_t destSize)
	{
		if (!dest) return NULL;
		if (MAX_PATH > destSize) return NULL;

		DWORD length = GetModuleFileName(NULL, dest, destSize);
		PathRemoveFileSpec(dest);
		return dest;
	}


	//==============================================================================
	CSystem::CSystem()
	{

	}

	//==============================================================================
	CSystem::~CSystem()
	{

	}

	void CSystem::Init()
	{
		m_Locale = "";

		m_AppPath = L"";


		TCHAR dest[MAX_PATH];
		GetThisPath(dest, MAX_PATH);

		m_AppPath = dest;


		///Detect run from VS Debugger
		if (IsDebuggerPresent())
		{
			m_AppPath += L"\\..\\..\\Resources\\";
			if (!g_FileSystem->IsDirExists(m_AppPath))
			{
				m_AppPath = dest;
				m_AppPath += L"\\Resources\\";
			}
		}
		else
		{
			m_AppPath += L"\\Resources\\";
			if (!g_FileSystem->IsDirExists(m_AppPath))
			{
				m_AppPath = dest;
				m_AppPath += L"\\..\\..\\Resources\\";
			}
		}

		m_SystemResolution.x = GetSystemMetrics(SM_CXSCREEN);
		m_SystemResolution.y = GetSystemMetrics(SM_CYSCREEN);
	}

	//==============================================================================
	std::wstring CSystem::GetSavePath(const std::string &company, const std::string &product, const std::string &publisher)
	{
		std::wstring res = g_FileSystem->GetSaveDir();
		if (publisher.empty() == false)
		{
			res += L"/" + pugi::as_wide(publisher);
			g_FileSystem->CreateDirectory(res);
		}
		res += L"/" + pugi::as_wide(company);
		g_FileSystem->CreateDirectory(res);
		res += L"/" + pugi::as_wide(product);
		g_FileSystem->CreateDirectory(res);
		res += L"/";
		return res;
	}

	//==============================================================================
	std::wstring CSystem::GetDocumentsPath()
	{
		wchar_t data[256];
		SHGetSpecialFolderPath(0, &data[0], CSIDL_MYDOCUMENTS, false);		
		return data;
	}

	//==============================================================================
	std::string &CSystem::GetLocale()
	{
		if (m_Locale == "")
		{
			g_Config->GetValue("locale", m_Locale);
		}
		return m_Locale;
	}

	//==============================================================================
	void CSystem::UpdateSize(Vector & windowSize, Vector & logicalSize, bool isFullscreen)
	{
		logicalSize = g_GameApplication->GetLogicalSize();

		m_SystemResolution = windowSize;

		m_ResolutionScale.y = m_SystemResolution.y / logicalSize.y;
		m_ResolutionScale.x = m_ResolutionScale.y;

		m_GameResolution.x = logicalSize.x * m_ResolutionScale.x;
		m_GameResolution.y = logicalSize.y * m_ResolutionScale.y;

		m_GameBounds = m_SystemResolution / m_ResolutionScale;
		if (m_GameBounds.y > logicalSize.y)
		{
			m_GameBounds.y = logicalSize.y;
		}

		if (m_SystemResolution.x / m_SystemResolution.y == 1.25f) //5x4 resolutions
		{
			m_ResolutionScale.x = m_SystemResolution.x / logicalSize.x;
			m_ResolutionScale.y = m_SystemResolution.y / logicalSize.y;

			m_GameResolution.x = logicalSize.x * m_ResolutionScale.x;
			m_GameResolution.y = logicalSize.y * m_ResolutionScale.y;

			m_GameBounds = m_GameResolution / m_ResolutionScale;
		}
		else if (m_GameBounds.x < 1024)
		{
			m_ResolutionScale.x = m_SystemResolution.x / logicalSize.x;
			m_ResolutionScale.y = m_ResolutionScale.x;

			m_GameResolution.x = logicalSize.x * m_ResolutionScale.x;
			m_GameResolution.y = logicalSize.y * m_ResolutionScale.y;


			m_GameBounds = m_GameResolution / m_ResolutionScale;
		}
		m_XOffset = (m_GameResolution.x - m_SystemResolution.x) / 2.0f;
		m_YOffset = (m_GameResolution.y - m_SystemResolution.y) / 2.0f;
	}

	//==============================================================================
	void CSystem::ConvertMousePos(Vector & vec)
	{
		const auto & windowSize = g_GameApplication->GetWindowSize();
		float _x = windowSize.x / m_GameResolution.x;
		float _y = windowSize.y / m_GameResolution.y;
		vec.x -= m_GameResolution.x / 2.0f;
		vec.y -= m_GameResolution.y / 2.0f;

		vec.x += m_XOffset;
		vec.y += m_YOffset;
		vec.x *= _x;
		vec.y *= _y;

		if (vec.y < -m_GameBounds.y / 2.0f)
		{
			vec.y = -m_GameBounds.y / 2.0f;
		}
		else if (vec.y > m_GameBounds.y / 2.0f)
		{
			vec.y = m_GameBounds.y / 2.0f;
		}
	}

	//==============================================================================
	bool CSystem::IsMobileSystem()
	{
		return false;
	}

	//==============================================================================
	void CSystem::ShowCursor(const bool show)
	{
		if (show)
		{
			SetCursor((HCURSOR)LoadCursor(NULL, IDC_ARROW));
			ULONG_PTR hc = GetClassLong(g_GameApplication->GetOperatingSystem()->GetWindow(), GCL_HCURSOR);
		}
		else
		{
			SetCursor(NULL);
		}

	}

	//////////////////////////////////////////////////////////////////////////
	void CSystem::Sleep(const int miliseconds)
	{
		::Sleep(miliseconds);
	}

	//////////////////////////////////////////////////////////////////////////
	int CSystem::GetMemoryUsage()
	{
		PROCESS_MEMORY_COUNTERS pmc;
		GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));
		return pmc.WorkingSetSize / 1024 / 1024;
	}

	//////////////////////////////////////////////////////////////////////////
	bool CSystem::SetWallpaper(const std::wstring &path)
	{
		std::wstring str = StringUtils::StringReplace(path, std::wstring(L"/"), std::wstring(L"\\"), true);
		
		auto pathW = str.c_str();
		wchar_t* tempWide = const_cast< wchar_t* >(pathW);
		
		bool res = SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, tempWide, SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
		return res;
	}
}
#endif
