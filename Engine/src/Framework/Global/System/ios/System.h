#ifndef NBG_FRAMEWORK_GLOBAL_SYSTEM_IOS
#define NBG_FRAMEWORK_GLOBAL_SYSTEM_IOS

#ifdef NBG_IOS

#include <string>
#include <ostream>

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Vector.h>

namespace NBG
{
	/** @brief Класс, реализующий доступ к информации о системе.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @copyright 2013 New Bridge Games
	*
	*/
	class CSystem
	{
	public:
        CSystem();
		~CSystem();

		
		void Init(){};

		std::string &GetLocale();		

		///*** FILE SYSTEM ***///
		bool IsFileExists(const std::string &file);
		bool IsFolderExists(const std::string &folder);
		void SysDeleteFile(const std::string &file);
        bool SysCreateFolder(const std::string &path);
        std::wstring & GetAppPath(){return m_AppPath;};
        std::wstring GetDocumentsPath(){return L"";};
        std::wstring GetSavePath(const std::string & company,const std::string & product, const std::string &publisher);
 
		STRING_VECTOR GetFolderContents(const std::string &folder);

		void UpdateSize(Vector & windowSize, Vector & logicalSize, bool isFullscreen);

		void ConvertMousePos(Vector & vec);

		/// Получить разрешение экрана.
		Vector GetSystemResolution()
		{
			return m_SystemResolution;
		};

		/// Установить разрешение экрана.
		void SetSystemResolution(const int width, const int height)
		{
			m_SystemResolution = Vector(width,height);
		};

		/// Получение разрешение экрана.
		Vector GetGameResolution()
		{
			return m_GameResolution;
		};

		/// Получить коэффициент масштабирования экрана
		Vector GetResolutionScale()
		{
			return m_ResolutionScale;
		};

		/// Получить границы экрана.
		Vector GetGameBounds()
		{
			return m_GameBounds;
		}

		/// Получить смещение по X для центра.
		float GetXOffset()
		{
			return m_XOffset;
		};

		/// Получить смещение по Y для центра.
		float GetYOffset()
		{
			return m_YOffset;
		};
        
        int GetMemoryUsage()
        {
            return 1;
        }
        

		bool IsMobileSystem();
        void ShowCursor(const bool show){};

        void SetPortraitMode(const bool portrait){m_IsPortrait = portrait;};
        
        void Sleep(const int miilseconds);
        
        bool SetWallpaper(const std::wstring &path)
        {
            return true;
        }

	private:
		
        
        bool m_IsPortrait;

		/// Системное разрешение экрана.
		Vector m_SystemResolution;
		/// Коэффициент масштабирования игры.
		Vector m_ResolutionScale;
		/// Игровое разрешение экрана.
		Vector m_GameResolution; 
		/// Игровые границы экрана.
		Vector m_GameBounds;
		/// Получить смещение по X для центра.
		float m_XOffset;
		/// Получить смещение по Y для центра.
		float m_YOffset;


		std::wstring m_AppPath;
		std::string m_Locale;
	};
}
#endif
#endif // NBG_FRAMEWORK_GLOBAL_SYSTEM_IOS
