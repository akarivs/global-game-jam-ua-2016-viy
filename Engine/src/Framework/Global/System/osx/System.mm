#ifdef NBG_OSX


#include "System.h" 
#include <Framework.h>

#import <Foundation/Foundation.h>
#include <CoreFoundation/CoreFoundation.h>
#import <AppKit/AppKit.h>

namespace NBG
{
	//==============================================================================
	CSystem::CSystem()
	{
		m_Locale = "";

		m_AppPath = L"";
        
        static char path[1024];
        CFBundleRef mainBundle = CFBundleGetMainBundle();
        assert(mainBundle);
        
        CFURLRef mainBundleURL = CFBundleCopyBundleURL(mainBundle);
        assert(mainBundleURL);
        
        CFStringRef cfStringRef = CFURLCopyFileSystemPath( mainBundleURL, kCFURLPOSIXPathStyle);
        assert(cfStringRef);
        
        CFStringGetCString(cfStringRef, path, 1024, kCFStringEncodingUTF8);
        CFRelease(mainBundleURL);
        CFRelease(cfStringRef);
        path[1023] = 0;
        
        strcat( path, "/" );
        
        m_AppPath = pugi::as_wide(path);
        m_AppPath += L"Contents/Resources/";
        
        std::string pathS = path;
        pathS += "Contents/Resources/";

        
        BOOL result = [[NSFileManager defaultManager] changeCurrentDirectoryPath: [ NSString stringWithUTF8String:pathS.c_str() ] ];
        
        if (result == NO)
        {
            CONSOLE("FAILED TO CHANGE DIRECTORY!");
        }
    }

	//==============================================================================
	CSystem::~CSystem()
	{

	}	

	//==============================================================================
	std::string &CSystem::GetLocale()
	{
        if (m_Locale == "")
		{
            bool appstore = false;
            g_Config->GetValue("appstore",appstore);
            if (appstore)
            {
                NSLocale *locale = [NSLocale currentLocale];
                NSString * localLanguage = [locale objectForKey:NSLocaleLanguageCode];
                m_Locale = [localLanguage UTF8String];                            
            }
            else
            {
                g_Config->GetValue("locale",m_Locale);
            }
        }
		return m_Locale;
	}

	//==============================================================================
	bool CSystem::IsFileExists(const std::string &file)
	{
		/*SDL_RWops * fileOp = SDL_RWFromFile(file.c_str(),"r");
		if (fileOp)
		{
			fileOp->close(fileOp);
			return true;
		}*/
		return false;
	}

	//==============================================================================
	bool CSystem::IsFolderExists(const std::string &folder)
	{
		std::string path = folder;
		/*DWORD ftyp = GetFileAttributesA(path.c_str());
		if (ftyp == INVALID_FILE_ATTRIBUTES)
			return false;  //something is wrong with your path!
		if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
			return true;   // this is a directory!*/
        return true;
	}

	//==============================================================================
	void CSystem::SysDeleteFile(const std::string &file)
	{
		//unlink(file.c_str());
	}

	//==============================================================================
	STRING_VECTOR CSystem::GetFolderContents(const std::string &folder)
	{
		std::vector<std::string> dirContents;
		return dirContents;
	}

	//==============================================================================
	void CSystem::UpdateSize(Vector & windowSize, Vector & logicalSize, bool isFullscreen)
	{
		logicalSize = g_GameApplication->GetLogicalSize();
        
		m_SystemResolution = windowSize;
        
        
		m_ResolutionScale.y = m_SystemResolution.y/logicalSize.y;
		m_ResolutionScale.x = m_ResolutionScale.y;
        
		m_GameResolution.x = logicalSize.x * m_ResolutionScale.x;
		m_GameResolution.y = logicalSize.y * m_ResolutionScale.y;
        
		m_GameBounds = m_SystemResolution / m_ResolutionScale;
		if (m_GameBounds.y > logicalSize.y)
		{
			m_GameBounds.y = logicalSize.y;
		}
        
		if (m_GameBounds.x < 1024)
		{
			m_ResolutionScale.x = m_SystemResolution.x / logicalSize.x;
			m_ResolutionScale.y = m_ResolutionScale.x;
            
			m_GameResolution.x = logicalSize.x * m_ResolutionScale.x;
			m_GameResolution.y = logicalSize.y * m_ResolutionScale.y;
            
            
			m_GameBounds = m_GameResolution / m_ResolutionScale;
		}
		m_XOffset = (m_GameResolution.x - m_SystemResolution.x)/2.0f;
		m_YOffset = (m_GameResolution.y - m_SystemResolution.y)/2.0f;

	}

	//==============================================================================
	void CSystem::ConvertMousePos(Vector & vec)
	{
		const auto & windowSize = g_GameApplication->GetWindowSize();
		float _x = windowSize.x / m_GameResolution.x;
		float _y = windowSize.y / m_GameResolution.y;
		vec.x -= m_GameResolution.x / 2.0f;
		vec.y -= m_GameResolution.y / 2.0f;
        
		vec.x += m_XOffset;
		vec.y += m_YOffset;
		vec.x *= _x;
		vec.y *= _y;
        
		if (vec.y < -m_GameBounds.y / 2.0f)
		{
			vec.y = -m_GameBounds.y / 2.0f;
		}
		else if (vec.y > m_GameBounds.y / 2.0f)
		{
			vec.y = m_GameBounds.y / 2.0f;
		}	}

	//==============================================================================
	bool CSystem::IsMobileSystem()
	{		
		return false;
	}
    
    //==============================================================================
    std::wstring CSystem::GetSavePath(const std::string & company,const std::string & product, const std::string & publisher)
    {
        m_GameName = product;
        
        static std::string buffer;
        char path[1024];
        CFStringGetCString( (__bridge CFStringRef)NSHomeDirectory() , path , sizeof(path) , kCFStringEncodingUTF8 );
        //NSString* ns_path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@""];
        //buffer = [ns_path UTF8String];
        buffer = path;
        
        
        std::string res = buffer + "/Library/";
   		g_FileSystem->CreateDirectory(res);
        res += "Application Support/";
        g_FileSystem->CreateDirectory(res);
        if (publisher.empty() == false)
		{
			res += "/" + publisher;
			g_FileSystem->CreateDirectory(res);
		}
        res += "/"+company;
		g_FileSystem->CreateDirectory(res);
        res += "/"+product;
        g_FileSystem->CreateDirectory(res);
		res += "/";
        return pugi::as_wide(res.c_str());
        return L"";
    }
    
    //==============================================================================
    std::wstring CSystem::GetDocumentsPath()
    {
        static std::string buffer;
        char path[1024];
        CFStringGetCString( (__bridge CFStringRef)NSHomeDirectory() , path , sizeof(path) , kCFStringEncodingUTF8 );
        //NSString* ns_path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@""];
        //buffer = [ns_path UTF8String];
        buffer = path;
        
        
        std::string res = buffer + "/Documents/";
        g_FileSystem->CreateDirectory(res);
        res += m_GameName;
        g_FileSystem->CreateDirectory(res);
        return pugi::as_wide(res.c_str());
    }
    
    //==============================================================================
    std::wstring CSystem::GetPicturesPath()
    {
        static std::string buffer;
        char path[1024];
        CFStringGetCString( (__bridge CFStringRef)NSHomeDirectory() , path , sizeof(path) , kCFStringEncodingUTF8 );
        //NSString* ns_path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@""];
        //buffer = [ns_path UTF8String];
        buffer = path;
        
        
        std::string res = buffer + "/Pictures/";
        g_FileSystem->CreateDirectory(res);
        res += m_GameName;
        g_FileSystem->CreateDirectory(res);
        return pugi::as_wide(res.c_str());
    }
    
    //==============================================================================
    std::wstring CSystem::GetMusicPath()
    {
        static std::string buffer;
        char path[1024];
        CFStringGetCString( (__bridge CFStringRef)NSHomeDirectory() , path , sizeof(path) , kCFStringEncodingUTF8 );
        //NSString* ns_path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@""];
        //buffer = [ns_path UTF8String];
        buffer = path;
        
        
        std::string res = buffer + "/Music/";
        g_FileSystem->CreateDirectory(res);
        res += m_GameName;
        g_FileSystem->CreateDirectory(res);
        return pugi::as_wide(res.c_str());
    }

    
    //////////////////////////////////////////////////////////////////////////
    bool CSystem::SetWallpaper(const std::wstring &path)
    {
        std::wstring str = path;
        std::string pathNew = pugi::as_utf8(str);
        
        bool res = true;
        
        auto pathW = str.c_str();
        wchar_t* tempWide = const_cast< wchar_t* >(pathW);
        
        NSString *errorMessage = [NSString stringWithCString:pathNew.c_str()
                                                    encoding:[NSString defaultCStringEncoding]];
        
        
        NSWorkspace *sws = [NSWorkspace sharedWorkspace];
        NSURL *image = [NSURL fileURLWithPath:errorMessage];
        NSError *err = nil;
        for (NSScreen *screen in [NSScreen screens]) {
            NSDictionary *opt = [sws desktopImageOptionsForScreen:screen];
            [sws setDesktopImageURL:image forScreen:screen options:opt error:&err];
            if (err) {
                NSLog(@"%@",[err localizedDescription]);
                res = false;
            }else{
                NSNumber *scr = [[screen deviceDescription] objectForKey:@"NSScreenNumber"];
                
            }
        }
        return res;
    }
    
    ///
    void CSystem::Sleep(const int miilseconds)
    {
        //sleep(miliseconds);
    }
    
    ///
    void CSystem::ShowCursor(const bool show)
    {
        SDL_ShowCursor(show);
    }
}
#endif
