#ifndef NBG_CORE_GAME_PACK
#define NBG_CORE_GAME_PACK

#include <map>
#include <vector>
#include <stdio.h>
#include "FileSystem.h"

namespace NBG
{
	/** @brief �����, ����������� ������ � ����� ������� ������.
	*
	* @author Vadim Simonov <akari.vs@gmail.com>
	* @description ���� ������� ���, ����� � ������ ������� ���� ���� � ���. ���� ���� ���, ��� � ��� ��� ������ �����, ������ ������ � �������� �������.
	* @copyright 2013-2014 New Bridge Games
	*
	*/
	class CGamePack
	{
	public:
		/// @name ������������
		CGamePack(void);
		~CGamePack(void);	

		/// ������������� ����.
		void Init();

		/// ���������� �� ���.
		bool PackExists()
		{
			return m_PackExists;
		};

		/// ������� ���� �� ����.
		CFileSystem::FileContainer ReadFile(const std::string &fileName);

		/// �������� ������ ������ � ���������� ������ ����.
		std::vector<std::string> GetDirectoryContents(const std::string &path);

	protected:
		/// ���������� � ��������� ����� � ����.
		struct PackedInfo
		{
			int size;
			int offset;
		};

		/// ���� �� ���.
		bool m_PackExists; 

		/// ����� ������ ����.
		std::map<std::string, PackedInfo>m_FileData;

		/// �������� ��� ����� ������ ����.
		std::map<std::string, PackedInfo>::iterator m_FileDataIter;

		/// ����� ���������� ���� � �� �����������.
		std::map<std::string, std::vector<std::string> >m_Directories;

		/// ��������� �� ���.
		FILE * m_FilePointer;
	};
}
#endif // NBG_CORE_GAME_PACK