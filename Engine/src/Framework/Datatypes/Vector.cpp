#include "Vector.h"
#include <math.h>

#include "IntVector.h"

namespace NBG
{
//==============================================================================
Vector::Vector()
{
    x = 0.0f;
    y = 0.0f;
    z = 0.0f;
}

//==============================================================================
Vector::Vector(const float &x, const float &y)
{
    this->x = x;
    this->y = y;
    this->z = 0.0f;
}

//==============================================================================
Vector::Vector(const float &x, const float &y, const float &z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}

//==============================================================================
Vector::Vector(IntVector vec)
{
	this->x = vec.x;
	this->y = vec.y;
	this->z = vec.z;
}

//==============================================================================
Vector::~Vector()
{

}

/* ��������� */
//==============================================================================
bool Vector::operator == (const IntVector &vec)
{
	return (this->x == vec.x && this->y == vec.y && this->z == vec.z);
}

//==============================================================================
bool Vector::operator == (const Vector &vec)
{
	return (float(this->x) == vec.x && float(this->y) == vec.y && float(this->z) == vec.z);
}

//==============================================================================
bool Vector::operator == (const float scalar)
{
	return (this->x == scalar && this->y == scalar && this->z == scalar);
}

//==============================================================================
float Vector::GetLength()
{
    return sqrt(x*x+y*y+z*z);
}

//==============================================================================
void Vector::Normalize()
{
    float len = GetLength();
    if (len == 0.0f)return;
    x /= len;
    y /= len;
    z /= len;
}

//==============================================================================
float Vector::DotProduct(const Vector &in)
{
    return (x*in.x)+(y*in.y)+(z*in.z);
}


//==============================================================================
Vector Vector::operator + (const Vector& vec)
{
    return Vector( this->x + vec.x, this->y + vec.y, this->z + vec.z);
}

//==============================================================================
Vector Vector::operator + (const IntVector& vec)
{
	return Vector(this->x + vec.x, this->y + vec.y, this->z + vec.z);
}

//==============================================================================
Vector Vector::operator + (const float scal)
{
    return Vector( this->x + scal, this->y + scal, this->z + scal);
}

//==============================================================================
Vector Vector::operator - (const Vector& vec)
{
    return Vector( this->x - vec.x, this->y - vec.y, this->z - vec.z );
}

//==============================================================================
Vector Vector::operator - (const IntVector& vec)
{
	return Vector(this->x - vec.x, this->y - vec.y, this->z - vec.z);
}

//==============================================================================
Vector Vector::operator - (const float scal)
{
    return Vector( this->x - scal, this->y - scal, this->z - scal );
}

//==============================================================================
Vector Vector::operator * (const Vector& vec)
{
    return Vector( this->x * vec.x, this->y * vec.y, this->z * vec.z );
}

//==============================================================================
Vector Vector::operator * (const IntVector& vec)
{
	return Vector(this->x * vec.x, this->y * vec.y, this->z * vec.z);
}

//==============================================================================
Vector Vector::operator * (const float scal)
{
    return Vector( this->x * scal, this->y * scal, this->z * scal);
}

//==============================================================================
Vector Vector::operator / (const Vector& vec)
{
	return Vector( this->x / vec.x, this->y / vec.y, this->z / vec.z );
}

//==============================================================================
Vector Vector::operator / (const IntVector& vec)
{
	return Vector(this->x / vec.x, this->y / vec.y, this->z / vec.z);
}

//==============================================================================
Vector Vector::operator / (const float scalar)
{
	return Vector( this->x / scalar, this->y / scalar, this->z / scalar );
}

//==============================================================================
void Vector::operator = (const Vector &vec)
{
	this->x = vec.x;
	this->y = vec.y;
	this->z = vec.z;
}

//==============================================================================
void Vector::operator = (const IntVector &vec)
{
	this->x = vec.x;
	this->y = vec.y;
	this->z = vec.z;
}

//==============================================================================
void Vector::operator = (const float scalar)
{
	x = y = z = scalar;
}

//==============================================================================
void Vector::operator += (const Vector& vec)
{
	x += vec.x;
	y += vec.y;
	z += vec.z;
}

//==============================================================================
void Vector::operator += (const IntVector& vec)
{
	x += vec.x;
	y += vec.y;
	z += vec.z;
}

//==============================================================================
void Vector::operator += (const float scalar)
{
	x += scalar;
	y += scalar;
	z += scalar;
}


//==============================================================================
void Vector::operator -= (const Vector& vec)
{
	x -= vec.x;
	y -= vec.y;
	z -= vec.z;
}

//==============================================================================
void Vector::operator -= (const IntVector& vec)
{
	x -= vec.x;
	y -= vec.y;
	z -= vec.z;
}

//==============================================================================
void Vector::operator -= (const float scalar)
{
	x -= scalar;
	y -= scalar;
	z -= scalar;
}

//==============================================================================
void Vector::operator *= (const Vector& vec)
{
	x *= vec.x;
	y *= vec.y;
	z *= vec.z;
}

//==============================================================================
void Vector::operator *= (const IntVector& vec)
{
	x *= vec.x;
	y *= vec.y;
	z *= vec.z;
}

//==============================================================================
void Vector::operator *= (const float scalar)
{
	x *= scalar;
	y *= scalar;
	z *= scalar;
}

//==============================================================================
void Vector::operator /= (const Vector& vec)
{
	x /= vec.x;
	y /= vec.y;
	z /= vec.z;
}

//==============================================================================
void Vector::operator /= (const IntVector& vec)
{
	x /= vec.x;
	y /= vec.y;
	z /= vec.z;
}

//==============================================================================
void Vector::operator /= (const float scalar)
{
	x /= scalar;
	y /= scalar;
	z /= scalar;
}
}
