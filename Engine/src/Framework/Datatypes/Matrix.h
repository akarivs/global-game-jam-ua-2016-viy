#ifndef NBG_CORE_DATATYPES_MATRIX
#define NBG_CORE_DATATYPES_MATRIX

#include <string>
#include <ostream>

#include <Framework/Datatypes/Transform.h>
#include <Framework/Datatypes/Vector.h>

namespace NBG
{
/** @brief Класс, реализующий работу матрицы 2х3
 *
 * @author Vadim Simonov <akari.vs@gmail.com>
 * @copyright 2013 New Bridge Games
 *
 */
class Matrix
{
public:
    /// @name Конструкторы
    Matrix();
    ~Matrix();

    /** @brief Заполняет матрицу значениями по умолчанию
     *
     *
     */
	inline void Identity()
	{
		m[0] = 1.0;
		m[1] = 0.0;
		m[2] = 0.0;
		m[3] = 1.0;
		m[4] = 0.0;
		m[5] = 0.0;
	}

    /** @brief Создаёт матрицу из трансформации
     *
     *
     */
	void CreateFromTransform(const Transform &trans, const bool useHotSpot = false);

    /** @brief Перемножает матрицы
     *
     *
     */
	void Multiply(Matrix * second);


	/** @brief Равны ли матрицы
	*
	*
	*/
	bool Equals(Matrix * second);



    /** @brief Перемещает матрицу
     *
     *
     */
	void Translate(const float x, const float y);

    /** @brief Вращает матрицу
     *
     *
     */
	void RotateYawPitchRoll(const float roll);

    /** @brief Масштабирует матрицу
     *
     *
     */
	void Scale(const float x, const float y);


	void Transpose();
	void Invert();

	float Determinant();

    /** @brief Применяет матрицу к точке
     *
     *
     */
	inline Vector & TransformPoint(const Vector &point)
	{
		m_Res.x = m[0] * point.x + m[2] * point.y + m[4];
		m_Res.y = m[1] * point.x + m[3] * point.y + m[5];
		return m_Res;
	}


	inline void TransformPoint(float &x, float &y)
	{
		m_Res.x = m[0] * x + m[2] * y + m[4];
		m_Res.y = m[1] * x + m[3] * y + m[5];
		x = m_Res.x;
		y = m_Res.y;
	}

    float m[6];
private:
	Vector m_Res;


};

}
#endif // NBG_CORE_DATATYPES_MATRIX
