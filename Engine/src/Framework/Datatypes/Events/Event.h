#ifndef C_EVENT
#define C_EVENT

// Event class

#include <functional>
#include <memory>

#include <Framework/Datatypes/Vector.h>


namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
			class CBaseWidget;
		}
	}

	enum class MouseButton;
}

enum EBaseEventTypes
{
	Event_KeyDown,
	Event_KeyUp,
	Event_MouseDown,
	Event_MouseUp,
	Event_MouseMove,
	Event_MouseMoveOutside,
	Event_MouseWheel,
	Event_MouseUpOutside,
	Event_Over,
	Event_Out,
};

class CEvent
{
public:
	CEvent(const int type) :
		type(type)
	{
	}
	const int type;
	NBG::optimus::ui::CBaseWidget * target;
	NBG::Vector mousePos;
	NBG::MouseButton mouseButton;
	int key;
	int wheelDelta;	
	float zoom;
	NBG::Vector zoomCenter;
};

#define eventFunctionPtr std::function<void(const CEvent & event)>

class CEventListener
{
public:
	static CEventListener * Create(eventFunctionPtr ptr)
	{
		auto ret = new CEventListener(ptr);
		return ret;
	}
	eventFunctionPtr functionPtr;
private:
	CEventListener(eventFunctionPtr ptr)
	{
		functionPtr = ptr;
	}
};
#endif