#ifndef C_EVENT_DISPATCHER
#define C_EVENT_DISPATCHER
#include "Event.h"

#include <map>
#include <list>

class CEventDispatcher
{
private:
	std::map<const int, std::map<int, std::list<CEventListener*> > > eventHandlerList;
public:
	
	//////////////////////////////////////////////////////////////////////////
	void AddEventListener(const int type, CEventListener * listener, int priority = 0)
	{		
		eventHandlerList[type][priority].push_back(listener);
	}

	//////////////////////////////////////////////////////////////////////////
	bool HasEventListener(const int type)
	{
		return (eventHandlerList.find(type) != eventHandlerList.end());		
	}

	//////////////////////////////////////////////////////////////////////////
	void DispatchEvent(const CEvent &event)
	{		
		if (!HasEventListener(event.type))
		{
			return;
		}

		
		auto &allFunctions = eventHandlerList[event.type];	
		
		for (auto i = allFunctions.rbegin(); i != allFunctions.rend(); ++i) 
		{
			auto &funcList = i->second;			
			for (auto f = funcList.begin(); f != funcList.end(); ++f)
			{
				(*f)->functionPtr(event);
			}				
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void RemoveEventListener(const int type, CEventListener * listener, bool useCapture = false)
	{	
		if (!HasEventListener(type))
		{
			return;
		}
		auto &allFunctions = eventHandlerList[type];		
		if (allFunctions.empty())
		{
			eventHandlerList.erase(type);
			return;
		}
		auto iter = allFunctions.begin();
		while (iter != allFunctions.end()) 
		{			
			iter->second.remove(listener);		
			if (iter->second.empty())
			{
				iter = allFunctions.erase(iter);
			}
			else
			{
				iter++;
			}
		}
		if (allFunctions.empty())
		{
			eventHandlerList.erase(type);
		}
	}
};
#endif