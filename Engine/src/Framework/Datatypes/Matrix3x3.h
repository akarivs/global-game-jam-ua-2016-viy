#ifndef NBG_CORE_DATATYPES_MATRIX_3x3
#define NBG_CORE_DATATYPES_MATRIX_3x3

#include <string>
#include <ostream>

#include <Framework/Datatypes/Transform.h>
#include <Framework/Datatypes/Vector.h>

namespace NBG
{
/** @brief Класс, реализующий работу матрицы 3х3
 *
 * @author Vadim Simonov <akari.vs@gmail.com>
 * @copyright 2013 New Bridge Games
 *
 */
class Matrix3x3
{
public:
    /// @name Конструкторы
	Matrix3x3();
	~Matrix3x3();

    /** @brief Заполняет матрицу значениями по умолчанию
     *
     *
     */
    void Identity();

    /** @brief Создаёт матрицу из трансформации
     *
     *
     */
    void CreateFromTransform(const Transform &trans, bool useHotSpot = false);

    /** @brief Перемножает матрицы
     *
     *
     */
	void Multiply(Matrix3x3 * second);

    /** @brief Перемещает матрицу
     *
     *
     */
    void Translate(const float x, const float y);

    /** @brief Вращает матрицу
     *
     *
     */
     void RotateYawPitchRoll(const float roll);

    /** @brief Масштабирует матрицу
     *
     *
     */
    void Scale(const float x, const float y);


	void Transpose();
	void Invert();

	float Determinant();

    /** @brief Применяет матрицу к точке
     *
     *
     */
	Vector & TransformPoint(const Vector &point);
	void TransformPoint(float &x, float &y);

    float m[9];
private:
	Vector m_Res;


};

}
#endif // NBG_CORE_DATATYPES_MATRIX
