#include "Matrix3x3.h"
#include <math.h>

#include <Framework/Utils/MathUtils.h>

namespace NBG
{

	Matrix3x3 __g_RotateMatrix3x3;
	Matrix3x3 __g_ScalingMatrix3x3;
	Matrix3x3 __g_TempMatrix3x3;
	Matrix3x3 __g_ResultMatrix3x3;
	Matrix3x3 __g_TranslationMatrix3x3;
	float __g_sinRoll3x3 = 0.0f;
	float __g_cosRoll3x3 = 0.0f;
	//==============================================================================
	Matrix3x3::Matrix3x3()
	{

	}

	//==============================================================================
	Matrix3x3::~Matrix3x3()
	{

	}

	//==============================================================================
	void Matrix3x3::Identity()
	{
		m[0] = 1.0;
		m[1] = 0.0;
		m[2] = 0.0;
		m[3] = 0.0;
		m[4] = 1.0;
		m[5] = 0.0;
		m[6] = 0.0;
		m[7] = 0.0;
		m[8] = 1.0;
	}

	//==============================================================================
	void Matrix3x3::CreateFromTransform(const Transform &trans, bool useHotSpot)
	{
		Identity();

		if (useHotSpot)
		{
			__g_TempMatrix3x3.Identity();
			__g_TempMatrix3x3.Translate(-trans.hx, -trans.hy);
			Multiply(&__g_TempMatrix3x3);
		}
		

		__g_TempMatrix3x3.Identity();
		__g_TempMatrix3x3.Scale(trans.scalex, trans.scaley);
		Multiply(&__g_TempMatrix3x3);

		if (trans.rz != 0.0f)
		{
			__g_TempMatrix3x3.Identity();
			__g_TempMatrix3x3.RotateYawPitchRoll(trans.rz);
			Multiply(&__g_TempMatrix3x3);
		}

		__g_TempMatrix3x3.Identity();
		__g_TempMatrix3x3.Translate(trans.x, trans.y);
		Multiply(&__g_TempMatrix3x3);
	}

	//////////////////////////////////////////////////////////////////////////
	void Matrix3x3::Transpose()
	{
		float tempM[9];
		tempM[0] = m[0];
		tempM[1] = m[3];
		tempM[2] = m[6];
		tempM[3] = m[1];
		tempM[4] = m[4];
		tempM[5] = m[7];
		tempM[6] = m[2];
		tempM[7] = m[5];
		tempM[8] = m[8];

		m[0] = tempM[0];
		m[1] = tempM[1];
		m[2] = tempM[2];
		m[3] = tempM[3];
		m[4] = tempM[4];
		m[5] = tempM[5];
		m[7] = tempM[6];
		m[8] = tempM[7];
		//m[9] = tempM[8];
	}

	//////////////////////////////////////////////////////////////////////////
	float Matrix3x3::Determinant()
	{
		return +m[0] * (m[4] * m[8] - m[7] * m[5])
			- m[1] * (m[3] * m[8] - m[6] * m[5])
			+ m[2] * (m[3] * m[7] - m[6] * m[4]);
	}

	//////////////////////////////////////////////////////////////////////////
	void Matrix3x3::Invert()
	{
		auto &res = __g_TempMatrix3x3.m;

		float det = Determinant();
		auto &tM = m;

		res[0] = tM[4] * tM[8] - tM[5] * tM[7];
		res[1] = -(tM[1] * tM[8] - tM[2] * tM[7]);
		res[2] = tM[1] * tM[5] - tM[2] * tM[4];

		res[3] = -(tM[3] * tM[8] - tM[5] * tM[6]);
		res[4] = tM[0] * tM[8] - tM[2] * tM[6];
		res[5] = -(tM[0] * tM[5] - tM[2] * tM[3]);

		res[6] = tM[3] * tM[7] - tM[4] * tM[6];
		res[7] = -(tM[0] * tM[1] - tM[6] * tM[7]);
		res[8] = tM[0] * tM[4] - tM[1] * tM[3];

		m[0] = res[0] / det;
		m[1] = res[1] / det;
		m[2] = res[2] / det;
		m[3] = res[3] / det;
		m[4] = res[4] / det;
		m[5] = res[5] / det;
		m[6] = res[6] / det;
		m[7] = res[7] / det;
		m[8] = res[8] / det;
//		m[9] = res[9] / det;
	}

	//==============================================================================
	void Matrix3x3::Multiply(Matrix3x3 * second)
	{
		auto & _m = __g_ResultMatrix3x3.m;
		const auto & _m1 = second->m;
		const auto & _m2 = this->m;

		const auto & _m1_0 = _m1[0];
		const auto & _m1_1 = _m1[1];
		const auto & _m1_2 = _m1[2];
		const auto & _m1_3 = _m1[3];
		const auto & _m1_4 = _m1[4];
		const auto & _m1_5 = _m1[5];
		const auto & _m1_6 = _m1[6];
		const auto & _m1_7 = _m1[7];
		const auto & _m1_8 = _m1[8];
		const auto & _m2_0 = _m2[0];
		const auto & _m2_1 = _m2[1];
		const auto & _m2_2 = _m2[2];
		const auto & _m2_3 = _m2[3];
		const auto & _m2_4 = _m2[4];
		const auto & _m2_5 = _m2[5];
		const auto & _m2_6 = _m2[6];
		const auto & _m2_7 = _m2[7];
		const auto & _m2_8 = _m2[8];

		_m[0] = _m1_0 * _m2_0 + _m1_1 * _m2_3 + _m1_2 * _m2_6;
		_m[1] = _m1_0 * _m2_1 + _m1_1 * _m2_4 + _m1_2 * _m2_7;
		_m[2] = _m1_0 * _m2_2 + _m1_1 * _m2_5 + _m1_2 * _m2_8;

		_m[3] = _m1_3 * _m2_0 + _m1_4 * _m2_3 + _m1_5 * _m2_6;
		_m[4] = _m1_3 * _m2_1 + _m1_4 * _m2_4 + _m1_5 * _m2_7;
		_m[5] = _m1_3 * _m2_2 + _m1_4 * _m2_5 + _m1_5 * _m2_8;

		_m[6] = _m1_6 * _m2_0 + _m1_7 * _m2_3 + _m1_8 * _m2_6;
		_m[7] = _m1_6 * _m2_1 + _m1_7 * _m2_4 + _m1_8 * _m2_7;
		_m[8] = _m1_6 * _m2_2 + _m1_7 * _m2_5 + _m1_8 * _m2_8;

		m[0] = _m[0];
		m[1] = _m[1];
		m[2] = _m[2];
		m[3] = _m[3];
		m[4] = _m[4];
		m[5] = _m[5];
		m[6] = _m[6];
		m[7] = _m[7];
		m[8] = _m[8];
	}



	//==============================================================================
	void Matrix3x3::Translate(const float x, const float y)
	{
		__g_TranslationMatrix3x3.Identity();
		__g_TranslationMatrix3x3.m[2] = x;
		__g_TranslationMatrix3x3.m[5] = y;
		Multiply(&__g_TranslationMatrix3x3);
	}

	//==============================================================================
	void Matrix3x3::RotateYawPitchRoll(const float roll)
	{
		__g_RotateMatrix3x3.Identity();

		__g_sinRoll3x3 = MathUtils::FastSin(roll);
		__g_cosRoll3x3 = MathUtils::FastCos(roll);
		__g_RotateMatrix3x3.m[0] = __g_cosRoll3x3;
		__g_RotateMatrix3x3.m[1] = -__g_sinRoll3x3;
		__g_RotateMatrix3x3.m[3] = __g_sinRoll3x3;
		__g_RotateMatrix3x3.m[4] = __g_cosRoll3x3;
		Multiply(&__g_RotateMatrix3x3);
	}

	//==============================================================================
	void Matrix3x3::Scale(const float x, const float y)
	{
		__g_ScalingMatrix3x3.Identity();
		__g_ScalingMatrix3x3.m[0] = x;
		__g_ScalingMatrix3x3.m[4] = y;
		Multiply(&__g_ScalingMatrix3x3);
	}

	//==============================================================================
	Vector & Matrix3x3::TransformPoint(const Vector & point)
	{
		m_Res.x = m[0] * point.x + m[1] * point.y + m[2];
		m_Res.y = m[3] * point.x + m[4] * point.y + m[5];
		return m_Res;
	}

	//////////////////////////////////////////////////////////////////////////
	//==============================================================================
	void Matrix3x3::TransformPoint(float &x, float &y)
	{
		m_Res.x = m[0] * x + m[1] * y + m[2];
		m_Res.y = m[3] * x + m[4] * y + m[5];
		x = m_Res.x;
		y = m_Res.y;
	}

}
