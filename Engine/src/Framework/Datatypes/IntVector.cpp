#include "IntVector.h"
#include <math.h>

namespace NBG
{
	//==============================================================================
	IntVector::IntVector()
	{
		x = 0.0f;
		y = 0.0f;
		z = 0.0f;
	}

	//==============================================================================
	IntVector::IntVector(const int &x, const int &y)
	{
		this->x = x;
		this->y = y;
		this->z = 0.0f;
	}

	//==============================================================================
	IntVector::IntVector(const int &x, const int &y, const int &z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	//==============================================================================
	IntVector::~IntVector()
	{

	}

	
	/* ��������� */
	//==============================================================================
	bool IntVector::operator == (const IntVector &vec)
	{
		return (this->x == vec.x && this->y == vec.y && this->z == vec.z);
	}

	//==============================================================================
	bool IntVector::operator == (const Vector &vec)
	{
		return (float(this->x) == vec.x && float(this->y) == vec.y && float(this->z) == vec.z);
	}

	//==============================================================================
	bool IntVector::operator == (const int scalar)
	{
		return (this->x == scalar && this->y == scalar && this->z == scalar);
	}

	//==============================================================================
	int IntVector::GetLength()
	{
		return sqrt(x*x + y*y + z*z);
	}

	//==============================================================================
	void IntVector::Normalize()
	{
		int len = GetLength();
		if (len == 0.0f)return;
		x /= len;
		y /= len;
		z /= len;
	}

	//==============================================================================
	int IntVector::DotProduct(const IntVector &in)
	{
		return (x*in.x) + (y*in.y) + (z*in.z);
	}


	//==============================================================================
	IntVector IntVector::operator + (const IntVector& vec)
	{
		return IntVector(this->x + vec.x, this->y + vec.y, this->z + vec.z);
	}

	//==============================================================================
	IntVector IntVector::operator + (const Vector& vec)
	{
		return IntVector(this->x + vec.x, this->y + vec.y, this->z + vec.z);
	}

	//==============================================================================
	IntVector IntVector::operator + (const int scal)
	{
		return IntVector(this->x + scal, this->y + scal, this->z + scal);
	}

	//==============================================================================
	IntVector IntVector::operator - (const IntVector& vec)
	{
		return IntVector(this->x - vec.x, this->y - vec.y, this->z - vec.z);
	}

	//==============================================================================
	IntVector IntVector::operator - (const Vector& vec)
	{
		return IntVector(this->x - vec.x, this->y - vec.y, this->z - vec.z);
	}

	//==============================================================================
	IntVector IntVector::operator - (const int scal)
	{
		return IntVector(this->x - scal, this->y - scal, this->z - scal);
	}

	//==============================================================================
	IntVector IntVector::operator * (const IntVector& vec)
	{
		return IntVector(this->x * vec.x, this->y * vec.y, this->z * vec.z);
	}

	//==============================================================================
	IntVector IntVector::operator * (const Vector& vec)
	{
		return IntVector(this->x * vec.x, this->y * vec.y, this->z * vec.z);
	}

	//==============================================================================
	IntVector IntVector::operator * (const int scal)
	{
		return IntVector(this->x * scal, this->y * scal, this->z * scal);
	}

	//==============================================================================
	IntVector IntVector::operator / (const IntVector& vec)
	{
		return IntVector(this->x / vec.x, this->y / vec.y, this->z / vec.z);
	}

	//==============================================================================
	IntVector IntVector::operator / (const Vector& vec)
	{
		return IntVector(this->x / vec.x, this->y / vec.y, this->z / vec.z);
	}

	//==============================================================================
	IntVector IntVector::operator / (const int scalar)
	{
		return IntVector(this->x / scalar, this->y / scalar, this->z / scalar);
	}

	//==============================================================================
	void IntVector::operator = (const IntVector &vec)
	{
		this->x = vec.x;
		this->y = vec.y;
		this->z = vec.z;
	}

	//==============================================================================
	void IntVector::operator = (const Vector &vec)
	{
		this->x = vec.x;
		this->y = vec.y;
		this->z = vec.z;
	}

	//==============================================================================
	void IntVector::operator = (const int scalar)
	{
		x = y = z = scalar;
	}

	//==============================================================================
	void IntVector::operator += (const IntVector& vec)
	{
		x += vec.x;
		y += vec.y;
		z += vec.z;
	}

	//==============================================================================
	void IntVector::operator += (const Vector& vec)
	{
		x += vec.x;
		y += vec.y;
		z += vec.z;
	}

	//==============================================================================
	void IntVector::operator += (const int scalar)
	{
		x += scalar;
		y += scalar;
		z += scalar;
	}


	//==============================================================================
	void IntVector::operator -= (const IntVector& vec)
	{
		x -= vec.x;
		y -= vec.y;
		z -= vec.z;
	}

	//==============================================================================
	void IntVector::operator -= (const Vector& vec)
	{
		x -= vec.x;
		y -= vec.y;
		z -= vec.z;
	}

	//==============================================================================
	void IntVector::operator -= (const int scalar)
	{
		x -= scalar;
		y -= scalar;
		z -= scalar;
	}

	//==============================================================================
	void IntVector::operator *= (const IntVector& vec)
	{
		x *= vec.x;
		y *= vec.y;
		z *= vec.z;
	}

	//==============================================================================
	void IntVector::operator *= (const Vector& vec)
	{
		x *= vec.x;
		y *= vec.y;
		z *= vec.z;
	}

	//==============================================================================
	void IntVector::operator *= (const int scalar)
	{
		x *= scalar;
		y *= scalar;
		z *= scalar;
	}

	//==============================================================================
	void IntVector::operator /= (const IntVector& vec)
	{
		x /= vec.x;
		y /= vec.y;
		z /= vec.z;
	}

	//==============================================================================
	void IntVector::operator /= (const Vector& vec)
	{
		x /= vec.x;
		y /= vec.y;
		z /= vec.z;
	}

	//==============================================================================
	void IntVector::operator /= (const int scalar)
	{
		x /= scalar;
		y /= scalar;
		z /= scalar;
	}

	//==============================================================================
	bool IntVector::operator != (const IntVector &vec)
	{
		return (this->x != vec.x || this->y != vec.y || this->z != vec.z);
	}

	//==============================================================================
	bool IntVector::operator != (const Vector &vec)
	{
		return (float(this->x) != vec.x || float(this->y) != vec.y || float(this->z) != vec.z);
	}

	//==============================================================================
	bool IntVector::operator != (const int scalar)
	{
		return (this->x != scalar || this->y != scalar || this->z != scalar);
	}
}
