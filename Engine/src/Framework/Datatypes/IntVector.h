#ifndef NBG_CORE_DATATYPES_VECTOR_INT
#define NBG_CORE_DATATYPES_VECTOR_INT

#include <string>
#include <ostream>

#include "Vector.h"

namespace NBG
{
/** @brief Класс, реализующий работу 3D int вектора.
 *
 * @author Vadim Simonov <akari.vs@gmail.com>
 * @copyright 2013 New Bridge Games
 *
 */
class IntVector
{
public:
    /// @name Конструкторы
	IntVector();
	IntVector(const int &x, const int &y);
	IntVector(const int &x, const int &y, const int &z);
	~IntVector();	

	bool operator == (const IntVector &vec);
	bool operator == (const Vector &vec);
	bool operator == (const int scalar);
	
	bool operator != (const IntVector &vec);
	bool operator != (const Vector &vec);
	bool operator != (const int scalar);

    /// @name Координаты

	int x;
    int y;
	int z;

    /// @name Методы

    /** @brief Возвращает длину вектора
     *
     *
     */
    int GetLength();
    /** @brief Нормализует вектор
     *
     *
     */
    void Normalize();
    /** @brief Возвращает результат скалярного произведения с входным вектором
     *
     *
     */
    int DotProduct(const IntVector &in);


    /// @name Стандартные математические операторы
	IntVector	operator + (const IntVector &vec);
	IntVector  operator + (const int scalar);
	IntVector	operator + (const Vector &vec);
	IntVector	operator - (const IntVector &vec);
	IntVector	operator - (const Vector &vec);
	IntVector  operator - (const int scalar);
	IntVector  operator * (const IntVector &vec);
	IntVector  operator * (const Vector &vec);
	IntVector  operator * (const int scalar);
	IntVector  operator / (const IntVector &vec);
	IntVector  operator / (const Vector &vec);
	IntVector  operator / (const int scalar);

	void operator = (const IntVector &vec);
	void operator = (const Vector &vec);
	void operator = (const int scalar);
	void operator += (const IntVector &vec);
	void operator += (const Vector &vec);
	void operator += (const int scalar);
	void operator -= (const IntVector &vec);
	void operator -= (const Vector &vec);
	void operator -= (const int scalar);
	void operator *= (const IntVector &vec);
	void operator *= (const Vector &vec);
	void operator *= (const int scalar);
	void operator /= (const IntVector &vec);
	void operator /= (const Vector &vec);
	void operator /= (const int scalar);

    /// @name Возможность вывода в лог-файл или консоль
	friend std::ostream& operator<< (std::ostream& stream, const IntVector& vector) {
            stream << "x: ";
            stream << vector.x;
            stream << " y: ";
            stream << vector.y;
            return stream;
    }

private:


};

}




#endif // NBG_CORE_DATATYPES_VECTOR_INT
