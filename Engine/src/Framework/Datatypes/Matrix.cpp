#include "Matrix.h"
#include <math.h>

#include <Framework/Utils/MathUtils.h>

namespace NBG
{

	Matrix __g_RotateMatrix;
	Matrix __g_ScalingMatrix;
	Matrix __g_TempMatrix;
	Matrix __g_ResultMatrix;
	Matrix __g_TranslationMatrix;
	float __g_sinRoll = 0.0f;
	float __g_cosRoll = 0.0f;
	//==============================================================================
	Matrix::Matrix()
	{
		__g_ScalingMatrix.Identity();
		__g_RotateMatrix.Identity();
		__g_TranslationMatrix.Identity();
	}

	//==============================================================================
	Matrix::~Matrix()
	{

	}

	//==============================================================================
	bool Matrix::Equals(Matrix * second)
	{
		auto &m2 = second->m;
		return m[0] == m2[0] &&
			m[1] == m2[1] &&
			m[2] == m2[2] &&
			m[3] == m2[3] &&
			m[4] == m2[4] &&
			m[5] == m2[5];
	}


	//==============================================================================
	void Matrix::CreateFromTransform(const Transform &trans, const bool useHotSpot)
	{
		Identity();

		if (useHotSpot)
		{
			__g_TranslationMatrix.m[4] = -trans.hx;
			__g_TranslationMatrix.m[5] = -trans.hy;
			Multiply(&__g_TranslationMatrix);
		}

		if (trans.scalex != 1.0f || trans.scaley != 1.0f)
		{
			__g_ScalingMatrix.m[0] = trans.scalex;
			__g_ScalingMatrix.m[3] = trans.scaley;
			Multiply(&__g_ScalingMatrix);
		}


		if (trans.rz != 0.0f)
		{
			__g_sinRoll = MathUtils::FastSin(trans.rz);
			__g_cosRoll = MathUtils::FastCos(trans.rz);
			__g_RotateMatrix.m[0] = __g_cosRoll;
			__g_RotateMatrix.m[1] = __g_sinRoll;
			__g_RotateMatrix.m[2] = -__g_sinRoll;
			__g_RotateMatrix.m[3] = __g_cosRoll;			
			Multiply(&__g_RotateMatrix);
		}


		if (trans.x != 0.0f || trans.y != 0.0f)
		{
			__g_TranslationMatrix.m[4] = trans.x;
			__g_TranslationMatrix.m[5] = trans.y;
			Multiply(&__g_TranslationMatrix);
		}
		
	}

	//////////////////////////////////////////////////////////////////////////
	void Matrix::Transpose()
	{
		float tempM[9];
		tempM[0] = m[0];
		tempM[1] = m[3];
		//	tempM[2] = m[6];
		tempM[3] = m[1];
		tempM[4] = m[4];
		//tempM[5] = m[7];
		tempM[6] = m[2];
		tempM[7] = m[5];
		//tempM[8] = m[8];

		m[0] = tempM[0];
		m[1] = tempM[1];
		m[2] = tempM[2];
		m[3] = tempM[3];
		m[4] = tempM[4];
		m[5] = tempM[5];
		//m[7] = tempM[6];
		//m[8] = tempM[7];
		//m[9] = tempM[8];
	}

	//////////////////////////////////////////////////////////////////////////
	float Matrix::Determinant()
	{
		return +m[0] * (m[4] * m[8] - m[7] * m[5])
			- m[1] * (m[3] * m[8] - m[6] * m[5])
			+ m[2] * (m[3] * m[7] - m[6] * m[4]);
	}

	//////////////////////////////////////////////////////////////////////////
	void Matrix::Invert()
	{
		auto &res = __g_TempMatrix.m;

		float det = Determinant();
		auto &tM = m;

		res[0] = tM[4] * tM[8] - tM[5] * tM[7];
		res[1] = -(tM[1] * tM[8] - tM[2] * tM[7]);
		res[2] = tM[1] * tM[5] - tM[2] * tM[4];

		res[3] = -(tM[3] * tM[8] - tM[5] * tM[6]);
		res[4] = tM[0] * tM[8] - tM[2] * tM[6];
		res[5] = -(tM[0] * tM[5] - tM[2] * tM[3]);

		res[6] = tM[3] * tM[7] - tM[4] * tM[6];
		res[7] = -(tM[0] * tM[1] - tM[6] * tM[7]);
		res[8] = tM[0] * tM[4] - tM[1] * tM[3];

		m[0] = res[0] / det;
		m[1] = res[1] / det;
		m[2] = res[2] / det;
		m[3] = res[3] / det;
		m[4] = res[4] / det;
		m[5] = res[5] / det;
		m[6] = res[6] / det;
		m[7] = res[7] / det;
		m[8] = res[8] / det;
		m[9] = res[9] / det;
	}

	//==============================================================================
	void Matrix::Multiply(Matrix * second)
	{
		auto & _m = __g_ResultMatrix.m;
		const auto & _m1 = second->m;
		const auto & _m2 = this->m;

		_m[0] = _m1[0] * _m2[0] + _m1[2] * _m2[1];
		_m[1] = _m1[1] * _m2[0] + _m1[3] * _m2[1];
		_m[2] = _m1[0] * _m2[2] + _m1[2] * _m2[3];
		_m[3] = _m1[1] * _m2[2] + _m1[3] * _m2[3];
		_m[4] = _m1[0] * _m2[4] + _m1[2] * _m2[5] + _m1[4];
		_m[5] = _m1[1] * _m2[4] + _m1[3] * _m2[5] + _m1[5];

		m[0] = _m[0];
		m[1] = _m[1];
		m[2] = _m[2];
		m[3] = _m[3];
		m[4] = _m[4];
		m[5] = _m[5];
	}



	//==============================================================================
	void Matrix::Translate(const float x, const float y)
	{
		//__g_TranslationMatrix.Identity();
		m[4] = x;
		m[5] = y;
		//Multiply(&__g_TranslationMatrix);
	}

	//==============================================================================
	void Matrix::RotateYawPitchRoll(const float roll)
	{
		//__g_RotateMatrix.Identity();

		__g_sinRoll = MathUtils::FastSin(roll);
		__g_cosRoll = MathUtils::FastCos(roll);
		m[0] = __g_cosRoll;
		m[1] = __g_sinRoll;
		m[2] = -__g_sinRoll;
		m[3] = __g_cosRoll;
		//Multiply(&__g_RotateMatrix);
	}

	//==============================================================================
	void Matrix::Scale(const float x, const float y)
	{
		//__g_ScalingMatrix.Identity();
		m[0] = x;
		m[3] = y;
		//Multiply(&__g_ScalingMatrix);
	}
}
