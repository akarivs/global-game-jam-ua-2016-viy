#ifndef NBG_CORE_DATATYPES_FLOAT_COLOR
#define NBG_CORE_DATATYPES_FLOAT_COLOR

#include <string>
#include <ostream>

namespace NBG
{
/** @brief Класс, реализующий работу с цветом.
 *
 * @author Vadim Simonov <akari.vs@gmail.com>
 * @copyright 2013 New Bridge Games
 *
 */
struct FloatColor
{
	float r;
	float g;
    float b;
    float a;

public:
    /// @name Конструкторы
    ///Базовый конструктор, заполняет цвет по умолчанию (255,255,255,255)
    FloatColor();
    ///Конструктор, заполняет цвет значениями
    FloatColor(const float &a, const float &r, const float &g, const float &b);
    ///Конструктор, заполняет цвет значением из упакованного цвета
    FloatColor(unsigned long color);
    ///Конструктор, заполняет цвет значением из строки в формате (255 255 255 255)
    FloatColor(const std::string &color);
    ~FloatColor(){};

    /// @name Составляющие цвета
    

    /// @name Методы
    ///Установка цвета из заданных значений
    void SetColor(const float &a, const float &r, const float &g, const float &b);
    ///Установка цвета из упакованного long
    void SetColor(unsigned long color);
    ///Установка цвета из строки в формате  "255 255 255 255" (argb)
    void SetColor(const std::string &color);
    
    ///Нормализует цвет (приводит в вид от 0 до 1)
    void Normalize();

    ///Упаковывает цвет в один unsigned long
    unsigned long GetPackedColor();
};
}
#endif // NBG_CORE_DATATYPES_FLOAT_COLOR
