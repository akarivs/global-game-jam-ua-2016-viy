#ifndef NBG_CORE_DATATYPES
#define NBG_CORE_DATATYPES

#include <string>
#include <iostream>
#include <stdio.h>

#include <vector>
#include <functional>

#ifdef NBG_WIN32
#include <Windows.h>
#endif

extern void NBG_Init();

namespace NBG
{
#ifndef NULL
#define NULL 0
#endif

#ifdef NBG_WIN32
	/// Структуры для работы со стенсил буффером.
	enum    CompareType{
		CMP_NEVER = 1,                      ///< Никогда не проходит тест
		CMP_LESS = 2,                       ///< Проходит тест, если значение меньше
		CMP_EQUAL = 3,                      ///< Проходит тест, при равенстве значений
		CMP_LESSEQUAL = 4,                  ///< Проходит тест, если значение меньше или равно
		CMP_GREATER = 5,                    ///< Проходит тест, если значение больше
		CMP_NOTEQUAL = 6,                   ///< Проходит тест, если значения не равны
		CMP_GREATEREQUAL = 7,               ///< Проходит тест, если значение больше или равно
		CMP_ALWAYS = 8,                     ///< Проходит тест всегда
		CMP_FORCE_DWORD = 0xffffffff
	};

	/// Структуры для работы со стенсил буффером.
	enum    StencilOperation{
		OP_KEEP = 1,
		OP_ZERO = 2,
		OP_REPLACE = 3,
		//OP_INCRSAT = 4,
		//OP_DECRSAT = 5,
		OP_INVERT = 6,
		OP_INCR = 7,
		OP_DECR = 8,
		OP_FORCE_DWORD = 0x7fffffff
	};
#else
	/// Структуры для работы со стенсил буффером.
	enum    CompareType{
		CMP_NEVER = 0x0200,                      ///< Никогда не проходит тест
		CMP_LESS = 0x0201,                       ///< Проходит тест, если значение меньше
		CMP_EQUAL = 0x0202,                      ///< Проходит тест, при равенстве значений
		CMP_LESSEQUAL = 0x0203,                  ///< Проходит тест, если значение меньше или равно
		CMP_GREATER = 0x0204,                    ///< Проходит тест, если значение больше
		CMP_NOTEQUAL = 0x0205,                   ///< Проходит тест, если значения не равны
		CMP_GREATEREQUAL = 0x0206,               ///< Проходит тест, если значение больше или равно
		CMP_ALWAYS = 0x0207,                     ///< Проходит тест всегда
		CMP_FORCE_DWORD = 0xffffffff
	};

	/// Структуры для работы со стенсил буффером.
	enum    StencilOperation{
		OP_KEEP = 0x1E00,
		OP_ZERO = 2,
		OP_REPLACE = 0x1E01,
		//OP_INCRSAT = 4,
		//OP_DECRSAT = 5,
		OP_INVERT = 0x150A,
		OP_INCR = 0x1E02,
		OP_DECR = 0x1E03,
		OP_FORCE_DWORD = 0x7fffffff
};

#endif

	/// Точка привязки спрайта.
	enum class HotSpot
	{
		HS_UL,
		HS_MU,
		HS_UR,
		HS_ML,
		HS_MID,
		HS_MR,
		HS_DL,
		HS_MD,
		HS_DR,
		HS_CUSTOM
	};

	/// @name Macroses
	/// Удобное C++ приведение типов
#define CAST(type, object) (    reinterpret_cast<type>(object))

}

#define STRING_VECTOR std::vector<std::string>

#ifdef NBG_ANDROID
#include <android/log.h>
#define CONSOLE(text,...)  __android_log_print(ANDROID_LOG_DEBUG, "NBG", text, ## __VA_ARGS__);
#define CONSOLE_ERROR(text,...) __android_log_print(ANDROID_LOG_DEBUG, "NBG", text, ## __VA_ARGS__);
#define CONSOLE_INFO(text,...) CONSOLE(text, ## __VA_ARGS__);
#define CONSOLE_SUCCESS(text,...) CONSOLE(text, ## __VA_ARGS__);
#elif defined(NBG_WIN32)
#define CONSOLE(text,...) SetConsoleTextAttribute( GetStdHandle( STD_OUTPUT_HANDLE ), FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY); printf(text, ## __VA_ARGS__); printf("\n");
#define CONSOLE_ERROR(text,...) SetConsoleTextAttribute( GetStdHandle( STD_OUTPUT_HANDLE ), FOREGROUND_RED|FOREGROUND_INTENSITY); printf("ERROR: "); printf(text, ## __VA_ARGS__); printf("\n"); SetConsoleTextAttribute( GetStdHandle( STD_OUTPUT_HANDLE ), FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
#define CONSOLE_INFO(text,...) SetConsoleTextAttribute( GetStdHandle( STD_OUTPUT_HANDLE ), FOREGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_BLUE); printf(text, ## __VA_ARGS__); printf("\n"); SetConsoleTextAttribute( GetStdHandle( STD_OUTPUT_HANDLE ), FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
#define CONSOLE_SUCCESS(text,...) SetConsoleTextAttribute( GetStdHandle( STD_OUTPUT_HANDLE ), FOREGROUND_GREEN|FOREGROUND_INTENSITY); printf(text, ## __VA_ARGS__); printf("\n"); SetConsoleTextAttribute( GetStdHandle( STD_OUTPUT_HANDLE ), FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
#else
#define CONSOLE(text,...) printf(text, ## __VA_ARGS__); printf("\n")
#define CONSOLE_ERROR(text,...) printf("ERROR: "); printf(text, ## __VA_ARGS__); printf("\n");
#define CONSOLE_INFO(text,...) printf(text, ## __VA_ARGS__); printf("\n");
#define CONSOLE_SUCCESS(text,...) printf(text, ## __VA_ARGS__); printf("\n"); 
#endif

typedef std::function<void(void)>	VOID_CALLBACK;
typedef std::function<void(void*)>	VOID_DATA_CALLBACK;


#ifdef NBG_DEBUG
#ifdef NBG_WIN32
#include "windows.h"

#ifdef _MSC_VER
#define DEBUG_BREAK __debugbreak()
#else
#define DEBUG_BREAK raise(SIGTRAP)
#endif

inline int MESSAGE_ERROR(std::string msg)
{
	MessageBeep(MB_ICONEXCLAMATION);
	int res = MessageBoxA(NULL, msg.c_str(), "Error", MB_ABORTRETRYIGNORE | MB_ICONERROR);
	if (res == IDABORT)
	{
		exit(0);
		return -1;
	}
	return res;
}

#define NBG_Assert(condition,message) \
	do { \
	if (! (condition)) { \
	std::string error = "Assertion `"; \
	error += #condition; \
	error += "` filed in "; \
	error += __FILE__; \
	error += " line: "; \
	error += __LINE__; \
	error += ": "; \
	error += #message; \
	int res = MESSAGE_ERROR(error.c_str()); \
	if (res == IDRETRY)\
	{ \
	DEBUG_BREAK; \
	} \
		} \
		} while (false)

#else
#define MESSAGE_ERROR(msg)   void()
#define DEBUG_BREAK void()
#define NBG_Assert(condition,message)   void()
#endif
#else
#define MESSAGE_ERROR(msg)   void()
#define NBG_Assert(condition,message)   void()
#define DEBUG_BREAK void()
#endif


enum class BlendMode
{
	DONT_CHANGE = -1,
	ADD,
	MOD,
	SCREEN,
	BLEND
};



/* Class Helpers */
#define GETTER_SETTER(varType, varName, funName)\
protected: varType varName;\
public: virtual varType Get##funName(void) const { return varName; }\
public: virtual void Set##funName(varType var){ varName = var; }

#define GETTER_SETTER_BOOL(varName)\
protected: bool m_Is##varName;\
public: virtual bool Is##varName(void) const { return m_Is##varName; }\
public: virtual void Set##varName(bool var){ m_Is##varName = var; }

#endif // NBG_CORE_DATATYPES
