#include "LocalizationManager.h"

#include <Framework.h>

#include <Framework/Global/ResourcesManager.h>
#include <Framework/Utils/StringUtils.h>



namespace NBG
{    
	//////////////////////////////////////////////////////////////////////////
	CLocalizationManager::CLocalizationManager()
	{
		m_DefaultText = L"default_text";
	}

	//////////////////////////////////////////////////////////////////////////
	CLocalizationManager::~CLocalizationManager()
	{
		std::map <std::string,std::vector<std::wstring> >stringsSwap;
		std::map<std::string, int>keysSwap;
		std::map<int,std::string>languagesSwap;
		
		m_Strings.swap(stringsSwap);
		m_Keys.swap(keysSwap);
		m_Languages.swap(languagesSwap);

	}

	//////////////////////////////////////////////////////////////////////////
	void CLocalizationManager::Init(const std::string &fileName)
	{
		//Получаем системную локаль
		m_Locale = g_System->GetLocale();
		if (m_Locale.empty()) ///если система не поддерживает локали, получаем локаль из настроечного файла
		{
			g_Config->GetValue("locale",m_Locale);			
		}
		///Если нет папки с указанной локалью, локаль ставим по умолчанию
		std::string path = "localization/"+m_Locale;

		if (g_FileSystem->IsDirExists(path) == false)
		{
			m_Locale = "en";
		}	

		std::map <std::string,std::vector<std::wstring> >stringsSwap;
		std::map<std::string, int>keysSwap;
		std::map<int,std::string>languagesSwap;
		
		m_Strings.swap(stringsSwap);
		m_Keys.swap(keysSwap);
		m_Languages.swap(languagesSwap);

		
		CXMLResource* xmlRes = (CXMLResource*)g_ResManager->GetResource(fileName);
		pugi::xml_document* doc = xmlRes->GetXML();
		if (!doc)
		{
			CONSOLE("Error: no such file - %s", fileName.c_str());
			return;
		}

		///Определяем формат
		auto node = doc->first_child();
		std::string val = node.attribute("version").value();		
		if (val == "NBS")
		{
			///Загружаем строки в формате редактора строк New Bridge
			LoadNBSXML(doc);
		}
		else
		{
			///Загружаем в формате Excel 2003
			LoadExcel2003XML(doc);
		}	
		g_ResManager->ReleaseResource(xmlRes);
	}

	//////////////////////////////////////////////////////////////////////////
	void CLocalizationManager::LoadExcel2003XML(pugi::xml_document * doc)
	{
		pugi::xml_node node = doc->first_child().child("Worksheet").child("Table");

		int counter = 0;
		int stringsId = 0;
		//получаем строки
		for (pugi::xml_node Row = node.child("Row"); Row; Row = Row.next_sibling("Row"))
		{

			if (counter == 1)
			{
				int cnt = 0;
				for (pugi::xml_node Cell = Row.child("Cell"); Cell; Cell = Cell.next_sibling("Cell"))
				{
					if (cnt > 1)
					{
						std::string id = Cell.first_child().child_value();
						if (!id.empty())
						{
							m_Languages[cnt] = id;
						}
					}
					cnt++;
				}
			}
			counter++;
			if (counter <= 2)continue;
			int cnt = 0;
			for (pugi::xml_node Cell = Row.child("Cell"); Cell; Cell = Cell.next_sibling("Cell"))
			{
				std::string id = Cell.first_child().child_value();
				if (cnt == 0)
				{
					if (!id.empty() && Cell.attribute("ss:MergeAcross").empty())
					{
						m_Keys[id] = stringsId;
						stringsId++;
					}
					else
					{
						break;
					}
				}
				cnt++;
				if (cnt <= 2)continue;
				if (id.empty())
				{
					id = "!no_text!";
				}
				///Разбиваем по языкам						
				m_Strings[m_Languages[cnt - 1]].push_back(pugi::as_wide(id));
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CLocalizationManager::LoadNBSXML(pugi::xml_document * doc)
	{
		///Подгружаем языки
		auto languagesNode = doc->first_child().child("languages");
		int cnt = 0;
		for (pugi::xml_node language = languagesNode.child("language"); language; language = language.next_sibling("language"))
		{
			m_Languages[cnt] = language.attribute("name").value();
			cnt++;
		}
		int languagesCount = m_Languages.size();

		
		///Подгружаем строки
		int keyId = 0;
		auto stringsNode = doc->first_child().child("strings");
		for (pugi::xml_node string = stringsNode.child("s"); string; string = string.next_sibling("s"))
		{
			m_Keys[string.attribute("key").value()] = keyId;

			for (int i = 0; i < languagesCount; i++)
			{
				auto lang = m_Languages[i];
				std::string str = string.attribute(lang.c_str()).value();
				if (str.empty())
				{
					str = "!!!TRANSLATE ME - ";
					str += string.attribute("key").value();
					str += "!!!";
				}
				m_Strings[lang].push_back(as_wide(str));
			}
			keyId++;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	const std::string & CLocalizationManager::GetLocale()
	{
		return m_Locale;
	}

	//////////////////////////////////////////////////////////////////////////
	const std::wstring & CLocalizationManager::GetText(const std::string &id)
	{		
		if (m_Keys.find(id) == m_Keys.end())
		{
			return m_DefaultText;
		}
		const auto &vec = m_Strings[m_Locale];
		const auto intId = m_Keys[id];
		if (intId < 0 || intId >= vec.size())
		{
			return m_DefaultText;
		}
		auto &str = m_Strings[m_Locale][m_Keys[id]];
		if (str[0] == L'#')
		{
			str = GetText(pugi::as_utf8(str.substr(1)));
		}
		return str;
	}

	//////////////////////////////////////////////////////////////////////////
	bool CLocalizationManager::IsLocaleWithoutSpaces()
	{
		if (m_Locale == "jp" || m_Locale == "zh")
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
