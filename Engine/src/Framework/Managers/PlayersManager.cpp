#include "PlayersManager.h"

#include <Framework.h>
#include <algorithm>

namespace NBG
{

	///Используется для простой защиты "от дурака" для шифрования сейвов.
	std::string players_version = "player";

	//////////////////////////////////////////////////////////////////////////
	int CBasePlayer::GetSize()
	{
		int size = 0;
		size += sizeof(m_Name); //name
		size += sizeof(bool); //isCreated
		size += sizeof(bool); //isFullscreen
		size += sizeof(float); //soundVolume
		size += sizeof(float); //musicVolume
		size += sizeof(bool); //isCustomCursor        
		return size;
	}

	//////////////////////////////////////////////////////////////////////////
	void CBasePlayer::SetName(const std::wstring name)
	{
		m_Name = name;
	}

	//////////////////////////////////////////////////////////////////////////
	void CBasePlayer::InitDefaultParams()
	{
		isCreated = true;
		m_IsCorrupted = false;
		m_IsFullscreen = false;
		m_SoundVolume = 50;
		m_MusicVolume = 50;
		m_IsCustomCursor = false;
	}

	//////////////////////////////////////////////////////////////////////////
	void CBasePlayer::Save(pugi::xml_document * doc)
	{
		pugi::xml_node system = doc->append_child("system");
		system.append_child("name").append_attribute("value").set_value(pugi::as_utf8(m_Name.c_str()).c_str());
		system.append_child("created").append_attribute("value").set_value(isCreated);
		system.append_child("fullscreen").append_attribute("value").set_value(m_IsFullscreen);
		system.append_child("sound_volume").append_attribute("value").set_value(m_SoundVolume);
		system.append_child("music_volume").append_attribute("value").set_value(m_MusicVolume);
		system.append_child("custom_cursor").append_attribute("value").set_value(m_IsCustomCursor);
	}

	//////////////////////////////////////////////////////////////////////////
	void CBasePlayer::Load(pugi::xml_document * doc)
	{
		pugi::xml_node system = doc->child("system");
		m_Name = pugi::as_wide(system.child("name").attribute("value").value());
		isCreated = system.child("created").attribute("value").as_bool();
		m_IsFullscreen = system.child("fullscreen").attribute("value").as_bool();
		m_SoundVolume = system.child("sound_volume").attribute("value").as_float();
		m_MusicVolume = system.child("music_volume").attribute("value").as_float();
		m_IsCustomCursor = system.child("custom_cursor").attribute("value").as_bool();
	}

	//////////////////////////////////////////////////////////////////////////
	CPlayersManager::CPlayersManager()
	{
		m_CurrentPlayer = -1;
	}

	//////////////////////////////////////////////////////////////////////////
	CPlayersManager::~CPlayersManager()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	void CPlayersManager::InitSavePath(const std::string company, const std::string product, const std::string publisher)
	{
		m_SavePath = g_System->GetSavePath(company, product, publisher);
		m_PlayersSave = m_SavePath + L"players.dat";
	}

	//////////////////////////////////////////////////////////////////////////
	void CPlayersManager::SetActivePlayer(const int id)
	{
		m_CurrentPlayer = id;
		SaveSystemConfig();
	}


	//////////////////////////////////////////////////////////////////////////
	void CPlayersManager::SavePlayers()
	{
		SaveSystemConfig();
		for (int i = 0; i < m_PlayersCount; i++)
		{
			std::wstring path = m_SavePath + L"player_" + StringUtils::ToWString(i) + L".dat";
			if (!m_Players[i]->isCreated)
			{
				g_FileSystem->DeleteFile(path);
				g_FileSystem->DeleteFile(path + L".size");
				continue;
			}
			pugi::xml_document doc;
			m_Players[i]->Save(&doc);

			std::ostringstream out;
			doc.save(out);
			std::string docStr = out.str();
			for (int j = 0; j < docStr.size(); j++)
			{
				docStr[j] = docStr[j] ^ 'a';
			}

			int szFile = 0;
			FILE * ops = g_FileSystem->OpenFile(path, L"w");
			fwrite(docStr.c_str(), docStr.size(), 1, ops);
			fseek(ops, 0, SEEK_END);
			szFile = ftell(ops);
			fseek(ops, 0, SEEK_SET);
			fclose(ops);


			path += L".size";
			ops = g_FileSystem->OpenFile(path, L"wb");
			fwrite(&szFile, sizeof(int), 1, ops);
			fclose(ops);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CPlayersManager::SaveCurrentPlayer()
	{
		SaveSystemConfig();
		if (m_CurrentPlayer < 0)m_CurrentPlayer = 0;
		int i = m_CurrentPlayer;
		{
			std::wstring path = m_SavePath + L"player_" + StringUtils::ToWString(i) + L".dat";
			if (!m_Players[i]->isCreated)
			{

				g_FileSystem->DeleteFile(path);
				g_FileSystem->DeleteFile(path + L".size");
			}
			pugi::xml_document doc;
			m_Players[i]->Save(&doc);

			std::ostringstream out;
			doc.save(out);
			std::string docStr = out.str();
			for (int i = 0; i < docStr.size(); i++)
			{
				docStr[i] = docStr[i] ^ 'a';
			}

			int szFile = 0;
			FILE * ops = g_FileSystem->OpenFile(path, L"w");
			fwrite(docStr.c_str(), docStr.size(), 1, ops);
			fseek(ops, 0, SEEK_END);
			szFile = ftell(ops);
			fseek(ops, 0, SEEK_SET);
			fclose(ops);

			path += L".size";
			ops = g_FileSystem->OpenFile(path, L"wb");
			fwrite(&szFile, sizeof(int), 1, ops);
			fclose(ops);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CPlayersManager::LoadPlayers()
	{
		std::wstring checkPath = m_SavePath + L"player_0.dat";
		FILE * ops = g_FileSystem->OpenFile(checkPath, L"r");
		if (!ops)
		{
			g_Config->GetValue("use_default_player", m_IsUseDefaultPlayer);
			if (m_IsUseDefaultPlayer)
			{
				m_Players[0]->InitDefaultParams();
				m_CurrentPlayer = 0;
				//SavePlayers();
			}
			else
			{
				m_CurrentPlayer = -1;
			}
			g_System->ShowCursor(GetCurrentPlayer()->IsCustomCursor() == false);
			g_GameApplication->SetFullscreen(GetCurrentPlayer()->IsFullscreen());
			g_SoundManager->SetGroupVolume("ambience", GetCurrentPlayer()->GetSoundVolume() / 100.0f);
			g_SoundManager->SetGroupVolume("sounds", GetCurrentPlayer()->GetSoundVolume() / 100.0f);
			g_SoundManager->SetGroupVolume("music", GetCurrentPlayer()->GetMusicVolume() / 100.0f);
			return;
		}
		fclose(ops);
		LoadSystemConfig();

		for (int i = 0; i < m_PlayersCount; i++)
		{
			std::wstring path = m_SavePath + L"player_" + StringUtils::ToWString(i) + L".dat";
			//m_Players[i]->savePath = path;
			int offset = 0;
			FILE * ops = g_FileSystem->OpenFile(path, L"r");
			if (ops)
			{
				fseek(ops, 0, SEEK_END);
				int size = ftell(ops);
				fseek(ops, 0, SEEK_SET);
				char * data = new char[size];
				fread(data, size, 1, ops);
				fclose(ops);

				if (size > 10)
				{
					if (!(data[0] == '<' && data[1] == '?' && data[2] == 'x' && data[3] == 'm' && data[4] == 'l'))
					{
						for (int j = 0; j < size; j++)
						{
							data[j] = data[j] ^ 'a';
						}
					}					
				}


				path += L".size";
				FILE * szFile = g_FileSystem->OpenFile(path, L"rb");
				int sz = 0;
				if (szFile)
				{
					fread(&sz, sizeof(int), 1, szFile);
					fclose(szFile);
				}

				pugi::xml_document doc;
				doc.load_buffer(data, size);
				std::ostringstream out;
				doc.save(out);
				std::string docStr = out.str();



				if (sz != size)
				{
					CONSOLE("File is corrupted! %s", pugi::as_utf8(path.c_str()).c_str());
					m_Players[i]->InitDefaultParams();
					m_Players[i]->m_IsCorrupted = true;

					pugi::xml_node system = doc.child("system");
					if (system.child("name").empty() == false)
						m_Players[i]->SetName(pugi::as_wide(system.child("name").attribute("value").value()));
				}
				else
				{
					pugi::xml_document doc;
					doc.load_buffer(data, size);
					m_Players[i]->Load(&doc);
				}
				delete[] data;
			}
		}
		g_System->ShowCursor(GetCurrentPlayer()->IsCustomCursor() == false);
		g_GameApplication->SetFullscreen(GetCurrentPlayer()->IsFullscreen());
		g_SoundManager->SetGroupVolume("ambience", GetCurrentPlayer()->GetSoundVolume() / 100.0f);
		g_SoundManager->SetGroupVolume("sounds", GetCurrentPlayer()->GetSoundVolume() / 100.0f);
		g_SoundManager->SetGroupVolume("music", GetCurrentPlayer()->GetMusicVolume() / 100.0f);
	}

	//////////////////////////////////////////////////////////////////////////
	int CPlayersManager::GetPlayersCount()
	{
		int playersCount = 0;
		for (size_t i = 0; i < m_Players.size(); i++)
		{
			if (m_Players[i]->isCreated)playersCount++;
		}
		return playersCount;
	}

	//////////////////////////////////////////////////////////////////////////
	void CPlayersManager::SaveCustomConfig(CConfig config, const std::string &filename)
	{
		pugi::xml_document doc;
		pugi::xml_node elements = doc.append_child("elements");
        
        auto valuesInt = config.GetIntValues();
        auto valuesBool = config.GetBoolValues();
        auto valuesFloat = config.GetFloatValues();
        auto valuesString = config.GetStringValues();
        
        for(auto var = valuesInt.begin(); var != valuesInt.end(); var++)
		{
			auto node = elements.append_child("element");
			node.append_attribute("id").set_value(var->first.c_str());
			node.append_attribute("type").set_value("int");
			node.append_attribute("value").set_value(var->second);
		}
        for(auto var = valuesBool.begin(); var != valuesBool.end(); var++)
		{
			auto node = elements.append_child("element");
			node.append_attribute("id").set_value(var->first.c_str());
			node.append_attribute("type").set_value("bool");
			node.append_attribute("value").set_value(var->second);
		}
		for(auto var = valuesFloat.begin(); var != valuesFloat.end(); var++)
		{
			auto node = elements.append_child("element");
			node.append_attribute("id").set_value(var->first.c_str());
			node.append_attribute("type").set_value("float");
			node.append_attribute("value").set_value(var->second);
		}
		for(auto var = valuesString.begin(); var != valuesString.end(); var++)
		{
			auto node = elements.append_child("element");
			node.append_attribute("id").set_value(var->first.c_str());
			node.append_attribute("type").set_value("string");
			node.append_attribute("value").set_value(var->second.c_str());
		}
		
		std::ostringstream out;
		doc.save(out);
		std::string docStr = out.str();		

		for (int i = 0; i < docStr.size(); i++)
		{
			docStr[i] = docStr[i] ^ 'a';
		}		
		g_FileSystem->SaveFile(m_SavePath + as_wide(filename), docStr.c_str(), docStr.size());
	}

	//////////////////////////////////////////////////////////////////////////
	CConfig CPlayersManager::LoadCustomConfig(const std::string &filename)
	{
		CConfig config;

		std::wstring path = m_SavePath + as_wide(filename);		
		int offset = 0;
		FILE * ops = g_FileSystem->OpenFile(path, L"r");
		if (ops)
		{
			fseek(ops, 0, SEEK_END);
			int size = ftell(ops);
			fseek(ops, 0, SEEK_SET);
			char * data = new char[size];
			fread(data, size, 1, ops);
			fclose(ops);

			for (int j = 0; j < size; j++)
			{
				data[j] = data[j] ^ 'a';
			}

			pugi::xml_document doc;
			doc.load_buffer(data, size);
			auto elements = doc.first_child();

			for (pugi::xml_node element = elements.child("element"); element; element = element.next_sibling("element"))
			{
				std::string type = element.attribute("type").value();
				if (type == "int")
				{
					config.SetValue(element.attribute("id").value(), element.attribute("value").as_int());
				}
				else if (type == "bool")
				{
					config.SetValue(element.attribute("id").value(), element.attribute("value").as_bool());
				}
				else if (type == "float")
				{
					config.SetValue(element.attribute("id").value(), element.attribute("value").as_float());
				}
				else if (type == "string")
				{
					config.SetValue(element.attribute("id").value(), std::string(element.attribute("value").value()));
				}
			}
		}
		return config;
	}


	//////////////////////////////////////////////////////////////////////////
	void CPlayersManager::SaveSystemConfig()
	{
		pugi::xml_document doc;
		pugi::xml_node config = doc.append_child("config");
		config.append_attribute("current_player").set_value(m_CurrentPlayer);
		std::ostringstream out;
		doc.save(out);
		std::string docStr = out.str();
		/*for(int i = 0; i < docStr.size(); i++)
		{
		docStr[i] = docStr[i] ^ players_version[i%players_version.size()];
		}		*/
		g_FileSystem->SaveFile(m_PlayersSave, docStr.c_str(), docStr.size());
	}

	//////////////////////////////////////////////////////////////////////////
	void CPlayersManager::LoadSystemConfig()
	{

		FILE * ops = g_FileSystem->OpenFile(m_PlayersSave, L"r");
		if (ops)
		{
			fseek(ops, 0, SEEK_END);
			int size = ftell(ops);
			fseek(ops, 0, SEEK_SET);
			char * data = new char[size];
			fread(data, size, 1, ops);
			fclose(ops);
			/*for(int i = 0; i < file.size; i++)
			{
			data[i] = data[i] ^ players_version[i%players_version.size()];
			}*/
			pugi::xml_document doc;
			doc.load_buffer(data, size);
			m_CurrentPlayer = doc.first_child().attribute("current_player").as_int();
			delete[] data;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	CBasePlayer * CPlayersManager::GetCurrentPlayer()
	{
		if (m_CurrentPlayer == -1)
		{
			return m_DefaultPlayer;
		}

		return m_Players[m_CurrentPlayer];

	}

	//////////////////////////////////////////////////////////////////////////
	void CPlayersManager::AddPlayer(const std::wstring name)
	{
		int id = GetPlayersCount();
		m_Players[id]->InitDefaultParams();
		m_Players[id]->SetName(name);
		if (m_CurrentPlayer != -1)
		{
			m_Players[id]->SetMusicVolume(m_Players[m_CurrentPlayer]->GetMusicVolume());
			m_Players[id]->SetSoundVolume(m_Players[m_CurrentPlayer]->GetSoundVolume());
			m_Players[id]->SetFullscreen(m_Players[m_CurrentPlayer]->IsFullscreen());
			m_Players[id]->SetCustomCursor(m_Players[m_CurrentPlayer]->IsCustomCursor());
		}
		m_CurrentPlayer = id;
		SavePlayers();
	}

	//////////////////////////////////////////////////////////////////////////
	void CPlayersManager::DeletePlayer(const int id)
	{
		m_Players[id]->isCreated = false;

		std::wstring path = m_SavePath + L"player_" + StringUtils::ToWString(id) + L".dat";
		g_FileSystem->DeleteFile(path);
		path += L".size";
		g_FileSystem->DeleteFile(path);


		for (int i = id; i < m_PlayersCount - 1; i++)
		{
			std::swap(m_Players[i], m_Players[i + 1]);
			m_Players[i]->SetId(i);
			m_Players[i + 1]->SetId(i + 1);
		}

		if (m_CurrentPlayer > id)m_CurrentPlayer--;
		if (m_CurrentPlayer >= GetPlayersCount())m_CurrentPlayer--;
		if (m_CurrentPlayer < 0)m_CurrentPlayer = 0;

		SavePlayers();
	}

	//////////////////////////////////////////////////////////////////////////
	void CPlayersManager::RussianSymbolsToLower(std::wstring &str)
	{		
		for (int i = 0; i < str.size(); i++)
		{
			wchar_t symbol = str[i];	
			if (symbol >= 1040 && symbol <= 1071)
			{
				symbol += 32;
				str[i] = symbol;
			}			
		}		
	}

	//////////////////////////////////////////////////////////////////////////
	bool CPlayersManager::IsNameDuplicates(const std::wstring name, const bool ignoreCurrentPlayer)
	{	
		auto name1 = name;				
		std::transform(name1.begin(), name1.end(), name1.begin(), towlower);
		RussianSymbolsToLower(name1);
		for (int i = 0; i<m_PlayersCount; i++)
		{
			if (ignoreCurrentPlayer && i == GetCurrentPlayer()->GetId())continue;
			if (!m_Players[i]->isCreated)continue;
			auto name2 = m_Players[i]->GetName();
			std::transform(name2.begin(), name2.end(), name2.begin(), towlower);
			RussianSymbolsToLower(name2);
			if (name2 == name1)return true;
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////
	void CPlayersManager::AfterInit()
	{
		for (int i = 0; i<m_PlayersCount; i++)
		{
			m_Players[i]->AfterInit();
		}
	}
}
