#include "GameApplication.h"

#include <Framework.h>
#include "../Extensions/optimus/optimus.h"
#include "../Extensions/inapp/InAppManager.h"
#include <mutex>

namespace NBG
{
	//////////////////////////////////////////////////////////////////////////
	CGameApplication::CGameApplication()
	{
		m_IsApplicationActive = true;
		m_IsInitialized = false;

		m_FullscreenSize = Vector(1024, 768);
		m_LogicalSize = Vector(1024, 768);
		m_WindowedSize = Vector(1024, 768);
		m_IsFullscreen = false;

		m_CurrentSize = m_WindowedSize;

		m_FontsPath = "xml/fonts.xml";
		m_SoundsPath = "xml/sounds.xml";
		m_StringsPath = "xml/strings.xml";
		m_ConfigPath = "xml/config.xml";
		m_AtlasesPath = "atlases";
		m_EditionsXMLPath = "edition.xml";
		m_EditionsFolderPath = "xml/editions";

		m_AppTime = 0.0f;
		m_IsStopped = false;
		m_IsPaused = false;		

		m_FileSystem = new CFileSystem();
		m_OperatingSystem = new COperatingSystem();
		m_TimeManager = new CTimeManager();
		m_TextureManager = new CTextureManager();
		

		m_UpdateLimit = 1.0f / 30.0f;
		m_DrawLimit = 1.0f / 120.0f;
		m_FramesCount = 0.0f;
		m_FramesTimer = 0.0f;

		m_UpdateCounter = m_DrawCounter = 0.0f;

		m_OnInitCallback = nullptr;
		m_OnDrawCallback = nullptr;
		m_AfterLoopCallback = nullptr;
		m_OnUpdateCallback = nullptr;
		m_OnMouseMoveCallback = nullptr;
		m_OnMouseWheelCallback = nullptr;
		m_OnRenderChangeCallback = nullptr;
		m_OnKeyDownCallback = nullptr;
		m_OnKeyUpCallback = nullptr;	

		m_DrawCallLimiter = -1;
	}

	//////////////////////////////////////////////////////////////////////////
	CGameApplication::~CGameApplication()
	{

	}

	//////////////////////////////////////////////////////////////////////////
	bool CGameApplication::Init()
	{
		MathUtils::Init();
		m_System.Init();
		m_EditionHelper.Init(m_EditionsXMLPath, m_EditionsFolderPath);
		m_FileSystem->Init();
		m_Config.LoadConfig(m_EditionHelper.ConvertPath(m_ConfigPath));
		bool useLog = false;
		m_Config.GetValue("use_log", useLog);
		if (useLog)
		{
			m_Log.Init("log.html");
		}
		m_LocalizationManager.Init(m_StringsPath);
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	void CGameApplication::StopApp()
	{
		if (!m_IsStopped)
		{
			m_IsStopped = true;			
			m_PlayersManager.SavePlayers();
			
			///run link.xml if exists
#ifdef NBG_WIN32
			bool survey = false;
			m_Config.GetValue("survey", survey);
			if (survey)
			{
				auto xml = CAST(CXMLResource*, m_ResourcesManager.GetResource("../link.xml"));
				std::string value = xml->GetXML()->first_child().attribute("link").value();
				if (value.size() > 0)
				{
					ShellExecuteA(NULL, "open", value.c_str(), NULL, NULL, SW_SHOWNORMAL);
				}				
			}
#endif
		}		
	}

	//////////////////////////////////////////////////////////////////////////
	bool CGameApplication::IsStopped()
	{
		return m_IsStopped;
	}

	//////////////////////////////////////////////////////////////////////////
	bool CGameApplication::IsPaused()
	{
		return m_IsPaused;
	}

	//////////////////////////////////////////////////////////////////////////
	void CGameApplication::Pause()
	{
		m_IsPaused = true;
	}

	//////////////////////////////////////////////////////////////////////////
	void CGameApplication::Resume()
	{
		m_IsPaused = false;
	}

	//////////////////////////////////////////////////////////////////////////
	void CGameApplication::ShareContext()
	{
		g_Render->ShareContext();
	}

	//////////////////////////////////////////////////////////////////////////
	void CGameApplication::UnShareContext()
	{
		g_Render->UnShareContext();
	}

	//////////////////////////////////////////////////////////////////////////
	bool CGameApplication::IsCheatsEnabled()
	{
		bool cheats = false;
		m_Config.GetValue("cheats", cheats);
		return cheats;
	}

	//////////////////////////////////////////////////////////////////////////
	bool CGameApplication::Run()
	{		

		m_CurrentSize = m_WindowedSize;
		Vector displaySize = m_WindowedSize;
		if (m_System.IsMobileSystem())
		{
			m_CurrentSize = m_FullscreenSize;
			displaySize = g_System->GetSystemResolution();
			m_CurrentSize = displaySize;

		}

		m_System.UpdateSize(displaySize, m_CurrentSize, m_IsFullscreen);
		m_OperatingSystem->CreateAppWindow();
		m_TimeManager->Init();
		m_Random.Init();
#ifdef NBG_IOS
#elif defined(NBG_ANDROID)
		m_SoundManager.Init();		
		if (m_SoundsPath.size() > 0)
		{
			m_SoundManager.LoadSounds(m_SoundsPath);
		}
#else
		m_Render.Init();
		if (m_System.IsMobileSystem())
		{
			m_CurrentSize = g_System->GetGameBounds();			

		}
		AfterRun();
#endif
		m_OperatingSystem->MainLoop();
#ifndef NBG_ANDROID		
		m_SoundManager.Destroy();
#endif
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	void CGameApplication::AfterRun()
	{
		m_TextureManager->InitDefaultTexture();		
		m_FontsManager.LoadFromXML(m_FontsPath);				
		m_AtlasHelper.LoadAtlases(m_AtlasesPath);
#ifndef NBG_ANDROID
		m_SoundManager.Init();
		if (m_SoundsPath.size() > 0)
		{
			m_SoundManager.LoadSounds(m_SoundsPath);
		}		
#endif

		m_TheoraVideoManager = new TheoraVideoManager();
		m_TheoraVideoManager->setLogFunction([](std::string str){});
		OpenAL_AudioInterfaceFactory *	iface_factory = new OpenAL_AudioInterfaceFactory();
		m_TheoraVideoManager->setAudioInterfaceFactory(iface_factory);

		std::string company, product, publisher;
		m_Config.GetValue("company_name", company);
		m_Config.GetValue("product_name", product);
		m_Config.GetValue("publisher_name", publisher);

		m_PlayersManager.InitSavePath(company, product, publisher);

		m_PlayersManager.LoadPlayers();
		m_PlayersManager.AfterInit();		

		g_Log->WriteLineSuccess("Complete loading engine!");
		g_Log->WriteLineInfo("Starting load game code!");
		bool inapp = false;
		g_Config->GetValue("inapp", inapp);
		if (inapp)
		{
			inapp::CInAppManager::GetInstance()->Init();
		}
		if (m_OnInitCallback != nullptr)
		{
			m_OnInitCallback();
		}		
	}

	//////////////////////////////////////////////////////////////////////////
	void CGameApplication::OnMouseUp(const int button)
	{		
		if (m_OnMouseUpCallback)
		{
			m_OnMouseUpCallback(button);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CGameApplication::OnMouseDown(const int button)
	{	
		if (m_OnMouseDownCallback)
		{
			m_OnMouseDownCallback(button);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CGameApplication::OnWheelMove(const int delta)
	{
		if (m_OnMouseWheelCallback)
		{
			m_OnMouseWheelCallback(delta);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CGameApplication::OnKeyUp(const int key)
	{	
		if (key == KEY_F5)
		{
			m_DrawCallLimiter = -1;
		}
		else if (key == KEY_F6)
		{
			m_DrawCallLimiter--;
			if (m_DrawCallLimiter < -1)
			{
				m_DrawCallLimiter = -1;
			}
		}
		else if (key == KEY_F7)
		{
			m_DrawCallLimiter++;
		}

		if (m_OnKeyUpCallback)
		{
			m_OnKeyUpCallback(key);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CGameApplication::OnKeyDown(const int key)
	{		
		if (m_OnKeyDownCallback)
		{
			m_OnKeyDownCallback(key);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CGameApplication::OnMouseMove()
	{		
		if (m_OnMouseMoveCallback)
		{
			m_OnMouseMoveCallback();
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CGameApplication::OnBeforeRenderChange()
	{		
		if (m_OnRenderChangeCallback)
		{
			m_OnRenderChangeCallback(RenderChangeType::Before);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CGameApplication::OnAfterRenderChange()
	{		
		if (m_OnRenderChangeCallback)
		{
			m_OnRenderChangeCallback(RenderChangeType::After);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CGameApplication::Update()
	{
		m_FrameTime = m_TimeManager->Reset();
		if (m_FrameTime > 0.04f)m_FrameTime = 0.04f;

		m_UpdateCounter += m_FrameTime;		
#ifdef NBG_ANDROID
		if (m_UpdateCounter < 0.032)
#else
		if (m_UpdateCounter < 0.016)
#endif
		{
			m_DrawCounter = 0;						
		}
		else
		{
			m_DrawCounter = 1;			
			m_UpdateCounter = 0.0;			
		}		

		if (IsCheatsEnabled() && g_Input->IsKeyDown(KEY_OEM_PLUS))
		{
			m_FrameTime *= 10.0f;
		}

		m_FramesTimer += m_FrameTime;		
		if (m_FramesTimer >= 1.0f)
		{
			m_FramesTimer = 0.0f;
			m_FramesCount = 1;
		}
		m_AppTime += m_FrameTime;

		m_SoundManager.Update();
		if (!m_IsApplicationActive)return;
		if (m_OnUpdateCallback != nullptr)
		{
			m_OnUpdateCallback();						
		}
	}

	//////////////////////////////////////////////////////////////////////////
	void CGameApplication::Draw()
	{
#ifndef NBG_OSX
#ifdef NBG_ANDROID
		if (m_DrawCounter == 0 || m_FramesCount>30)return;
#endif
		if (m_DrawCounter == 0 || m_FramesCount>60)return;
#endif
		m_FramesCount++;
        g_Render->Begin();
#ifndef NBG_IOS
		g_StencilHelper->Begin();
		Transform trans;
		g_StencilHelper->DrawRectangle(&trans,g_System->GetGameBounds());
		g_StencilHelper->Switch();
#endif
		if (m_OnDrawCallback != nullptr)
		{
			m_RenderState.Reset();
			m_OnDrawCallback(m_RenderState);
		}
#ifndef NBG_IOS
		g_StencilHelper->End();
#endif
		g_Render->End();
		if (m_AfterLoopCallback != nullptr)
		{
			m_AfterLoopCallback();
		}		
	}
	
	//////////////////////////////////////////////////////////////////////////
	CGameApplication * CGameApplication::SetFullscreenSize(const int w, const int h)
	{
		m_FullscreenSize.x = w;
		m_FullscreenSize.y = h;
		return this;
	}

	//////////////////////////////////////////////////////////////////////////
	CGameApplication * CGameApplication::SetLogicalSize(const int w, const int h)
	{
		m_LogicalSize.x = w;
		m_LogicalSize.y = h;
		return this;
	}

	//////////////////////////////////////////////////////////////////////////
	CGameApplication * CGameApplication::SetWindowedSize(const int w, const int h)
	{
		m_WindowedSize.x = w;
		m_WindowedSize.y = h;
		return this;
	}

	//////////////////////////////////////////////////////////////////////////
	CGameApplication * CGameApplication::SetFontsPath(const std::string &path)
	{
		m_FontsPath = path;
		return this;
	}

	//////////////////////////////////////////////////////////////////////////
	CGameApplication * CGameApplication::SetStringsPath(const std::string &path)
	{
		m_StringsPath = path;
		return this;
	}

	//////////////////////////////////////////////////////////////////////////
	CGameApplication * CGameApplication::SetConfigPath(const std::string &configPath)
	{
		m_ConfigPath = configPath;
		return this;
	}

	//////////////////////////////////////////////////////////////////////////
	CGameApplication * CGameApplication::SetAtlasesPath(const std::string &configPath)
	{
		m_AtlasesPath = configPath;
		return this;
	}

	//////////////////////////////////////////////////////////////////////////
	CGameApplication * CGameApplication::SetEditionsPath(const std::string &xml, const std::string &folder)
	{
		m_EditionsXMLPath = xml;
		m_EditionsFolderPath = folder;
		return this;
	}

	//////////////////////////////////////////////////////////////////////////
	CGameApplication * CGameApplication::SetFullscreen(const bool fullscreen)
	{
		m_IsFullscreen = fullscreen;
		if (fullscreen)
		{
			m_CurrentSize = m_FullscreenSize;
		}
		else
		{
			m_CurrentSize = m_WindowedSize;
		}
		m_OperatingSystem->SetFullscreenMode(fullscreen);
		return this;
	}
	

	//////////////////////////////////////////////////////////////////////////
	NBG::CConfig * CGameApplication::GetConfig()
	{
		return &m_Config;
	}

	//////////////////////////////////////////////////////////////////////////
	NBG::CLog * CGameApplication::GetLog()
	{
		return &m_Log;
	}
	
	//////////////////////////////////////////////////////////////////////////
	CFontsManager * CGameApplication::GetFontsManager()
	{
		return &m_FontsManager;
	}

	//////////////////////////////////////////////////////////////////////////
	CLocalizationManager * CGameApplication::GetLocalizationManager()
	{
		return &m_LocalizationManager;
	}

	//////////////////////////////////////////////////////////////////////////
	CPlayersManager * CGameApplication::GetPlayersManager()
	{
		return &m_PlayersManager;
	}

	//////////////////////////////////////////////////////////////////////////
	TheoraVideoManager * CGameApplication::GetTheoraVideoManager()
	{
		return m_TheoraVideoManager;
	}

	//////////////////////////////////////////////////////////////////////////
	CSoundManager * CGameApplication::GetSoundManager()
	{
		return &m_SoundManager;
	}	

	//////////////////////////////////////////////////////////////////////////
	COperatingSystem * CGameApplication::GetOperatingSystem()
	{
		return CAST(COperatingSystem*, m_OperatingSystem);
	}

	//////////////////////////////////////////////////////////////////////////
	CTimeManager * CGameApplication::GetTimeManager()
	{
		return CAST(CTimeManager*, m_TimeManager);
	}

	//////////////////////////////////////////////////////////////////////////
	CTextureManager * CGameApplication::GetTextureManager()
	{
		return CAST(CTextureManager*, m_TextureManager);
	}

	//////////////////////////////////////////////////////////////////////////
	CFileSystem * CGameApplication::GetFileSystem()
	{
		return CAST(CFileSystem*, m_FileSystem);
	}

	//////////////////////////////////////////////////////////////////////////
	CAtlasHelper * CGameApplication::GetAtlasHelper()
	{
		return &m_AtlasHelper;
	}

	//////////////////////////////////////////////////////////////////////////
	CResourcesManager * CGameApplication::GetResourcesManager()
	{
		return &m_ResourcesManager;
	}

	//////////////////////////////////////////////////////////////////////////
	CEditionHelper * CGameApplication::GetEditionHelper()
	{
		return &m_EditionHelper;
	}	

	//////////////////////////////////////////////////////////////////////////
	CRender * CGameApplication::GetRender()
	{
		return &m_Render;
	}

	//////////////////////////////////////////////////////////////////////////
	CSystem * CGameApplication::GetSystem()
	{
		return &m_System;
	}

	//////////////////////////////////////////////////////////////////////////
	CInput * CGameApplication::GetInput()
	{
		return &m_Input;
	}

	//////////////////////////////////////////////////////////////////////////
	CRandom * CGameApplication::GetRandom()
	{
		return &m_Random;
	}

	//////////////////////////////////////////////////////////////////////////
	CBinaryHelper * CGameApplication::GetBinaryHelper()
	{
		return &m_BinaryHelper;
	}

	//////////////////////////////////////////////////////////////////////////
	CStencilHelper * CGameApplication::GetStencilHelper()
	{
		return &m_StencilHelper;
	}

	//////////////////////////////////////////////////////////////////////////
	void CGameApplication::SetWindowTitle(const std::wstring &title)
	{
		m_OperatingSystem->SetTitle(title);
	}

	//////////////////////////////////////////////////////////////////////////
	double CGameApplication::GetFrameTime()
	{
		return m_FrameTime;
	}

	//////////////////////////////////////////////////////////////////////////
	double CGameApplication::GetAppTime()
	{
		return m_AppTime;
	}

	//////////////////////////////////////////////////////////////////////////
	void CGameApplication::ResetAppTime()
	{
		m_AppTime = 0.0f;
	}

	//////////////////////////////////////////////////////////////////////////
	Vector & CGameApplication::GetWindowSize()
	{
		if (m_System.IsMobileSystem())
		{
			auto b = g_System->GetGameBounds();
			return b;
		}
		return m_CurrentSize;
	}

	//////////////////////////////////////////////////////////////////////////
	bool CGameApplication::IsFullscreen()
	{
		return m_IsFullscreen;
	}
}
