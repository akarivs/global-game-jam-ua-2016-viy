#include <string>
#include <vector>
#include <map>
#include <mutex>
#include <algorithm>

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Color.h>
#include <Framework/Datatypes/FloatColor.h>
#include <Framework/Datatypes/Event.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/IntVector.h>
#include <Framework/Datatypes/Texture.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Matrix.h>
#include <Framework/Datatypes/Matrix3x3.h>
#include <Framework/Datatypes/Transform.h>

#include <Framework/Structures.h>

#include <Framework/Global/Input.h>
#include <Framework/Global/Render.h>
#include <Framework/Global/Render/RenderState.h>
#include <Framework/Global/System.h>
#include <Framework/Global/Random.h>
#include <Framework/Global/TimeManager.h>

#include <Framework/Global/ResourcesManager.h>
#include <Framework/Global/Resources/TextureResource.h>
#include <Framework/Global/Resources/XMLResource.h>

#include <Framework/Helpers/AtlasHelper.h>
#include <Framework/Helpers/BinaryHelper.h>
#include <Framework/Helpers/EditionHelper.h>
#include <Framework/Helpers/StencilHelper.h>

#include <Framework/Utils/StringUtils.h>
#include <Framework/Utils/MathUtils.h>
#include <Framework/Utils/Config.h>
#include <Framework/Utils/Log.h>

#include <Framework/GameApplication.h>


namespace NBG
{
	extern CGameApplication * g_GameApplication;
	extern std::mutex * g_GlobalMutex;
}


#define g_Input g_GameApplication->GetInput()
#define g_MousePos g_Input->GetMousePos()
#define g_System g_GameApplication->GetSystem()
#define g_Render g_GameApplication->GetRender()
#define g_Random g_GameApplication->GetRandom()

#define g_Config			g_GameApplication->GetConfig()
#define g_Log g_GameApplication->GetLog()
#define g_FrameTime			g_GameApplication->GetFrameTime()
#define g_FontsManager		g_GameApplication->GetFontsManager()
#define g_LocaleManager		g_GameApplication->GetLocalizationManager()
#define g_PlayersManager	g_GameApplication->GetPlayersManager()
#define g_SoundManager		g_GameApplication->GetSoundManager()
#define g_Cursor			g_GameApplication->GetCursor()
#define g_FileSystem		g_GameApplication->GetFileSystem()
#define g_ResManager		g_GameApplication->GetResourcesManager()
#define g_TimeManager		g_GameApplication->GetTimeManager()

#define g_AtlasHelper		g_GameApplication->GetAtlasHelper()
#define g_EditionHelper		g_GameApplication->GetEditionHelper()
#define g_BinaryHelper		g_GameApplication->GetBinaryHelper()
#define g_StencilHelper		g_GameApplication->GetStencilHelper()

using namespace NBG;

#ifndef __forceinline
	#define __forceinline inline
#endif