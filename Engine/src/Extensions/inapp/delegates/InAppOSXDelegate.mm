#include "InAppOSXDelegate.h"

#include "../InAppManager.h"
#include <thread>

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>


#ifdef NBG_OSX
#import <Foundation/Foundation.h>
#import "StoreKit/StoreKit.h"


#define kProductsLoadedNotification         @"ProductsLoaded"
#define kProductPurchasedNotification       @"ProductPurchased"
#define kProductPurchaseFailedNotification  @"ProductPurchaseFailed"

@interface IAPHelper_objc : NSObject   {
    NSSet * _productIdentifiers;
    NSArray * _products;
    NSMutableSet * _purchasedProducts;
    SKProductsRequest * _request;
    
}

@property (retain) NSSet *productIdentifiers;
@property (retain) NSArray * products;
@property (retain) NSMutableSet *purchasedProducts;
@property (retain) SKProductsRequest *request;

@property NBG::inapp::CInAppOSXDelegate * cppDelegate;

- (void)requestProducts;
- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers;
- (bool)canMakePayments;
- (void)setNullCallback;
- (void)buyProductIdentifier:(NSString *)productIdentifier;
- (int)getProductIndexByIdentifier:(NSString*)productIdentifier;
- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue;
- (void)userTryRestoreTransaction;
- (void)paymentQueue;
+ (id) sharedHelper;
//- (void)buyProductIdentifier:(NSString *)productIdentifier;

@end
#endif

namespace NBG
{
    namespace inapp
    {
        //////////////////////////////////////////////////////////////////////////
        CInAppOSXDelegate::CInAppOSXDelegate()
        {
            
        }
        
        //////////////////////////////////////////////////////////////////////////
        CInAppOSXDelegate::~CInAppOSXDelegate()
        {
            
        }
        
        //////////////////////////////////////////////////////////////////////////
        void CInAppOSXDelegate::Init()
        {
            // Locate the receipt
            NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
            
            // Test whether the receipt is present at the above path
            if(![[NSFileManager defaultManager] fileExistsAtPath:[receiptURL path]])
            {
                // Validation fails
                exit(173);
                return;
            }
            // Proceed with further receipt validation steps
            LoadProducts("xml/inapp.xml");
        }
  
        
        //////////////////////////////////////////////////////////////////////////
        void CInAppOSXDelegate::LoadProducts(const std::string &path)
        {
            LoadFromXML(path);
            
            std::vector<std::string> data;
            for(auto vec = m_InApps.begin(); vec != m_InApps.end(); vec++)
            {
                data.push_back(vec->second.key);
            }
            
            /* Exploiting Clang's block->lambda bridging. */
            id nsstrings = [NSMutableArray new];
            std::for_each(data.begin(), data.end(), ^(std::string str) {
                id nsstr = [NSString stringWithUTF8String:str.c_str()];
                [nsstrings addObject:nsstr];
            });

            SKProductsRequest *productsRequest = [[SKProductsRequest alloc]initWithProductIdentifiers:[NSSet setWithArray:nsstrings]];
                
            // Keep a strong reference to the request.
            productsRequest.delegate = [IAPHelper_objc sharedHelper];
            [[IAPHelper_objc sharedHelper] setCppDelegate:this];
            [productsRequest start];
        }
        
        void CInAppOSXDelegate::ReSendRequest()
        {
            std::vector<std::string> data;
            for(auto vec = m_InApps.begin(); vec != m_InApps.end(); vec++)
            {
                data.push_back(vec->second.key);
            }
            
            /* Exploiting Clang's block->lambda bridging. */
            id nsstrings = [NSMutableArray new];
            std::for_each(data.begin(), data.end(), ^(std::string str) {
                id nsstr = [NSString stringWithUTF8String:str.c_str()];
                [nsstrings addObject:nsstr];
            });
            
            SKProductsRequest *productsRequest = [[SKProductsRequest alloc]initWithProductIdentifiers:[NSSet setWithArray:nsstrings]];
            
            // Keep a strong reference to the request.
            productsRequest.delegate = [IAPHelper_objc sharedHelper];
            [productsRequest start];

        }
        
        //////////////////////////////////////////////////////////////////////////
        bool CInAppOSXDelegate::Purchase(const std::string &id)
        {
            std::string key = m_InApps[id].key;
            [[IAPHelper_objc sharedHelper] buyProductIdentifier:[NSString stringWithUTF8String:key.c_str()]];
            return true;
        }
        
        //////////////////////////////////////////////////////////////////////////
        void CInAppOSXDelegate::SetProductInfo(const std::string &id, const std::string &title, const std::string &description, const float price, const std::string &priceLocale)
        {
            auto & el = m_InApps[m_InAppsKey[id]];
            el.title = title;
            el.description = description;
            el.price = price;
            el.price_symbol = priceLocale;
            el.price_string = priceLocale;
        }
        
        //////////////////////////////////////////////////////////////////////////
        void CInAppOSXDelegate::RestorePurchases()
        {
             [[IAPHelper_objc sharedHelper] userTryRestoreTransaction];
        }
    }
}

@interface IAPHelper_objc () <SKProductsRequestDelegate,SKPaymentTransactionObserver>
@end

@implementation IAPHelper_objc

@synthesize productIdentifiers = _productIdentifiers;
@synthesize products = _products;
@synthesize purchasedProducts = _purchasedProducts;
@synthesize request = _request;


+ (IAPHelper_objc *)sharedHelper
{
    static dispatch_once_t once;
    static IAPHelper_objc * sharedHelper;
    
    dispatch_once(&once,
                  ^{
                      NSSet * productIdentifiers = [NSSet setWithObjects:
                                                    @"rutaelansesoftBonusBucksTRAD1", // наш продукт 1
                                                    @"rutaelansesoftUpgradeToFullTRAD1", //  наш продукт 2
                                                    nil];
                      sharedHelper = [[self alloc] initWithProductIdentifiers:productIdentifiers];
                  }
                  );
    return sharedHelper;
}

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers{
    
    /*if ((self = [super init])) {
        _productIdentifiers = [productIdentifiers retain];
        
        // Check for previously purchased products
        NSMutableSet * purchasedProducts = [NSMutableSet set];
        for (NSString * productIdentifier in _productIdentifiers)
        {
            BOOL productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:productIdentifier];
            if (productPurchased)
            {
                [purchasedProducts addObject:productIdentifier];
                NSLog(@"Previously purchased: %@", productIdentifier);
            }
            NSLog(@"Not purchased: %@", productIdentifier);
        }
        self.purchasedProducts = purchasedProducts;
    }*/
    return self;
}

- (bool)canMakePayments
{
    return [SKPaymentQueue canMakePayments];
}
/*
- (void)requestProductsWithCallback:(iOSBridge::Callbacks::IAPCallback*)callback
{
    //latestIAPCallback = callback;
    [self requestProducts];
}*/

-(void) setNullCallback
{
    
}
- (void)requestProducts
{
    self.request = [[SKProductsRequest alloc] initWithProductIdentifiers:_productIdentifiers];
    _request.delegate = self;
    [_request start];
    
}

// Request fail.
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error{
    NSLog(@"Fail request! Error: %@", error);
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));
    self.cppDelegate->ReSendRequest();
}

// productsRequest это ф-ция обратного вызова (протокол SKProductsRequestDelegate) , которая начинает работать после получения ответа от сервера
// запрос к серверу осуществляет ф-ция requestProduct
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
      [[SKPaymentQueue defaultQueue] addTransactionObserver:[IAPHelper_objc sharedHelper]];
    
    //NSLog(@"Received products results...");
    self.products = response.products;
    self.request = nil;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductsLoadedNotification object:_products];
    
    
    if (self.products.count>0)
    {
        /*CCArray* items = CCArray::create();
        items->retain();*/
        
        NSArray * skProducts = response.products;
        for (SKProduct * skProduct in skProducts) {
            NSLog(@"Найден продукт: %@ %@ %0.2f",
                  skProduct.productIdentifier,
                  skProduct.localizedTitle,
                  skProduct.price.floatValue);

            NSNumberFormatter *priceFormatter = [[NSNumberFormatter alloc] init];
            [priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
            [priceFormatter setLocale:skProduct.priceLocale];
            
            self.cppDelegate->SetProductInfo(skProduct.productIdentifier.UTF8String,skProduct.localizedTitle.UTF8String,skProduct.localizedDescription.UTF8String, skProduct.price.floatValue, [priceFormatter stringFromNumber:skProduct.price].UTF8String);
            
            /*iOSBridge::Callbacks::IAPItem* item = new iOSBridge::Callbacks::IAPItem();
            item->identification = skProduct.productIdentifier.UTF8String;
            item->name = std::string("noname");
            item->localizedDescription = skProduct.localizedDescription.UTF8String;
            item->localizedTitle = skProduct.localizedTitle.UTF8String;
            items->addObject(item);*/
        }
        /*if (latestIAPCallback)
            latestIAPCallback->productsDownloaded(items);
        }*/
    }

}

- (void)productsLoaded:(NSNotification *)notification {
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (void)recordTransaction:(SKPaymentTransaction *)transaction {
    // Optional: Record the transaction on the server side...
}


- (void)provideContent:(NSString *)productIdentifier {
    
    //NSLog(@"Toggling flag for: %@", productIdentifier);
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:productIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [_purchasedProducts addObject:productIdentifier];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchasedNotification object:productIdentifier];
    
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    
    //NSLog(@"completeTransaction...");
    
    [self recordTransaction: transaction];
    //
    [self provideContent: transaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    NSString *product = transaction.payment.productIdentifier;
    std::string stdproduct =  std::string([product UTF8String]);
    
    ///Завершаем успешно покупку
    auto callback = inapp::CInAppManager::GetInstance()->GetCallback();
    if (callback)
    {
        callback(self.cppDelegate->GetKey(stdproduct), inapp::EInAppTransactionStatus::SUCCESS);
    }
    inapp::CInAppManager::GetInstance()->HideLoader();
}

- (void)userTryRestoreTransaction {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    
    //NSLog(@"restoreTransaction...");
    
    [self recordTransaction: transaction];
    [self provideContent: transaction.originalTransaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    NSString *product = transaction.payment.productIdentifier;
    std::string stdproduct =  std::string([product UTF8String]);
    
    ///Восстанавливаем покупку
    /*
     auto callback = self.cppDelegate->GetCallback();
    if (callback)
    {
        callback(self.cppDelegate->GetKey(stdproduct), inapp::EInAppTransactionStatus::RESTORED);
    }
     */
}
- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    NSLog(@"%@",error);
    auto callback = inapp::CInAppManager::GetInstance()->GetCallback();
    if (callback)
    {
        callback("", inapp::EInAppTransactionStatus::RESTORE_FAIL);
    }
    inapp::CInAppManager::GetInstance()->HideLoader();
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    NSMutableArray* purchasedItemIDs;
    purchasedItemIDs = [[NSMutableArray alloc] init];
    NSLog(@"received restored transactions: %i", queue.transactions.count);
    int cnt = queue.transactions.count;
    if (!cnt)
    { // пытался восстановить не существующую покупку
        auto callback = inapp::CInAppManager::GetInstance()->GetCallback();
        if (callback)
        {
            callback("", inapp::EInAppTransactionStatus::RESTORE_FAIL);
        }
    }
    else {
        for (SKPaymentTransaction *transaction in queue.transactions)
        {
            //NSString *productID = transaction.payment.productIdentifier;
            [self recordTransaction: transaction];
            [self provideContent: transaction.originalTransaction.payment.productIdentifier];
            NSString *product = transaction.payment.productIdentifier;
            std::string stdproduct =  std::string([product UTF8String]);
            ///Восстановили покупку
            auto callback = inapp::CInAppManager::GetInstance()->GetCallback();
            if (callback)
            {
                callback(self.cppDelegate->GetKey(stdproduct), inapp::EInAppTransactionStatus::RESTORED);
            }
        }
    }
    inapp::CInAppManager::GetInstance()->HideLoader();
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        NSLog(@"Transaction error: %@", transaction.error.localizedDescription);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchaseFailedNotification object:transaction];
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    NSString *product = transaction.payment.productIdentifier;
    std::string stdproduct =  std::string([product UTF8String]);
    
    ///Отменили покупку продукта
    auto callback = inapp::CInAppManager::GetInstance()->GetCallback();
    if (callback)
    {
        callback(self.cppDelegate->GetKey(stdproduct), inapp::EInAppTransactionStatus::CANCELED);
    }
    inapp::CInAppManager::GetInstance()->HideLoader();
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    }
}

- (void)buyProductIdentifier:(NSString *)productIdentifier
{
    NSLog(@"Покупаем %@...", productIdentifier);
    
    for (SKProduct * product in _products)
    {
        if ([product.productIdentifier isEqualToString:productIdentifier])
        {
            SKPayment * payment = [SKPayment paymentWithProduct:product];
            [[SKPaymentQueue defaultQueue] addPayment:payment];
            return;
        }
    }
    
    inapp::CInAppManager::GetInstance()->ShowErrorMessage();
    inapp::CInAppManager::GetInstance()->HideLoader();
}

- (int)getProductIndexByIdentifier:(NSString*)productIdentifier
{
    NSLog(@"Ищем индекс для %@...", productIdentifier);
    int index=0;
    bool bFound = NO;
    for (SKProduct * product in _products)
    {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            bFound=YES;
            break;
        }
        index++;
    }
    if (!bFound) index=-1;
        return index;
}


- (void)dealloc
{
   /* [_productIdentifiers release];
    _productIdentifiers = nil;
    [_products release];
    _products = nil;
    [_purchasedProducts release];
    _purchasedProducts = nil;
    [_request release];
    _request = nil;
    [super dealloc];*/
}



@end
