#ifndef INAPP_DELEGATE_OSX
#define INAPP_DELEGATE_OSX




#include <string>
#include <functional>



#include "../InAppStructures.h"
#include "../IInAppBaseDelegate.h"

namespace NBG
{
    namespace inapp
    {
        class CInAppOSXDelegate : public IInAppBaseDelegate
        {
        public:
            CInAppOSXDelegate();
            ~CInAppOSXDelegate();
            
            void Init();
            
            virtual void LoadProducts(const std::string &path);
            virtual bool Purchase(const std::string &id);
            virtual void RestorePurchases();
            
            void SetProductInfo(const std::string &id, const std::string &title, const std::string &description, const float price, const std::string &priceLocale);
            void ReSendRequest();
          
            std::string GetKey(const std::string &id)
            {
                return m_InAppsKey[id];
            }
            
        private:
        };
    }
}



#endif