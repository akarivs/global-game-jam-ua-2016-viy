#include "InAppWindowsDummyDelegate.h"
#include "../InAppManager.h"
namespace NBG
{
	namespace inapp
	{
		//////////////////////////////////////////////////////////////////////////
		CInAppWindowsDummyDelegate::CInAppWindowsDummyDelegate()
		{

		}

		//////////////////////////////////////////////////////////////////////////
		CInAppWindowsDummyDelegate::~CInAppWindowsDummyDelegate()
		{

		}

		//////////////////////////////////////////////////////////////////////////
		void CInAppWindowsDummyDelegate::Init()
		{
			LoadProducts("xml/inapp.xml");
		}

		//////////////////////////////////////////////////////////////////////////
		void CInAppWindowsDummyDelegate::LoadProducts(const std::string &path)
		{
			LoadFromXML(path);

			for (auto iter = m_InApps.begin(); iter != m_InApps.end(); iter++)
			{
				iter->second.price = 5.99;
				iter->second.price_symbol = "$";
				iter->second.SetPriceString();
			}
		}

		//////////////////////////////////////////////////////////////////////////
		bool CInAppWindowsDummyDelegate::Purchase(const std::string &id)
		{
			auto queue = CActionQueue::Create();
			queue->AddAction(CSleepAction::Create(1.0f));
			queue->AddAction(CCallbackAction::Create(std::bind(&CInAppWindowsDummyDelegate::DummyWait, this, id, EInAppTransactionStatus::SUCCESS)));
			CInAppManager::GetInstance()->GetLoader()->AddChild(queue);			
			return true;
		}

		//////////////////////////////////////////////////////////////////////////
		void CInAppWindowsDummyDelegate::RestorePurchases()
		{
			auto queue = CActionQueue::Create();
			queue->AddAction(CSleepAction::Create(1.0f));
			queue->AddAction(CCallbackAction::Create(std::bind(&CInAppWindowsDummyDelegate::DummyWait, this, "", EInAppTransactionStatus::RESTORED)));
			CInAppManager::GetInstance()->GetLoader()->AddChild(queue);			
		}

		//////////////////////////////////////////////////////////////////////////
		void CInAppWindowsDummyDelegate::DummyWait(const std::string &id, EInAppTransactionStatus status)
		{			
			if (status == EInAppTransactionStatus::SUCCESS)
			{
				auto callback = CInAppManager::GetInstance()->GetCallback();
				callback(id, EInAppTransactionStatus::SUCCESS);
			}
			else
			{
				auto callback = CInAppManager::GetInstance()->GetCallback();
				for (auto iter = m_InApps.begin(); iter != m_InApps.end(); iter++)
				{
					callback(iter->second.id, EInAppTransactionStatus::RESTORED);
				}
			}			
			CInAppManager::GetInstance()->HideLoader();
		}

	}
}