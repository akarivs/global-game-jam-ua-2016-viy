#ifndef INAPP_DELEGATE_WINDOWS_DUMMY
#define INAPP_DELEGATE_WINDOWS_DUMMY

#include <string>
#include <functional>


#include "../InAppStructures.h"
#include "../IInAppBaseDelegate.h"

namespace NBG
{
	namespace inapp
	{
		class CInAppWindowsDummyDelegate : public IInAppBaseDelegate
		{
		public:
			CInAppWindowsDummyDelegate();
			~CInAppWindowsDummyDelegate();

			virtual void LoadProducts(const std::string &path);
			virtual bool Purchase(const std::string &id);
			virtual void RestorePurchases();
			virtual void Init();
		private:
			void DummyWait(const std::string &id, EInAppTransactionStatus status);
		};
	}
}

#endif