#include "InAppManager.h"

#include "delegates/InAppWindowsDummyDelegate.h"
#include "delegates/InAppOSXDelegate.h"



namespace NBG
{
	namespace inapp
	{
		class CLoader : public CBaseWidget
		{
		public:
			OPTIMUS_CREATE(CLoader);
		private:
			CLoader()
			{
				SetCatchTouch(true);
			};

			virtual ReturnCodes OnMouseDown(const Vector &mousePos, const int mouseButton)
			{
				CBaseWidget::OnMouseDown(mousePos, mouseButton);
				return ReturnCodes::Block;
			}

			virtual ReturnCodes OnMouseUp(const Vector &mousePos, const int mouseButton)
			{
				CBaseWidget::OnMouseUp(mousePos, mouseButton);
				return ReturnCodes::Block;
			}

			virtual ReturnCodes OnMouseMove(const Vector &mousePos)
			{
				CBaseWidget::OnMouseMove(mousePos);
				return ReturnCodes::Block;
			}

			virtual ReturnCodes Update(const float dt)
			{
				if (m_IsDisabled)return ReturnCodes::Skip;
				CBaseWidget::Update(dt);
				return ReturnCodes::Block;
			}
		};



		CInAppManager * CInAppManager::self = nullptr;

		//////////////////////////////////////////////////////////////////////////
		CInAppManager::CInAppManager()
		{
            m_IsInit = false;
			m_ErrorWindow = nullptr;
		}
        
        //////////////////////////////////////////////////////////////////////////
        void CInAppManager::Init()
        {
            m_IsInit = true;
#ifdef NBG_WIN32
#ifdef INAPP_BFG
#else
            m_Delegate = new CInAppWindowsDummyDelegate();
            m_Delegate->Init();

#endif
#elif defined(NBG_OSX)
            m_Delegate = new CInAppOSXDelegate();
            m_Delegate->Init();
#endif
			

			m_Loader = CLoader::Create();
			m_Loader->SetImmortal(true);
			m_Loader->LoadFromXML("xml/inapp/loader.xml");
			m_Loader->SetVisible(false);
			m_Loader->SetDisabled(true);
            
        }

		//////////////////////////////////////////////////////////////////////////
		CInAppManager::~CInAppManager()
		{
            
		}

		//////////////////////////////////////////////////////////////////////////
		void CInAppManager::ShowLoader()
		{
            m_IsFullscreen = g_GameApplication->IsFullscreen();
            if (m_IsFullscreen)
            {
                g_GameApplication->SetFullscreen(false);
            }
            
			m_Loader->SetParent(nullptr);

			auto scene = optimus::managers::CScenesManager::GetInstance()->GetCurrentScene();
			auto & childs = scene->GetChilds();
			if (std::find(childs.begin(), childs.end(), m_Loader) == childs.end())
			{
				scene->AddChild(m_Loader);
			}		

			m_Loader->SetVisible(true);
			m_Loader->SetDisabled(false);

			m_Loader->SetLayer(100000000);
		}

		//////////////////////////////////////////////////////////////////////////
		void CInAppManager::HideLoader()
		{
			m_Loader->SetVisible(false);
			m_Loader->SetDisabled(true);
		}

		//////////////////////////////////////////////////////////////////////////
		void CInAppManager::LoadProducts(const std::string &path)
		{
            if (!m_IsInit)return;
			ShowLoader();
			m_Delegate->LoadProducts(path);
		}

		//////////////////////////////////////////////////////////////////////////
		bool CInAppManager::Purchase(const std::string &id)
		{
            if (!m_IsInit)return false;
			ShowLoader();
			m_Delegate->Purchase(id);
			return true;
		}

		//////////////////////////////////////////////////////////////////////////
		void CInAppManager::RestorePurchases()
		{
            if (!m_IsInit)return;
			ShowLoader();
			m_Delegate->RestorePurchases();
		}
        
        //////////////////////////////////////////////////////////////////////////
        void CInAppManager::ShowErrorMessage()
        {
			if (m_ErrorWindow)
			{
				m_ErrorWindow->SetParent(nullptr);

				auto scene = optimus::managers::CScenesManager::GetInstance()->GetCurrentScene();
				auto & childs = scene->GetChilds();
				if (std::find(childs.begin(), childs.end(), m_ErrorWindow) == childs.end())
				{
					scene->AddChild(m_ErrorWindow);
				}				
				m_ErrorWindow->SetLayer(100000000);
				m_ErrorWindow->Show();
			}			
        }        
	}
}