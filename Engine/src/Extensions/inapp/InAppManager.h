#ifndef INAPP
#define INAPP

#include <string>
#include <functional>
#include <Extensions/optimus/optimus.h>


#include "InAppStructures.h"
#include "IInAppBaseDelegate.h"

namespace NBG
{
	namespace inapp
	{
		class CInAppManager
		{
		public:
			~CInAppManager();
			static CInAppManager * GetInstance()
			{
				if (self == nullptr)
				{
					self = new CInAppManager();
				}
				return self;
			}
            void Init();
			void LoadProducts(const std::string &path);

			bool Purchase(const std::string &id);
			void RestorePurchases();
            
            void SetCallback(IN_APP_PURCHASE_CALLBACK callback)
            {
                m_Callback = callback;
            }
            IN_APP_PURCHASE_CALLBACK GetCallback()
            {
                return m_Callback;
            }

			optimus::ui::CBaseWidget * GetLoader()
			{
				return m_Loader;
			}


			IInAppBaseDelegate * GetDelegate()
			{
				return m_Delegate;
			}

			void ShowLoader();
			void HideLoader();
            
            void ShowErrorMessage();

		private:
			CInAppManager();
			static CInAppManager * self;
            bool m_IsInit;
			IInAppBaseDelegate * m_Delegate;

			optimus::ui::CBaseWidget * m_Loader;
            bool m_IsFullscreen;

			GETTER_SETTER(CWindowWidget*, m_ErrorWindow, ErrorWindow);

		
            
            IN_APP_PURCHASE_CALLBACK m_Callback;
		};
	}
}

#endif