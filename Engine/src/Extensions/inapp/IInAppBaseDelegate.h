#ifndef INAPP_DELEGATE_BASE
#define INAPP_DELEGATE_BASE

#include <string>
#include <functional>
#include <map>
#include <iomanip>
#include <iostream>

#include <Framework.h>

#include "InAppStructures.h"

namespace NBG
{
	namespace inapp
	{
        struct SInAppDescription
        {
            std::string id;
            std::string key;
            float price;
			std::string price_symbol;
            std::string title;
            std::string description;
			std::string price_string;

			void SetPriceString()
			{				
				std::stringstream stream;
				stream << std::fixed << std::setprecision(2) << price;
				price_string = price_symbol + stream.str();
			}

        };
        
		class IInAppBaseDelegate
		{
		public:						
			virtual void LoadProducts(const std::string &path) = 0;
			virtual bool Purchase(const std::string &id) = 0;
			virtual void RestorePurchases() = 0;
            virtual void Init() = 0;

			const SInAppDescription & GetInApp(const std::string &id)
			{
				return m_InApps[id];
			}
		protected:
            std::map<std::string, SInAppDescription> m_InApps;
            std::map<std::string, std::string> m_InAppsKey;
            void LoadFromXML(const std::string &path)
            {
                auto xml = CAST(CXMLResource*, g_ResManager->GetResource(path))->GetXML();
                auto inapps = xml->child("inapps");
                
                for (pugi::xml_node inapp = inapps.child("inapp"); inapp; inapp = inapp.next_sibling("inapp"))
                {
                    std::string id = inapp.attribute("id").value();
                    std::string key = inapp.attribute("key").value();
                    
                    SInAppDescription inappDesc;
                    inappDesc.id = id;
                    inappDesc.key = key;
                    inappDesc.price = 0.0;
                    
                    m_InApps[id] = inappDesc;
                    m_InAppsKey[key] = id;
                }
            }
		};
	}
}

#endif