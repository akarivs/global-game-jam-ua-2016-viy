#ifndef IN_APP_STRUCTURES
#define IN_APP_STRUCTURES

namespace NBG
{
	namespace inapp
	{
		enum class EInAppTransactionStatus
		{
			SUCCESS,
			ERROR_UNDEFINED,
			ERROR_NO_FUNDS,
            RESTORED,
            RESTORE_FAIL,
            CANCELED
		};
#define IN_APP_PURCHASE_CALLBACK std::function<void(const std::string&, EInAppTransactionStatus)>
	}
}



#endif