#ifndef OPTIMUS_HELPERS_ACTIONS
#define OPTIMUS_HELPERS_ACTIONS

#include <initializer_list>

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "../ui/BaseWidget.h"
#include "actions/AlphaToAction.h"
#include "actions/BaseAction.h"
#include "actions/ColorToAction.h"
#include "actions/FloatToAction.h"
#include "actions/MoveToAction.h"
#include "actions/MoveToParticleAction.h"
#include "actions/MoveToBezierAction.h"
#include "actions/MoveToBezierParticleAction.h"
#include "actions/MoveByAction.h"
#include "actions/MoveByParticleAction.h"
#include "actions/HideAction.h"
#include "actions/PlaySoundAction.h"
#include "actions/RotateToAction.h"
#include "actions/RotateByAction.h"
#include "actions/ScaleToAction.h"
#include "actions/ScaleByAction.h"
#include "actions/SleepAction.h"
#include "actions/ShowAction.h"
#include "actions/CallbackAction.h"
#include "actions/CallbackLUAAction.h"

#include <queue>
#include <deque>


namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Класс, реализующий работу с цепочками действий, которые можно назначить из xml.
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/		


			class CActionQueue : public ui::CBaseWidget
			{
			public:
				OPTIMUS_CREATE(CActionQueue);
				virtual ~CActionQueue();
				
				enum class RepeatType
				{
					RepeatNone,
					RepeatLimited,
					RepeatUnlimited,
					RepeatPingPongUnlimited,
					RepeatPingPongLimited
				};

				virtual void Init();


				void Start();
				void Pause();
				void Stop();

				void SetRepeatType(const RepeatType type, const int count = 0)
				{
					m_RepeatType = type;
					m_RepeatCount = count;
				}

				void __LuaSetRepeatType(const int type, const int count = 0)
				{
					SetRepeatType(RepeatType(type), count);					
				}

				void AddAction(CBaseAction * action);
				void AddActions(std::initializer_list<CBaseAction*> l);

				CActionQueue * SkipAction();

				CBaseAction * GetAction();
				void PopAction();

				bool IsEmpty();

				std::deque<CBaseAction*>& GetActions();

				
				virtual ui::ReturnCodes Update(const float dt);
			private:
				CActionQueue();
				std::deque<CBaseAction*> m_Actions;
				std::deque<CBaseAction*> m_BaseActions;

				bool TryToDestroy();

				RepeatType m_RepeatType;
				int m_RepeatCount;

			};
		}
	}

}
#endif //OPTIMUS_HELPERS_ACTIONS
