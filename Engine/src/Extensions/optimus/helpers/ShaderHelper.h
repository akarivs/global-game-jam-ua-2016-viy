#ifndef OPTIMUS_HELPERS_SHADER
#define OPTIMUS_HELPERS_SHADER

#ifdef NBG_WIN32
#include <d3d9.h>
#include <d3dx9.h>
#endif

#include <Framework/Global/Resources/TextureResource.h>

#include <string>

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Класс, для помощи работы с шейдерами
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CShader		
			{
			public:
#ifdef NBG_WIN32
				LPDIRECT3DVERTEXSHADER9      vertexShader;				
				LPD3DXCONSTANTTABLE          vertexTable;
				LPDIRECT3DPIXELSHADER9		 pixelShader;
				LPD3DXCONSTANTTABLE			 pixelTable;

				LPD3DXEFFECT effect;
				D3DXHANDLE texture;
#endif
			};

			class CShaderHelper
			{
			public:
				/// @name Конструктор деструктор
				static CShaderHelper * GetInstance()
				{
					if (self == nullptr)
					{
						self = new CShaderHelper();
					}
					return self;
				}
				~CShaderHelper();			
				CShader * CreateShader(const std::string &shaderPath);
				void UseShader(CShader * shader, NBG::CTextureResource * texture);
				void EndShader(CShader * shader);
			private:			
				CShaderHelper();
				static CShaderHelper * self;				
			};
		}
	}	
}

#endif ///OPTIMUS_HELPERS_LUA
