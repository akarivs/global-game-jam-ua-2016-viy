#ifndef OPTIMUS_HELPERS_RATE_MY_APP
#define OPTIMUS_HELPERS_RATE_MY_APP

#include "../ui/WindowWidget.h"
#include <Framework/Utils/Config.h>
#include <string>

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Класс, для показа окна Rate My App
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2015 New Bridge Games
			*
			*/
			
			class CRateMyAppHelper : public ui::CWindowWidget
			{
			public:
				/// @name Конструктор деструктор
				static CRateMyAppHelper * GetInstance()
				{
					if (self == nullptr)
					{
						self = CRateMyAppHelper::Create();
					}
					return self;
				}		
				~CRateMyAppHelper();
				virtual void Init();				
				bool IsCanShow();
			private:			
				OPTIMUS_CREATE(CRateMyAppHelper);
				CRateMyAppHelper();
				static CRateMyAppHelper * self;

				CConfig config;
			};
		}
	}	
}
#endif ///OPTIMUS_HELPERS_RATE_MY_APP
