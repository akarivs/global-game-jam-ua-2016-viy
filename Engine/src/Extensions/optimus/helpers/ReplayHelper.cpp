#include "ReplayHelper.h"

#include <Framework.h>
#include "../optimus.h"


namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{

			CReplayHelper * CReplayHelper::self = nullptr;
			//////////////////////////////////////////////////////////////////////////
			CReplayHelper::CReplayHelper()
			{
				m_RecordReplay = false;
				m_IsPlay = false;

				m_OnMouseMoveCallback = nullptr;
				m_OnMouseUpCallback = nullptr;
				m_OnMouseDownCallback = nullptr;
			}

			//////////////////////////////////////////////////////////////////////////
			CReplayHelper::~CReplayHelper()
			{

			}

			//////////////////////////////////////////////////////////////////////////
			void CReplayHelper::StartRecord()
			{
				m_Events.clear();
				m_RecordReplay = true;

				m_StartTime = 0.0f;
				m_IsPlay = false;				
			}

			//////////////////////////////////////////////////////////////////////////
			void CReplayHelper::OnMouseMove(const Vector &mousePos)
			{
				if (!m_RecordReplay)return;
				
				SReplayEvent event;
				event.type = EReplayType::Sleep;
				event.sleepTime = m_StartTime;
				m_Events.push_back(event);

				m_StartTime = 0.0f;

				
				event.type = EReplayType::MouseMove;
				event.mousePos = mousePos;
				m_Events.push_back(event);
			}

			//////////////////////////////////////////////////////////////////////////
			void CReplayHelper::OnMouseDown(const Vector &mousePos, const int mouseButton)
			{
				if (!m_RecordReplay)return;	

				SReplayEvent event;
				event.type = EReplayType::Sleep;
				event.sleepTime = m_StartTime;
				m_Events.push_back(event);

				m_StartTime = 0.0f;

				event.type = EReplayType::MouseDown;
				event.mousePos = mousePos;
				event.mouseButton = mouseButton;
				m_Events.push_back(event);
			}

			//////////////////////////////////////////////////////////////////////////
			void CReplayHelper::OnMouseUp(const Vector &mousePos, const int mouseButton)
			{
				if (!m_RecordReplay)return;
				

				SReplayEvent event;
				event.type = EReplayType::Sleep;
				event.sleepTime = m_StartTime;
				m_Events.push_back(event);

				m_StartTime = 0.0f;

				
				event.type = EReplayType::MouseUp;
				event.mousePos = mousePos;
				event.mouseButton = mouseButton;
				m_Events.push_back(event);
			}


			//////////////////////////////////////////////////////////////////////////
			void CReplayHelper::StopRecord(const std::string & file)
			{
				if (!m_RecordReplay)return;				

				xml_document doc;
				auto replay = doc.append_child("replay");
				for (int i = 0; i < m_Events.size(); i++)
				{
					const auto & replayEvent = m_Events[i];
					auto event = replay.append_child("event");
					event.append_attribute("type").set_value((int)replayEvent.type);
					event.append_attribute("mouse_x").set_value(replayEvent.mousePos.x);
					event.append_attribute("mouse_y").set_value(replayEvent.mousePos.y);
					event.append_attribute("sleep").set_value(replayEvent.sleepTime);
				}
				std::ostringstream out;
				doc.save(out);
				std::string docStr = out.str();

				g_FileSystem->SaveFile(file, docStr.c_str(), docStr.size());

				m_RecordReplay = false;
			}

			//////////////////////////////////////////////////////////////////////////
			void CReplayHelper::StartReplay(const std::string & file)
			{
				m_RecordReplay = false;
				m_IsPlay = true;
				m_ReplayTimer = 0.0f;

                auto emptyQueue = std::queue<SReplayEvent>();
				m_Replay.swap(emptyQueue);

				auto xml = CAST(CXMLResource*, g_ResManager->GetResource(file))->GetXML();
				auto replay = xml->child("replay");

				for (pugi::xml_node event = replay.child("event"); event; event = event.next_sibling("event"))
				{
					std::string id = event.attribute("id").value();

					SReplayEvent eventS;
					eventS.type = (EReplayType)event.attribute("type").as_int();
					eventS.mousePos.x = event.attribute("mouse_x").as_float();
					eventS.mousePos.y = event.attribute("mouse_y").as_float();
					eventS.sleepTime = event.attribute("sleep").as_float();

					m_Replay.push(eventS);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CReplayHelper::Update(const float dt)
			{
				if (m_RecordReplay)
				{
					m_StartTime += dt;
				}
				else if (m_IsPlay)
				{
					m_ReplayTimer += dt;

					if (m_Replay.size() == 0)
					{
						m_IsPlay = false;
						CONSOLE("End replay");
						return;
					}

					auto &curEvt = m_Replay.front();
					if (curEvt.type == EReplayType::Sleep)
					{
						if (m_ReplayTimer >= curEvt.sleepTime)
						{
							m_ReplayTimer -= curEvt.sleepTime;
							m_Replay.pop();							

							if (m_Replay.size() == 0)
							{
								m_IsPlay = false;
								CONSOLE("End replay");
								return;
							}
							else
							{
								curEvt = m_Replay.front();
							}
						}
						else
						{
							return;
						}
					}	

					if (curEvt.type == EReplayType::MouseMove)
					{
						g_Input->SetMousePos(curEvt.mousePos.x, curEvt.mousePos.y, false);
						if (m_OnMouseMoveCallback)
						{
							m_OnMouseMoveCallback(curEvt.mousePos);
						}						
					}
					else if (curEvt.type == EReplayType::MouseDown)
					{
						if (m_OnMouseDownCallback)
						{
							m_OnMouseDownCallback(curEvt.mousePos, curEvt.mouseButton);
						}
					}
					else if (curEvt.type == EReplayType::MouseUp)
					{
						if (m_OnMouseUpCallback)
						{
							m_OnMouseUpCallback(curEvt.mousePos, curEvt.mouseButton);
						}
					}
					m_Replay.pop();
				}
			}

		}
	}
}