#include "ActionHelper.h"

#include "../optimus.h"
#include <algorithm>

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{


			//////////////////////////////////////////////////////////////////////////
			CActionQueue::CActionQueue():CBaseWidget()
			{

			}

			//////////////////////////////////////////////////////////////////////////
			CActionQueue::~CActionQueue()
			{
				for (int i = 0; i < m_Actions.size(); i++)
				{
					delete m_Actions[i];
				}
				m_Actions.clear();
			}

			//////////////////////////////////////////////////////////////////////////
			void CActionQueue::Init()
			{
				m_RepeatType  = RepeatType::RepeatNone;
				m_RepeatCount = 0;
			}

			//////////////////////////////////////////////////////////////////////////
			void CActionQueue::Start()
			{
				SetUpdatable(true);
			}

			//////////////////////////////////////////////////////////////////////////
			void CActionQueue::Pause()
			{
				SetUpdatable(false);
			}

			//////////////////////////////////////////////////////////////////////////
			void CActionQueue::Stop()
			{
				CBaseAction * action;
				while (IsEmpty() == false)
				{
					action = GetAction();
					action->Stop();
					PopAction();
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CActionQueue::AddAction(CBaseAction * action)
			{
				if (action == nullptr)
				{
					CONSOLE_ERROR("Action is nullptr!");
					return;

				}
				m_Actions.push_back(action);
				m_BaseActions.push_back(action);
			}

			//////////////////////////////////////////////////////////////////////////
			void CActionQueue::AddActions(std::initializer_list<CBaseAction*> l)
			{
				for (auto el : l)
				{
					m_Actions.push_back(el);
					m_BaseActions.push_back(el);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			CActionQueue * CActionQueue::SkipAction()
			{
				CBaseAction * action = GetAction();
				action->Stop();
				PopAction();
				if (IsEmpty())
				{
					SetDisabled(true);
					SetUpdatable(false);
					//Destroy();
					return NULL;
				}
				return this;
			}

			//////////////////////////////////////////////////////////////////////////
			CBaseAction * CActionQueue::GetAction()
			{
				return m_Actions.front();
			}

			//////////////////////////////////////////////////////////////////////////
			std::deque<CBaseAction*>& CActionQueue::GetActions()
			{
				return m_Actions;
			}

			//////////////////////////////////////////////////////////////////////////
			void CActionQueue::PopAction()
			{
				auto action = m_Actions.front();
				delete action;
				m_Actions.pop_front();
			}

			//////////////////////////////////////////////////////////////////////////
			bool CActionQueue::IsEmpty()
			{
				return m_Actions.size() == 0;
			}

			//////////////////////////////////////////////////////////////////////////
			bool CActionQueue::TryToDestroy()
			{
				if (m_RepeatType == RepeatType::RepeatUnlimited)
				{
					m_Actions = m_BaseActions;		
					for (auto action : m_Actions)
					{
						action->Reset();
					}
				}
				else if (m_RepeatType == RepeatType::RepeatPingPongUnlimited)
				{
					m_Actions = m_BaseActions;
					std::reverse(m_Actions.begin(), m_Actions.end());
					for (auto action : m_Actions)
					{
						action->Reset();
					}
				}
				else if (m_RepeatType == RepeatType::RepeatLimited)
				{
					if (m_RepeatCount == 0)
					{
						SetDisabled(true);
						SetUpdatable(false);
						//Destroy();
						return true;
					}
					m_RepeatCount--;
					m_Actions = m_BaseActions;
					for (auto action : m_Actions)
					{
						action->Reset();
					}
				}
				else if (m_RepeatType == RepeatType::RepeatPingPongLimited)
				{
					if (m_RepeatCount == 0)
					{
						SetDisabled(true);
						SetUpdatable(false);
						//Destroy();
						return true;
					}
					m_RepeatCount--;
					m_Actions = m_BaseActions;
					std::reverse(m_Actions.begin(), m_Actions.end());
					for (auto action : m_Actions)
					{
						action->Reset();
					}
				}
				else
				{
					SetDisabled(true);
					SetUpdatable(false);
					//Destroy();
					return true;
				}
				return false;
			}

			//////////////////////////////////////////////////////////////////////////
			ui::ReturnCodes CActionQueue::Update(const float dt)
			{					
				auto res = CBaseWidget::Update(dt);
				if (res > ui::ReturnCodes::SkipDontProcess)return res;

				if (IsEmpty())
				{
					if (TryToDestroy())
					{
						return ui::ReturnCodes::Skip;
					}
                    return ui::ReturnCodes::Skip;
				}

				auto queueIter = m_Actions.begin();

				CBaseAction * action = *queueIter;
				if (action->IsStarted() == false)
				{
					action->Start();
				}

				bool needContinue = false;				
				while (action->IsComplete() || action->GetActionTime() == 0.0f)
				{
					if (action->IsEnded())
					{
						bool needToNewUpdate = false;
						if (action->GetActionTime() == 0.0f)
						{
							needToNewUpdate = true;
						}		
						//delete action;
						queueIter = m_Actions.erase(queueIter);
						action = nullptr;
						if (IsEmpty())
						{
							needContinue = true;
						}
						else
						{
							if (needToNewUpdate)
							{
								Update(dt);
							}
						}

						break;
					}
					else
					{
						action->Update(dt);
						queueIter++;
						if (queueIter == m_Actions.end())
						{
							needContinue = false;
							break;
						}
						else
						{
							action = *queueIter;
							if (action->IsStarted() == false)
							{
								action->Start();
							}
						}
					}
				}				
				if (needContinue)
				{
					return ui::ReturnCodes::Skip;
				}
				if (action != nullptr)
				{
					action->Update(dt);
				}				
				return ui::ReturnCodes::Skip;
			}
		}
	}
}