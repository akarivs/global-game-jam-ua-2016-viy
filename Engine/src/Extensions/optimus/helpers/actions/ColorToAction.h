#ifndef OPTIMUS_HELPERS_ACTIONS_COLOR_TO
#define OPTIMUS_HELPERS_ACTIONS_COLOR_TO


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "../../ui/BaseWidget.h"
#include "../../optimus_enum.h"
#include "BaseAction.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Действие - сменить цвет объекта
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CColorToAction : public CBaseAction
			{
			public:
				/// @name Конструктор/деструктор
				static CColorToAction * Create(ui::CBaseWidget * object, const float time, Color color, Tween tween = Tween::EASE_LINEAR, const float blockTime = -1.0f)
				{
					return new CColorToAction(object, time, color, tween, blockTime);
				}

				virtual void Reset();
				
				virtual ~CColorToAction();
				virtual void Start();
				virtual void Stop();
				virtual bool IsEnded();

				///LUA HELPERS
				static CColorToAction * __LuaCreate(ui::CBaseWidget * object, const float time, float alpha)
				{
					return Create(object,time,alpha);
				}
			protected:
				virtual void VirtualUpdate(const float dt);
			private:
				CColorToAction(ui::CBaseWidget *, const float time, Color color, Tween tween, const float blockTime);
				ui::CBaseWidget * m_Object;								
				FloatColor m_Color;
				FloatColor m_StartColor;
				FloatColor m_BaseColor;
			};
		}
	}
}
#endif //OPTIMUS_HELPERS_ACTIONS_COLOR_TO
