#include "CallbackAction.h"

#include "../../optimus.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			//////////////////////////////////////////////////////////////////////////
			CCallbackAction::CCallbackAction(std::function<void()> func) :CBaseAction()
			{				
				m_Time = 0.0f;
				m_ActionTime = 0.0f;
				m_BlockTime = m_ActionTime;				
				m_Callback = func;
			}

			//////////////////////////////////////////////////////////////////////////
			CCallbackAction::CCallbackAction(std::function<void(void * data)> func, void * data) :CBaseAction()
			{
				m_Time = 0.0f;
				m_ActionTime = 0.0f;
				m_BlockTime = m_ActionTime;
				m_CallbackData = func;
				m_CallbackDataPointer = data;
			}

			//////////////////////////////////////////////////////////////////////////
			CCallbackAction::~CCallbackAction()
			{

			}		

			//////////////////////////////////////////////////////////////////////////
			void CCallbackAction::Reset()
			{
				CBaseAction::Reset();				
			}

			//////////////////////////////////////////////////////////////////////////
			void CCallbackAction::Start()
			{
				CBaseAction::Start();

				if (m_Callback != nullptr)
				{
					m_Callback();
				}
				else if (m_CallbackData != nullptr)
				{
					m_CallbackData(m_CallbackDataPointer);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CCallbackAction::Stop()
			{
				if (m_Callback != nullptr)
				{
					m_Callback();
				}
				else if (m_CallbackData != nullptr)
				{
					m_CallbackData(m_CallbackDataPointer);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			bool CCallbackAction::IsEnded()
			{
				return true;
			}

			//////////////////////////////////////////////////////////////////////////
			void CCallbackAction::VirtualUpdate(const float dt)
			{
				
			}
		}
	}
}
