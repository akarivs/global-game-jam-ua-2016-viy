#include "PlaySoundAction.h"

#include "../../optimus.h"
#include <Framework.h>

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			//////////////////////////////////////////////////////////////////////////
			CPlaySoundAction::CPlaySoundAction(const std::string &soundName):CBaseAction()
			{				
				m_Time = 0.0f;
				m_ActionTime = 0.0f;				
				m_BlockTime = 0.0f;				
				m_SoundName = soundName;
			}

			//////////////////////////////////////////////////////////////////////////
			CPlaySoundAction::~CPlaySoundAction()
			{

			}	

			//////////////////////////////////////////////////////////////////////////
			void CPlaySoundAction::Reset()
			{
				CBaseAction::Reset();
			}

			//////////////////////////////////////////////////////////////////////////
			void CPlaySoundAction::Start()
			{
				CBaseAction::Start();

				g_SoundManager->Play(m_SoundName);
			}

			//////////////////////////////////////////////////////////////////////////
			void CPlaySoundAction::Stop()
			{
				g_SoundManager->Play(m_SoundName);
			}

			//////////////////////////////////////////////////////////////////////////
			bool CPlaySoundAction::IsEnded()
			{
				return true;
			}
		}
	}
}
