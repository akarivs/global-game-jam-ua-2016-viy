#ifndef OPTIMUS_HELPERS_ACTIONS_MOVE_TO
#define OPTIMUS_HELPERS_ACTIONS_MOVE_TO


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "../../ui/BaseWidget.h"
#include "../../optimus_enum.h"
#include "BaseAction.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Действие - переместить объект
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CMoveToAction : public CBaseAction
			{
			public:
				/// @name Конструктор/деструктор
				static CMoveToAction * Create(ui::CBaseWidget * object, const float time, Vector position, Tween tween = Tween::EASE_LINEAR, const float blockTime = -1.0f)
				{
					return new CMoveToAction(object,time,position,tween,blockTime);
				}

				virtual void Reset();
				
				virtual ~CMoveToAction();
				virtual void Start();
				virtual void Stop();
				virtual bool IsEnded();

				///LUA HELPERS
				static CMoveToAction * __LuaCreate(ui::CBaseWidget * object, const float time, Vector position)
				{
					return Create(object,time,position);
				}
			protected:
				virtual void VirtualUpdate(const float dt);
			private:
				CMoveToAction(ui::CBaseWidget *, const float time, Vector position, Tween tween, const float blockTime);
				ui::CBaseWidget * m_Object;								
				Vector m_Position;
				Vector m_StartPosition;
				Vector m_BasePosition;
			};
		}
	}
}
#endif //OPTIMUS_HELPERS_ACTIONS_MOVE_TO
