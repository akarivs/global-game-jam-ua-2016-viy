#ifndef OPTIMUS_HELPERS_ACTIONS_CALLBACK
#define OPTIMUS_HELPERS_ACTIONS_CALLBACK


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "../../optimus_enum.h"
#include <functional>
#include "BaseAction.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Действие - вызвать колбэк
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CCallbackAction : public CBaseAction
			{
			public:
				/// @name Конструктор/деструктор
				static CCallbackAction * Create(std::function<void()> func)
				{
					return new CCallbackAction(func);
				}
				static CCallbackAction * Create(std::function<void(void *)> func, void * data)
				{
					return new CCallbackAction(func,data);
				}

				virtual void Reset();
				
				virtual ~CCallbackAction();
				virtual void Start();
				virtual void Stop();
				virtual bool IsEnded();
			protected:
				virtual void VirtualUpdate(const float dt);
			private:
				CCallbackAction(std::function<void()> func);
				CCallbackAction(std::function<void(void *)> func, void * data);

				std::function<void()> m_Callback;
				std::function<void(void *)> m_CallbackData;
				void * m_CallbackDataPointer;

				ui::CBaseWidget * m_Object;								
				Vector m_Position;
				Vector m_StartPosition;
				Vector m_BasePosition;
			};
		}
	}
}
#endif //OPTIMUS_HELPERS_ACTIONS_CALLBACK
