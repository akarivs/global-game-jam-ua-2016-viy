#include "ScaleToAction.h"

#include "../../optimus.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			//////////////////////////////////////////////////////////////////////////
			CScaleToAction::CScaleToAction(ui::CBaseWidget * object, const float time, Vector scale, Tween tween, const float blockTime) :CBaseAction()
			{				
				m_Object = object;
				if (m_Object == NULL)
				{
					return;
				}
				m_Time = time;
				if (blockTime < 0.0f)
				{
					m_ActionTime = m_Time;
				}
				else
				{
					m_ActionTime = blockTime;
				}
				m_BlockTime = m_ActionTime;
				m_Scale = scale;	
				m_BaseScale = scale;

				m_Tween = tween;
				InitTween();
			}

			//////////////////////////////////////////////////////////////////////////
			CScaleToAction::~CScaleToAction()
			{

			}		

			//////////////////////////////////////////////////////////////////////////
			void CScaleToAction::Reset()
			{
				CBaseAction::Reset();
				m_Scale = m_BaseScale;
			}

			//////////////////////////////////////////////////////////////////////////
			void CScaleToAction::Start()
			{
				CBaseAction::Start();

				if (m_Time == 0.0f)
				{
					m_Object->SetScale(m_Scale);
				}
				else
				{
					m_StartScale = m_Object->GetScale();
					m_Scale -= m_StartScale;
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CScaleToAction::Stop()
			{
				m_Object->SetScale(m_Scale);
			}

			//////////////////////////////////////////////////////////////////////////
			bool CScaleToAction::IsEnded()
			{
				return m_UpdateTimer >= m_Time;
			}

			//////////////////////////////////////////////////////////////////////////
			void CScaleToAction::VirtualUpdate(const float dt)
			{
				if (m_Time == 0.0f)return;
				m_UpdateTimer += dt;
				if (m_UpdateTimer > m_Time)m_UpdateTimer = m_Time;

				
				Vector scale = Vector(m_TweenFunc(m_UpdateTimer, m_StartScale.x, m_Scale.x, m_Time),
					m_TweenFunc(m_UpdateTimer, m_StartScale.y, m_Scale.y, m_Time));
				m_Object->SetScale(scale);
			}
		}
	}
}
