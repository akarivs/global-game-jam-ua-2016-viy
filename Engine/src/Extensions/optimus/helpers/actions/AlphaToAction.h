#ifndef OPTIMUS_HELPERS_ACTIONS_ALPHA_TO
#define OPTIMUS_HELPERS_ACTIONS_ALPHA_TO


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "../../ui/BaseWidget.h"
#include "../../optimus_enum.h"
#include "BaseAction.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Действие - сменить прозрачность объекта
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CAlphaToAction : public CBaseAction
			{
			public:
				/// @name Конструктор/деструктор
				static CAlphaToAction * Create(ui::CBaseWidget * object, const float time, float alpha, Tween tween = Tween::EASE_LINEAR, const float blockTime = -1.0f)
				{
					return new CAlphaToAction(object, time, alpha, tween, blockTime);
				}

				virtual void Reset();
				
				virtual ~CAlphaToAction();
				virtual void Start();
				virtual void Stop();
				virtual bool IsEnded();

				///LUA HELPERS
				static CAlphaToAction * __LuaCreate(ui::CBaseWidget * object, const float time, float alpha)
				{
					return Create(object,time,alpha);
				}
			protected:
				virtual void VirtualUpdate(const float dt);
			private:
				CAlphaToAction(ui::CBaseWidget *, const float time, float alpha, Tween tween, const float blockTime);
				ui::CBaseWidget * m_Object;								
				float m_Alpha;
				float m_StartAlpha;
				float m_BaseAlpha;				
			};
		}
	}
}
#endif //OPTIMUS_HELPERS_ACTIONS_ROTATE_TO
