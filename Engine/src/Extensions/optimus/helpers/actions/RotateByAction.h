#ifndef OPTIMUS_HELPERS_ACTIONS_ROTATE_BY
#define OPTIMUS_HELPERS_ACTIONS_ROTATE_BY


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "../../ui/BaseWidget.h"
#include "../../optimus_enum.h"
#include "BaseAction.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Действие - повернуть объект относительно его текущего поворота
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CRotateByAction : public CBaseAction
			{
			public:
				/// @name Конструктор/деструктор
				static CRotateByAction * Create(ui::CBaseWidget * object, const float time, float angle, Tween tween = Tween::EASE_LINEAR, const float blockTime = -1.0f)
				{
					return new CRotateByAction(object, time, angle, tween, blockTime);
				}

				virtual void Reset();
				
				virtual ~CRotateByAction();
				virtual void Start();
				virtual void Stop();
				virtual bool IsEnded();

				///LUA HELPERS
				static CRotateByAction * __LuaCreate(ui::CBaseWidget * object, const float time, float angle)
				{
					return Create(object,time,angle);
				}
			protected:
				virtual void VirtualUpdate(const float dt);
			private:
				CRotateByAction(ui::CBaseWidget *, const float time, float angle, Tween tween, const float blockTime);
				ui::CBaseWidget * m_Object;								
				float m_Angle;
				float m_StartAngle;
				float m_BaseAngle;				
			};
		}
	}
}
#endif //OPTIMUS_HELPERS_ACTIONS_ROTATE_BY
