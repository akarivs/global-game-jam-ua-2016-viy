#include "FloatToAction.h"

#include "../../optimus.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			//////////////////////////////////////////////////////////////////////////
			CFloatToAction::CFloatToAction(float * value, const float time, float destination, Tween tween, const float blockTime) :CBaseAction()
			{				
				m_Pointer = value;
				if (m_Pointer == nullptr)
				{
					NBG_Assert(0, "No pointer in action!");
					return;
				}
				m_Time = time;
				if (blockTime < 0.0f)
				{
					m_ActionTime = m_Time;
				}
				else
				{
					m_ActionTime = blockTime;
				}
				m_BlockTime = m_ActionTime;
				m_Destination = destination;
				m_BaseDestination = destination;

				m_Tween = tween;
				InitTween();
			}

			//////////////////////////////////////////////////////////////////////////
			CFloatToAction::~CFloatToAction()
			{

			}		

			//////////////////////////////////////////////////////////////////////////
			void CFloatToAction::Reset()
			{
				CBaseAction::Reset();
				m_Destination = m_BaseDestination;
			}

			//////////////////////////////////////////////////////////////////////////
			void CFloatToAction::Start()
			{				
				CBaseAction::Start();

				if (m_Time == 0.0f)
				{
					*m_Pointer = m_Destination;					
				}
				else
				{
					m_StartDestination = *m_Pointer;
					m_Destination -= m_StartDestination;							
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CFloatToAction::Stop()
			{
				*m_Pointer = m_Destination;				
			}

			//////////////////////////////////////////////////////////////////////////
			bool CFloatToAction::IsEnded()
			{
				return m_UpdateTimer >= m_Time;
			}

			//////////////////////////////////////////////////////////////////////////
			void CFloatToAction::VirtualUpdate(const float dt)
			{
				if (m_Time == 0.0f)return;
				m_UpdateTimer += dt;
				if (m_UpdateTimer > m_Time)m_UpdateTimer = m_Time;

				float destination = m_TweenFunc(m_UpdateTimer, m_StartDestination, m_Destination, m_Time);
				*m_Pointer = destination;
			}
		}
	}
}
