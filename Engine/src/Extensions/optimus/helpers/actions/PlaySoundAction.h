#ifndef OPTIMUS_HELPERS_ACTIONS_PLAY_SOUND
#define OPTIMUS_HELPERS_ACTIONS_PLAY_SOUND


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "../../ui/BaseWidget.h"
#include "../../optimus_enum.h"
#include "BaseAction.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Действие - воспроизвести звук
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CPlaySoundAction : public CBaseAction
			{
			public:
				/// @name Конструктор/деструктор
				static CPlaySoundAction * Create(const std::string &soundName)
				{
					return new CPlaySoundAction(soundName);
				}				
				virtual ~CPlaySoundAction();

				virtual void Reset();				
				virtual void Start();
				virtual void Stop();
				virtual bool IsEnded();


			private:
				CPlaySoundAction(const std::string &soundName);
				std::string m_SoundName;
			};
		}
	}
}
#endif //OPTIMUS_HELPERS_ACTIONS_PLAY_SOUND
