#include "BaseAction.h"

#include "../../optimus.h"



namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			//////////////////////////////////////////////////////////////////////////
			CBaseAction::CBaseAction()
			{
				m_IsComplete = false;
				m_IsStarted = false;
				m_UpdateTimer = 0.0f;
			}

			//////////////////////////////////////////////////////////////////////////
			CBaseAction::~CBaseAction()
			{

			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseAction::Reset()
			{
				m_IsComplete = false;
				m_IsStarted = false;
				m_Timer = 0.0f;				

				if (m_BlockTime < 0.0f)
				{
					m_ActionTime = m_Time;
				}
				else
				{
					m_ActionTime = m_BlockTime;
				}
				m_UpdateTimer = 0.0f;
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseAction::Start()
			{
				m_Timer = 0.0f;
				m_IsStarted = true;
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseAction::Stop()
			{

			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseAction::InitTween()
			{
				switch (m_Tween)
				{
				case Tween::EASE_IN_CIRCULAR: m_TweenFunc = std::bind(Tweens::easeInCircular, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_IN_CUBIC: m_TweenFunc = std::bind(Tweens::easeInCubic, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_IN_ELASTIC: m_TweenFunc = std::bind(Tweens::easeInElastic, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_IN_OUT_CIRCULAR: m_TweenFunc = std::bind(Tweens::easeInOutCircular, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_IN_OUT_CUBIC: m_TweenFunc = std::bind(Tweens::easeInOutCubic, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_IN_OUT_ELASTIC: m_TweenFunc = std::bind(Tweens::easeInOutElastic, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_IN_OUT_QUAD: m_TweenFunc = std::bind(Tweens::easeInOutQuad, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_IN_OUT_SIN: m_TweenFunc = std::bind(Tweens::easeInOutSine, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_IN_QUAD: m_TweenFunc = std::bind(Tweens::easeInQuad, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_IN_SIN: m_TweenFunc = std::bind(Tweens::easeInSine, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_LINEAR: m_TweenFunc = std::bind(Tweens::easeNone, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_OUT_CIRCULAR: m_TweenFunc = std::bind(Tweens::easeOutCircular, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_OUT_CUBIC: m_TweenFunc = std::bind(Tweens::easeOutCubic, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_OUT_ELASTIC: m_TweenFunc = std::bind(Tweens::easeOutElastic, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_OUT_IN_CIRCULAR: m_TweenFunc = std::bind(Tweens::easeOutInCircular, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_OUT_IN_CUBIC: m_TweenFunc = std::bind(Tweens::easeOutInCubic, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_OUT_IN_QUAD: m_TweenFunc = std::bind(Tweens::easeOutInQuad, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_OUT_IN_SIN: m_TweenFunc = std::bind(Tweens::easeOutInSine, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_OUT_QUAD: m_TweenFunc = std::bind(Tweens::easeOutQuad, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				case Tween::EASE_OUT_SIN: m_TweenFunc = std::bind(Tweens::easeOutSine, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4); break;
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseAction::Update(const float dt)
			{
				m_Timer += dt;
				if (m_Timer >= m_ActionTime)
				{
					m_IsComplete = true;
				}
				VirtualUpdate(dt);
			}
		}
	}
}