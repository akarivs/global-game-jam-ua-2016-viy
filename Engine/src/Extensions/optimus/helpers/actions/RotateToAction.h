#ifndef OPTIMUS_HELPERS_ACTIONS_ROTATE_TO
#define OPTIMUS_HELPERS_ACTIONS_ROTATE_TO


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "../../ui/BaseWidget.h"
#include "../../optimus_enum.h"
#include "BaseAction.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Действие - повернуть объект
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CRotateToAction : public CBaseAction
			{
			public:
				/// @name Конструктор/деструктор
				static CRotateToAction * Create(ui::CBaseWidget * object, const float time, float angle, bool clockwise = true, Tween tween = Tween::EASE_LINEAR, const float blockTime = -1.0f)
				{
					return new CRotateToAction(object, time, angle, clockwise, tween, blockTime);
				}

				virtual void Reset();
				
				virtual ~CRotateToAction();
				virtual void Start();
				virtual void Stop();
				virtual bool IsEnded();

				///LUA HELPERS
				static CRotateToAction * __LuaCreate(ui::CBaseWidget * object, const float time, float angle, bool clockwise)
				{
					return Create(object,time,angle,clockwise);
				}
			protected:
				virtual void VirtualUpdate(const float dt);
			private:
				CRotateToAction(ui::CBaseWidget *, const float time, float angle, bool clockwise, Tween tween, const float blockTime);
				ui::CBaseWidget * m_Object;								
				float m_Angle;
				float m_StartAngle;
				float m_BaseAngle;
				bool m_Clockwise;
			};
		}
	}
}
#endif //OPTIMUS_HELPERS_ACTIONS_ROTATE_TO
