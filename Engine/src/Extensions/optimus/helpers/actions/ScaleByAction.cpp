#include "ScaleByAction.h"

#include "../../optimus.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			//////////////////////////////////////////////////////////////////////////
			CScaleByAction::CScaleByAction(ui::CBaseWidget * object, const float time, Vector scale, Tween tween, const float blockTime) :CBaseAction()
			{				
				m_Object = object;
				if (m_Object == NULL)
				{
					return;
				}
				m_Time = time;
				if (blockTime < 0.0f)
				{
					m_ActionTime = m_Time;
				}
				else
				{
					m_ActionTime = blockTime;
				}
				m_BlockTime = m_ActionTime;
				m_Scale = scale;	
				m_BaseScale = scale;

				m_Tween = tween;
				InitTween();
			}

			//////////////////////////////////////////////////////////////////////////
			CScaleByAction::~CScaleByAction()
			{

			}		

			//////////////////////////////////////////////////////////////////////////
			void CScaleByAction::Reset()
			{
				CBaseAction::Reset();
				m_Scale = m_BaseScale;
			}

			//////////////////////////////////////////////////////////////////////////
			void CScaleByAction::Start()
			{
				CBaseAction::Start();

				if (m_Time == 0.0f)
				{
					m_Object->SetScale(m_Scale);
				}
				else
				{
					m_StartScale = m_Object->GetScale();					
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CScaleByAction::Stop()
			{
				m_Object->SetScale(m_StartScale + m_Scale);
			}

			//////////////////////////////////////////////////////////////////////////
			bool CScaleByAction::IsEnded()
			{
				return m_UpdateTimer >= m_Time;
			}

			//////////////////////////////////////////////////////////////////////////
			void CScaleByAction::VirtualUpdate(const float dt)
			{
				if (m_Time == 0.0f)return;
				m_UpdateTimer += dt;
				if (m_UpdateTimer > m_Time)m_UpdateTimer = m_Time;

				
				Vector scale = Vector(m_TweenFunc(m_UpdateTimer, m_StartScale.x, m_Scale.x, m_Time),
					m_TweenFunc(m_UpdateTimer, m_StartScale.y, m_Scale.y, m_Time));
				m_Object->SetScale(scale);
			}
		}
	}
}
