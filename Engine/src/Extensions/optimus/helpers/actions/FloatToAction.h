#ifndef OPTIMUS_HELPERS_ACTIONS_FLOAT_TO
#define OPTIMUS_HELPERS_ACTIONS_FLOAT_TO


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "../../ui/BaseWidget.h"
#include "../../optimus_enum.h"
#include "BaseAction.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Действие - изменить float значение по указателю
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CFloatToAction : public CBaseAction
			{
			public:
				/// @name Конструктор/деструктор
				static CFloatToAction * Create(float * value, const float time, float destination, Tween tween = Tween::EASE_LINEAR, const float blockTime = -1.0f)
				{
					return new CFloatToAction(value, time, destination, tween, blockTime);
				}

				virtual void Reset();
				
				virtual ~CFloatToAction();
				virtual void Start();
				virtual void Stop();
				virtual bool IsEnded();

				///LUA HELPERS
				static CFloatToAction * __LuaCreate(float * value, const float time, float destination)
				{
					return Create(value,time,destination);
				}
			protected:
				virtual void VirtualUpdate(const float dt);
			private:
				CFloatToAction(float * value, const float time, float destination, Tween tween, const float blockTime);
				float * m_Pointer;								
				float m_Destination;
				float m_StartDestination;
				float m_BaseDestination;				
			};
		}
	}
}
#endif //OPTIMUS_HELPERS_ACTIONS_FLOAT_TO
