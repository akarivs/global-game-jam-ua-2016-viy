#ifndef OPTIMUS_HELPERS_ACTIONS_MOVE_BY
#define OPTIMUS_HELPERS_ACTIONS_MOVE_BY


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "../../ui/BaseWidget.h"
#include "../../optimus_enum.h"
#include "BaseAction.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Действие - переместить объект относительно его позиции
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CMoveByAction : public CBaseAction
			{
			public:
				/// @name Конструктор/деструктор
				static CMoveByAction * Create(ui::CBaseWidget * object, const float time, Vector position, Tween tween = Tween::EASE_LINEAR, const float blockTime = -1.0f)
				{
					return new CMoveByAction(object, time, position, tween, blockTime);
				}

				virtual void Reset();
				
				virtual ~CMoveByAction();
				virtual void Start();
				virtual void Stop();
				virtual bool IsEnded();

				///LUA HELPERS
				static CMoveByAction * __LuaCreate(ui::CBaseWidget * object, const float time, Vector position, float blockTime)
				{
					return Create(object, time, position, Tween::EASE_LINEAR, blockTime);
				}
			protected:
				virtual void VirtualUpdate(const float dt);
			private:
				CMoveByAction(ui::CBaseWidget *, const float time, Vector position, Tween tween, const float blockTime);
				ui::CBaseWidget * m_Object;								
				Vector m_Position;
				Vector m_StartPosition;
				Vector m_BasePosition;
			};
		}
	}
}
#endif //OPTIMUS_HELPERS_ACTIONS_MOVE_BY
