#include "MoveToBezierAction.h"

#include "../../optimus.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			//////////////////////////////////////////////////////////////////////////
			CMoveToBezierAction::CMoveToBezierAction(ui::CBaseWidget * object, const float time, Vector position, Vector bezier, Tween tween, const float blockTime) :CBaseAction()
			{				
				m_Object = object;
				if (m_Object == NULL)
				{
					return;
				}
				m_Time = time;
				if (blockTime < 0.0f)
				{
					m_ActionTime = m_Time;
				}
				else
				{
					m_ActionTime = blockTime;
				}
				m_BlockTime = m_ActionTime;
				m_Position = position;	
				m_BasePosition = position;
				m_Bezier = bezier;

				m_Tween = tween;
				InitTween();
			}

			//////////////////////////////////////////////////////////////////////////
			CMoveToBezierAction::~CMoveToBezierAction()
			{

			}		

			//////////////////////////////////////////////////////////////////////////
			void CMoveToBezierAction::Reset()
			{
				CBaseAction::Reset();
				m_Position = m_BasePosition;
			}

			//////////////////////////////////////////////////////////////////////////
			void CMoveToBezierAction::Start()
			{
				CBaseAction::Start();

				if (m_Time == 0.0f)
				{
					m_Object->SetPosition(m_Position);
				}
				else
				{
					m_StartPosition = m_Object->GetPosition();
					m_Position -= m_StartPosition;
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CMoveToBezierAction::Stop()
			{
				m_Object->SetPosition(m_StartPosition + m_Position);
			}

			//////////////////////////////////////////////////////////////////////////
			bool CMoveToBezierAction::IsEnded()
			{
				return m_UpdateTimer >= m_Time;
			}

			//////////////////////////////////////////////////////////////////////////
			void CMoveToBezierAction::VirtualUpdate(const float dt)
			{
				if (m_Time == 0.0f)return;
				m_UpdateTimer += dt;
				if (m_UpdateTimer >= m_Time)
				{
					m_UpdateTimer = m_Time;
				}			
			
				float time = m_TweenFunc(m_UpdateTimer, 0.0f, 1.0f, m_Time);
				Vector pos = MathUtils::GetQuadBezierPoint(m_StartPosition, m_Bezier, m_StartPosition+m_Position, time);	
				m_Object->SetPosition(pos);
			}
		}
	}
}
