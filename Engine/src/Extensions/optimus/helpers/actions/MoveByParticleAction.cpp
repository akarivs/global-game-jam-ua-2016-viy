#include "MoveByParticleAction.h"

#include "../../optimus.h"


namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			//////////////////////////////////////////////////////////////////////////
			CMoveByParticleAction::CMoveByParticleAction(particles::CEmmiterWidget * object, const float time, Vector position, Tween tween, const float blockTime) :CBaseAction()
			{				
				m_Object = object;
				if (m_Object == NULL)
				{
					return;
				}
				m_Time = time;
				if (blockTime < 0.0f)
				{
					m_ActionTime = m_Time;
				}
				else
				{
					m_ActionTime = blockTime;
				}
				m_BlockTime = m_ActionTime;
				m_Position = position;	
				m_BasePosition = position;

				m_Tween = tween;
				InitTween();
			}

			//////////////////////////////////////////////////////////////////////////
			CMoveByParticleAction::~CMoveByParticleAction()
			{

			}		

			//////////////////////////////////////////////////////////////////////////
			void CMoveByParticleAction::Reset()
			{
				CBaseAction::Reset();
				m_Position = m_BasePosition;
			}

			//////////////////////////////////////////////////////////////////////////
			void CMoveByParticleAction::Start()
			{
				CBaseAction::Start();

				if (m_Time == 0.0f)
				{
					m_Object->SetParticleOffset(m_Object->GetParticleOffset()+m_Position);
				}
				else
				{
					m_StartPosition = m_Object->GetParticleOffset();
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CMoveByParticleAction::Stop()
			{
				m_Object->SetParticleOffset(m_StartPosition + m_Position);
			}

			//////////////////////////////////////////////////////////////////////////
			bool CMoveByParticleAction::IsEnded()
			{
				return m_UpdateTimer >= m_Time;
			}

			//////////////////////////////////////////////////////////////////////////
			void CMoveByParticleAction::VirtualUpdate(const float dt)
			{
				if (m_Time == 0.0f)return;
				m_UpdateTimer += dt;
				if (m_UpdateTimer > m_Time)m_UpdateTimer = m_Time;

				
				Vector pos = Vector(m_TweenFunc(m_UpdateTimer, m_StartPosition.x, m_Position.x, m_Time),
					m_TweenFunc(m_UpdateTimer, m_StartPosition.y, m_Position.y, m_Time));
				m_Object->SetParticleOffset(pos);
			}
		}
	}
}
