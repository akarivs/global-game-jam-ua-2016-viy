#ifndef OPTIMUS_HELPERS_ACTIONS_CALLBACK_LUA
#define OPTIMUS_HELPERS_ACTIONS_CALLBACK_LUA


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "../../optimus_enum.h"
#include <functional>
#include "BaseAction.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Действие - вызвать колбэк из Lua
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CCallbackLUAAction : public CBaseAction
			{
			public:
				/// @name Конструктор/деструктор
				static CCallbackLUAAction * Create(const int id)
				{
					return new CCallbackLUAAction(id);
				}                    
				virtual void Reset();
				
				virtual ~CCallbackLUAAction();
				virtual void Start();
				virtual void Stop();
				virtual bool IsEnded();
			protected:
			private:
				CCallbackLUAAction(const int id);
                                int m_CallbackId;		
			};
		}
	}
}
#endif //OPTIMUS_HELPERS_ACTIONS_CALLBACK_LUA
