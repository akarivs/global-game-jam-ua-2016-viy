#ifndef OPTIMUS_HELPERS_ACTIONS_BASE
#define OPTIMUS_HELPERS_ACTIONS_BASE


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Vector.h>

#include "../../ui/BaseWidget.h"
#include "../../optimus_enum.h"
#include <queue>

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{		
			class CBaseAction
			{
			public:
				/// @name Конструктор/деструктор				

				static CBaseAction* Create()
				{
					return new CBaseAction();
				}
				virtual ~CBaseAction();

				virtual void Start();
				virtual void Stop();
				virtual void Reset();

				float GetActionTime()
				{
					return m_ActionTime;
				};

				///Передать ли управление следующему экшну
				bool IsComplete()
				{
					return m_IsComplete;
				};

				///Завершился ли полностью апдейт экшна (даже если передали управление другому, этот ещё нужно апдейтить)
				virtual bool IsEnded()
				{
					return true;
				}

				bool IsStarted()
				{
					return m_IsStarted;
				};
				void Update(const float dt);
			protected:
				CBaseAction();
				virtual void VirtualUpdate(const float dt){};
				float m_Timer;
				float m_ActionTime;
				float m_BlockTime;
				float m_Time;
				float m_UpdateTimer;
				void InitTween();

				Tween m_Tween;
				std::function<float(float, float, float, float)> m_TweenFunc;

				bool m_IsComplete;
				bool m_IsStarted;
			};
		}
	}
}
#endif //OPTIMUS_HELPERS_ACTIONS_BASE
