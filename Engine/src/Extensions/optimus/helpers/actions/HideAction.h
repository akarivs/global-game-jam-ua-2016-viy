#ifndef OPTIMUS_HELPERS_ACTIONS_HIDE
#define OPTIMUS_HELPERS_ACTIONS_HIDE


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "../../ui/BaseWidget.h"
#include "../../optimus_enum.h"
#include "BaseAction.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Действие - увести в альфу
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CHideAction : public CBaseAction
			{
			public:
				/// @name Конструктор/деструктор
				static CHideAction * Create(ui::CBaseWidget * object, const float time, Tween tween = Tween::EASE_LINEAR, const float blockTime = -1.0f)
				{
					return new CHideAction(object, time, tween, blockTime);
				}

				virtual void Reset();
				
				virtual ~CHideAction();
				virtual void Start();
				virtual void Stop();
				virtual bool IsEnded();

				///LUA HELPERS
				static CHideAction * __LuaCreate(ui::CBaseWidget * object, const float time, const float blockTime)
				{
					return Create(object, time, Tween::EASE_LINEAR,blockTime);
				}
			protected:
				virtual void VirtualUpdate(const float dt);
			private:
				CHideAction(ui::CBaseWidget *, const float time, Tween tween, const float blockTime);
				ui::CBaseWidget * m_Object;								
				float m_Alpha;
				float m_StartAlpha;
				float m_BaseAlpha;
			};
		}
	}
}
#endif //OPTIMUS_HELPERS_ACTIONS_HIDE
