#ifndef OPTIMUS_HELPERS_ACTIONS_MOVE_BY_PARTICLE
#define OPTIMUS_HELPERS_ACTIONS_MOVE_BY_PARTICLE


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "../../ui/BaseWidget.h"
#include "../../particles/EmmiterWidget.h"
#include "../../optimus_enum.h"
#include "BaseAction.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Действие - переместить эммитер относительно его позиции
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CMoveByParticleAction : public CBaseAction
			{
			public:
				/// @name Конструктор/деструктор
				static CMoveByParticleAction * Create(particles::CEmmiterWidget * object, const float time, Vector position, Tween tween = Tween::EASE_LINEAR, const float blockTime = -1.0f)
				{
					return new CMoveByParticleAction(object, time, position, tween, blockTime);
				}

				virtual void Reset();
				
				virtual ~CMoveByParticleAction();
				virtual void Start();
				virtual void Stop();
				virtual bool IsEnded();
			protected:
				virtual void VirtualUpdate(const float dt);
			private:
				CMoveByParticleAction(particles::CEmmiterWidget *, const float time, Vector position, Tween tween, const float blockTime);
				particles::CEmmiterWidget * m_Object;
				Vector m_Position;
				Vector m_StartPosition;
				Vector m_BasePosition;
			};
		}
	}
}
#endif //OPTIMUS_HELPERS_ACTIONS_MOVE_BY_PARTICLE
