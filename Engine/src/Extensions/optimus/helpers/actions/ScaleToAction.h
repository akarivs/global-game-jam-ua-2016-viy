#ifndef OPTIMUS_HELPERS_ACTIONS_SCALE_BY
#define OPTIMUS_HELPERS_ACTIONS_SCALE_BY


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "../../ui/BaseWidget.h"
#include "../../optimus_enum.h"
#include "BaseAction.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Действие - масштабировать объект
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CScaleToAction : public CBaseAction
			{
			public:
				/// @name Конструктор/деструктор
				static CScaleToAction * Create(ui::CBaseWidget * object, const float time, Vector scale, Tween tween = Tween::EASE_LINEAR, const float blockTime = -1.0f)
				{
					return new CScaleToAction(object, time, scale, tween, blockTime);
				}

				virtual void Reset();
				
				virtual ~CScaleToAction();
				virtual void Start();
				virtual void Stop();
				virtual bool IsEnded();

				///LUA HELPERS
				static CScaleToAction * __LuaCreate(ui::CBaseWidget * object, const float time, Vector scale)
				{
					return Create(object, time, scale);
				}
			protected:
				virtual void VirtualUpdate(const float dt);
			private:
				CScaleToAction(ui::CBaseWidget *, const float time, Vector scale, Tween tween, const float blockTime);
				ui::CBaseWidget * m_Object;								
				Vector m_Scale;
				Vector m_StartScale;
				Vector m_BaseScale;
			};
		}
	}
}
#endif //OPTIMUS_HELPERS_ACTIONS_SCALE_BY
