#ifndef OPTIMUS_HELPERS_ACTIONS_MOVE_TO_PARTICLE
#define OPTIMUS_HELPERS_ACTIONS_MOVE_TO_PARTICLE


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "../../ui/BaseWidget.h"
#include "../../particles/EmmiterWidget.h"
#include "../../optimus_enum.h"
#include "BaseAction.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Действие - переместить объект
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CMoveToParticleAction : public CBaseAction
			{
			public:
				/// @name Конструктор/деструктор
				static CMoveToParticleAction * Create(particles::CEmmiterWidget * object, const float time, Vector position, Tween tween = Tween::EASE_LINEAR, const float blockTime = -1.0f)
				{
					return new CMoveToParticleAction(object, time, position, tween, blockTime);
				}

				virtual void Reset();
				
				virtual ~CMoveToParticleAction();
				virtual void Start();
				virtual void Stop();
				virtual bool IsEnded();

				///LUA HELPERS
				static CMoveToParticleAction * __LuaCreate(particles::CEmmiterWidget * object, const float time, Vector position)
				{
					return Create(object,time,position);
				}
			protected:
				virtual void VirtualUpdate(const float dt);
			private:
				CMoveToParticleAction(particles::CEmmiterWidget *, const float time, Vector position, Tween tween, const float blockTime);
				particles::CEmmiterWidget * m_Object;
				Vector m_Position;
				Vector m_StartPosition;
				Vector m_BasePosition;
			};
		}
	}
}
#endif //OPTIMUS_HELPERS_ACTIONS_MOVE_TO
