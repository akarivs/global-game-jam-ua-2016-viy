#ifndef OPTIMUS_HELPERS_ACTIONS_SLEEP
#define OPTIMUS_HELPERS_ACTIONS_SLEEP


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "../../ui/BaseWidget.h"
#include "../../optimus_enum.h"
#include "BaseAction.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Действие - приостановить выполнение очереди действий
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CSleepAction : public CBaseAction
			{
			public:
				/// @name Конструктор/деструктор
				static CSleepAction * Create(const float time)
				{
					return new CSleepAction(time);
				}				
				virtual ~CSleepAction();		


			private:
				CSleepAction(const float time);
				ui::CBaseWidget * m_Object;												
			};
		}
	}
}
#endif //OPTIMUS_HELPERS_ACTIONS_SLEEP
