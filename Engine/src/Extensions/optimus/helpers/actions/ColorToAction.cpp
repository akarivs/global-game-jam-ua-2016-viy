#include "ColorToAction.h"

#include "../../optimus.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			//////////////////////////////////////////////////////////////////////////
			CColorToAction::CColorToAction(ui::CBaseWidget * object, const float time, Color color, Tween tween, const float blockTime) :CBaseAction()
			{				
				m_Object = object;
				if (m_Object == NULL)
				{
					return;
				}
				m_Time = time;
				if (blockTime < 0.0f)
				{
					m_ActionTime = m_Time;
				}
				else
				{
					m_ActionTime = blockTime;
				}
				m_BlockTime = m_ActionTime;
				m_Color = color.GetPackedColor();
				m_BaseColor = m_Color;

				m_Tween = tween;
				InitTween();
			}

			//////////////////////////////////////////////////////////////////////////
			CColorToAction::~CColorToAction()
			{

			}		

			//////////////////////////////////////////////////////////////////////////
			void CColorToAction::Reset()
			{
				CBaseAction::Reset();
				m_Color = m_BaseColor;
			}

			//////////////////////////////////////////////////////////////////////////
			void CColorToAction::Start()
			{				
				CBaseAction::Start();

				if (m_Time == 0.0f)
				{
					m_Object->SetColor(m_Color);
				}
				else
				{
					m_StartColor = m_Object->GetColor().GetPackedColor();
					m_Color.a -= m_StartColor.a;
					m_Color.r -= m_StartColor.r;
					m_Color.g -= m_StartColor.g;
					m_Color.b -= m_StartColor.b;
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CColorToAction::Stop()
			{
				m_Object->SetColor(m_Color);
			}

			//////////////////////////////////////////////////////////////////////////
			bool CColorToAction::IsEnded()
			{
				return m_UpdateTimer >= m_Time;
			}

			//////////////////////////////////////////////////////////////////////////
			void CColorToAction::VirtualUpdate(const float dt)
			{
				if (m_Time == 0.0f)return;
				m_UpdateTimer += dt;
				if (m_UpdateTimer > m_Time)m_UpdateTimer = m_Time;

				float alpha = m_TweenFunc(m_UpdateTimer, m_StartColor.a, m_Color.a, m_Time);
				float r = m_TweenFunc(m_UpdateTimer, m_StartColor.r, m_Color.r, m_Time);
				float g = m_TweenFunc(m_UpdateTimer, m_StartColor.g, m_Color.g, m_Time);
				float b = m_TweenFunc(m_UpdateTimer, m_StartColor.b, m_Color.b, m_Time);

				if (g > 200)
				{
					int aaa = 10;
				}
				m_Object->SetColor(Color(alpha,r,g,b));
			}
		}
	}
}
