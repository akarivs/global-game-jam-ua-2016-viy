#include "RotateToAction.h"

#include "../../optimus.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			//////////////////////////////////////////////////////////////////////////
			CRotateToAction::CRotateToAction(ui::CBaseWidget * object, const float time, float angle, bool clockwise, Tween tween, const float blockTime) :CBaseAction()
			{				
				m_Object = object;
				if (m_Object == NULL)
				{
					return;
				}
				m_Time = time;
				if (blockTime < 0.0f)
				{
					m_ActionTime = m_Time;
				}
				else
				{
					m_ActionTime = blockTime;
				}
				m_BlockTime = m_ActionTime;
				m_Angle = angle;	
				m_BaseAngle = angle;

				m_Clockwise = clockwise;

				m_Tween = tween;
				InitTween();
			}

			//////////////////////////////////////////////////////////////////////////
			CRotateToAction::~CRotateToAction()
			{

			}		

			//////////////////////////////////////////////////////////////////////////
			void CRotateToAction::Reset()
			{
				CBaseAction::Reset();
				m_Angle = m_BaseAngle;
			}

			//////////////////////////////////////////////////////////////////////////
			void CRotateToAction::Start()
			{
				CBaseAction::Start();

				if (m_Time == 0.0f)
				{
					m_Object->SetRotation(m_Angle);
				}
				else
				{
					m_StartAngle = m_Object->GetRotation();
					if (m_Clockwise)
					{
						if (m_Angle >= m_StartAngle)
						{
							m_Angle -= m_StartAngle;
						}
						else
						{
							m_Angle = 360 - m_StartAngle + m_Angle;
						}
					}						
					else
					{
						if (m_Angle <= m_StartAngle)
						{
							m_Angle = m_Angle - m_StartAngle;
						}
						else
						{
							m_Angle = -(m_StartAngle+360- m_Angle);
						}
					}
						
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CRotateToAction::Stop()
			{
				m_Object->SetRotation(m_Angle);
			}

			//////////////////////////////////////////////////////////////////////////
			bool CRotateToAction::IsEnded()
			{
				return m_UpdateTimer >= m_Time;
			}

			//////////////////////////////////////////////////////////////////////////
			void CRotateToAction::VirtualUpdate(const float dt)
			{
				if (m_Time == 0.0f)return;
				m_UpdateTimer += dt;
				if (m_UpdateTimer > m_Time)m_UpdateTimer = m_Time;

				float angle = m_TweenFunc(m_UpdateTimer, m_StartAngle, m_Angle, m_Time);
				m_Object->SetRotation(angle);
			}
		}
	}
}
