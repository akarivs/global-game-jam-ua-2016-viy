#include "RotateByAction.h"

#include "../../optimus.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			//////////////////////////////////////////////////////////////////////////
			CRotateByAction::CRotateByAction(ui::CBaseWidget * object, const float time, float angle, Tween tween, const float blockTime) :CBaseAction()
			{				
				m_Object = object;
				if (m_Object == NULL)
				{
					return;
				}
				m_Time = time;
				if (blockTime < 0.0f)
				{
					m_ActionTime = m_Time;
				}
				else
				{
					m_ActionTime = blockTime;
				}
				m_BlockTime = m_ActionTime;
				m_Angle = angle;	
				m_BaseAngle = angle;				

				m_Tween = tween;
				InitTween();
			}

			//////////////////////////////////////////////////////////////////////////
			CRotateByAction::~CRotateByAction()
			{

			}		

			//////////////////////////////////////////////////////////////////////////
			void CRotateByAction::Reset()
			{
				CBaseAction::Reset();
				m_Angle = m_BaseAngle;
			}

			//////////////////////////////////////////////////////////////////////////
			void CRotateByAction::Start()
			{
				CBaseAction::Start();

				if (m_Time == 0.0f)
				{
					m_Object->SetRotation(m_Angle);
				}
				else
				{
					m_StartAngle = m_Object->GetRotation();	
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CRotateByAction::Stop()
			{
				m_Object->SetRotation(m_StartAngle + m_Angle);
			}

			//////////////////////////////////////////////////////////////////////////
			bool CRotateByAction::IsEnded()
			{
				return m_UpdateTimer >= m_Time;
			}

			//////////////////////////////////////////////////////////////////////////
			void CRotateByAction::VirtualUpdate(const float dt)
			{
				if (m_Time == 0.0f)return;
				m_UpdateTimer += dt;
				if (m_UpdateTimer > m_Time)m_UpdateTimer = m_Time;

				float angle = m_TweenFunc(m_UpdateTimer, m_StartAngle, m_Angle, m_Time);
				m_Object->SetRotation(angle);
			}
		}
	}
}
