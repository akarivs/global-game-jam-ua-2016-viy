#include "CallbackLUAAction.h"

#include "../../optimus.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			//////////////////////////////////////////////////////////////////////////
			CCallbackLUAAction::CCallbackLUAAction(const int id) :CBaseAction()
			{				
				m_Time = 0.0f;
				m_ActionTime = 0.0f;
				m_BlockTime = m_ActionTime;				
				m_CallbackId = luaL_ref(CLuaHelper::GetInstance()->GetState(),LUA_REGISTRYINDEX);			
			}

			//////////////////////////////////////////////////////////////////////////
			CCallbackLUAAction::~CCallbackLUAAction()
			{

			}		

			//////////////////////////////////////////////////////////////////////////
			void CCallbackLUAAction::Reset()
			{
				CBaseAction::Reset();				
			}

			//////////////////////////////////////////////////////////////////////////
			void CCallbackLUAAction::Start()
			{
				CBaseAction::Start();

				auto state = CLuaHelper::GetInstance()->GetState();

				lua_rawgeti(state, LUA_REGISTRYINDEX, m_CallbackId);
				int error = lua_pcall(state, 0, 0, 0);
				if(error)
				{
					CONSOLE_ERROR("%s", lua_tostring(state, -1));
					lua_pop(state, 1);
				}
				//luaL_unref(state, LUA_REGISTRYINDEX, m_CallbackId);
			}

			//////////////////////////////////////////////////////////////////////////
			void CCallbackLUAAction::Stop()
			{
				
			}

			//////////////////////////////////////////////////////////////////////////
			bool CCallbackLUAAction::IsEnded()
			{
				return true;
			}
		}
	}
}
