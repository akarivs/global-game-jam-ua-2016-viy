#include "HideAction.h"

#include "../../optimus.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			//////////////////////////////////////////////////////////////////////////
			CHideAction::CHideAction(ui::CBaseWidget * object, const float time, Tween tween, const float blockTime) :CBaseAction()
			{				
				m_Object = object;
				if (m_Object == NULL)
				{
					return;
				}
				m_Time = time;
				if (blockTime < 0.0f)
				{
					m_ActionTime = m_Time;
				}
				else
				{
					m_ActionTime = blockTime;
				}
				m_BlockTime = m_ActionTime;
				m_Alpha = 0.0f;	
				m_BaseAlpha = 0.0f;

				m_Tween = tween;
				InitTween();
			}

			//////////////////////////////////////////////////////////////////////////
			CHideAction::~CHideAction()
			{

			}		

			//////////////////////////////////////////////////////////////////////////
			void CHideAction::Reset()
			{
				CBaseAction::Reset();
				m_Alpha = m_BaseAlpha;
			}

			//////////////////////////////////////////////////////////////////////////
			void CHideAction::Start()
			{
				CBaseAction::Start();

				if (m_Time == 0.0f)
				{
					m_Object->SetOpacity(0.0f);
				}
				else
				{
					m_StartAlpha = m_Object->GetOpacity();
					m_Alpha -= m_StartAlpha;
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CHideAction::Stop()
			{
				m_Object->SetOpacity(m_Alpha);
			}

			//////////////////////////////////////////////////////////////////////////
			bool CHideAction::IsEnded()
			{
				return m_UpdateTimer >= m_Time;
			}

			//////////////////////////////////////////////////////////////////////////
			void CHideAction::VirtualUpdate(const float dt)
			{
				if (m_Time == 0.0f)return;
				m_UpdateTimer += dt;
				if (m_UpdateTimer > m_Time)m_UpdateTimer = m_Time;

				
				float opacity = m_TweenFunc(m_UpdateTimer, m_StartAlpha, m_Alpha, m_Time);
				m_Object->SetOpacity(opacity);
			}
		}
	}
}
