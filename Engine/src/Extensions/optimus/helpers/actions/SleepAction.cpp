#include "SleepAction.h"

#include "../../optimus.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			//////////////////////////////////////////////////////////////////////////
			CSleepAction::CSleepAction(const float time) :CBaseAction()
			{				
				m_Time = time;
				m_ActionTime = m_Time;				
				m_BlockTime = m_ActionTime;				
			}

			//////////////////////////////////////////////////////////////////////////
			CSleepAction::~CSleepAction()
			{

			}								
		}
	}
}
