#include "ShowAction.h"

#include "../../optimus.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			//////////////////////////////////////////////////////////////////////////
			CShowAction::CShowAction(ui::CBaseWidget * object, const float time, Tween tween, const float blockTime) :CBaseAction()
			{				
				m_Object = object;
				if (m_Object == NULL)
				{
					return;
				}
				m_Time = time;
				if (blockTime < 0.0f)
				{
					m_ActionTime = m_Time;
				}
				else
				{
					m_ActionTime = blockTime;
				}
				m_BlockTime = m_ActionTime;
				m_Alpha = 255.0f;	
				m_BaseAlpha = 255.0f;

				m_Tween = tween;
				InitTween();
			}

			//////////////////////////////////////////////////////////////////////////
			CShowAction::~CShowAction()
			{

			}		

			//////////////////////////////////////////////////////////////////////////
			void CShowAction::Reset()
			{
				CBaseAction::Reset();
				m_Alpha = m_BaseAlpha;
			}

			//////////////////////////////////////////////////////////////////////////
			void CShowAction::Start()
			{
				CBaseAction::Start();

				if (m_Time == 0.0f)
				{
					m_Object->SetOpacity(255.0f);
					m_Object->SetVisible(true);
				}
				else
				{
					m_Object->SetVisible(true);
					m_StartAlpha = m_Object->GetOpacity();
					m_Alpha -= m_StartAlpha;
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CShowAction::Stop()
			{
				m_Object->SetOpacity(m_Alpha);
			}

			//////////////////////////////////////////////////////////////////////////
			bool CShowAction::IsEnded()
			{
				return m_UpdateTimer >= m_Time;
			}

			//////////////////////////////////////////////////////////////////////////
			void CShowAction::VirtualUpdate(const float dt)
			{
				if (m_Time == 0.0f)return;
				m_UpdateTimer += dt;
				if (m_UpdateTimer > m_Time)m_UpdateTimer = m_Time;

				
				float opacity = m_TweenFunc(m_UpdateTimer, m_StartAlpha, m_Alpha, m_Time);
				m_Object->SetOpacity(opacity);
			}
		}
	}
}
