#include "AlphaToAction.h"

#include "../../optimus.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			//////////////////////////////////////////////////////////////////////////
			CAlphaToAction::CAlphaToAction(ui::CBaseWidget * object, const float time, float angle, Tween tween, const float blockTime) :CBaseAction()
			{				
				m_Object = object;
				if (m_Object == NULL)
				{
					NBG_Assert(0, "No object in action!");
					return;
				}
				m_Time = time;
				if (blockTime < 0.0f)
				{
					m_ActionTime = m_Time;
				}
				else
				{
					m_ActionTime = blockTime;
				}
				m_BlockTime = m_ActionTime;
				m_Alpha = angle;	
				m_BaseAlpha = angle;				

				m_Tween = tween;
				InitTween();
			}

			//////////////////////////////////////////////////////////////////////////
			CAlphaToAction::~CAlphaToAction()
			{

			}		

			//////////////////////////////////////////////////////////////////////////
			void CAlphaToAction::Reset()
			{
				CBaseAction::Reset();
				m_Alpha = m_BaseAlpha;
			}

			//////////////////////////////////////////////////////////////////////////
			void CAlphaToAction::Start()
			{				
				CBaseAction::Start();

				if (m_Time == 0.0f)
				{
					m_Object->SetOpacity(m_Alpha);
				}
				else
				{
					m_StartAlpha = m_Object->GetOpacity();
					m_Alpha -= m_StartAlpha;							
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CAlphaToAction::Stop()
			{
				m_Object->SetOpacity(m_Alpha);
			}

			//////////////////////////////////////////////////////////////////////////
			bool CAlphaToAction::IsEnded()
			{
				return m_UpdateTimer >= m_Time;
			}

			//////////////////////////////////////////////////////////////////////////
			void CAlphaToAction::VirtualUpdate(const float dt)
			{
				if (m_Time == 0.0f)return;
				m_UpdateTimer += dt;
				if (m_UpdateTimer > m_Time)m_UpdateTimer = m_Time;

				float alpha = m_TweenFunc(m_UpdateTimer, m_StartAlpha, m_Alpha, m_Time);
				m_Object->SetOpacity(alpha);
			}
		}
	}
}
