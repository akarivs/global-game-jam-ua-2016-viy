#ifndef OPTIMUS_HELPERS_ACTIONS_MOVE_TO_BEZIER_PARTICLE
#define OPTIMUS_HELPERS_ACTIONS_MOVE_TO_BEZIER_PARTICLE


#include <Framework/Datatypes.h>
#include <Framework/Datatypes/FRect.h>
#include <Framework/Datatypes/Vector.h>

#include "../../ui/BaseWidget.h"
#include "../../particles/EmmiterWidget.h"
#include "../../optimus_enum.h"
#include "BaseAction.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Действие - переместить объект
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CMoveToBezierParticleAction : public CBaseAction
			{
			public:
				/// @name Конструктор/деструктор
				static CMoveToBezierParticleAction * Create(particles::CEmmiterWidget * object, const float time, Vector position, Vector bezier, Tween tween = Tween::EASE_LINEAR, const float blockTime = -1.0f)
				{
					return new CMoveToBezierParticleAction(object, time, position, bezier, tween, blockTime);
				}

				virtual void Reset();
				
				virtual ~CMoveToBezierParticleAction();
				virtual void Start();
				virtual void Stop();
				virtual bool IsEnded();

				///LUA HELPERS
				static CMoveToBezierParticleAction * __LuaCreate(particles::CEmmiterWidget * object, const float time, Vector position, Vector bezier)
				{
					return Create(object,time,position, bezier);
				}
			protected:
				virtual void VirtualUpdate(const float dt);
			private:
				CMoveToBezierParticleAction(particles::CEmmiterWidget *, const float time, Vector position, Vector bezier, Tween tween, const float blockTime);
				particles::CEmmiterWidget * m_Object;
				Vector m_Bezier;
				Vector m_Position;
				Vector m_StartPosition;
				Vector m_BasePosition;
			};
		}
	}
}
#endif //OPTIMUS_HELPERS_ACTIONS_MOVE_TO_BEZIER_PARTICLE
