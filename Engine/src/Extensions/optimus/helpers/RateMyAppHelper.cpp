#include "RateMyAppHelper.h"

#include <Framework.h>
#include "../optimus.h"

#ifdef NBG_OSX
#import <CoreFoundation/CoreFoundation.h>
#import <ApplicationServices/ApplicationServices.h>
#endif

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{

			CRateMyAppHelper * CRateMyAppHelper::self = nullptr;
			//////////////////////////////////////////////////////////////////////////
			CRateMyAppHelper::CRateMyAppHelper()
			{
				config = g_PlayersManager->LoadCustomConfig("ratemyapp_config.cfg");
			}

			//////////////////////////////////////////////////////////////////////////
			CRateMyAppHelper::~CRateMyAppHelper()
			{

			}

			//////////////////////////////////////////////////////////////////////////
			bool CRateMyAppHelper::IsCanShow()
			{
				bool isSkipOnce = false;
				bool isSkipTwice = false;
				bool isRated = false;
				config.GetValue("skip_once", isSkipOnce);
				config.GetValue("skip_twice", isSkipTwice);
				config.GetValue("is_rated", isRated);

				if (isRated || isSkipTwice)
				{
					return false;
				}

				float time = g_GameApplication->GetAppTime();				

				///���������� ������ ��� ����������� ������
				if (isSkipOnce == false)
				{					
					if (time >= 1800.0f)
					{
						g_GameApplication->ResetAppTime();
						return true;
					}
				}
				else ///���������� ������ ��� ������� ������
				{
					if (time >= 3600.0f)
					{
						g_GameApplication->ResetAppTime();
						return true;
					}
				}
				return false;
			}

			//////////////////////////////////////////////////////////////////////////
			void CRateMyAppHelper::Init()
			{
				RemoveAllChilds(true);

				CWindowWidget::Init();
				SetImmortal(true);

				LoadFromXML("xml/screens/common/ratemyapp.xml");


				auto btnRate = GetChildByTag<CButtonWidget>(100,true);
				if (btnRate)
				{
					btnRate->SetButtonListener([&](CBaseWidget * sender, CButtonWidget_EventType type)
					{
						if (type == CButtonWidget_EventType::Event_Click)
						{	
							Hide();

							config.SetValue("is_rated", true);
							g_PlayersManager->SaveCustomConfig(config,"ratemyapp_config.cfg");

							std::string rateLink = "";							
							g_Config->GetValue("rate_link", rateLink);														
							{
#ifdef NBG_WIN32
								ShellExecuteA(NULL, "open", rateLink.c_str(), NULL, NULL, SW_SHOWNORMAL);
#elif defined(NBG_OSX)
								g_GameApplication->SetFullscreen(false);
								CFURLRef url = CFURLCreateWithBytes(
									NULL,                        // allocator
									(UInt8*)rateLink.c_str(),     // URLBytes
									rateLink.length(),            // length
									kCFStringEncodingASCII,      // encoding
									NULL                         // baseURL
									);
								LSOpenCFURLRef(url, 0);
								CFRelease(url);
#endif

							}
						}
					});
				}

				auto btnNo = GetChildByTag<CButtonWidget>(101, true);
				if (btnNo)
				{
					btnNo->SetButtonListener([&](CBaseWidget * sender, CButtonWidget_EventType type)
					{
						if (type == CButtonWidget_EventType::Event_Click)
						{
							Hide();
							bool isSkipOnce = false;
							bool isSkipTwice = false;
							bool isRated = false;
							config.GetValue("skip_once", isSkipOnce);
							config.GetValue("skip_twice", isSkipTwice);


							if (isSkipOnce == false)
							{
								config.SetValue("skip_once", true);
							}
							else
							{
								config.SetValue("skip_twice", true);
							}
							g_PlayersManager->SaveCustomConfig(config, "ratemyapp_config.cfg");
						}
					});
				}
			}
		}
	}
}