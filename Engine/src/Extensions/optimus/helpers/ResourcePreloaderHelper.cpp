#include "ResourcePreloaderHelper.h"

#include <Framework.h>
#include "../optimus.h"

#include <thread>



namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{		
			CResourcePreloaderHelper * CResourcePreloaderHelper::self = nullptr;
			//////////////////////////////////////////////////////////////////////////
			CResourcePreloaderHelper::CResourcePreloaderHelper()
			{
				m_Callback = nullptr;
			}

			//////////////////////////////////////////////////////////////////////////
			CResourcePreloaderHelper::~CResourcePreloaderHelper()
			{
				
			}

			//////////////////////////////////////////////////////////////////////////
			void CResourcePreloaderHelper::Preload(const std::string &xmlPath, VOID_CALLBACK callback)
			{
				m_Callback = callback;

				std::thread thr(std::bind([&](const std::string &xmlPath)
				{
					CXMLResource * xml = CAST(CXMLResource*, g_ResManager->GetResource(g_EditionHelper->ConvertPath(xmlPath)));
					pugi::xml_document * doc = xml->GetXML();
					auto child = doc->first_child();
					
					for (pugi::xml_node res = child.child("res"); res; res = res.next_sibling("res"))
					{
						g_ResManager->GetResource(res.attribute("path").value());
					}
					g_ResManager->ReleaseResource(xml);

					if (m_Callback)
					{
						m_Callback();
					}
				},xmlPath));

				thr.detach();
			}
		}
	}
}