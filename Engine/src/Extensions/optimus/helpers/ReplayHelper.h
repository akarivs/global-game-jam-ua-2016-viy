#ifndef OPTIMUS_HELPERS_REPLAY
#define OPTIMUS_HELPERS_REPLAY

#include <string>

#include "../ui/BaseWidget.h"

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Класс, для записи игры (пользовательских действий) и дальнейшего воспроизведения
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2015 New Bridge Games
			*
			*/

			class CReplayHelper
			{
			public:
				/// @name Конструктор деструктор
				static CReplayHelper * GetInstance()
				{
					if (self == nullptr)
					{
						self = new CReplayHelper();
					}
					return self;
				}
				~CReplayHelper();
				
				void StartRecord();
				void StopRecord(const std::string & file);

				void StartReplay(const std::string & file);				

				void OnMouseMove(const Vector &mousePos);
				void OnMouseUp(const Vector &mousePos, const int mouseButton);
				void OnMouseDown(const Vector &mousePos, const int mouseButton);

				void Update(const float dt);
			private:			
				CReplayHelper();
				static CReplayHelper * self;

				enum class EReplayType
				{
					MouseMove,
					MouseUp,
					MouseDown,
					Sleep
				};
				struct SReplayEvent
				{
					EReplayType type;
					float sleepTime;
					int mouseButton;
					Vector mousePos;
				};
				std::vector<SReplayEvent> m_Events;
				std::queue<SReplayEvent> m_Replay;

				bool m_RecordReplay;
				float m_StartTime;

				GETTER_SETTER(std::function<void(const Vector &)>, m_OnMouseMoveCallback, OnMouseMoveCallback);
				GETTER_SETTER(std::function<void(const Vector &, const int)>, m_OnMouseDownCallback, OnMouseDownCallback);
				GETTER_SETTER(std::function<void(const Vector &, const int)>, m_OnMouseUpCallback, OnMouseUpCallback);

				  
				
				bool m_IsPlay;
				float m_ReplayTimer;

			};
		}
	}	
}

#endif ///OPTIMUS_HELPERS_REPLAY
