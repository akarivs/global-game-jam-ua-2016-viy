#ifndef OPTIMUS_HELPERS_LUA
#define OPTIMUS_HELPERS_LUA

#include <Framework/Datatypes/Transform.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/Mesh.h>

#include <../external/slb/SLB.hpp>
#include <lua.hpp>

#include "../ui/BaseWidget.h"


namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Класс, реализующий помощь в работе со скриптами.
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CLuaHelper
			{
			public:
				/// @name Конструктор деструктор
				static CLuaHelper * GetInstance()
				{
					if (self == nullptr)
					{
						self = new CLuaHelper();						
					}
					return self;
				}
				~CLuaHelper();

				void Init();	
				void InitDatatypes();
				void InitUI();
				void InitActions();
				void InitGlobalManagers();
				void OnSceneChange(ui::CBaseWidget * scene);
				void RunScript(ui::CBaseWidget * obj, const std::string &script);

				template<typename T>
				void RunScript(T * obj, const std::string &script)
				{
					if (script.size() == 0)return;
					SLB::push(m_LuaState, obj);
					lua_setglobal(m_LuaState, "this");
					luaL_dostring(m_LuaState, "SLB.using(SLB)\n");
					int status = luaL_dostring(m_LuaState, script.c_str());
					ReportErrors(m_LuaState, status);
				}

				lua_State * GetState()
				{
					return m_LuaState;
				}

			private:			
				CLuaHelper();
				static CLuaHelper * self;			

				void ReportErrors(lua_State * state, const int status);
				lua_State *m_LuaState;
			};
		}
	}	
}

#endif ///OPTIMUS_HELPERS_LUA
