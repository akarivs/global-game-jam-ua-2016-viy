#ifndef OPTIMUS_HELPERS_RESOURCE_PRELOADER
#define OPTIMUS_HELPERS_RESOURCE_PRELOADER

#include <Framework/Datatypes.h>
#include <string>

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			/** @brief Класс, для предзагрузки требуемых ресурсов из XML
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CResourcePreloaderHelper
			{
			public:
				/// @name Конструктор деструктор
				static CResourcePreloaderHelper * GetInstance()
				{
					if (self == nullptr)
					{
						self = new CResourcePreloaderHelper();
					}
					return self;
				}
				~CResourcePreloaderHelper();				

				void Preload(const std::string &xmlPath, VOID_CALLBACK callback);
			private:			
				CResourcePreloaderHelper();
				static CResourcePreloaderHelper * self;

				VOID_CALLBACK m_Callback;
			};
		}
	}	
}

#endif ///OPTIMUS_HELPERS_LUA
