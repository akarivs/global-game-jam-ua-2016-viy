#include "LuaHelper.h"

#include <Framework.h>
#include "../optimus.h"
#include <functional>

#include <lualib.h>

namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			class CCastHelper
			{
			public:
				CCastHelper(){};
				~CCastHelper(){};

				ui::CVideoWidget * CastToVideo(ui::CBaseWidget * wdg)
				{
					return CAST(ui::CVideoWidget*, wdg);
				}

				ui::CClipWidget * CastToClip(ui::CBaseWidget * wdg)
				{
					return CAST(ui::CClipWidget*, wdg);
				}

				ui::CImageWidget * CastToImage(ui::CBaseWidget* wdg)
				{
					return CAST(ui::CImageWidget*, wdg);
				}

				ui::CSpriteAnimationWidget * CastToSpriteAnimation(ui::CBaseWidget* wdg)
				{
					return CAST(ui::CSpriteAnimationWidget*, wdg);
				}

				particles::CEmmiterWidget * CastToEmmiter(ui::CBaseWidget* wdg)
				{
					return CAST(particles::CEmmiterWidget*, wdg);
				}
			};


			CLuaHelper * CLuaHelper::self = nullptr;
			//////////////////////////////////////////////////////////////////////////
			CLuaHelper::CLuaHelper()
			{

			}

			//////////////////////////////////////////////////////////////////////////
			CLuaHelper::~CLuaHelper()
			{

			}

			//////////////////////////////////////////////////////////////////////////
			void CLuaHelper::Init()
			{
				m_LuaState = luaL_newstate();
				luaL_Reg lualibs[] =
				{
					{ "math", luaopen_math },
					{ "base", luaopen_base },
					{ NULL, NULL }
				};

				const luaL_Reg *lib = lualibs;
				for (; lib->func != NULL; lib++)
				{
					lib->func(m_LuaState);
					lua_settop(m_LuaState, 0);
				}
				luaL_openlibs(m_LuaState);
				SLB::Manager::defaultManager()->registerSLB(m_LuaState);
				InitDatatypes();
				InitUI();
				InitActions();
				InitGlobalManagers();


				/* VECTOR */
				SLB::Class<CCastHelper>("CCastHelper")
					.constructor()
					.set("ToVideo", &CCastHelper::CastToVideo)
					.set("ToClip", &CCastHelper::CastToClip)
					.set("ToSpriteAnimation", &CCastHelper::CastToSpriteAnimation)
					.set("ToImage", &CCastHelper::CastToImage)
					.set("ToEmmiter", &CCastHelper::CastToEmmiter);


				CCastHelper * castHelper = new CCastHelper();
				SLB::push(m_LuaState, castHelper);
				lua_setglobal(m_LuaState, "g_Cast");
			}

			//////////////////////////////////////////////////////////////////////////
			void CLuaHelper::InitDatatypes()
			{
				/* VECTOR */
				SLB::Class<Vector>("Vector")
					.constructor<float, float>()
					.set("Normalize", &Vector::Normalize)
					.set("DotProduct", &Vector::DotProduct)
					.set("GetLength", &Vector::GetLength)
					.property("x", &Vector::x)
					.property("y", &Vector::y)
					.property("z", &Vector::z);

				/* COLOR */
				SLB::Class<FloatColor>("FloatColor")
					.constructor<const std::string>()
					.set("SetColor", &FloatColor::SetColor)
					.set("Normalize", &FloatColor::Normalize)
					.set("GetPackedColor", &FloatColor::GetPackedColor)
					.property("a", &FloatColor::a)
					.property("r", &FloatColor::r)
					.property("g", &FloatColor::g)
					.property("b", &FloatColor::b);


				/* COLOR */
				SLB::Class<Color>("Color")
					.constructor<const std::string>()
					.set("SetColor", &Color::SetColor)
					.set("Normalize", &Color::Normalize)
					.set("GetPackedColor", &Color::GetPackedColor)
					.set("GetA", &Color::GetA)
					.set("GetR", &Color::GetR)
					.set("GetG", &Color::GetG)
					.set("GetB", &Color::GetB)
					.set("SetA", &Color::SetA)
					.set("SetR", &Color::SetR)
					.set("SetG", &Color::SetG)
					.set("SetB", &Color::SetB);
			}


			//////////////////////////////////////////////////////////////////////////
			void CLuaHelper::InitUI()
			{
				SLB::Class<ui::CBaseWidget>("CBaseWidget")
					.constructor<>(&ui::CBaseWidget::Create)
					.set("Destroy", &ui::CBaseWidget::Destroy)
					.set("AddChild", &ui::CBaseWidget::AddChild)
					.set("RemoveChild", &ui::CBaseWidget::RemoveChild)
					.set("MoveToParent", &ui::CBaseWidget::MoveToParent)
					.set("GetParent", &ui::CBaseWidget::GetParent)
					.set("GetChildByTag", &ui::CBaseWidget::GetChildByTag<ui::CBaseWidget>)
					.set("GetChildByName", &ui::CBaseWidget::GetChildByName<ui::CBaseWidget>)
					.set("SetDisabled", &ui::CBaseWidget::SetDisabled)
					.set("SetVisible", &ui::CBaseWidget::SetVisible)
					.set("SetCatchTouch", &ui::CBaseWidget::SetCatchTouch)
					.set("SetCascadeColorChange", &ui::CBaseWidget::SetCascadeColorChange)
					.set("SetTag", &ui::CBaseWidget::SetTag)
					.set("GetTag", &ui::CBaseWidget::GetTag)
					.set("SetLayer", &ui::CBaseWidget::SetLayer)
					.set("GetLayer", &ui::CBaseWidget::GetLayer)
					.set("SetColor", &ui::CBaseWidget::SetColor)
					.set("GetColor", &ui::CBaseWidget::GetColor)
					.set("SetRotation", &ui::CBaseWidget::SetRotation)
					.set("GetRotation", &ui::CBaseWidget::GetRotation)
					.set("SetOpacity", &ui::CBaseWidget::SetOpacity)
					.set("SetScale", (void (ui::CBaseWidget::*)(const Vector&))&ui::CBaseWidget::SetScale)
					.set("SetScaleOne", (void (ui::CBaseWidget::*)(const float))&ui::CBaseWidget::SetScale)
					.set("SetScaleF", (void (ui::CBaseWidget::*)(const float, const float, const float))&ui::CBaseWidget::SetScale)
					.set("SetScaleX", &ui::CBaseWidget::SetScaleX)
					.set("SetScaleY", &ui::CBaseWidget::SetScaleY)
					.set("SetPositionX", &ui::CBaseWidget::SetPositionX)
					.set("SetPositionY", &ui::CBaseWidget::SetPositionY)					
					.set("SetPositionF", (void (ui::CBaseWidget::*)(const float, const float, const float))&ui::CBaseWidget::SetPosition)
					.set("SetPosition", (void (ui::CBaseWidget::*)(const Vector&))&ui::CBaseWidget::SetPosition)
					.set("GetPosition", &ui::CBaseWidget::GetPosition);

				SLB::Class<ui::CImageWidget>("CImageWidget")
					.constructor<>(&ui::CImageWidget::Create)
					.inherits<ui::CBaseWidget>()
					.set("SetTexture", (void (ui::CImageWidget::*)(const std::string&))&ui::CImageWidget::SetTexture);

				SLB::Class<ui::CSpriteAnimationWidget>("CSpriteAnimationWidget")
					.constructor<>(&ui::CSpriteAnimationWidget::__LuaCreate)
					.inherits<ui::CBaseWidget>()
					.set("Play", &ui::CSpriteAnimationWidget::Play)
					.set("RandomFrame", &ui::CSpriteAnimationWidget::RandomFrame)
					.set("Stop", &ui::CSpriteAnimationWidget::Stop)
					.set("SetCallback", &ui::CSpriteAnimationWidget::SetCallbackLua)
					.set("SetFramesMap", &ui::CSpriteAnimationWidget::__LuaSetFramesMap)
					.set("SetTexture", (void(ui::CSpriteAnimationWidget::*)(const std::string&))&ui::CSpriteAnimationWidget::SetTexture);

				SLB::Class<ui::CVideoWidget>("CVideoObject")
					.constructor<>(&ui::CVideoWidget::__LuaCreate)
					.inherits<ui::CBaseWidget>()
					.set("Init", (void (ui::CVideoWidget::*)(const std::string &, const bool, const bool, const bool, const bool))&ui::CVideoWidget::Init)
					.set("Play", &ui::CVideoWidget::Play)
					.set("Stop", &ui::CVideoWidget::Stop)
					.set("SetCallback", &ui::CVideoWidget::__LuaSetCallback);

				SLB::Class<ui::CWindowWidget>("CWindowWidget")
					.constructor<>(&ui::CWindowWidget::Create)
					.inherits<ui::CBaseWidget>()
					.set("Show", &ui::CWindowWidget::Show)
					.set("Hide", &ui::CWindowWidget::Hide);

				SLB::Class<particles::CEmmiterWidget>("CEmmiterWidget")
					.constructor<>(&particles::CEmmiterWidget::__luaCreate)
					.inherits<ui::CBaseWidget>()
					.set("Play", &particles::CEmmiterWidget::Play)
					.set("Stop", &particles::CEmmiterWidget::Stop)
					.set("Shoot", &particles::CEmmiterWidget::Shoot);

				SLB::Class<ui::CClipWidget>("CClipWidget")
					.constructor<>(&ui::CClipWidget::Create)
					.inherits<ui::CBaseWidget>()
					.set("LoadFromXML", &ui::CClipWidget::LoadFromXML)
					.set("Play", &ui::CClipWidget::Play)					
					.set("SetAnimationType", &ui::CClipWidget::__LuaSetAnimationType)
					.set("Stop", &ui::CClipWidget::Stop);
			}

			//////////////////////////////////////////////////////////////////////////
			void CLuaHelper::InitActions()
			{
				SLB::Class<helpers::CBaseAction>("CBaseAction")
					.constructor<>(&helpers::CBaseAction::Create);

				
				SLB::push(m_LuaState, (int)CActionQueue::RepeatType::RepeatUnlimited);
				lua_setglobal(m_LuaState, "RepeatType_Unlimited");

				SLB::push(m_LuaState, (int)AnimationType::ForwardOnce); lua_setglobal(m_LuaState, "AnimationType_ForwardOnce");
				SLB::push(m_LuaState, (int)AnimationType::BackwardOnce); lua_setglobal(m_LuaState, "AnimationType_BackwardOnce");
				SLB::push(m_LuaState, (int)AnimationType::ForwardLoop); lua_setglobal(m_LuaState, "AnimationType_ForwardLoop");
				SLB::push(m_LuaState, (int)AnimationType::BackwardLoop); lua_setglobal(m_LuaState, "AnimationType_BackwardLoop");
				SLB::push(m_LuaState, (int)AnimationType::PingPong); lua_setglobal(m_LuaState, "AnimationType_PingPong");

				


				

				SLB::Class<helpers::CActionQueue>("CActionQueue")
					.constructor<>(&helpers::CActionQueue::Create)
					.inherits<ui::CBaseWidget>()
					.set("AddAction", &helpers::CActionQueue::AddAction)
					.set("Stop", &helpers::CActionQueue::Stop)
					.set("SkipAction", &helpers::CActionQueue::SkipAction)
					.set("Pause", &helpers::CActionQueue::Pause)
					.set("SetRepeatType", &helpers::CActionQueue::__LuaSetRepeatType);

				SLB::Class<helpers::CAlphaToAction>("CAlphaToAction")
					.constructor<>(&helpers::CAlphaToAction::__LuaCreate)
					.inherits<helpers::CBaseAction>();

				SLB::Class<helpers::CMoveToParticleAction>("CMoveToParticleAction")
					.constructor<>(&helpers::CMoveToParticleAction::__LuaCreate)
					.inherits<helpers::CBaseAction>();

				SLB::Class<helpers::CMoveToAction>("CMoveToAction")
					.constructor<>(&helpers::CMoveToAction::__LuaCreate)
					.inherits<helpers::CBaseAction>();

				SLB::Class<helpers::CMoveByAction>("CMoveByAction")
					.constructor<>(&helpers::CMoveByAction::__LuaCreate)
					.inherits<helpers::CBaseAction>();

				SLB::Class<helpers::CMoveToBezierAction>("CMoveToBezierAction")
					.constructor<>(&helpers::CMoveToBezierAction::__LuaCreate)
					.inherits<helpers::CBaseAction>();

				SLB::Class<helpers::CSleepAction>("CSleepAction")
					.constructor<>(&helpers::CSleepAction::Create)
					.inherits<helpers::CBaseAction>();

				SLB::Class<helpers::CHideAction>("CHideAction")
					.constructor<>(&helpers::CHideAction::__LuaCreate)
					.inherits<helpers::CBaseAction>();

				SLB::Class<helpers::CShowAction>("CShowAction")
					.constructor<>(&helpers::CShowAction::__LuaCreate)
					.inherits<helpers::CBaseAction>();

				SLB::Class<helpers::CScaleToAction>("CScaleToAction")
					.constructor<>(&helpers::CScaleToAction::__LuaCreate)
					.inherits<helpers::CBaseAction>();

				SLB::Class<helpers::CScaleByAction>("CScaleByAction")
					.constructor<>(&helpers::CScaleByAction::__LuaCreate)
					.inherits<helpers::CBaseAction>();

				SLB::Class<helpers::CCallbackLUAAction>("CCallbackAction")
					.constructor<>(&helpers::CCallbackLUAAction::Create)
					.set("IsEnded", &helpers::CCallbackLUAAction::IsEnded)
					.set("Start", &helpers::CCallbackLUAAction::Start)
					.inherits<helpers::CBaseAction>();

			}

			//////////////////////////////////////////////////////////////////////////
			void CLuaHelper::InitGlobalManagers()
			{
				SLB::Class<CSoundManager>("CSoundManager")
					.constructor()
					.set("Play", &CSoundManager::Play)
					.set("IsPlaying", &CSoundManager::IsPlaying)
					.set("Stop", &CSoundManager::Stop);

				SLB::push(m_LuaState, g_SoundManager);
				lua_setglobal(m_LuaState, "g_SoundManager");
			}


			//////////////////////////////////////////////////////////////////////////
			void CLuaHelper::OnSceneChange(ui::CBaseWidget * scene)
			{
				SLB::push(m_LuaState, scene);
				lua_setglobal(m_LuaState, "g_Scene");
			}

			//////////////////////////////////////////////////////////////////////////
			void CLuaHelper::RunScript(ui::CBaseWidget * obj, const std::string &script)
			{
				SLB::push(m_LuaState, obj);
				lua_setglobal(m_LuaState, "this");
				luaL_dostring(m_LuaState, "SLB.using(SLB)\n");
				int status = luaL_dostring(m_LuaState, script.c_str());
				ReportErrors(m_LuaState, status);
			}

			//////////////////////////////////////////////////////////////////////////
			void CLuaHelper::ReportErrors(lua_State *l, const int status)
			{
				if (status != 0)
				{
					CONSOLE_ERROR("LUA:: -> %s", lua_tostring(l, -1));
					lua_pop(l, 1); // remove error message		
				}
			}
		}
	}
}