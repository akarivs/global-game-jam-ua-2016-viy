#include "ShaderHelper.h"

#include <Framework.h>
#include "../optimus.h"


namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{

			CShaderHelper * CShaderHelper::self = nullptr;
			//////////////////////////////////////////////////////////////////////////
			CShaderHelper::CShaderHelper()
			{

			}

			//////////////////////////////////////////////////////////////////////////
			CShaderHelper::~CShaderHelper()
			{

			}

			//////////////////////////////////////////////////////////////////////////
			void CShaderHelper::UseShader(CShader * shader, NBG::CTextureResource * texture)
			{
				g_Render->DrawBatch();
#ifdef NBG_WIN32
				if (shader)
				{
					IDirect3DTexture9* _texture = nullptr;
					HRESULT hr = S_OK;
					if (texture)
					{
						_texture = (IDirect3DTexture9*)texture->GetTexture()->GetTextureData();
					}
					
					//5g_Render->GetDevice()->SetTexture(0, _texture);
					shader->effect->SetTexture(shader->texture, _texture);
					shader->effect->CommitChanges();
					UINT numPasses = 0;
					shader->effect->Begin(&numPasses,0);										
					shader->effect->BeginPass(0);
		

					
				}
				else
				{					
					//shader->effect->End();
					//g_Render->GetDevice()->SetVertexShader(nullptr);
					//g_Render->GetDevice()->SetPixelShader(nullptr);
				}
#endif
			}

			//////////////////////////////////////////////////////////////////////////
			void CShaderHelper::EndShader(CShader * shader)
			{

#ifdef NBG_WIN32
				if (shader)
				{
					shader->effect->EndPass();
					shader->effect->End();
				}
#endif
			}


			//////////////////////////////////////////////////////////////////////////
			CShader * CShaderHelper::CreateShader(const std::string &shaderPath)
			{
				CShader * shader = new CShader();
				std::string path = shaderPath;

#ifdef NBG_WIN32
				LPD3DXBUFFER errors;
				path = "Resources/" + shaderPath + ".hlsl";

				D3DXCreateEffectFromFileA(g_Render->GetDevice(), path.c_str(), 0, 0, D3DXSHADER_DEBUG, 0, &shader->effect, &errors);
				// Some Error checking   
				if (errors)
				{
					CONSOLE_ERROR("%s", errors->GetBufferPointer());
					MessageBoxA(0, (char *)errors->GetBufferPointer(), 0, 0);
					errors->Release();
					return nullptr;
				}
				// Retrieve a Technique from your effects handler
				D3DXHANDLE mhTech = shader->effect->GetTechniqueByName("GreyscaleObject");
				// Set the technique to be used 
				shader->effect->SetTechnique(mhTech);

				D3DXHANDLE texture = shader->effect->GetParameterByName(0, "xTexture");
				D3DXHANDLE projection = shader->effect->GetParameterByName(0, "WorldViewProjection");
				
				shader->texture = texture;


				// Set effect values indicating the handler and the data as paramters
				shader->effect->SetMatrix(projection, &(g_Render->GetMatrixWorlViewProjection()));			


				/*HRESULT hr;
				LPD3DXBUFFER pCode;
				DWORD dwShaderFlags = D3DXSHADER_DEBUG;
				ID3DXBuffer * pBufferErrors = 0;

				std::string vPath = shaderPath + ".vsh";
				std::string pPath = shaderPath + ".psh";

				// Assemble the vertex shader from the file
				hr = D3DXCompileShaderFromFileA(vPath.c_str(), nullptr, nullptr, "main",
				"vs_2_0", dwShaderFlags, &pCode,
				&pBufferErrors, &shader->vertexTable);

				if (FAILED(hr))
				{
				MessageBoxA(nullptr, (const char*)pBufferErrors->GetBufferPointer(), "Vertex Shader Compile Error",
				MB_OK | MB_ICONEXCLAMATION);
				return nullptr;
				}

				// Create the vertex shader
				g_Render->GetDevice()->CreateVertexShader((DWORD*)pCode->GetBufferPointer(),
				&shader->vertexShader);
				pCode->Release();

				//
				// Create a HLSL based pixel shader.
				//

				// Assemble the vertex shader from the file
				hr = D3DXCompileShaderFromFileA(pPath.c_str(), nullptr, nullptr, "main",
				"ps_2_0", dwShaderFlags, &pCode,
				&pBufferErrors, &shader->pixelTable);

				if (FAILED(hr))
				{
				LPVOID pCompilErrors = pBufferErrors->GetBufferPointer();
				MessageBoxA(nullptr, (const char*)pCompilErrors, "Pixel Shader Compile Error",
				MB_OK | MB_ICONEXCLAMATION);
				return nullptr;
				}

				// Create the vertex shader
				g_Render->GetDevice()->CreatePixelShader((DWORD*)pCode->GetBufferPointer(),
				&shader->pixelShader);
				pCode->Release();

				*/
#endif
				return shader;
			}
		}
	}
}