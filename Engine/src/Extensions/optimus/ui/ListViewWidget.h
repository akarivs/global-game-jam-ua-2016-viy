#ifndef OPTIMUS_UI_LISTVIEW_WIDGET
#define OPTIMUS_UI_LISTVIEW_WIDGET

#include <Framework/Utils/MathUtils.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Global/Resources/TextureResource.h>
#include <Framework/Helpers/AtlasHelper.h>

#include <../External/xml/pugixml.hpp>
#include <queue>



#include "ImageWidget.h"
#include "SliderWidget.h"

#include "../optimus_macro.h"
#include "../optimus_enum.h"

using namespace NBG;
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
			class CSliderWidget;

			enum class CListViewWidget_EventType
			{
				Event_Scroll,
				Event_ReachTop,
				Event_ReachBottom,
			};


			enum class CListView_DirectionType
			{
				Horizontal,
				Vertical
			};

			typedef std::function < void(CBaseWidget * sender, CListViewWidget_EventType type) > CListViewWidget_EventListener;

			class CListViewWidget: public CBaseWidget
			{
			public:
				OPTIMUS_CREATE(CListViewWidget);				
				virtual ~CListViewWidget();

				virtual void OnLoadFromXMLNode(const pugi::xml_node &node);				

				virtual void Init();

				void SetListViewListener(CListViewWidget_EventListener callback);
				void SetSlider(CSliderWidget * slider);

				virtual ReturnCodes OnMouseWheel(const int delta);

				CBaseWidget * GetContainer()
				{
					return m_Container;
				}

				///Add widget to the list
				void AddWidget(CBaseWidget * widget);		

				void Clear();

				virtual ReturnCodes Update(const float dt);
				virtual ReturnCodes Draw(CRenderState & renderState);
				virtual ReturnCodes AfterDraw(CRenderState & renderState);
				void UpdatePosition(float pos);
			protected:
				CListViewWidget();
				ReturnCodes EventListener(CBaseWidget * sender, CBaseWidget_EventType type, CBaseWidget_EventData data);

				void SliderListener(CSliderWidget * sender, CSliderWidget_EventType type, const float value);

				CBaseWidget * m_Container;
				

				bool m_IsDrag;
				bool m_IsMouseDown;
				Vector m_DragStartPos;
				Vector m_DragContainerStartPos;
				float m_DragStartTime;

				float m_Inertion;

				GETTER_SETTER(float, m_ItemsDistance, ItemsDistance);
				GETTER_SETTER(float, m_InertionPower, InertionPower);
				GETTER_SETTER(float, m_InertionStopage, InertionStopage);
				GETTER_SETTER(CListView_DirectionType, m_Direction, Direction);			
				

				CSliderWidget * m_Slider;
				CBaseWidget * m_EventCatcher;
				
				CListViewWidget_EventListener m_ListViewEventListener;				
			};
		}
	}
}
#endif //OPTIMUS_UI_LISTVIEW_WIDGET