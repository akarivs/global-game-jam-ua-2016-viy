#include "SpriteAnimationWidget.h"	

#include <Framework.h>
#include "../optimus.h"



namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
			//////////////////////////////////////////////////////////////////////////
			CSpriteAnimationWidget::CSpriteAnimationWidget():CBaseWidget()
			{
				
			}

			//////////////////////////////////////////////////////////////////////////
			CSpriteAnimationWidget::~CSpriteAnimationWidget()
			{

			}

			//////////////////////////////////////////////////////////////////////////
			void CSpriteAnimationWidget::Init()
			{
				m_Image = ui::CImageWidget::Create();
				AddChild(m_Image);
				m_IsStoped = true;
				m_IsPongForward = true;

				m_IsPaused = true;				
				m_SpeedMultiplier = 1.0f;

				SetCascadeColorChange(true);

				m_UV.left = -1;

			}


			//////////////////////////////////////////////////////////////////////////
			void CSpriteAnimationWidget::OnLoadFromXMLNode(const pugi::xml_node &node)
			{
				m_AnimName = node.attribute("texture").value();


				

				m_FramesCount = node.attribute("frames_count").as_int();
				m_StartFrame = node.attribute("start_frame").as_int();
				m_FramesInterval = node.attribute("interval").as_float();
				std::string animType = node.attribute("animation_type").value();

				AnimationType type;
				if (animType == "forward_once")type = AnimationType::ForwardOnce;
				else if (animType == "forward_loop")type = AnimationType::ForwardLoop;
				else if (animType == "backward_once")type = AnimationType::BackwardOnce;
				else if (animType == "backward_loop")type = AnimationType::BackwardLoop;
				else if (animType == "ping_pong")type = AnimationType::PingPong;

				if (node.attribute("frames_map").empty() == false)
				{
					auto map = StringUtils::ExplodeString(node.attribute("frames_map").value(), ',');
					std::vector<int>fMap;
					fMap.resize(map.size());
					for (int i = 0; i < map.size(); i++)
					{
						fMap[i] = NBG::StringUtils::ToInt(map[i]);
					}
					InitAnimation(m_AnimName, fMap, m_FramesInterval, type);
				}
				else
				{
					InitAnimation(m_AnimName, m_FramesCount, m_FramesInterval, type, m_StartFrame);
				}
				

				if (node.attribute("play_from_start").as_bool())
				{
					if (node.attribute("random_start_frame").as_bool())
					{
						m_CurrentFrame = g_Random->RandI(0, m_FramesCount - 1);
					}
					Play();
				}

				m_Image->SetTag(GetTag());

				if (node.attribute("uv").empty() == false)
				{
					std::string uv = node.attribute("uv").value();
					auto vec = StringUtils::ExplodeString(uv, ' ');
					FRect uvR;
					uvR.left = StringUtils::ToFloat(vec[0]);
					uvR.top = StringUtils::ToFloat(vec[1]);
					uvR.right = StringUtils::ToFloat(vec[2]);
					uvR.bottom = StringUtils::ToFloat(vec[3]);
					SetUV(uvR);
				}

				if (node.attribute("repeated").empty() == false)
				{
					SetRepeatedTexture();
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CSpriteAnimationWidget::__LuaSetFramesMap(const std::string &map)
			{
				auto mapS = StringUtils::ExplodeString(map, ',');
				std::vector<int>fMap;
				fMap.resize(mapS.size());
				for (int i = 0; i < mapS.size(); i++)
				{
					fMap[i] = NBG::StringUtils::ToInt(mapS[i]);
				}
				m_FramesMap = fMap;
				m_FramesCount = fMap.size();
			}

			//////////////////////////////////////////////////////////////////////////
			void CSpriteAnimationWidget::Pause()
			{
				m_IsPaused = true;
			}

			//////////////////////////////////////////////////////////////////////////
			void CSpriteAnimationWidget::Stop()
			{
				if (m_FramesMap.size() == 0)return;
				m_IsPaused = true;
				if (m_AnimType == AnimationType::ForwardOnce)
				{
					m_CurrentFrame = 0;
				}
				else if (m_AnimType == AnimationType::ForwardLoop)
				{
					m_CurrentFrame = 0;
				}
				else if (m_AnimType == AnimationType::BackwardOnce)
				{
					m_CurrentFrame = m_FramesCount - 1;
				}
				else if (m_AnimType == AnimationType::BackwardLoop)
				{
					m_CurrentFrame = m_FramesCount - 1;
				}
				else if (m_AnimType == AnimationType::PingPong)
				{
					m_CurrentFrame = 0;
				}
				SetTexture(m_AnimName + "/" + StringUtils::ToString(m_FramesMap[m_CurrentFrame]) + m_AnimExtension);
			}

			//////////////////////////////////////////////////////////////////////////
			void CSpriteAnimationWidget::Play()
			{
				m_IsPaused = false;
				m_IsStoped = false;
			}

			//////////////////////////////////////////////////////////////////////////
			void CSpriteAnimationWidget::OnBlendModeChanged()
			{
				m_Image->SetBlendMode(m_BlendMode);
			}

			//////////////////////////////////////////////////////////////////////////
			void CSpriteAnimationWidget::SetRepeatedTexture()
			{
				for (int i = 0; i < m_FramesCount; i++)
				{
					CTextureResource * res = CAST(CTextureResource*, g_ResManager->GetResource(m_AnimName + "/" + StringUtils::ToString(m_FramesMap[i]) + m_AnimExtension));
					if (res)
					{
						g_GameApplication->GetTextureManager()->SetRepeatTexture(res->GetTexture());
					}
					m_Image->SetRepeatedTexture(true);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CSpriteAnimationWidget::InitAnimation(const std::string & texture, const int framesCount, const float interval, AnimationType animType, const int startFrame)
			{
				m_AnimExtension = ".";
				m_AnimExtension += StringUtils::GetExtension(texture);
				m_AnimName = StringUtils::RemoveExtension(texture);

				m_FramesMap.resize(framesCount);
				int frameId = 0;
				for (int i = startFrame; i < startFrame+framesCount; i++)
				{
					m_FramesMap[frameId] = i;
					frameId++;
				}

				m_FramesCount = framesCount;
				m_FramesInterval = interval;
				m_StartFrame = startFrame;				

				m_CurrentTime = 0.0f;
				m_CurrentFrame = m_StartFrame;

				m_AnimType = animType;

				if (m_AnimType == AnimationType::ForwardOnce)
				{
					m_CurrentFrame = 0;
				}
				else if (m_AnimType == AnimationType::ForwardLoop)
				{
					m_CurrentFrame = 0;
				}
				else if (m_AnimType == AnimationType::BackwardOnce)
				{
					m_CurrentFrame = m_FramesCount - 1;
				}
				else if (m_AnimType == AnimationType::BackwardLoop)
				{
					m_CurrentFrame = m_FramesCount - 1;

				}
				else if (m_AnimType == AnimationType::PingPong)
				{
					m_CurrentFrame = 0;
				}
				SetTexture(m_AnimName + "/" + StringUtils::ToString(m_FramesMap[m_CurrentFrame]) + m_AnimExtension);
			}

			//////////////////////////////////////////////////////////////////////////
			void CSpriteAnimationWidget::InitAnimation(const std::string & texture, const std::vector<int> framesMap, const float interval, AnimationType animType)
			{
				m_AnimExtension = ".";
				m_AnimExtension += StringUtils::GetExtension(texture);
				m_AnimName = StringUtils::RemoveExtension(texture);

				m_FramesMap = framesMap;				

				m_FramesCount = m_FramesMap.size();
				m_FramesInterval = interval;
				m_StartFrame = 0;

				m_CurrentTime = 0.0f;
				m_CurrentFrame = m_StartFrame;

				m_AnimType = animType;

				if (m_AnimType == AnimationType::ForwardOnce)
				{
					m_CurrentFrame = 0;
				}
				else if (m_AnimType == AnimationType::ForwardLoop)
				{
					m_CurrentFrame = 0;
				}
				else if (m_AnimType == AnimationType::BackwardOnce)
				{
					m_CurrentFrame = m_FramesCount - 1;
				}
				else if (m_AnimType == AnimationType::BackwardLoop)
				{
					m_CurrentFrame = m_FramesCount - 1;

				}
				else if (m_AnimType == AnimationType::PingPong)
				{
					m_CurrentFrame = 0;
				}
				SetTexture(m_AnimName + "/" + StringUtils::ToString(m_FramesMap[m_CurrentFrame]) + m_AnimExtension);
			}

			//////////////////////////////////////////////////////////////////////////
			void CSpriteAnimationWidget::RandomFrame()
			{
				m_CurrentFrame = g_Random->RandI(0, m_FramesMap.size() - 1);
				NextFrame();
			}


			//////////////////////////////////////////////////////////////////////////
			void CSpriteAnimationWidget::NextFrame()
			{
				if (m_FramesMap.size() == 0)return;
				switch (m_AnimType)
				{
				case AnimationType::ForwardOnce:
					m_CurrentFrame++;
					if (m_CurrentFrame >= m_FramesCount)
					{
						m_CurrentFrame = m_FramesCount - 1;
						m_IsStoped = true;
					}
					break;
				case AnimationType::ForwardLoop:
					m_CurrentFrame++;
					if (m_CurrentFrame >= m_FramesCount)
					{
						m_CurrentFrame = 0;
					}
					break;
				case AnimationType::BackwardOnce:
					m_CurrentFrame--;
					if (m_CurrentFrame < 0)
					{
						m_CurrentFrame = 0;
						m_IsStoped = true;
					}
					break;
				case AnimationType::BackwardLoop:
					m_CurrentFrame--;
					if (m_CurrentFrame < 0)
					{
						m_CurrentFrame = m_FramesCount - 1;
					}
					break;
				case AnimationType::PingPong:
					if (m_IsPongForward)
					{
						m_CurrentFrame++;
						if (m_CurrentFrame >= m_FramesCount)
						{
							m_CurrentFrame = m_FramesCount - 1;
							m_IsPongForward = false;
						}
					}
					else
					{
						m_CurrentFrame--;
						if (m_CurrentFrame < 0)
						{
							m_CurrentFrame = 0;
							m_IsPongForward = true;
						}
					}
					break;
				}
				if (m_Callbacks.size() > 0)
				{
					auto iter = m_Callbacks.find(m_FramesMap[m_CurrentFrame]);
					if (iter != m_Callbacks.end())
					{
						iter->second();
					}
				}				
				if (m_CallbacksLua.size() > 0)
				{
					auto iter = m_CallbacksLua.find(m_FramesMap[m_CurrentFrame]);
					if (iter != m_CallbacksLua.end())
					{
						iter->second->Start();
					}
				}
				SetTexture(m_AnimName + "/" + StringUtils::ToString(m_FramesMap[m_CurrentFrame]) + m_AnimExtension);
			}

			//////////////////////////////////////////////////////////////////////////
			void CSpriteAnimationWidget::SetTexture(const std::string &texturePath)
			{
				m_Image->SetTexture(texturePath);
				SetSize(m_Image->GetSize());		
				if (m_UV.left != -1.0f)
				{
					m_Image->SetUV(m_UV);
				}
			}

	
			//////////////////////////////////////////////////////////////////////////
			void CSpriteAnimationWidget::SetCallback(const int frame, std::function<void()> callback)
			{
				m_Callbacks[frame] = callback;
			}			


			//////////////////////////////////////////////////////////////////////////
			void CSpriteAnimationWidget::SetCallbackLua(const int frame, helpers::CCallbackLUAAction * callback)
			{
				m_CallbacksLua[frame] = callback;
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CSpriteAnimationWidget::Update(const float dt)
			{
				if (m_FramesMap.size() == 0)return ReturnCodes::Skip;
				CBaseWidget::Update(dt);
				if (m_IsDisabled || m_IsUpdatable == false)return ReturnCodes::Skip;
				if (m_IsStoped || m_IsPaused)return ReturnCodes::Skip;
				m_CurrentTime += dt * m_SpeedMultiplier;
				while (m_CurrentTime > m_FramesInterval)
				{
					m_CurrentTime -= m_FramesInterval;
					NextFrame();
				}
				return ReturnCodes::Skip;
			}
		}
	}
}