#ifndef OPTIMUS_UI_VIDEO_WIDGET
#define OPTIMUS_UI_VIDEO_WIDGET

#include "ImageWidget.h"
#include <theora_player/TheoraPlayer.h>

#include "../helpers/actions/CallbackLUAAction.h"

namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
			/** @brief Класс, реализующий вывод видео. Как с альфа каналом так и с непрозрачным
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CVideoWidget : public ui::CImageWidget
			{
			public:
				OPTIMUS_CREATE(CVideoWidget);
				static CVideoWidget * Create(const std::string &path, const bool playFromStart, const bool loop, const bool destroyAtEnd, const bool isOpaque)
				{
					auto ret = Create();
					ret->Init(path,playFromStart,loop,destroyAtEnd,isOpaque);
					return ret;
				}
				virtual ~CVideoWidget();

				virtual void OnRenderChange(const RenderChangeType type);


				virtual void Init(){ CImageWidget::Init(); };
				virtual void OnLoadFromXMLNode(const pugi::xml_node &node);
				void Init(const std::string &path, const bool playFromStart, const bool loop, const bool destroyAtEnd, const bool isOpaque);

				virtual void OnFocusChange(const FocusChangeType type);				

				void SetCallback(VOID_CALLBACK callback);
				void __LuaSetCallback(helpers::CCallbackLUAAction * callback);


				void Play();
				void Pause();
				void Stop();

				virtual ReturnCodes Update(const float dt);
				virtual ReturnCodes Draw(CRenderState & state);

				float GetVideoTime();

				GETTER_SETTER(float, m_Duration, Duration);

				///LUA_HELPERS
				static CVideoWidget * __LuaCreate(const std::string &path, const bool playFromStart, const bool loop, const bool destroyAtEnd, const bool isOpaque)
				{
					return Create(path,playFromStart,loop,destroyAtEnd,isOpaque);
				}
			private:
				CVideoWidget();

				TheoraVideoClip *m_Clip;
				Texture m_Texture;



				bool m_IsOpaque;
				bool m_IsDestroyAtEnd;

				VOID_CALLBACK m_Callback;
				helpers::CCallbackLUAAction * m_LuaCallback;
				bool m_HaveCallback;

				bool m_IsPaused;

				std::string m_Path;
			protected:
			};
		}
	}
}
#endif //OPTIMUS_UI_VIDEO_WIDGET

