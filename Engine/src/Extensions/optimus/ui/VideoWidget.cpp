#include "VideoWidget.h"
#include <theora_player/TheoraDataSource.h>


#include <Framework.h>
#include "../optimus.h"

namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
			class TheoraMemoryDataSource : public TheoraDataSource
			{
				unsigned long mSize, mReadPointer;
				unsigned char* mData;
			public:
				TheoraMemoryDataSource(unsigned char * data, unsigned long size);
				~TheoraMemoryDataSource();

				int read(void* output, int nBytes);
				void seek(unsigned long byte_index);
				unsigned long size();
				unsigned long tell();

				std::string repr() { return ""; }
				std::string getFilename() { return ""; }
			};

			TheoraMemoryDataSource::TheoraMemoryDataSource(unsigned char * data, unsigned long size) :
				mReadPointer(0),
				mData(0)
			{
				mSize = size;
				mData = data;
			}

			TheoraMemoryDataSource::~TheoraMemoryDataSource()
			{
				if (mData) delete[] mData;
			}

			int TheoraMemoryDataSource::read(void* output, int nBytes)
			{
				int n = (mReadPointer + nBytes <= mSize) ? nBytes : mSize - mReadPointer;
				if (!n) return 0;
				memcpy(output, mData + mReadPointer, n);
				mReadPointer += n;
				return n;
			}

			void TheoraMemoryDataSource::seek(unsigned long byte_index)
			{
				mReadPointer = byte_index;
			}

			unsigned long TheoraMemoryDataSource::size()
			{
				return mSize;
			}

			unsigned long TheoraMemoryDataSource::tell()
			{
				return mReadPointer;
			}


			//////////////////////////////////////////////////////////////////////////
			CVideoWidget::CVideoWidget():CImageWidget()
			{
				m_Clip = nullptr;
				m_Texture.SetTextureData(nullptr);
				m_HaveCallback = false;
				m_LuaCallback = nullptr;
			}

			//////////////////////////////////////////////////////////////////////////
			CVideoWidget::~CVideoWidget()
			{
				if (m_Clip)
				{
					g_GameApplication->GetTheoraVideoManager()->destroyVideoClip(m_Clip);
					m_Clip = NULL;
				}
				if (m_Texture.GetTextureData() != NULL)
				{
					g_Render->ReleaseTexture(&m_Texture);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CVideoWidget::OnLoadFromXMLNode(const pugi::xml_node &node)
			{
				std::string path = node.attribute("video").value();
				bool playFromStart = node.attribute("play_from_start").as_bool();
				bool loop = node.attribute("loop").as_bool();				
				bool destroyAtEnd = node.attribute("destroy_at_end").as_bool();
				bool isOpaque = node.attribute("opaque").as_bool();

				Init(path, playFromStart, loop, destroyAtEnd, isOpaque);
			}

			//////////////////////////////////////////////////////////////////////////
			void CVideoWidget::SetCallback(VOID_CALLBACK callback)
			{
				m_Callback = callback;
				m_HaveCallback = true;
			}

			//////////////////////////////////////////////////////////////////////////
			void CVideoWidget::__LuaSetCallback(helpers::CCallbackLUAAction * callback)
			{
				m_LuaCallback = callback;				
			}

			//////////////////////////////////////////////////////////////////////////
			void CVideoWidget::Init(const std::string &path, const bool playFromStart, const bool loop, const bool destroyAtEnd, const bool isOpaque)
			{
				m_IsPaused = false;
				m_IsOpaque = isOpaque;
				m_Path = path;
				m_IsDestroyAtEnd = destroyAtEnd;

				
				TheoraVideoManager * mgr = g_GameApplication->GetTheoraVideoManager();

				if (mgr)
				{
					std::string res = path;


					auto f = g_FileSystem->ReadFile(res, "rb");
					if (f.status != IFileSystem::FileOk)
					{
						CONSOLE_ERROR("Cannot load video: %s",res.c_str());
						return;
					}
					auto ds = new TheoraMemoryDataSource((unsigned char*)f.data, f.size);					
					m_Clip = mgr->createVideoClip(ds,TH_RGB,4);
					m_Clip->waitForCache();					
					m_Clip->setAutoRestart(loop);
					m_Duration = m_Clip->getDuration();
					TheoraVideoFrame *frame = m_Clip->getNextFrame();

					if (!isOpaque)
					{
						Vector scale = GetScale();
						SetScale(scale.x * 0.5f, scale.y);
					}
					SetSize(m_Clip->getWidth(), m_Clip->getHeight());					

					m_Texture.SetSize(GetSize().x, GetSize().y);
					m_Texture.SetTextureData(NULL);

				
					if (frame)
					{
						if (m_IsOpaque)
						{
							g_Render->CreateOpaqueTextureFromTheoraFrame(frame->getBuffer(), frame->mBpp, GetSize(), &m_Texture);							
						}
						else
						{
							g_Render->CreateTransparentTextureFromTheoraFrame(frame->getBuffer(), frame->mBpp, GetSize(), &m_Texture);
						}
						m_Clip->popFrame();
					}

					if (playFromStart == false)
					{
						m_Clip->stop();
						m_IsPaused = true;
					}				
				}
			}


			void CVideoWidget::OnFocusChange(const FocusChangeType type)
			{
				if (!m_Clip)return;
				if (type == FocusChangeType::Lost)
				{
					if (!m_IsPaused)
						m_Clip->pause();
				}
				else if (type == FocusChangeType::Get)
				{
					if (m_IsPaused == false)
					{
						m_Clip->play();
						m_IsPaused = false;
					}						
				}
			}			

			//////////////////////////////////////////////////////////////////////////
			void CVideoWidget::Play()
			{
				if (!m_Clip)return;
				if (!m_Clip->isDone())
				{
					m_Clip->play();
					m_Clip->waitForCache();
					m_IsPaused = false;
				}				
			}

			//////////////////////////////////////////////////////////////////////////
			void CVideoWidget::Pause()
			{
				if (!m_Clip)return;
				m_Clip->pause();				
				m_IsPaused = true;
			}

			//////////////////////////////////////////////////////////////////////////
			void CVideoWidget::Stop()
			{
				if (!m_Clip)return;
				m_Clip->stop();
				m_Clip->waitForCache();				
				m_IsPaused = true;
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CVideoWidget::Update(const float dt)
			{
				auto res = CImageWidget::Update(dt);
				if (!m_Clip)return ReturnCodes::Skip;

				if (m_Clip->isDone() == false)
				{
					if (m_Clip->isPaused() == false)
					{						
						m_Clip->update(dt);
						m_Clip->decodedAudioCheck();						
					}					
				}
				return ReturnCodes::Skip;
			}

			//////////////////////////////////////////////////////////////////////////
			float CVideoWidget::GetVideoTime()
			{
				if (!m_Clip)return 0.0f;
				return m_Clip->getTimePosition();
			}

			//////////////////////////////////////////////////////////////////////////
			void CVideoWidget::OnRenderChange(const RenderChangeType type)
			{
				if (type == RenderChangeType::After)
				{
#ifdef NBG_ANDROID
					if (m_Texture.GetTextureData() != nullptr)
					{
						g_Render->ReleaseTexture(&m_Texture);
					}
					m_Texture.SetTextureData(nullptr);
#endif
				}
			}


			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CVideoWidget::Draw(CRenderState & state)
			{
				if (!IsVisible())return ReturnCodes::Skip;
                
    
				if (!m_Clip && !m_Texture.GetTextureData())return ReturnCodes::Skip;
				auto texture = &m_Texture;
				bool needToCallCallback = false;
				if (m_Clip)
				{
					if (m_Clip->isDone())
					{
						if (m_HaveCallback)
						{
							needToCallCallback = true;
						}
						if (m_IsDestroyAtEnd)
						{
							if (needToCallCallback)
							{
								m_Callback();
							}
							if (m_LuaCallback)
							{
								m_LuaCallback->Start();
							}
							g_Render->ReleaseTexture(&m_Texture);							
							SetVisible(false);	
							g_GameApplication->GetTheoraVideoManager()->destroyVideoClip(m_Clip);
							m_Clip = NULL;
							return ReturnCodes::Skip;
						}												
						else
						{
#ifdef NBG_ANDROID
							if (m_Texture.GetTextureData() == nullptr)
							{
								TheoraVideoFrame *frame = m_Clip->getNextFrame();
								if (frame)
								{
									if (m_IsOpaque)
									{
										g_Render->CreateOpaqueTextureFromTheoraFrame(frame->getBuffer(), frame->mBpp, GetSize(), texture);
									}
									else
									{
										g_Render->CreateTransparentTextureFromTheoraFrame(frame->getBuffer(), frame->mBpp, GetSize(), texture);
									}
									m_Clip->popFrame();
								}								
							}
#endif
						}
					}
					else
					{						
						TheoraVideoFrame *frame = m_Clip->getNextFrame();						
						if (frame)
						{							
							if (m_IsOpaque)
							{
								g_Render->CreateOpaqueTextureFromTheoraFrame(frame->getBuffer(), frame->mBpp, GetSize(), texture);
							}
							else
							{
								g_Render->CreateTransparentTextureFromTheoraFrame(frame->getBuffer(), frame->mBpp, GetSize(), texture);
							}
							m_Clip->popFrame();							
						}						
					}
				}
				

				
				UpdateMesh(state);				
				
				g_Render->DrawBatch();
				g_Render->SetBlendMode(BlendMode::BLEND);
				g_Render->DrawMesh(texture, &m_Mesh);
				g_Render->DrawBatch();
				if (needToCallCallback)
				{
					if (m_Callback != nullptr)
					{
						m_Callback();
					}				
					if (m_LuaCallback)
					{
						m_LuaCallback->Start();
					}
				}
				return ReturnCodes::Skip;
			}
		}
	}
}
