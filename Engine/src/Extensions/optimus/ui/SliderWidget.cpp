#include "SliderWidget.h"
#include <Framework.h>
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{

			//////////////////////////////////////////////////////////////////////////
			CSliderWidget::CSliderWidget():CBaseWidget()
			{				

			}

			//////////////////////////////////////////////////////////////////////////
			CSliderWidget::~CSliderWidget()
			{
				
			}

			//////////////////////////////////////////////////////////////////////////
			void CSliderWidget::Init()
			{
				m_Base = CImageWidget::Create();
				m_Fill = CImageWidget::Create();
				m_Fill->SetAnchor(HotSpot::HS_UL);
				m_Fill->SetHotSpot(HotSpot::HS_UL);				
				m_Fill->SetCatchTouch(true);
				m_Base->SetCatchTouch(true);
				m_Button = nullptr;

				AddChild(m_Base);
				m_Base->AddChild(m_Fill);

				m_Percent = 100.0f;
				m_IsDrag = false;

				SetCatchTouch(true);

				m_SliderListener = nullptr;

				m_Mode = CSliderWidget_Mode::Slider;
			}

			//////////////////////////////////////////////////////////////////////////
			void CSliderWidget::OnLoadFromXMLNode(const pugi::xml_node &node)
			{
				std::string basePath = node.attribute("base").value();
				std::string fillPath = node.attribute("fill").value();
				std::string buttonPath = node.attribute("button").value();

				m_Fill->SetPositionX(node.attribute("fill_offset_x").as_float());
				m_Fill->SetPositionY(node.attribute("fill_offset_y").as_float());

				float percent = node.attribute("percent").as_float();

				Init(basePath,fillPath,buttonPath);
				SetPercent(percent);
			}

			//////////////////////////////////////////////////////////////////////////
			void CSliderWidget::SetMode(CSliderWidget_Mode mode, float sliderPercent)
			{
				m_Mode = mode;				

				if (mode == CSliderWidget_Mode::List)
				{
					if (m_Button)
					{	
						m_Button->SetScale9Enabled(true);
						m_Button->SetSize(GetSize().x * sliderPercent, m_Button->GetSize().y);
						m_Button->SetHotSpot(HotSpot::HS_ML);
						m_Button->SetAnchor(HotSpot::HS_ML);

						
						auto baseColor = m_Button->GetBaseColor();

						m_Fill->SetVisible(false);						
						m_Button->SetPositionX(0.0f);
						m_Button->SetPositionY(0.0f);


						

						m_Fill->RemoveChild(m_Button);
						AddChild(m_Button);											

						m_Button->SetColor(baseColor);
					}
				}
			}

		
			//////////////////////////////////////////////////////////////////////////
			void CSliderWidget::Init(const std::string &baseTexture, const std::string &fillTexture, const std::string buttonPath)
			{
				m_Base->SetTexture(baseTexture);
				m_Fill->SetTexture(fillTexture);
				if (buttonPath.empty() == false)
				{
					m_Button = CButtonWidget::Create();
					m_Button->SetTexture(buttonPath);					
					m_Button->SetAnchor(HotSpot::HS_MR);
					m_Fill->AddChild(m_Button);
				}			
				SetSize(m_Fill->GetSize());
			}	

			//////////////////////////////////////////////////////////////////////////
			void CSliderWidget::SetSliderListener(CSliderWidget_EventListener listener)
			{
				m_SliderListener = listener;
			}


			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CSliderWidget::OnMouseMove(const Vector &mousePos)
			{
				CBaseWidget::OnMouseMove(mousePos);
				if (m_Button == nullptr)return ReturnCodes::Skip;
				if (m_IsDrag)
				{					
					float percent = ConvertToLocalSpace(mousePos).x / GetSize().x;
					percent = m_DragPercent + (percent - m_DragPercent);					

					if (percent < 0.0f)
					{
						percent = 0.0f;
					}
					else if (percent > 1.0f)
					{
						percent = 1.0f;
					}
					SetPercent(percent, true);
					return ReturnCodes::Block;
				}
				return ReturnCodes::Skip;
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CSliderWidget::OnMouseUp(const Vector &mousePos, const int mouseButton)
			{
				CBaseWidget::OnMouseUp(mousePos, mouseButton);
				if (m_Button == nullptr)return ReturnCodes::Skip;
				if (mouseButton == (int)MouseButton::Left)
				{
					if (m_IsDrag)
					{
						m_IsDrag = false;
						if (m_SliderListener)
						{
							m_SliderListener(this, CSliderWidget_EventType::Event_DragComplete, m_Percent);
						}
						return ReturnCodes::Block;
					}
					else
					{
						if (IsContains(mousePos))
						{
							float percent = ConvertToLocalSpace(mousePos).x / GetSize().x;							

							if (percent < 0.0f)
							{
								percent = 0.0f;
							}
							else if (percent > 1.0f)
							{
								percent = 1.0f;
							}
							SetPercent(percent, true);

							if (m_SliderListener)
							{
								m_SliderListener(this, CSliderWidget_EventType::Event_DragComplete, m_Percent);
							}

							return ReturnCodes::Block;
						}
					}					
				}
				return ReturnCodes::Skip;
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CSliderWidget::OnMouseDown(const Vector &mousePos, const int mouseButton)
			{
				CBaseWidget::OnMouseDown(mousePos, mouseButton);
				if (m_Button == nullptr)return ReturnCodes::Skip;

				if (mouseButton == (int)MouseButton::Left)
				{
					if (m_Button->IsContains(mousePos))
					{
						m_IsDrag = true;
						m_DragStartPos = ConvertToLocalSpace(mousePos);
						m_DragPercent = m_Percent;
						return ReturnCodes::Block;
					}
				}
				return ReturnCodes::Skip;
			}

			//////////////////////////////////////////////////////////////////////////
			void CSliderWidget::SetPercent(const float percent, bool callListener)
			{
				m_Percent = percent;
				if (m_Percent < 0.0f)m_Percent = 0.0f;
				if (m_Percent > 1.0f)m_Percent = 1.0f;
				m_Fill->Cut(percent,1.0f);

				if (m_Mode == CSliderWidget_Mode::List)
				{
					if (m_Button)
					{
						m_Button->SetPositionX((m_Size.x - m_Button->GetSize().x) * percent);
					}
				}

				if (callListener && m_SliderListener)
				{
					m_SliderListener(this, CSliderWidget_EventType::Event_ValueChange, m_Percent);
				}
			}
		}
	}
}