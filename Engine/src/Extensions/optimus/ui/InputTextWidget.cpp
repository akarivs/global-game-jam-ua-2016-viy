#include "InputTextWidget.h"

#include <Framework.h>
#include "../optimus.h"

namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
			//////////////////////////////////////////////////////////////////////////
			CInputTextWidget::CInputTextWidget() :CBaseWidget()
			{
				m_Label = nullptr;
			}

			//////////////////////////////////////////////////////////////////////////
			CInputTextWidget::~CInputTextWidget()
			{

			}

			//////////////////////////////////////////////////////////////////////////
			CInputTextWidget* CInputTextWidget::Create(FontDescription * fontDesc)
			{
				auto ret = Create();
				ret->InitLabel(fontDesc);
				return ret;
			}


			//////////////////////////////////////////////////////////////////////////
			void CInputTextWidget::OnLoadFromXMLNode(const pugi::xml_node &node)
			{
				InitLabel(g_FontsManager->GetFont(node.attribute("font").value()));
				if (node.attribute("max_symbols").empty() == false)
				{
					SetMaxSymbols(node.attribute("max_symbols").as_int());
				}
				if (node.attribute("mask").empty() == false)
				{
					std::string text = node.attribute("mask").value();
					if (text.substr(0, 1) == "#")
					{
						m_Mask = g_LocaleManager->GetText(text.substr(1).c_str());
					}
					else
					{
						m_Mask = pugi::as_wide(text);
					}					
				}
				SetHotSpot(GetHotSpot());
			}

			//////////////////////////////////////////////////////////////////////////
			void CInputTextWidget::InitLabel(FontDescription * fontDesc)
			{
				m_Label = CTextWidget::Create(fontDesc);
				m_CursorLabel = CTextWidget::Create(fontDesc,L"|");
				AddChild(m_Label);
				m_CursorLabel->SetAnchor(HotSpot::HS_ML);
				m_CursorLabel->SetVisible(false);
				m_Label->AddChild(m_CursorLabel);				
			}

			//////////////////////////////////////////////////////////////////////////
			void CInputTextWidget::Init()
			{
				m_Mask = L"0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNMйцукенгшщззхъфывапролджэячсмитьбюЙЦУКЕНГШЩЗХЪФЫВАПРОЛДДЖЭЯЧСМИТЬБЮіІїЇєЄ";

				m_InputTextEventListener = nullptr;
				m_IsCatchTouch = true;
				m_IsCatchKeyboard = true;
				m_IsUpdatable = true;

				m_Symbols = L"";
				m_CursorPos = 0;
				m_CursorAnimTime = 0.0f;
				m_IsShowCursor = false;
				m_MaxSymbols = 10;

				m_Label = nullptr;
				m_CursorLabel = nullptr;

				m_IsTextFocused = false;
				UpdateCursor();

				SetCascadeColorChange(true);
				SetEventListener(OPTIMUS_CALLBACK_3(&CInputTextWidget::EventListener, this));
			}

			//////////////////////////////////////////////////////////////////////////
			void CInputTextWidget::SetInputTextListener(CInputTextWidget_EventListener callback)
			{
				m_InputTextEventListener = callback;
			}

			//////////////////////////////////////////////////////////////////////////
			void CInputTextWidget::SetTextFocused(const bool focused)
			{
				m_IsTextFocused = focused;
				UpdateCursor();
				m_CursorLabel->SetVisible(m_IsTextFocused);
			}


			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CInputTextWidget::EventListener(CBaseWidget * sender, CBaseWidget_EventType type, CBaseWidget_EventData data)
			{
				if (type == CBaseWidget_EventType::Event_MouseUp)
				{					
					ReturnCodes code = ReturnCodes::Block;
					if (m_IsTextFocused == false)
					{
						code = ReturnCodes::Skip;
					}
					m_IsTextFocused = true;
					UpdateCursor();
					m_CursorLabel->SetVisible(m_IsTextFocused);
					return code;
				}
				else if (type == CBaseWidget_EventType::Event_MouseUpOutside)
				{						
					m_IsTextFocused = false;
					UpdateCursor();
					m_CursorLabel->SetVisible(m_IsTextFocused);
					return ReturnCodes::Skip;
				}
                else if (type == CBaseWidget_EventType::Event_KeyUp)
				{
#ifndef NBG_WIN32
                    std::wstring strAll = g_Input->GetLastInput();
                    if (strAll.size() > 0)
                    {
                        for (int i=0; i<strAll.size(); i++)
                        {
                            std::wstring str = strAll.substr(i,1);
                            if (m_Symbols.size() < (size_t)m_MaxSymbols)
                            {
                                if (m_Mask.find(str) != std::wstring::npos)
                                {
                                    m_Symbols.insert(m_CursorPos, str);
                                    SetText(m_Symbols);
                                    m_CursorPos++;
                                    m_IsShowCursor = true;
                                    m_CursorAnimTime = 0.7f;
                                    UpdateCursor();
                                    
                                }
                            }
                        }
                        
                        g_Input->SetLastInputChar(0);
                        return ReturnCodes::Block;
                        
                        
                    }
#endif
                }
				else if (type == CBaseWidget_EventType::Event_KeyDown)
				{
					if (!m_IsTextFocused)return ReturnCodes::Skip;
					int key = data.key;
					switch (key)
					{
					case KEY_LEFT:
						m_CursorPos--;
						m_IsShowCursor = true;
						m_CursorAnimTime = 0.7f;
						if (m_CursorPos < 0)m_CursorPos = 0;
						UpdateCursor();
						g_Input->SetLastInputChar(0);
						return ReturnCodes::Block;
						break;
					case KEY_RIGHT:
						m_CursorPos++;
						m_IsShowCursor = true;
						m_CursorAnimTime = 0.7f;
						if ((size_t)m_CursorPos > m_Symbols.size())m_CursorPos = m_Symbols.size();
						UpdateCursor();
						g_Input->SetLastInputChar(0);
						return ReturnCodes::Block;
						break;
					case KEY_BACK:
						if (m_CursorPos > 0)
						{
							m_Symbols.erase(m_CursorPos-1, 1);
							SetText(m_Symbols);
							m_CursorPos--;
							m_IsShowCursor = true;
							m_CursorAnimTime = 0.7f;
							if (m_CursorPos < 0)m_CursorPos = 0;
							UpdateCursor();
						}
						g_Input->SetLastInputChar(0);
						return ReturnCodes::Block;
						break;
					case KEY_DELETE:
						if ((size_t)m_CursorPos < m_Symbols.size())
						{
							m_Symbols.erase(m_CursorPos, 1);
							m_IsShowCursor = true;
							m_CursorAnimTime = 0.7f;
							SetText(m_Symbols);
							UpdateCursor();
						}
						g_Input->SetLastInputChar(0);
						return ReturnCodes::Block;
						break;
					case KEY_SPACE:
						g_Input->SetLastInputChar(0);
						return ReturnCodes::Block;
						break;
                            #ifdef NBG_WIN32
					default:
						if (m_Symbols.size() < (size_t)m_MaxSymbols)
						{
							std::wstring str = g_Input->GetLastInput();
							if (m_Mask.find(str) != std::wstring::npos)
							{
								if (str.size() > 0)
								{
									m_Symbols.insert(m_CursorPos, str);
									SetText(m_Symbols);
									m_CursorPos++;
									m_IsShowCursor = true;
									m_CursorAnimTime = 0.7f;
									UpdateCursor();
									g_Input->SetLastInputChar(0);
									return ReturnCodes::Block;
								}								
							}

						}
						break;
                            #endif
					}
					return ReturnCodes::Skip;
				}
				return ReturnCodes::Skip;
			}

			//////////////////////////////////////////////////////////////////////////
			void CInputTextWidget::SetText(const std::wstring &text)
			{
				m_Label->SetText(text);				
				m_Symbols = text;
				if (m_InputTextEventListener)
				{
					m_InputTextEventListener(CInputTextWidget_EventType::Event_TextChange, this);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CInputTextWidget::OnColorChanged()
			{
				if (m_CursorLabel)
				{
					m_CursorLabel->SetColor(GetColor());
				}				
			}

			//////////////////////////////////////////////////////////////////////////
			void CInputTextWidget::MoveCursorToStart()
			{
				m_CursorPos = 0;
				UpdateCursor();
			}

			//////////////////////////////////////////////////////////////////////////
			void CInputTextWidget::MoveCursorToEnd()
			{
				m_CursorPos = m_Symbols.size();
				UpdateCursor();
			}

			//////////////////////////////////////////////////////////////////////////
			void CInputTextWidget::OnHotSpotChanged()
			{
				if (m_Label)
				{
					m_Label->SetHotSpot(m_HotSpot);
					m_Label->SetAnchor(m_HotSpot);
				}				
			}


			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CInputTextWidget::Update(const float dt)
			{
				auto ret = CBaseWidget::Update(dt);
				if (ret > ReturnCodes::SkipDontProcess)return ret;

				if (!m_IsTextFocused)return ReturnCodes::Skip;
				m_CursorAnimTime -= g_FrameTime;
				if (m_CursorAnimTime <= 0.0f)
				{
					m_CursorAnimTime = 0.7f;
					m_IsShowCursor = !m_IsShowCursor;
					m_CursorLabel->SetVisible(m_IsShowCursor);
				}
				return ReturnCodes::Skip;
			}

			//////////////////////////////////////////////////////////////////////////
			void CInputTextWidget::UpdateCursor()
			{				
				if (!m_IsTextFocused)return;
				Vector pos = m_Label->GetPosition();

				if (m_Label->GetText().size() == 0)
				{
					m_Label->SetText(L" ");
				}
				m_CursorLabel->SetPosition(m_Label->GetStringWidth(0, m_CursorPos) + m_CursorLabel->GetStringWidth() / 2, pos.y);				
				m_CursorLabel->SetVisible(true);
			}
		}
	}
}