#include "TextWidget.h"

#include <framework.h>
#include <Framework/Managers/FontsManager.h>

#include "ImageWidget.h"
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
			//////////////////////////////////////////////////////////////////////////
			CTextWidget::CTextWidget():CBaseWidget()
			{
				

			}

			//////////////////////////////////////////////////////////////////////////
			CTextWidget::~CTextWidget()
			{
			
			}

			//////////////////////////////////////////////////////////////////////////
			void CTextWidget::Init()
			{
				m_CharacterPadding = 0.0f;				
				m_BaseLine = 10.0f;
				m_BaseCharacterPadding = 10.0f;
				m_SpaceSize = 8.0f;
				m_LineHeight = 22.0f;
				m_Offset.x = m_Offset.y = 0.0f;
				m_PrevHotSpot = m_HotSpot;
				m_TextContainer = CBaseWidget::Create();

				m_BoundingBox.right = 0.0f;
				m_BoundingBox.bottom = 0.0f;
				
				m_TextContainer->SetCascadeColorChange(true);
				m_TextContainer->SetBlockUpdateBoundPoints(true);
				//m_TextContainer->SetUseHotSpotInCalculations(true);
			}




			//////////////////////////////////////////////////////////////////////////
			void CTextWidget::OnLoadFromXMLNode(const pugi::xml_node &node)
			{
				std::string font = node.attribute("font").value();
				NBG::FontDescription * desc = g_FontsManager->GetFont(font);
				std::string text = node.attribute("text").value();
				std::wstring resText = L"";
				if (text.substr(0, 1) == "#")
				{
					resText = g_LocaleManager->GetText(text.substr(1).c_str());
				}
				else
				{
					resText = pugi::as_wide(text);
				}			

				Init(desc, resText);
				bool changeText = false;

				if (node.attribute("letter_spacing").empty() == false)
				{
					m_CharacterPadding = node.attribute("letter_spacing").as_float();
					m_BaseCharacterPadding = m_CharacterPadding;
					changeText = true;
				}

				if (node.attribute("space_size").empty() == false)
				{
					m_SpaceSize = node.attribute("space_size").as_float();
					changeText = true;
				}

				if (node.attribute("line_height").empty() == false)
				{
					m_LineHeight = node.attribute("line_height").as_float();
					changeText = true;
				}


				if (node.attribute("max_width").empty() == false)
				{
					FRect r;
					r.right = node.attribute("max_width").as_float();
					m_BoundingBox = r;
					changeText = true;
				}

				if (changeText)
				{
					SetText(resText);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CTextWidget::Init(NBG::FontDescription * fontDesc, const std::wstring str)
			{
				AddChild(m_TextContainer);
				m_FontDesc = fontDesc;

				m_Chars = fontDesc->chars;
				m_CharacterPadding = fontDesc->characterPadding;
				m_CharsCount = fontDesc->charsCount;
				m_Kernings = fontDesc->kernings;
				m_KerningsCount = fontDesc->kerningsCount;
				m_KerningsMap = fontDesc->kerningsMap;
				m_BaseLine = fontDesc->baseLine;
				m_SpaceSize = fontDesc->spaceSize;
				m_LineHeight = fontDesc->lineHeight;
				m_TextureId = fontDesc->texture;

				m_BaseCharacterPadding = m_CharacterPadding;
				m_BaseText = str;

				m_Texture = CAST(NBG::CTextureResource*, g_ResManager->GetResource(m_TextureId));

				SetText(str);
			}

			//////////////////////////////////////////////////////////////////////////
			void CTextWidget::SetFont(NBG::FontDescription * fontDesc)
			{				
				m_FontDesc = fontDesc;

				m_Chars = fontDesc->chars;
				m_CharacterPadding = fontDesc->characterPadding;
				m_CharsCount = fontDesc->charsCount;
				m_Kernings = fontDesc->kernings;
				m_KerningsCount = fontDesc->kerningsCount;
				m_KerningsMap = fontDesc->kerningsMap;
				m_BaseLine = fontDesc->baseLine;
				m_SpaceSize = fontDesc->spaceSize;
				m_LineHeight = fontDesc->lineHeight;
				m_TextureId = fontDesc->texture;

				m_BaseCharacterPadding = m_CharacterPadding;
			
				m_Texture = CAST(NBG::CTextureResource*, g_ResManager->GetResource(m_TextureId));

				SetText(m_Text);
				
				m_TextContainer->RemoveAllChilds(true);
			}

			//////////////////////////////////////////////////////////////////////////
			void CTextWidget::OnHotSpotChanged()
			{
				if (m_HotSpot == m_PrevHotSpot)return;
				m_PrevHotSpot = m_HotSpot;
				SetText(m_Text);
			}

			//////////////////////////////////////////////////////////////////////////
			void CTextWidget::OnColorChanged()
			{
				m_TextContainer->SetColor(GetColor());
			}

			//////////////////////////////////////////////////////////////////////////
			void CTextWidget::OnScaleChanged()
			{

			}

			//////////////////////////////////////////////////////////////////////////
			void CTextWidget::PreprocessString()
			{
				m_Colors.clear();

				///Ищем управляющие тэги
				///Цвет
				std::wstring color = L"";
				std::wstring what = L"<color:";
				std::wstring whatEnd = L"</color>";
				std::wstring to = L"";
				for (size_t index = 0; index = m_Text.find(what, index), index != std::string::npos;)
				{
					color = m_Text.substr(index + what.size(), 8);
					
					auto colorS = StringUtils::HexWStringToColor(color);
										

					
					m_Text.replace(index, what.length() + 9, to);
					int newIndex = m_Text.find(whatEnd, 0);
					m_Text.replace(newIndex, whatEnd.length(), to);

					for (int j = index; j < newIndex; j++)
					{
						m_Colors[j] = colorS;
					}					
				}								
			}

			//////////////////////////////////////////////////////////////////////////
			void CTextWidget::SetText(const std::wstring text)
			{
				//if (text == m_Text)return;
				if (m_Chars.size() == 0)return;
				m_Sprites = &m_TextContainer->GetChilds();
				m_TextContainer->SetPosition(0.0f, 0.0f);
				m_CurrentSprite = 0;

				m_Offset.x = 0.0f;
				m_Offset.y = 0.0f;
				m_Text = text;
				PreprocessString();
				int size = m_Text.length();
				///Если уже было что-то закешировано - удалить массив и пересоздать его заново				
				m_CachedSymbols.resize(size);
				int id;				


				bool isLocaleWithoutSpaces = g_LocaleManager->IsLocaleWithoutSpaces();


				bool foundOperator = false;

				///Пройтись в цикле по всем символам и найти им соответствие в массиве символов шрифта
				for (size_t i = 0; i < m_Text.length(); i++)
				{			
					if (m_Text[i] == L'^')
					{
						foundOperator = !foundOperator;						
					}
					if (foundOperator || m_Text[i] == L'^')
					{
						m_CachedSymbols[i] = SERVICE_ID;
						continue;
					}

					id = (int)m_Text[i];
					if (id == 32) //SPACE
					{
						m_CachedSymbols[i] = SPACE_ID;
						continue;
					}
					else if (id == 10) //NEW LINE
					{
						m_CachedSymbols[i] = NEW_LINE_ID;
						continue;
					}
					CharOffsets* character = GetCharById(id);
					if (character)
					{
						m_CachedSymbols[i] = character->arrayId;
					}
					else
					{
						m_CachedSymbols[i] = -1;
					}
				}

				int posX = 0;
				int posY = 0;
				int previousSpace = -1;
				int previousId = 0;
				int width = 0;
				int lineWidth = 0;
				int prevRectI = -1;

				NBG::Vector sizeLabel;
				foundOperator = false;

				int length = m_Text.length();

				float height = 0.0f;
				for (size_t i = 0; i < m_Text.length(); ++i)
				{					
					id = m_CachedSymbols[i];
	
					
					if (m_BoundingBox.right > 0.0f)
					{
						int sz = 0;
						if (id == SPACE_ID)
						{
							lineWidth += m_SpaceSize;
						}
						else if (id == NEW_LINE_ID)
						{
							//lineWidth = 0;
						}
						else if (id == SERVICE_ID)
						{
							
						}
						else
						{
							if (id != -1)
							{            
								CharOffsets* character = &m_Chars[id];
								lineWidth += character->xOffset + character->xAdvance + m_CharacterPadding;
								if (character->size.y > height)height = character->size.y;
							}       
						}

						if ((id == SPACE_ID || id == NEW_LINE_ID ||  i + 1 == length || isLocaleWithoutSpaces) && lineWidth >= m_BoundingBox.right)
						{				
							if (isLocaleWithoutSpaces)
							{
								if (i > 0)
								{
									i--;
								}
								id = NEW_LINE_ID;
								lineWidth = 0;
							}
							else
							{
								if (prevRectI == i)
								{
									//id = NEW_LINE_ID;
									prevRectI = i + 1;
									//lineWidth = 0;
								}
								else
								{
									prevRectI = i;
									if (previousSpace != -1)
									{
										i = previousSpace;
									}

									if (i + 1 == length && previousSpace == -1)
									{

									}
									else
									{
										if (id == SPACE_ID)
										{
											previousSpace = i;
										}
										id = NEW_LINE_ID;
										lineWidth = 0;
									}
								}
							}											
						}
					}

					if (id == SPACE_ID)
					{
						previousSpace = i;
					}



					if (id == NEW_LINE_ID)
					{
						if (m_HotSpot == HotSpot::HS_ML)
						{
							DrawLine(posX, posY - m_BaseLine, previousId, i);
						}
						else if (m_HotSpot == HotSpot::HS_UL)
						{
							DrawLine(posX, posY, previousId, i);
						}
						else if (m_HotSpot == HotSpot::HS_UR)
						{
							width = _GetStringWidth(previousId, i);
							DrawLine(posX - width, posY, previousId, i);
						}
						else if (m_HotSpot == HotSpot::HS_MU)
						{
							width = _GetStringWidth(previousId, i);
							DrawLine(posX - width / 2, posY, previousId, i);
						}
						else if (m_HotSpot == HotSpot::HS_MID)
						{
							width = _GetStringWidth(previousId, i);
							DrawLine(posX - width / 2, posY - m_BaseLine, previousId, i);
						}
						else if (m_HotSpot == HotSpot::HS_MR)
						{
							width = _GetStringWidth(previousId, i);
							DrawLine(posX - width, posY - m_BaseLine, previousId, i);
						}
						else if (m_HotSpot == HotSpot::HS_MD)
						{
							width = _GetStringWidth(previousId, i);
							DrawLine(posX - width / 2.0f, posY - m_BaseLine, previousId, i);
						}
						else if (m_HotSpot == HotSpot::HS_DL)
						{
							DrawLine(posX, posY - m_BaseLine, previousId, i);
						}
						else if (m_HotSpot == HotSpot::HS_DR)
						{
							width = _GetStringWidth(previousId, i);
							DrawLine(posX - width, posY - m_BaseLine, previousId, i);
						}
						int w = _GetStringWidth(previousId, i);
						if (w > sizeLabel.x)sizeLabel.x = w;

						lineWidth = 0;
						previousId = i + 1;
						posY += m_LineHeight;
						sizeLabel.y += m_LineHeight;
						m_Offset.y -= (m_LineHeight / 2.0f);
						if (m_HotSpot == HotSpot::HS_UL || m_HotSpot == HotSpot::HS_MU || m_HotSpot == HotSpot::HS_UR)
						{
							m_Offset.y = 0;
						}
						posX = 0.0f;
						continue;
					}
				}
				if (m_HotSpot == HotSpot::HS_ML)
				{
					DrawLine(posX, posY - m_BaseLine, previousId, m_Text.length());
				}
				else if (m_HotSpot == HotSpot::HS_UL)
				{
					DrawLine(posX, posY, previousId, m_Text.length());
				}
				else if (m_HotSpot == HotSpot::HS_MU)
				{
					width = _GetStringWidth(previousId, m_Text.length());
					DrawLine(posX - width / 2, posY, previousId, m_Text.length());
				}
				else if (m_HotSpot == HotSpot::HS_UR)
				{
					width = _GetStringWidth(previousId, m_Text.length());
					DrawLine(posX - width, posY, previousId, m_Text.length());
				}
				else if (m_HotSpot == HotSpot::HS_MID)
				{
					width = _GetStringWidth(previousId, m_Text.length());
					DrawLine(posX - width / 2, posY - m_BaseLine, previousId, m_Text.length());
				}
				else if (m_HotSpot == HotSpot::HS_MD)
				{
					width = _GetStringWidth(previousId, m_Text.length());
					DrawLine(posX - width / 2.0f, posY - m_BaseLine, previousId, m_Text.length());
				}
				else if (m_HotSpot == HotSpot::HS_DL)
				{
					width = _GetStringWidth(previousId, m_Text.length());
					DrawLine(posX, posY - m_BaseLine, previousId, m_Text.length());
				}
				else if (m_HotSpot == HotSpot::HS_MR)
				{
					width = _GetStringWidth(previousId, m_Text.length());
					DrawLine(posX - width, posY - m_BaseLine, previousId, m_Text.length());
				}
				else if (m_HotSpot == HotSpot::HS_DR)
				{
					width = _GetStringWidth(previousId, m_Text.length());
					DrawLine(posX - width, posY - m_BaseLine, previousId, m_Text.length());
				}

				int w = _GetStringWidth(previousId, m_Text.length());
				if (w > sizeLabel.x)sizeLabel.x = w;

				sizeLabel.y += m_BaseLine;				

				if (m_HotSpot == HotSpot::HS_UL)
				{
					m_TextContainer->SetAnchor(HotSpot::HS_UL);
				}
				else if (m_HotSpot == HotSpot::HS_MU)
				{
					m_TextContainer->SetAnchor(HotSpot::HS_MU);
				}
				else if (m_HotSpot == HotSpot::HS_DL)
				{
					m_TextContainer->SetPosition(-sizeLabel.x / 2.0f, -sizeLabel.y / 2.0f);
				}
				else if (m_HotSpot == HotSpot::HS_ML)
				{
					m_TextContainer->SetAnchor(HotSpot::HS_ML);
					m_TextContainer->SetPosition(0.0f, -sizeLabel.y / 2.0f);
				}
				else if (m_HotSpot == HotSpot::HS_MID)
				{
					m_TextContainer->SetAnchor(HotSpot::HS_MID);
					m_TextContainer->SetPosition(0.0f, -sizeLabel.y / 2.0f);
				}
				else if (m_HotSpot == HotSpot::HS_MD)
				{
					m_TextContainer->SetPosition(0.0f, -sizeLabel.y / 2.0f);
				}
				else if (m_HotSpot == HotSpot::HS_UR)
				{
					m_TextContainer->SetAnchor(HotSpot::HS_UR);
				}
				else if (m_HotSpot == HotSpot::HS_MR)
				{
					m_TextContainer->SetAnchor(HotSpot::HS_MR);
					m_TextContainer->SetPosition(0.0f, -sizeLabel.y / 2.0f);
				}
				else if (m_HotSpot == HotSpot::HS_DR)
				{
					m_TextContainer->SetPosition(sizeLabel.x / 2.0f, -sizeLabel.y / 2.0f);
				}
				else
				{
					m_TextContainer->SetPosition(0.0f, m_Offset.y);
				}
			
				if (sizeLabel.y == m_BaseLine)
				{					
					SetSize(sizeLabel.x, height);
				}
				else
				{					
					SetSize(sizeLabel.x, sizeLabel.y + m_BaseLine);
				}
				

				

				if (m_Sprites->size() > m_CurrentSprite)
				{
					for (int i = m_CurrentSprite; i < m_Sprites->size(); i++)
					{
						(*m_Sprites)[i]->SetVisible(false);
					}
				}
			}

			//////////////////////////////////////////////////////////////////////////
			CharOffsets* CTextWidget::GetCharById(const unsigned short &id)
			{
				///Функция быстрого поиска по сортированному массиву
				int n1 = 0;
				int n2 = m_CharsCount - 1;
				int m = 0;
				if (n2 > n1)
				{
					do
					{
						m = (n1 + n2) >> 1;

						if (m_Chars[m].id == id)
						{
							return &m_Chars[m];
						}
						if (m_Chars[m].id > id)
						{
							n2 = m - 1;
						}
						else
						{
							n1 = m + 1;
						}
					} while (n1 <= n2);
				}
				if (n1 >= 0 && n1 < m_CharsCount && m_Chars[n1].id == id)
				{
					return &m_Chars[n1];
				}					
				return NULL;
			}

			//////////////////////////////////////////////////////////////////////////
			float CTextWidget::GetStringWidth()
			{
				return _GetStringWidth(0, m_Text.length())*GetScale().x;
			}

			//////////////////////////////////////////////////////////////////////////
			float CTextWidget::_GetStringWidth()
			{
				return GetStringWidth(0, m_Text.length());
			}

			//////////////////////////////////////////////////////////////////////////
			float CTextWidget::GetStringWidth(const int &sIndex, const int &eIndex)
			{
				return _GetStringWidth(sIndex, eIndex)*GetScale().x;
			}

			//////////////////////////////////////////////////////////////////////////
			float CTextWidget::_GetStringWidth(const int &sIndex, const int &eIndex)
			{
				if (sIndex >= m_CachedSymbols.size() || eIndex > m_CachedSymbols.size())return 0;
				float maxWidth = 0;

				unsigned int id = 0;
				float width = 0;
				int prevId = -1;
				for (int i = sIndex; i < eIndex; ++i)
				{
					id = m_CachedSymbols[i];
					if (id == -1)continue;
					if (id == SPACE_ID)
					{
						width += m_SpaceSize;
						continue;
					}
					else if (id == SERVICE_ID)
					{
						width += m_SpaceSize;
						continue;
					}
					if (id == NEW_LINE_ID)
					{
						if (width > maxWidth)
						{
							maxWidth = width;
						}
						width = 0;
						continue;
					}
					CharOffsets* character = &m_Chars[m_CachedSymbols[i]];
					if (character)
					{
						width += character->xOffset + character->xAdvance + m_CharacterPadding;
						if (prevId != -1)
						{
							width += GetKerningAmount(prevId, character->id);
						}
						prevId = character->id;
					}
				}
				if (width > maxWidth)
				{
					maxWidth = width;
				}

				return maxWidth;
			}

			//////////////////////////////////////////////////////////////////////////
			void CTextWidget::DrawLine(int x, int y, const int &sIndex, const int &eIndex)
			{
				Color clrWhite(255, 255, 255, 255);
				unsigned int id = 0;
				unsigned short prevId = -1;
				bool foundPrevSprite = false;

				for (int i = sIndex; i < eIndex; ++i)
				{
					id = m_CachedSymbols[i];
					if (id == -1)continue;
					if (id == SPACE_ID)
					{
						x += (float)(m_SpaceSize + (int)m_CharacterPadding);
						continue;
					}
					else if (id == SERVICE_ID)
					{
						continue;
					}
					CharOffsets* character = &m_Chars[m_CachedSymbols[i]];
					if (character)
					{
						foundPrevSprite = false;
						CImageWidget * obj = nullptr;
						if (m_Sprites->size() > m_CurrentSprite)
						{
							foundPrevSprite = true;
							obj = CAST(CImageWidget*,(*m_Sprites)[m_CurrentSprite]);							
							obj->SetVisible(true);
						}
						else
						{
							obj = CImageWidget::Create();	
							obj->SetBlockUpdateBoundPoints(true);							
						}				
						m_CurrentSprite++;	

						auto colorIter = m_Colors.find(i);
						if (colorIter != m_Colors.end())
						{
							obj->SetIgnoreCascadeColorChange(true);
							obj->SetColor(colorIter->second);
						}
						else
						{
							obj->SetIgnoreCascadeColorChange(false);							
							obj->SetColor(clrWhite);							
						}

						if (obj->GetTag() != character->id + (int)_ServiceTags::TextSymbols)
						{
							obj->SetTexture(m_Texture);
							obj->SetUV(character->uv);
							obj->SetSize(character->size);
							obj->SetHotSpot(HotSpot::HS_UL);
						}
						obj->SetTag(character->id + (int)_ServiceTags::TextSymbols);
						if (prevId != -1)
						{
							x += GetKerningAmount(prevId, character->id);
						}
						obj->SetPosition((float)x, (float)y + (float)character->yOffset);						
						if (foundPrevSprite == false)
							m_TextContainer->AddChild(obj);
						x += (float)(character->xOffset + character->xAdvance + m_CharacterPadding);
						prevId = character->id;
					}
				}
			}

			//////////////////////////////////////////////////////////////////////////
			int CTextWidget::GetKerningAmount(const unsigned short first, const unsigned short second)
			{				
				auto &el = m_KerningsMap[first];
				for (int i = 0; i < el.size(); i++)
				{
					auto &element = el[i];					
					if (element.id == second)return element.offset;
				}
				return 0.0f;
			}
		}
	}
}
