#include "CursorWidget.h"

#include <Framework.h>
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{

			//////////////////////////////////////////////////////////////////////////
			CCursorWidget::CCursorWidget() :CBaseWidget()
			{				

			}

			//////////////////////////////////////////////////////////////////////////
			CCursorWidget::~CCursorWidget()
			{
				
			}

			//////////////////////////////////////////////////////////////////////////
			void CCursorWidget::Init()
			{
				m_Cursor = ui::CImageWidget::Create();				
				AddChild(m_Cursor);
			}

		
			//////////////////////////////////////////////////////////////////////////
			void CCursorWidget::SetTexture(const std::string &texturePath)
			{
				m_Cursor->SetTexture(texturePath);
				SetSize(m_Cursor->GetSize());				
			}			
		}
	}
}