#ifndef OPTIMUS_UI_BUTTON_WIDGET
#define OPTIMUS_UI_BUTTON_WIDGET

#include <Framework/Utils/MathUtils.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Global/Resources/TextureResource.h>
#include <Framework/Helpers/AtlasHelper.h>

#include <../External/xml/pugixml.hpp>
#include <queue>

#include "ImageWidget.h"
#include "../optimus_macro.h"
#include "../optimus_enum.h"

using namespace NBG;
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
			enum class CButtonWidget_EventType
			{
				Event_Click = 100,
				Event_DisabledClick,				
			};
			typedef std::function < void(CBaseWidget * sender, CButtonWidget_EventType type) > CButtonWidget_EventListener;
			class CButtonWidget : public CBaseWidget
			{
			public:
				OPTIMUS_CREATE(CButtonWidget);
				static CButtonWidget * Create(const std::string &textureFolderPath)
				{
					auto ret = Create();
					ret->SetTexture(textureFolderPath);
					return ret;
				}
				virtual ~CButtonWidget();

				virtual bool IsContains(const Vector &pos);


				virtual void OnLoadFromXMLNode(const pugi::xml_node &node);

				
				virtual bool IsDisabled();
				virtual void OnSetDisabled();


				inline virtual void OnSizeChanged()
				{
					if (m_Normal)
					{
						m_Normal->SetSize(GetSize());
						m_Over->SetSize(GetSize());
						m_Clicked->SetSize(GetSize());
						m_Disabled->SetSize(GetSize());


						m_Size = m_Normal->GetSize();
						UpdateHotSpot();
						UpdateAnchors();
					}
				};

				void SetScale9Enabled(const bool enabled)
				{
					CBaseWidget::SetScale9Enabled(enabled);

					m_Normal->SetScale9Enabled(enabled);
					m_Over->SetScale9Enabled(enabled);
					m_Clicked->SetScale9Enabled(enabled);
					m_Disabled->SetScale9Enabled(enabled);
				}

				virtual void Init();

				void SetButtonListener(CButtonWidget_EventListener callback);

				void SetTexture(const std::string &textureFolderPath);
			protected:
				CButtonWidget();
				ReturnCodes EventListener(CBaseWidget * sender, CBaseWidget_EventType type, CBaseWidget_EventData data);
				GETTER_SETTER_BOOL(UseAlphaMask);
				GETTER_SETTER(std::string, m_SoundName, SoundName);
				GETTER_SETTER_BOOL(TrustedDisabled);

				CImageWidget * m_Normal;
				CImageWidget * m_Over;
				CImageWidget * m_Disabled;
				CImageWidget * m_Clicked;

				CImageWidget * m_CurrentState;

				CButtonWidget_EventListener m_ButtonEventListener;

				void SetState(CImageWidget * state);				
			};
		}
	}
}
#endif //OPTIMUS_UI_BUTTON_WIDGET