#ifndef OPTIMUS_UI_WIDGETS_FACTORY
#define OPTIMUS_UI_WIDGETS_FACTORY

#include <functional>
#include "../optimus_macro.h"
#include "../optimus_enum.h"
#include <map>
#include <string>

namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
			class CBaseWidget;

			class CWidgetsFactory
			{
			public:
				/// @name Конструктор деструктор
				static CWidgetsFactory * GetInstance()
				{
					if (self == nullptr)
					{
						self = new CWidgetsFactory();
					}
					return self;
				}
				~CWidgetsFactory(){};

				template <class T>
				void Add(const std::string &name)
				{
					m_WidgetsMap[name] = std::bind([&](){return T::Create(); });
				}

				CBaseWidget * Get(std::string name)
				{
					return m_WidgetsMap[name]();
				}
			private:
				CWidgetsFactory();
				static CWidgetsFactory * self;
				std::map < std::string, std::function<CBaseWidget*()> > m_WidgetsMap;
			};
		}
	}
}
#endif //OPTIMUS_UI_BASE_WIDGET