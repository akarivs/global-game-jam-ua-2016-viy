#ifndef OPTIMUS_UI_WINDOW_WIDGET
#define OPTIMUS_UI_WINDOW_WIDGET

#include <Framework/Datatypes/FRect.h>
#include "BaseWidget.h"

namespace NBG
{
	namespace optimus
	{
		namespace ui
		{

			/** @brief Класс, реализующий вывод окна.
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CWindowWidget : public CBaseWidget
			{
			public:
				/// @name Конструктор/деструктор
				OPTIMUS_CREATE(CWindowWidget);				
				virtual ~CWindowWidget();

				/// Виртуальные наследуемые функции
				virtual void OnLoadFromXMLNode(const pugi::xml_node &node);
				virtual void Init();				

				void SetModal(const bool isModal);
				bool IsModal(){ return m_IsModal; }
				bool IsShowing(){ return m_IsShowing; }

				
				virtual ReturnCodes OnMouseMove(const Vector &mousePos){ auto res = CBaseWidget::OnMouseMove(mousePos); if (m_IsModal){ return ReturnCodes::Block; } return res; };
				virtual ReturnCodes OnMouseUp(const Vector &mousePos, const int mouseButton) { auto res = CBaseWidget::OnMouseUp(mousePos, mouseButton); if (m_IsModal){ return ReturnCodes::Block; } return res; };
				virtual ReturnCodes OnMouseDown(const Vector &mousePos, const int mouseButton) { auto res = CBaseWidget::OnMouseDown(mousePos, mouseButton); if (m_IsModal){ return ReturnCodes::Block; } return res; };
				virtual ReturnCodes OnKeyDown(const int key) { auto res = CBaseWidget::OnKeyDown(key); if (m_IsModal){ return ReturnCodes::Block; } return res; };
				virtual ReturnCodes OnKeyUp(const int key);
				
				
				virtual ReturnCodes Update(const float dt);

				virtual void OnShowStart(){};
				virtual void OnShowEnd(){};

				virtual void OnHideStart(){};
				virtual void OnHideEnd(){};

				virtual void ShowFunction();
				virtual void HideFunction();
				
				void Show();
				void Hide();	

				virtual ReturnCodes Draw(CRenderState &rs);
			protected:
				CWindowWidget();
				bool m_IsModal;		
				bool m_IsShowing;
				GETTER_SETTER_BOOL(Darker);
				GETTER_SETTER_BOOL(CloseOnEsc);
				GETTER_SETTER(std::string, m_OpenSound, OpenSound);
				GETTER_SETTER(std::string, m_CloseSound, CloseSound);

			};
		}
	}
}
#endif
