#include "BaseWidget.h"

#include "WidgetsFactory.h"
#include <Framework.h>
#include <algorithm>
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
			Matrix CBaseWidget::tempMatrix;
			//////////////////////////////////////////////////////////////////////////
			CBaseWidget::CBaseWidget()
			{
				m_ChildsLock = false;
				m_Parent = nullptr;

				m_IsCatchTouch = false;
				m_IsCatchKeyboard = true;
				m_IsUpdatable = true;
				m_IsDrawable = true;
				m_IsVisible = true;
				m_IsDisabled = false;
				m_IsDestroyed = false;
				m_HotSpot = HotSpot::HS_MID;
				m_SortMode = SortModes::ByLayer;
				m_Tag = -1;
				m_IsNeedToDestroyChilds = false;
				m_Layer = 0;
				m_IsUseHotSpotInCalculations = false;
				m_RealLayer = 0;
				m_TopLayer = 0;
				m_Anchor = HotSpot::HS_MID;
				m_IsFocused = false;
				m_IsRoot = false;
				m_IsWasFocusedOnMouseDown = false;
				m_IsDebugDraw = false;
				m_IsBlockUpdateAnchors = false;
				m_IsBlockUpdateBoundPoints = false;
				m_IsScale9Enabled = false;
				m_IsIgnoreCascadeColorChange = false;
				m_IsImmortal = false;
				m_Angle = 0.0f;
				m_IsCascadeOpacityChange = true;
				m_IsCascadeColorChange = false;

				__prevTransform.x = -9999.0f;

				m_BoundPoints.resize(4);

				m_BlendMode = BlendMode::BLEND;
				m_BaseEventListener = nullptr;

				m_OnDestroyCallback = nullptr;

				m_ParentTransform = new Transform();

				m_IsBlockDelayedProcess = false;
			}

			//////////////////////////////////////////////////////////////////////////
			CBaseWidget::~CBaseWidget()
			{				
				auto size = m_Childs.size();
				if (size > 0)
				{
					CBaseWidget ** vec = &m_Childs[0];
					for (int i = 0; i < size; i++)
					{
						auto obj = vec[i];
						if (obj && obj->IsDestroyed() == false)
						{
							obj->Destroy();
						}
					}
				}
				if (m_OnDestroyCallback)
				{
					m_OnDestroyCallback();
				}
				delete m_ParentTransform;
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetEventListener(CBaseWidget_EventListener callback)
			{
				m_BaseEventListener = callback;
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::LoadFromXML(const std::string path)
			{
				CXMLResource * xml = CAST(CXMLResource*, g_ResManager->GetResource(path));
				pugi::xml_document * doc = xml->GetXML();
				LoadFromXMLNode(doc->first_child());
				g_ResManager->ReleaseResource(xml);
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::LoadFromXMLNode(pugi::xml_node node, CBaseWidget * parent)
			{
				if (node.empty())return;
				CBaseWidget * obj = this;
				if (parent)
				{
					while (!node.empty())
					{
						if (std::string(node.name()) == "object")
						{
							obj = CWidgetsFactory::GetInstance()->Get(node.attribute("type").value());
							obj->LoadFromXMLNode(node);
							parent->AddChild(obj);
						}
						node = node.next_sibling();
					}
				}
				else
				{
					m_IsBlockUpdateAnchors = true;
					m_IsBlockUpdateBoundPoints = true;


					if (!node.attribute("template").empty())
					{
						LoadFromXML(node.attribute("template").value());
					}

					auto nodeName = node.attribute("name");
					auto nodeTag = node.attribute("tag");
					auto nodeScale = node.attribute("scale");
					auto nodeSX = node.attribute("sx");
					auto nodeSY = node.attribute("sy");
					auto nodeSZ = node.attribute("sz");
					auto nodeColor = node.attribute("color");


					if (!nodeName.empty())
					{
						SetName(nodeName.value());
					}
					if (!nodeTag.empty())
					{
						SetTag(nodeTag.as_int());
					}

					float x = node.attribute("x").as_float();
					float y = node.attribute("y").as_float();
					float z = node.attribute("z").as_float();
					float width = node.attribute("width").as_float();
					float height = node.attribute("height").as_float();
					int layer = node.attribute("layer").as_int();
					float angle = node.attribute("angle").as_float();

					float sx = 1.0f;
					float sy = 1.0f;
					float sz = 1.0f;

					if (!nodeScale.empty())
					{
						sx = sy = sz = nodeScale.as_float();
					}
					else
					{
						if (!nodeSX.empty())
							sx = nodeSX.as_float();
						if (!nodeSY.empty())
							sy = nodeSY.as_float();
						if (!nodeSZ.empty())
							sz = nodeSZ.as_float();
					}

					Color clr;
					if (!nodeColor.empty())
					{
						clr.SetColor(nodeColor.value());
						SetColor(clr);
					}

					if (!node.attribute("blend").empty())
					{
						std::string mode = node.attribute("blend").value();
						if (mode == "add")m_BlendMode = BlendMode::ADD;
						else if (mode == "mod")m_BlendMode = BlendMode::MOD;
						else if (mode == "screen")m_BlendMode = BlendMode::SCREEN;
						SetBlendMode(m_BlendMode);
					}

					if (!node.attribute("hide").empty())
					{
						SetVisible(!node.attribute("hide").as_bool());
					}


					SetPosition(x, y, z);
					SetSize(width, height);
					SetScale(sx, sy, sz);
					SetRotation(angle);
					SetLayer(layer);

					if (!node.attribute("scale9").empty())
					{
						SetScale9Enabled(node.attribute("scale9").as_bool());
					}

					SetHotSpot((HotSpot)StringUtils::StringToHotSpot(node.attribute("hot_spot").value()));
					SetAnchor((HotSpot)StringUtils::StringToHotSpot(node.attribute("anchor").value()));



					m_IsBlockUpdateAnchors = false;
					m_IsBlockUpdateBoundPoints = false;
					UpdateAnchors();


					OnLoadFromXMLNode(node);

					if (!node.attribute("disable").empty())
					{
						SetDisabled(node.attribute("disable").as_bool());
					}

					if (!node.attribute("touch").empty())
					{
						SetCatchTouch(node.attribute("touch").as_bool());
					}

					if (!node.attribute("updatable").empty())
					{
						SetUpdatable(node.attribute("updatable").as_bool());
					}

					if (!node.attribute("cascade_opacity").empty())
					{
						SetCascadeOpacityChange(node.attribute("cascade_opacity").as_bool());
					}

					m_IsDebugDraw = node.attribute("debug").as_bool();

					LoadFromXMLNode(node.first_child(), this);
					AfterLoadFromXMLNode();
					if (m_SortMode == SortModes::ByLayer)
					{
						std::stable_sort(m_Childs.begin(), m_Childs.end(), CBaseWidget::SortByLayer);
						int i = 0;
						auto size = m_Childs.size();
						if (size > 0)
						{
							auto ** vec = &m_Childs[0];
							for (int i = 0; i < size; i++)
							{
								auto obj = vec[i];
								obj->SetRealLayer(i);
							}
						}
					}
				}
				UpdateAnchors();
			}

			//==============================================================================
			void CBaseWidget::BringForward()
			{
				if (!m_Parent)
				{
					AddDelayedSort(Delayed_BringForwardSelf, NULL);
					return;
				}
				m_Parent->BringForward(this);
			}

			//==============================================================================
			void CBaseWidget::BringBackward()
			{
				if (!m_Parent)
				{
					AddDelayedSort(Delayed_BringBackwardSelf, NULL);
					return;
				}
				m_Parent->BringBackward(this);
			}

			//==============================================================================
			void CBaseWidget::BringBefore(CBaseWidget * place)
			{
				if (!m_Parent)
				{
					AddDelayedSort(Delayed_BringBeforeSelf, place);
					return;
				}
				m_Parent->BringBefore(this, place);
			}

			//==============================================================================
			void CBaseWidget::BringAfter(CBaseWidget * place)
			{
				if (!m_Parent)
				{
					AddDelayedSort(Delayed_BringAfterSelf, place);
					return;
				}
				m_Parent->BringAfter(this, place);
			}

			//==============================================================================
			void CBaseWidget::BringForward(CBaseWidget * child)
			{
				if (m_ChildsLock)
				{
					AddDelayedSort(Delayed_BringForward, child);
					return;
				}

				if (m_SortMode == SortModes::Manual)
				{
					for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); ++m_ChildsIter)
					{
						if ((*m_ChildsIter) == child)
						{
							m_Childs.erase(m_ChildsIter);
							break;
						}
					}
					m_Childs.push_back(child);
				}
				else if (m_SortMode == SortModes::ByLayer)
				{
					child->SetLayer(m_TopLayer + 1);
				}


			}

			//==============================================================================
			void CBaseWidget::BringBackward(CBaseWidget * child)
			{
				if (m_ChildsLock)
				{
					AddDelayedSort(Delayed_BringBackward, child);
					return;
				}
				if (m_SortMode == SortModes::Manual)
				{
					for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); ++m_ChildsIter)
					{
						if ((*m_ChildsIter) == child)
						{
							m_Childs.erase(m_ChildsIter);
							break;
						}
					}
					m_Childs.insert(m_Childs.begin(), child);
				}
				else if (m_SortMode == SortModes::ByLayer)
				{
					child->SetLayer(0);
				}
			}

			//==============================================================================
			void CBaseWidget::BringBefore(CBaseWidget * child, CBaseWidget * place)
			{
				if (m_ChildsLock)
				{
					AddDelayedSort(Delayed_BringBefore, child, place);
					return;
				}
				if (m_SortMode == SortModes::Manual)
				{
					for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); m_ChildsIter++)
					{
						if ((*m_ChildsIter) == child)
						{
							m_Childs.erase(m_ChildsIter);
							break;
						}
					}
					for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); m_ChildsIter++)
					{
						if ((*m_ChildsIter) == place)
						{
							m_ChildsIter++;
							m_Childs.insert(m_ChildsIter, child);
							break;
						}
					}
				}
				else if (m_SortMode == SortModes::ByLayer)
				{
					child->SetLayer(place->GetLayer() - 1);
				}


			}

			//==============================================================================
			void CBaseWidget::BringAfter(CBaseWidget * child, CBaseWidget * place)
			{
				if (m_ChildsLock)
				{
					AddDelayedSort(Delayed_BringAfter, child, place);
					return;
				}
				if (m_SortMode == SortModes::Manual)
				{
					for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); m_ChildsIter++)
					{
						if ((*m_ChildsIter) == child)
						{
							m_Childs.erase(m_ChildsIter);
							break;
						}
					}
					for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); m_ChildsIter++)
					{
						if ((*m_ChildsIter) == place)
						{
							m_Childs.insert(m_ChildsIter, child);
							break;
						}
					}
				}
				else if (m_SortMode == SortModes::ByLayer)
				{
					child->SetLayer(place->GetLayer() + 1);
				}
			}

			//==============================================================================
			int CBaseWidget::GetTopLayer()
			{
				return m_TopLayer;
			}

			//==============================================================================
			void CBaseWidget::SetLayer(const int layer)
			{
				m_Layer = layer;

				if (m_Parent && m_Parent->GetSortMode() == SortModes::ByLayer)
				{
					m_Parent->OnChildLayerChange(this);
				}
			}

			//==============================================================================
			bool CBaseWidget::SortByLayer(CBaseWidget *a, CBaseWidget *b)
			{
				return (a->GetLayer() < b->GetLayer());
			}

			//==============================================================================
			void CBaseWidget::OnChildLayerChange(CBaseWidget * child)
			{
				if (child->GetLayer() > m_TopLayer)
				{
					m_TopLayer = child->GetLayer();
				}
				std::stable_sort(m_Childs.begin(), m_Childs.end(), CBaseWidget::SortByLayer);



				auto size = m_Childs.size();
				if (size > 0)
				{
					auto ** vec = &m_Childs[0];
					for (int i = 0; i < size; i++)
					{
						auto obj = vec[i];
						obj->SetRealLayer(i);

					}
				}
			}




			//////////////////////////////////////////////////////////////////////////
			Vector CBaseWidget::GetContentSize()
			{
				int xMax = -99999;
				int xMin = 99999;
				int yMax = -99999;
				int yMin = 99999;

				UpdateBoundPoints();
				for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); m_ChildsIter++)
				{
					(*m_ChildsIter)->UpdateBoundPoints();
					auto &bounds = (*m_ChildsIter)->GetBounds();
					for (auto v : bounds)
					{
						if (v.x > xMax)xMax = v.x;
						if (v.x < xMin)xMin = v.x;

						if (v.y > yMax)yMax = v.y;
						if (v.y < yMin)yMin = v.y;
					}
				}
				for (auto v : m_BoundPoints)
				{
					if (v.x > xMax)xMax = v.x;
					if (v.x < xMin)xMin = v.x;

					if (v.y > yMax)yMax = v.y;
					if (v.y < yMin)yMin = v.y;
				}
				return Vector(abs(xMax - xMin), abs(yMax - yMin));
			}


			///*POSITION*///
			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetPosition(const Vector &position)
			{
				m_Transform.x = position.x;
				m_Transform.y = position.y;
				m_Transform.z = position.z;

				m_SavedTransform = m_Transform;
				if (!m_Parent){ OnPositionChanged(); }
				UpdateAnchors();
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetPositionX(const float x)
			{
				m_Transform.x = x;
				m_SavedTransform.x = m_Transform.x;
				if (!m_Parent){ OnPositionChanged(); }
				UpdateAnchors();
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetPositionY(const float y)
			{
				m_Transform.y = y;
				m_SavedTransform.y = m_Transform.y;
				if (!m_Parent){ OnPositionChanged(); }
				UpdateAnchors();
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetPositionZ(const float z)
			{
				m_Transform.z = z;
				m_SavedTransform.z = m_Transform.z;
				if (!m_Parent){ OnPositionChanged(); }
				UpdateAnchors();
			}

			//////////////////////////////////////////////////////////////////////////
			Vector CBaseWidget::GetPosition()
			{
				return Vector(m_SavedTransform.x, m_SavedTransform.y, m_SavedTransform.z);
			}

			//////////////////////////////////////////////////////////////////////////
			float CBaseWidget::GetPositionX()
			{
				return m_SavedTransform.x;
			}

			//////////////////////////////////////////////////////////////////////////
			float CBaseWidget::GetPositionY()
			{
				return m_SavedTransform.y;
			}

			//////////////////////////////////////////////////////////////////////////
			float CBaseWidget::GetPositionZ()
			{
				return m_SavedTransform.z;
			}

			///*SCALE*///
			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetScale(const Vector &scale)
			{
				m_Transform.scalex = scale.x;
				m_Transform.scaley = scale.y;
				m_Transform.scalez = scale.z;

				OnScaleChanged();
				SendOnChangeParentTransform(this);
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetScaleX(const float x)
			{
				m_Transform.scalex = x;
				OnScaleChanged();
				SendOnChangeParentTransform(this);
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetScaleY(const float y)
			{
				m_Transform.scaley = y;
				OnScaleChanged();
				SendOnChangeParentTransform(this);
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetScaleZ(const float z)
			{
				m_Transform.scalez = z;
				OnScaleChanged();
				SendOnChangeParentTransform(this);
			}

			//////////////////////////////////////////////////////////////////////////
			Vector CBaseWidget::GetScale()
			{
				return Vector(m_Transform.scalex, m_Transform.scaley, m_Transform.scalez);
			}

			//////////////////////////////////////////////////////////////////////////
			float CBaseWidget::GetScaleX()
			{
				return m_Transform.scalex;
			}

			//////////////////////////////////////////////////////////////////////////
			float CBaseWidget::GetScaleY()
			{
				return m_Transform.scaley;
			}

			//////////////////////////////////////////////////////////////////////////
			float CBaseWidget::GetScaleZ()
			{
				return m_Transform.scalez;
			}

			///*HOT SPOT*///
			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetHotSpot(const HotSpot type, const Vector offset /* = Vector(0 , 0, 0)*/)
			{
				m_HotSpot = type;
				m_HotSpotOffset = offset;
				UpdateHotSpot();
				OnHotSpotChanged();
			}

			//////////////////////////////////////////////////////////////////////////
			Vector CBaseWidget::GetHotSpotOffset(const HotSpot &hotSpot)
			{
				Vector ret;
				switch (hotSpot)
				{
				case HotSpot::HS_UL:
					ret.x = 0.0f;
					ret.y = 0.0f;
					ret.z = 0.0f;
					break;
				case HotSpot::HS_MU:
					ret.x = GetSize().x / 2.0f;
					ret.y = 0.0f;
					ret.z = 0.0f;
					break;
				case HotSpot::HS_UR:
					ret.x = GetSize().x;
					ret.y = 0.0f;
					ret.z = 0.0f;
					break;
				case HotSpot::HS_ML:
					ret.x = 0.0f;
					ret.y = GetSize().y / 2.0f;
					ret.z = 0.0f;
					break;
				case HotSpot::HS_MID:
					ret.x = GetSize().x / 2.0f;
					ret.y = GetSize().y / 2.0f;
					ret.z = 0.0f;
					break;
				case HotSpot::HS_MR:
					ret.x = GetSize().x;
					ret.y = GetSize().y / 2.0f;
					ret.z = 0.0f;
					break;
				case HotSpot::HS_DL:
					ret.x = 0.0f;
					ret.y = GetSize().y;
					ret.z = 0.0f;
					break;
				case HotSpot::HS_MD:
					ret.x = GetSize().x / 2.0f;
					ret.y = GetSize().y;
					ret.z = 0.0f;
					break;
				case HotSpot::HS_DR:
					ret.x = GetSize().x;
					ret.y = GetSize().y;
					ret.z = 0.0f;
					break;
				case HotSpot::HS_CUSTOM:
					return m_HotSpotOffset;
					break;
				}
				return ret;
			}



			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::UpdateHotSpot()
			{
				Vector offset = GetHotSpotOffset(m_HotSpot);
				m_Transform.hx = offset.x;
				m_Transform.hy = offset.y;
				m_Transform.hz = offset.z;
				SendOnChangeParentTransform(this);
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SendOnChangeParentTransform(CBaseWidget * object)
			{
				/*
				if (m_IsBlockUpdateAnchors)return;
				if (object->GetChildsCount() > 0)
				{
					const auto &childs = object->GetChilds();
					for (int i = 0; i < childs.size(); i++)
					{
						CBaseWidget * obj = childs[i];
						obj->OnParentTransformChanged();
						obj->SendOnChangeParentTransform(obj);
					}
				}


				if (object->GetChildsQueueCount() > 0)
				{
					auto &childsQueue = object->GetChildsQueue();
					for (int i = 0; i < childsQueue.size(); i++)
					{
						CBaseWidget * obj = childsQueue[i];
						obj->OnParentTransformChanged();
						obj->SendOnChangeParentTransform(obj);
					}
				*/
			}

			///*ANCHORS*///
			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetAnchor(const HotSpot anchor)
			{
				m_Anchor = anchor;
				UpdateAnchors();
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::UpdateAnchors()
			{
				if (m_IsBlockUpdateAnchors)return;
				SendOnChangeParentTransform(this);
				if (!m_Parent)
				{
					for (size_t i = 0; i < m_Childs.size(); i++)
					{
						CBaseWidget * obj = m_Childs[i];
						obj->UpdateAnchors();
					}
					return;
				}
				Vector parentSize = m_Parent->GetSizeForAnchor();
				if (parentSize.x == 0.0f && parentSize.y == 0.0f)
				{
					if (g_GameApplication)
					{
						parentSize = g_GameApplication->GetWindowSize();
					}						
				}
				Vector parentOffset = m_Parent->GetHotSpotOffset(m_Parent->GetHotSpot());
				switch (m_Anchor)
				{
				case HotSpot::HS_UL:
					m_Transform.x = m_SavedTransform.x - parentSize.x / 2.0f;
					m_Transform.y = m_SavedTransform.y - parentSize.y / 2.0f;
					break;
				case HotSpot::HS_MU:
					m_Transform.x = m_SavedTransform.x;
					m_Transform.y = m_SavedTransform.y - parentSize.y / 2.0f;
					break;
				case HotSpot::HS_UR:
					m_Transform.x = m_SavedTransform.x + parentSize.x / 2.0f;
					m_Transform.y = m_SavedTransform.y - parentSize.y / 2.0f;
					break;
				case HotSpot::HS_ML:
					m_Transform.x = m_SavedTransform.x - parentSize.x / 2.0f;
					m_Transform.y = m_SavedTransform.y;
					break;
				case HotSpot::HS_MR:
					m_Transform.x = m_SavedTransform.x + parentSize.x / 2.0f;
					m_Transform.y = m_SavedTransform.y;
					break;
				case HotSpot::HS_DL:
					m_Transform.x = m_SavedTransform.x - parentSize.x / 2.0f;
					m_Transform.y = m_SavedTransform.y + parentSize.y / 2.0f;
					break;
				case HotSpot::HS_MD:
					m_Transform.x = m_SavedTransform.x;
					m_Transform.y = m_SavedTransform.y + parentSize.y / 2.0f;
					break;
				case HotSpot::HS_DR:
					m_Transform.x = m_SavedTransform.x + parentSize.x / 2.0f;
					m_Transform.y = m_SavedTransform.y + parentSize.y / 2.0f;
					break;
				case HotSpot::HS_MID:
					m_Transform.x = m_SavedTransform.x;
					m_Transform.y = m_SavedTransform.y;
					break;
				}

				switch (m_Parent->GetHotSpot())
				{
				case HotSpot::HS_UL:
					m_Transform.x += parentSize.x / 2.0f;
					m_Transform.y += parentSize.y / 2.0f;
					break;
				case HotSpot::HS_MU:
					m_Transform.y += parentSize.y / 2.0f;
					break;
				case HotSpot::HS_UR:
					m_Transform.x -= parentSize.x / 2.0f;
					m_Transform.y += parentSize.y / 2.0f;
					break;
				case HotSpot::HS_ML:
					m_Transform.x += parentSize.x / 2.0f;
					break;
				case HotSpot::HS_MR:
					m_Transform.x -= parentSize.x / 2.0f;
					break;
				case HotSpot::HS_DL:
					m_Transform.x += parentSize.x / 2.0f;
					m_Transform.y -= parentSize.y / 2.0f;
					break;
				case HotSpot::HS_MD:
					m_Transform.y -= parentSize.y / 2.0f;
					break;
				case HotSpot::HS_DR:
					m_Transform.x -= parentSize.x / 2.0f;
					m_Transform.y -= parentSize.y / 2.0f;
					break;
				case HotSpot::HS_CUSTOM:
					m_Transform.x += parentSize.x / 2.0f - m_Parent->GetHotSpotOffset(HotSpot::HS_CUSTOM).x;
					m_Transform.y += parentSize.y / 2.0f - m_Parent->GetHotSpotOffset(HotSpot::HS_CUSTOM).y;
					break;

				}

				OnPositionChanged();
				for (size_t i = 0; i < m_Childs.size(); i++)
				{
					CBaseWidget * obj = m_Childs[i];
					obj->UpdateAnchors();
				}				
			}

			//////////////////////////////////////////////////////////////////////////
			Vector CBaseWidget::ConvertToLocalSpace(const Vector &pos)
			{
				auto trans = m_Transform.GetAbsoluteTransform();
				m_TempMatrix3x3.CreateFromTransform(trans,true);
				m_TempMatrix3x3.Invert();

				return m_TempMatrix3x3.TransformPoint(Vector(pos.x, pos.y));
			}

			///*COLOR*///
			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetColor(Color clr, const bool changeBaseColor)
			{
				if (changeBaseColor)
				{
					m_BaseColor = clr;
					if (m_Parent)
					{
						auto pColor = m_Parent->GetColor();
						if (m_Parent->IsCascadeColorChange() && m_IsIgnoreCascadeColorChange == false)
						{
							clr.r *= pColor.r / 255.0f;
							clr.g *= pColor.g / 255.0f;
							clr.b *= pColor.b / 255.0f;
						}
						if (m_Parent->IsCascadeOpacityChange())
						{
							clr.a *= pColor.a / 255.0f;
						}
					}
				}
				m_Color = clr;

				OnColorChanged();
				for (int i = 0; i < m_Childs.size(); i++)
				{
					CBaseWidget * obj = m_Childs[i];
					Color baseColor = obj->GetBaseColor();
					if (m_IsCascadeOpacityChange)
					{
						baseColor.a *= m_Color.a / 255.0f;
					}
					if (m_IsCascadeColorChange && obj->IsIgnoreCascadeColorChange() == false)
					{
						baseColor.r *= m_Color.r / 255.0f;
						baseColor.g *= m_Color.g / 255.0f;
						baseColor.b *= m_Color.b / 255.0f;
					}
					obj->SetColor(baseColor, false);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetOpacity(float alpha)
			{
				m_Color.a = alpha;
				m_BaseColor.a = alpha;
				SetColor(m_Color, false);
			}

			//////////////////////////////////////////////////////////////////////////
			bool CBaseWidget::IsContains(const Vector &pos)
			{
				UpdateBoundPoints();
				return MathUtils::IsPointInsidePoly(pos, m_BoundPoints);
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::UpdateBoundPoints()
			{	
				m_TempMatrix.CreateFromTransform(m_Transform.GetAbsoluteTransform(), true);

				pUL.x = 0.0f; pUL.y = 0.0f;
				pUR.x = GetSize().x; pUR.y = 0.0f;
				pDL.x = 0.0f; pDL.y = GetSize().y;
				pDR.x = GetSize().x; pDR.y = GetSize().y;
				

				pUL = m_TempMatrix.TransformPoint(pUL);
				pUR = m_TempMatrix.TransformPoint(pUR);
				pDL = m_TempMatrix.TransformPoint(pDL);
				pDR = m_TempMatrix.TransformPoint(pDR);

				m_BoundPoints[0] = pUL;
				m_BoundPoints[1] = pUR;
				m_BoundPoints[2] = pDR;
				m_BoundPoints[3] = pDL;
			}



			///*CHILD SYSTEM*///
			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::AddChild(CBaseWidget * child)
			{
				if (!child)
				{
					CONSOLE_ERROR("Child is NULL!");

					return;
				}
				if (child == this)
				{
					CONSOLE_ERROR("You try to add object as his child! %s", child->GetName().c_str());

					return;
				}

				child->SetParent(this);

				///���� ������ ������� �� ������, ����� ��������� ������ � ������
				if (m_ChildsLock)
				{
					m_ChildsQueue.push_back(child);

					return;
				}


				if (child->GetParent() && child->GetParent() != this)
				{
					CONSOLE_ERROR("Object - %s was already added to parent %s!'", child->GetName().c_str(), child->GetParent()->GetName().c_str());
					return;
				}
				m_Childs.push_back(child);
				OnChildAdded(child);
				if (m_SortMode == SortModes::ByLayer)
				{
					int layer = child->GetLayer();
					if (layer > m_TopLayer)
					{
						m_TopLayer = layer;
					}
					std::stable_sort(m_Childs.begin(), m_Childs.end(), CBaseWidget::SortByLayer);
					int i = 0;
					for (int i = 0; i < m_Childs.size(); i++)
					{
						auto obj = m_Childs[i];
						obj->SetRealLayer(i);
					}
				}
				child->OnAddedToParent();
				child->UpdateAnchors();
				child->SendOnChangeParentTransform(child);
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::RemoveChild(CBaseWidget * child)
			{
				for (auto m_ChildsIter = m_Childs.begin(); m_ChildsIter != m_Childs.end(); m_ChildsIter++)
				{
					if ((*m_ChildsIter) == child)
					{
						(*m_ChildsIter)->SetParent(NULL);
						m_Childs.erase(m_ChildsIter);

						return;
					}
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::RemoveAllChilds(const bool cleanup)
			{
				for (int i = 0; i < m_Childs.size(); i++)
				{
					auto child = m_Childs[i];
					child->SetParent(NULL);
					if (cleanup)
					{
						child->Destroy();
					}
				}
				if (cleanup)
				{
					m_Childs.clear();
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::SetParent(CBaseWidget * parent)
			{
				m_Parent = parent;
				if (parent)
				{
					m_Transform.SetParent(parent->GetTransform());
				}
				else
				{
					m_Transform.SetParent(nullptr);
				}
				SetColor(GetColor());
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::MoveToParent(CBaseWidget * newParent, bool saveObjectPos)
			{
				if (GetParent() == nullptr)
				{
					newParent->AddChild(this);
					return;
				}
				if (GetParent() == newParent)
				{
					return;
				}
				auto oldTrans = m_Transform.GetAbsoluteTransform();
				m_Parent->RemoveChild(this);
				newParent->AddChild(this);
				m_Transform.SetParent(newParent->GetTransform());
				if (saveObjectPos)
				{
					auto newTrans = m_Transform.GetAbsoluteTransform();

					m_Transform.x += oldTrans.x - newTrans.x;
					m_Transform.y += oldTrans.y - newTrans.y;

					m_Transform.rz += oldTrans.rz - newTrans.rz;

					m_Transform.scalex += oldTrans.scalex - newTrans.scalex;
					m_Transform.scaley += oldTrans.scaley - newTrans.scaley;


					
					OnPositionChanged();
					OnRotationChanged();
					OnScaleChanged();					

					m_SavedTransform = m_Transform;
					UpdateAnchors();
				}
			}

			//////////////////////////////////////////////////////////////////////////
			CBaseWidget * CBaseWidget::GetChildByTag(const int tag, const bool recursive /* = false */)
			{
				for (size_t i = 0; i < m_Childs.size(); ++i)
				{
					auto child = m_Childs[i];
					if (child->GetTag() == tag)
					{
						return child;
					}
					else
					{
						if (recursive)
						{
							child = child->GetChildByTag(tag, true);
							if (child)return child;
						}
					}
				}
				for (size_t i = 0; i < m_ChildsQueue.size(); ++i)
				{
					auto child = m_ChildsQueue[i];
					if (child->GetTag() == tag)
					{
						return child;
					}
					else
					{
						if (recursive)
						{
							child = child->GetChildByTag(tag, true);
							if (child)return child;
						}
					}
				}
				return nullptr;
			}

				//////////////////////////////////////////////////////////////////////////
			CBaseWidget * CBaseWidget::GetChildByName(const std::string &name, const bool recursive /* = false */)
			{
				return GetChildByName<CBaseWidget>(name, recursive);
			}

			//////////////////////////////////////////////////////////////////////////			
			void CBaseWidget::ProceedDelayedSort()
			{
				for (int i = 0; i < m_DelayedSortItems.size(); i++)
				{
					auto &item = m_DelayedSortItems[i];
					switch (item.type)
					{
					case Delayed_BringForward:
						BringForward(item.obj1);
						break;
					case Delayed_BringBackward:
						BringBackward(item.obj1);
						break;
					case Delayed_BringBefore:
						BringBefore(item.obj1, item.obj2);
						break;
					case Delayed_BringAfter:
						BringAfter(item.obj1, item.obj2);
						break;
					case Delayed_BringForwardSelf:
						if (m_Parent)BringForward();
						break;
					case Delayed_BringBackwardSelf:
						if (m_Parent)BringBackward();
						break;
					case Delayed_BringBeforeSelf:
						if (m_Parent)BringBefore(item.obj1);
						break;
					case Delayed_BringAfterSelf:
						if (m_Parent)BringAfter(item.obj1);
						break;
					}
				}
				m_DelayedSortItems.clear();
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CBaseWidget::OnMouseMove(const Vector &mousePos)
			{
				if (m_IsVisible == false)return ReturnCodes::Skip;
				if (!m_IsCatchTouch)return ReturnCodes::SkipDontProcess;
				auto code = ReturnCodes::Skip;

				LOCK_CHILDS_VECTOR
					auto size = m_Childs.size();
				if (size > 0)
				{
					CBaseWidget ** vec = &m_Childs[0];
					for (int i = size - 1; i >= 0; --i)
					{
						CBaseWidget * obj = vec[i];
						if (obj->IsCatchTouch() == false)continue;
						if (obj->IsDisabled() == true)continue;
						code = obj->OnMouseMove(mousePos);
						if (!(code == ReturnCodes::Skip || code == ReturnCodes::SkipDontProcess))
						{
							break;
						}
					}
				}

				UNLOCK_CHILDS_VECTOR

					if ((code == ReturnCodes::Skip || code == ReturnCodes::SkipDontProcess) && m_BaseEventListener != nullptr)
					{
						CBaseWidget_EventData data;
						data.mousePos = mousePos;
						if (IsContains(mousePos))
						{
							code = m_BaseEventListener(this, CBaseWidget_EventType::Event_MouseMove, data);
							if (!m_IsFocused)
							{
								m_IsFocused = true;
								if (code == ReturnCodes::Skip)
								{
									code = m_BaseEventListener(this, CBaseWidget_EventType::Event_Over, data);

									CEvent event(Event::ET_Over);
									event.target = this;
									event.mousePos = mousePos;
									DispatchEvent(event);
								}
							}
						}
						else
						{
							code = m_BaseEventListener(this, CBaseWidget_EventType::Event_MouseMoveOutside, data);
							if (m_IsFocused)
							{
								m_IsFocused = false;
								if (code == ReturnCodes::Skip)
								{
									code = m_BaseEventListener(this, CBaseWidget_EventType::Event_Out, data);

									CEvent event(Event::ET_Out);
									event.target = this;
									event.mousePos = mousePos;
									DispatchEvent(event);
								}
							}
						}
					}
				return code;
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CBaseWidget::OnMouseDown(const Vector &mousePos, const int mouseButton)
			{
				if (m_IsVisible == false)return ReturnCodes::Skip;
				if (!m_IsCatchTouch)return ReturnCodes::SkipDontProcess;
				auto code = ReturnCodes::Skip;

				LOCK_CHILDS_VECTOR
					for (int i = m_Childs.size() - 1; i >= 0; --i)
					{
						CBaseWidget * obj = m_Childs[i];
						if (obj->IsCatchTouch() == false)continue;
						if (obj->IsDisabled() == true)continue;
						code = obj->OnMouseDown(mousePos, mouseButton);
						if (!(code == ReturnCodes::Skip || code == ReturnCodes::SkipDontProcess))
						{
							break;
						}
					}
				UNLOCK_CHILDS_VECTOR
					if ((code == ReturnCodes::Skip || code == ReturnCodes::SkipDontProcess) && m_BaseEventListener != nullptr)
					{
						if (IsContains(mousePos))
						{
							CBaseWidget_EventData data;
							data.mousePos = mousePos;
							data.mouseButton = (MouseButton)mouseButton;
							m_IsWasFocusedOnMouseDown = true;
							code = m_BaseEventListener(this, CBaseWidget_EventType::Event_MouseDown, data);


							CEvent event(Event::ET_MouseDown);
							event.target = this;
							event.mousePos = mousePos;
							event.mouseButton = (MouseButton)mouseButton;
							DispatchEvent(event);
						}
					}
				return code;
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CBaseWidget::OnMouseUp(const Vector &mousePos, const int mouseButton)
			{
				if (m_IsVisible == false)return ReturnCodes::Skip;
				if (!m_IsCatchTouch)return ReturnCodes::SkipDontProcess;
				auto code = ReturnCodes::Skip;

				LOCK_CHILDS_VECTOR
					for (int i = m_Childs.size() - 1; i >= 0; --i)
					{
						CBaseWidget * obj = m_Childs[i];
						if (obj->IsCatchTouch() == false)continue;
						if (obj->IsDisabled() == true)continue;
						code = obj->OnMouseUp(mousePos, mouseButton);
						if (!(code == ReturnCodes::Skip || code == ReturnCodes::SkipDontProcess))
						{
							break;
						}
					}
				UNLOCK_CHILDS_VECTOR

					if ((code == ReturnCodes::Skip || code == ReturnCodes::SkipDontProcess) && m_BaseEventListener != nullptr)
					{
						CBaseWidget_EventData data;
						data.mousePos = mousePos;
						data.mouseButton = (MouseButton)mouseButton;

						if (IsContains(mousePos))
						{
							code = m_BaseEventListener(this, CBaseWidget_EventType::Event_MouseUp, data);
							CEvent event(Event::ET_MouseUp);
							event.target = this;
							event.mousePos = mousePos;
							event.mouseButton = (MouseButton)mouseButton;
							DispatchEvent(event);
						}
						else
						{
							code = m_BaseEventListener(this, CBaseWidget_EventType::Event_MouseUpOutside, data);
						}
						m_IsWasFocusedOnMouseDown = false;
					}
				return code;
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CBaseWidget::OnMouseWheel(const int delta)
			{
				if (m_IsVisible == false)return ReturnCodes::Skip;
				if (!m_IsCatchTouch)return ReturnCodes::SkipDontProcess;
				auto code = ReturnCodes::Skip;

				LOCK_CHILDS_VECTOR
					for (int i = m_Childs.size() - 1; i >= 0; --i)
					{
						CBaseWidget * obj = m_Childs[i];
						if (obj->IsCatchTouch() == false)continue;
						if (obj->IsDisabled() == true)continue;
						code = obj->OnMouseWheel(delta);
						if (!(code == ReturnCodes::Skip || code == ReturnCodes::SkipDontProcess))
						{
							break;
						}
					}
				UNLOCK_CHILDS_VECTOR

					if (m_IsFocused)
					{
						if ((code == ReturnCodes::Skip || code == ReturnCodes::SkipDontProcess) && m_BaseEventListener != nullptr)
						{
							CBaseWidget_EventData data;
							data.wheelDelta = delta;

							{
								code = m_BaseEventListener(this, CBaseWidget_EventType::Event_MouseWheel, data);
							}
						}
					}
				return code;
			}


			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CBaseWidget::OnKeyDown(const int key)
			{
				if (!m_IsCatchKeyboard)return ReturnCodes::SkipDontProcess;
				auto code = ReturnCodes::Skip;


				LOCK_CHILDS_VECTOR
					for (int i = m_Childs.size() - 1; i >= 0; --i)
					{
						CBaseWidget * obj = m_Childs[i];
						if (obj->IsCatchKeyboard() == false)continue;
						if (obj->IsDisabled() == true)continue;
						code = obj->OnKeyDown(key);
						if (!(code == ReturnCodes::Skip || code == ReturnCodes::SkipDontProcess))
						{
							break;
						}
					}
				UNLOCK_CHILDS_VECTOR

					if ((code == ReturnCodes::Skip || code == ReturnCodes::SkipDontProcess) && m_BaseEventListener != nullptr)
					{
						CBaseWidget_EventData data;
						data.key = key;
						code = m_BaseEventListener(this, CBaseWidget_EventType::Event_KeyDown, data);
					}
				return code;
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CBaseWidget::OnKeyUp(const int key)
			{
				if (!m_IsCatchKeyboard)return ReturnCodes::SkipDontProcess;
				auto code = ReturnCodes::Skip;


				LOCK_CHILDS_VECTOR
					for (int i = m_Childs.size() - 1; i >= 0; --i)
					{
						CBaseWidget * obj = m_Childs[i];
						if (obj->IsCatchKeyboard() == false)continue;
						if (obj->IsDisabled() == true)continue;
						code = obj->OnKeyUp(key);
						if (!(code == ReturnCodes::Skip || code == ReturnCodes::SkipDontProcess))
						{
							break;
						}
					}
				UNLOCK_CHILDS_VECTOR

					if ((code == ReturnCodes::Skip || code == ReturnCodes::SkipDontProcess) && m_BaseEventListener != nullptr)
					{
						CBaseWidget_EventData data;
						data.key = key;
						code = m_BaseEventListener(this, CBaseWidget_EventType::Event_KeyUp, data);
					}
				return code;
			}

			//////////////////////////////////////////////////////////////////////////
			void CBaseWidget::ProcessDelayed()
			{				
				if (m_IsDestroyed)return;
				if (m_IsBlockDelayedProcess)return;

				int sizeQueue = m_ChildsQueue.size();
				if (sizeQueue > 0)
				{					
					for (int i = 0; i < m_ChildsQueue.size(); i++)
					{
						AddChild(m_ChildsQueue[i]);
					}
					m_ChildsQueue.clear();
				}

				bool isNeedToDestroy = false;
				auto sizeA = m_Childs.size();
				if (sizeA > 0)
				{
					CBaseWidget ** vec = &m_Childs[0];
					for (int i = sizeA - 1; i >= 0; --i)
					{
						auto obj = vec[i];
						if (obj->IsDestroyed())
						{
							isNeedToDestroy = true;
							break;
						}
					}
				}
				

				if (isNeedToDestroy)
				{
					auto m_ChildsIter = m_Childs.begin();
					while (m_ChildsIter != m_Childs.end())
					{
						CBaseWidget * obj = (*m_ChildsIter);
						if (obj->IsDestroyed())
						{
							m_ChildsIter = m_Childs.erase(m_ChildsIter);
							continue;
						}
						m_ChildsIter++;
					}					
				}

				auto size = m_Childs.size();
				if (size > 0)
				{
					CBaseWidget ** vec = &m_Childs[0];
					for (int i = size - 1; i >= 0; --i)
					{
						auto obj = vec[i];
						obj->ProcessDelayed();																			
					}
				}
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CBaseWidget::Update(const float dt)
			{
				if (!m_IsUpdatable)return ReturnCodes::SkipDontProcess;
				if (m_Childs.size() == 0)return ReturnCodes::Skip;

				auto code = ReturnCodes::Skip;

				LOCK_CHILDS_VECTOR
					CBaseWidget ** vec = &m_Childs[0];
				for (int i = m_Childs.size() - 1; i >= 0; --i)
				{
                    
					CBaseWidget * obj = vec[i];
					if (obj->IsDestroyed())
					{
						m_IsNeedToDestroyChilds = true;
						continue;
					}
					code = obj->Update(dt);
					if (obj->IsDestroyed())
					{
						m_IsNeedToDestroyChilds = true;
						continue;
					}
					if (!(code == ReturnCodes::Skip || code == ReturnCodes::SkipDontProcess))
					{
						break;
					}
				}
				UNLOCK_CHILDS_VECTOR
					return code;
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CBaseWidget::___Draw(CRenderState & state)
			{
				if (m_DelayedSortItems.size() > 0)
				{
					ProceedDelayedSort();
				}
				if (!m_IsDrawable)return ReturnCodes::SkipDontProcess;
				if (!m_IsVisible)
				{
					return ReturnCodes::SkipDontProcess;
				}

				if (m_Color.a == 0.0f)return ReturnCodes::Skip;

				if (m_IsDebugDraw)
				{
					g_Render->DrawLine(m_BoundPoints[0], m_BoundPoints[1], 3, Color(180, 255, 255, 255));
					g_Render->DrawLine(m_BoundPoints[1], m_BoundPoints[2], 3, Color(180, 255, 255, 255));
					g_Render->DrawLine(m_BoundPoints[2], m_BoundPoints[3], 3, Color(180, 255, 255, 255));
					g_Render->DrawLine(m_BoundPoints[3], m_BoundPoints[0], 3, Color(180, 255, 255, 255));
				}
				Draw(state);

				
				LOCK_CHILDS_VECTOR
					auto size = m_Childs.size();
				if (size > 0)
				{					
					bool isEqualsTempMatrix = false;
					state.PushMatrix();					
					if (__prevTransform.x == m_Transform.x &&
						__prevTransform.y == m_Transform.y &&
						__prevTransform.hx == m_Transform.hx &&
						__prevTransform.hy == m_Transform.hy &&
						__prevTransform.rz == m_Transform.rz && 
						__prevTransform.scalex == m_Transform.scalex && 
						__prevTransform.scaley == m_Transform.scaley)
					{
						tempMatrix = __prevMatrix;
						isEqualsTempMatrix = true;
					}
					else
					{
						tempMatrix.CreateFromTransform(m_Transform, m_IsUseHotSpotInCalculations);
						__prevMatrix = tempMatrix;
					}

					if (__prevGlobalMatrix.Equals(state.GetMatrixPointer()) && isEqualsTempMatrix)
					{
						state.SetMatrix(__prevGlobalMatrixTransformed);
					}
					else
					{
						__prevGlobalMatrix = state.GetMatrix();
						state.ApplyMatrix(tempMatrix);
						__prevGlobalMatrixTransformed = state.GetMatrix();						
					}
					
					__prevTransform = m_Transform;

					CBaseWidget ** vec = &m_Childs[0];
					for (int i = 0; i < size; ++i)
					{
						CBaseWidget * obj = vec[i];
						if (obj->IsDrawable() == false)continue;
						if (obj->IsVisible() == false)continue;
						if (obj->GetOpacity() == 0.0f)continue;
						auto code = obj->___Draw(state);
						if (!(code == ReturnCodes::Skip || code == ReturnCodes::SkipDontProcess))
						{
							UNLOCK_CHILDS_VECTOR
								return code;
						}
					}

					state.PopMatrix();
				}
				UNLOCK_CHILDS_VECTOR
				AfterDraw(state);			
				return ReturnCodes::Skip;
			}
		}
	}
}