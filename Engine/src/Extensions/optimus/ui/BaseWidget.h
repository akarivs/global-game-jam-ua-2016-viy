#ifndef OPTIMUS_UI_BASE_WIDGET
#define OPTIMUS_UI_BASE_WIDGET

#include <Framework/Utils/MathUtils.h>

#include <Framework/Datatypes/Matrix.h>
#include <Framework/Datatypes/Matrix3x3.h>
#include <Framework/Datatypes/Color.h>
#include <Framework/Datatypes/Event.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/Transform.h>

#include <../External/xml/pugixml.hpp>
#include <queue>
#include <functional>
#include <mutex>

#include <Framework/Datatypes.h>
#include "../optimus_macro.h"
#include "../optimus_enum.h"

#include <Framework/Global/Render.h>
#include <Framework/Global/Render/RenderState.h>
#include <Framework/Global/OperatingSystem.h>
#include <Framework/Global/Input.h>

#include <Framework/Datatypes/Events/EventDispatcher.h>

using namespace NBG;
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
#define LOCK_CHILDS_VECTOR m_ChildsLock = true;
#define UNLOCK_CHILDS_VECTOR m_ChildsLock = false;
			enum class CBaseWidget_EventType			
			{
				Event_KeyDown,
				Event_KeyUp,
				Event_MouseDown,
				Event_MouseUp,
				Event_MouseMove,
				Event_MouseMoveOutside,
				Event_MouseWheel,
				Event_MouseUpOutside,
				Event_Over,
				Event_Out
			};
			struct CBaseWidget_EventData
			{
				Vector mousePos;
				MouseButton mouseButton;
				int key;
				int wheelDelta;
			};
			class CBaseWidget;
			typedef std::function < ReturnCodes(CBaseWidget * sender, CBaseWidget_EventType type, CBaseWidget_EventData data) > CBaseWidget_EventListener;
			class CBaseWidget : public CEventDispatcher
			{
			public:		
				OPTIMUS_CREATE(CBaseWidget);				
				virtual ~CBaseWidget();

				virtual void Init(){};

				Transform * GetTransform()
				{
					return &m_Transform;
				}

				Transform GetAbsoluteTransform()
				{					
					if (m_ParentTransform == nullptr)
					{
						if (m_Transform.parent)
						{
							///TODO: Check this moment
							m_ParentTransform = new Transform();							
							*m_ParentTransform = m_Transform.parent->GetAbsoluteTransform();
						}	
					}
					Transform t = m_Transform;
					t.AddTransform(m_ParentTransform);
					t.parent = nullptr;
					return m_Transform;
				}

				void LoadFromXML(const std::string path);				
				void LoadFromXMLNode(pugi::xml_node node, CBaseWidget * parent = nullptr);

				void SetEventListener(CBaseWidget_EventListener callback);

				virtual void OnHardRestart(){};

				
				

				///*POSITION*///
				void SetPosition(const Vector &position);
				void SetPositionX(const float x);
				void SetPositionY(const float y);
				void SetPositionZ(const float z);
				inline void SetPosition(const float x, const float y, const float z = 0.0f)
				{
					SetPosition(Vector(x, y, z));
				};				
				Vector GetPosition();
				float GetPositionX();
				float GetPositionY();
				float GetPositionZ();

				///*SCALE*///
				void SetScale(const Vector &scale);
				void SetScaleX(const float x);
				void SetScaleY(const float y);
				void SetScaleZ(const float z);
				inline void SetScale(const float scale)
				{
					SetScale(Vector(scale, scale, scale));
				}
				inline void SetScale(const float x, const float y, const float z = 0.0f)
				{
					SetScale(Vector(x, y, z));
				};
				Vector GetScale();
				float GetScaleX();
				float GetScaleY();
				float GetScaleZ();



				///*Size*///
				void SetSize(const Vector& size)
				{
					m_Size = size;
					UpdateHotSpot();
					OnSizeChanged();
					UpdateAnchors();					
				}
				void SetSize(const float x, const float y, const float z = 0.0f)
				{
					m_Size = Vector(x,y,z);
					SetSize(m_Size);
				}
				void SetSizeX(const float x)
				{
					m_Size.x = x;
					SetSize(m_Size);
				}
				void SetSizeY(const float y)
				{
					m_Size.y = y;
					SetSize(m_Size);
				}
				void SetSizeZ(const float z)
				{
					m_Size.z = z;
					SetSize(m_Size);
				}
				virtual Vector GetSize()
				{
					return m_Size;
				}
				virtual Vector GetSizeForAnchor()
				{
					return m_Size;
				}
				float GetSizeX()
				{
					return m_Size.x;
				}
				float GetSizeY()
				{
					return m_Size.y;
				}
				float GetSizeZ()
				{
					return m_Size.z;
				}

				Vector GetContentSize();

				///*COLOR*///
				///��������� �����
				void SetColor(Color clr, const bool changeBaseColor = true);
				void SetOpacity(float alpha);
				///��������� �����
				inline Color GetColor()
				{
					return m_Color;
				};
				inline float GetOpacity()
				{
					return (float)m_Color.a;
				};
				inline Color GetBaseColor()
				{
					return m_BaseColor;
				};

				void ReserveChilds(const int childs)
				{
					int size = m_Childs.size();
					m_Childs.reserve(size + childs);

				}
				

				///*ROTATION*///
				///��������� �������� � ��������
				inline void SetRotation(const float &angle)
				{
					m_Angle = angle;
					if (m_Angle >= 360.0f)
					{
						m_Angle -= 360.0f;
					}
					else if (m_Angle < 0.0f)
					{
						m_Angle += 360.0f;
					}
					m_Transform.rz = MathUtils::ToRadian(m_Angle);					
					OnRotationChanged();
					SendOnChangeParentTransform(this);
				};
				///��������� �������� � ��������
				inline float GetRotation()
				{
					return m_Angle;
				};

				///*HOT SPOT*///
				//Set object hotspot
				void SetHotSpot(const HotSpot type, const Vector offset = Vector(0, 0, 0));
				///Get hotspot offset
				Vector GetHotSpotOffset(const HotSpot &hotSpot);
				inline HotSpot GetHotSpot()
				{ 
					return m_HotSpot; 
				};
				void UpdateHotSpot();

				std::vector<Vector>& GetBounds()
				{
					return m_BoundPoints;
				};

				///*ANCHOR *///
				void SetAnchor(const HotSpot anchor);
				void UpdateAnchors();

				///*SORT*///
				///������� ������ ����������
				///������ ������ �� ����� ����
				void BringForward();
				///������ ������ �� ����� ���
				void BringBackward();
				///������ ������ ����� �������� place
				void BringBefore(CBaseWidget * place);
				///������ ������ ����� ������� place
				void BringAfter(CBaseWidget * place);

				///������ ������ �� ����� ����
				void BringForward(CBaseWidget * child);
				///������ ������ �� ����� ���
				void BringBackward(CBaseWidget * child);
				///������ ������ ����� �������� place
				void BringBefore(CBaseWidget * child, CBaseWidget * place);
				///������ ������ ����� ������� place
				void BringAfter(CBaseWidget * child, CBaseWidget * place);

				///������� ���������� �� �����
				///�������� ������� ������������ ����
				int GetTopLayer();
				///���������� ���� �������
				void SetLayer(const int layer);
				///�������� ���� �������
				inline int GetLayer()
				{
					return m_Layer;
				};
				///�����������, ��� ���������� �� �����
				static bool SortByLayer(CBaseWidget *a, CBaseWidget *b);
				///������� � ������, ����� � ������ �������� ����.
				void OnChildLayerChange(CBaseWidget * child);


				virtual bool IsContains(const Vector &pos);


				///CHILD SYSTEM///
				void AddChild(CBaseWidget * child);
				void RemoveChild(CBaseWidget * child);
				void RemoveAllChilds(const bool cleanup = true);
				void SetParent(CBaseWidget * parent);
				void MoveToParent(CBaseWidget * newParent, bool saveObjectPos = true);
				CBaseWidget * GetParent()
				{
					return m_Parent;
				}
				CBaseWidget * GetChildByTag(const int tag, const bool recursive = false);
				CBaseWidget * GetChildByName(const std::string &name, const bool recursive = false);

				template<typename T>
				T * GetChildByTag(const int tag, const bool recursive = false)
				{
					for (size_t i = 0; i < m_Childs.size(); ++i)
					{
						auto child = m_Childs[i];
						if (child->GetTag() == tag)
						{
							return CAST(T*, child);
						}
						else
						{
							if (recursive)
							{
								child = child->GetChildByTag<T>(tag, true);
								if (child)return CAST(T*, child);
							}
						}
					}
					for (size_t i = 0; i < m_ChildsQueue.size(); ++i)
					{
						auto child = m_ChildsQueue[i];
						if (child->GetTag() == tag)
						{
							return CAST(T*, child);
						}
						else
						{
							if (recursive)
							{
								child = child->GetChildByTag<T>(tag, true);
								if (child)return CAST(T*, child);
							}
						}
					}
					return nullptr;
				}

				template<typename T>
				T * GetChildByName(const std::string &name, const bool recursive = false)
				{
					for (size_t i = 0; i < m_Childs.size(); ++i)
					{
						auto child = m_Childs[i];
						if (child->GetName() == name)
						{
							return CAST(T*, child);
						}
						else
						{
							if (recursive)
							{
								child = child->GetChildByName<T>(name, true);
								if (child)return CAST(T*, child);
							}
						}
					}
					for (size_t i = 0; i < m_ChildsQueue.size(); ++i)
					{
						auto child = m_ChildsQueue[i];
						if (child->GetName() == name)
						{
							return CAST(T*, child);
						}
						else
						{
							if (recursive)
							{
								child = child->GetChildByName<T>(name, true);
								if (child)return CAST(T*, child);
							}
						}
					}
					return nullptr;
				}

				int GetChildsCount()
				{
					return m_Childs.size();
				}

				int GetChildsQueueCount()
				{
					return m_ChildsQueue.size();
				}

				std::vector<CBaseWidget *>& GetChilds(){ return m_Childs; };
				std::deque<CBaseWidget*>&GetChildsQueue() { return m_ChildsQueue; }

				virtual void OnPositionChanged(){};
				virtual void OnBlendModeChanged(){};
				virtual void OnRotationChanged(){};
				virtual void OnScaleChanged(){};
				virtual void OnSizeChanged(){};
				virtual void OnColorChanged(){};
				virtual void OnHotSpotChanged(){};
				virtual void OnLoadFromXMLNode(const pugi::xml_node &node){};
				virtual void AfterLoadFromXMLNode(){};
				virtual void OnParentTransformChanged(){};
				virtual void OnSetDisabled(){};
				virtual void OnChildAdded(CBaseWidget * child){};
				virtual void OnAddedToParent(){};

				void UpdateBoundPoints();

				Vector ConvertToLocalSpace(const Vector &pos);

				///���������� ���������� ���������� �������.
				enum EDelayedSortType
				{
					Delayed_BringForward,
					Delayed_BringBackward,
					Delayed_BringBefore,
					Delayed_BringAfter,
					Delayed_BringForwardSelf,
					Delayed_BringBackwardSelf,
					Delayed_BringBeforeSelf,
					Delayed_BringAfterSelf,
				};
				void AddDelayedSort(EDelayedSortType sortType, CBaseWidget * obj1, CBaseWidget* obj2 = nullptr)
				{
					SDelayedSortItem item;
					item.type = sortType;
					item.obj1 = obj1;
					item.obj2 = obj2;
					m_DelayedSortItems.push_back(item);
				}
				struct SDelayedSortItem
				{
					EDelayedSortType type;
					CBaseWidget * obj1;
					CBaseWidget * obj2;
				};
				void ProceedDelayedSort();
				std::vector<SDelayedSortItem>m_DelayedSortItems;

				void SetOnDestroyCallback(std::function<void()> callback)
				{
					m_OnDestroyCallback = callback;
				}
				

				virtual void OnRenderChange(const RenderChangeType type)
				{
					for (int i = 0; i < m_Childs.size(); i++)
					{
						m_Childs[i]->OnRenderChange(type);
					}
					UpdateAnchors();
				};

				virtual void OnFocusChange(const FocusChangeType type)
				{
					for (int i = 0; i < m_Childs.size(); i++)
					{
						m_Childs[i]->OnFocusChange(type);
					}					
				};

				void SetDisabled(const bool disabled)
				{
					m_IsDisabled = disabled;
					OnSetDisabled();
				}

				virtual bool IsDisabled()
				{
					return m_IsDisabled;
				}

				virtual void SetScale9Enabled(const bool enabled)
				{
					m_IsScale9Enabled = enabled;
				}

				bool IsScale9Enabled()
				{
					return m_IsScale9Enabled;
				}

				void SetBlendMode(BlendMode mode)
				{
					m_BlendMode = mode;
					OnBlendModeChanged();
				}

				BlendMode GetBlendMode()
				{
					return m_BlendMode;
				}

				virtual ReturnCodes OnMouseMove(const Vector &mousePos);
				virtual ReturnCodes OnMouseDown(const Vector &mousePos, const int mouseButton);
				virtual ReturnCodes OnMouseUp(const Vector &mousePos, const int mouseButton);
				virtual ReturnCodes OnMouseWheel(const int delta);

				virtual ReturnCodes OnKeyDown(const int key);
				virtual ReturnCodes OnKeyUp(const int key);

				virtual ReturnCodes Update(const float dt);
				virtual ReturnCodes Draw(CRenderState & state){ return ReturnCodes::Skip; };
				virtual ReturnCodes AfterDraw(CRenderState & state){ return ReturnCodes::Skip; };
				
				virtual ReturnCodes ___Draw(CRenderState & state);

				void ProcessDelayed();

				static Matrix tempMatrix;
			private:
				Vector pUL;
				Vector pUR;
				Vector pDL;
				Vector pDR;
				
				Transform __prevTransform;
				Matrix __prevMatrix;
				Matrix __prevGlobalMatrix;
				Matrix __prevGlobalMatrixTransformed;

				bool m_IsNeedToDestroyChilds;
			protected:
				CBaseWidget();
				void SendOnChangeParentTransform(CBaseWidget * obj);					

				///BASE SETTINGS///
				GETTER_SETTER(std::string, m_Name, Name);		
				BlendMode m_BlendMode;				
				GETTER_SETTER(int, m_Tag, Tag);				
				GETTER_SETTER(int, m_RealLayer, RealLayer); ///position of object in childs vector
				GETTER_SETTER_BOOL(CatchTouch);
				GETTER_SETTER_BOOL(CatchKeyboard);
				GETTER_SETTER_BOOL(Updatable);
				GETTER_SETTER_BOOL(Drawable);
				bool m_IsDisabled;
				bool m_IsScale9Enabled;
				GETTER_SETTER_BOOL(Visible);
				GETTER_SETTER_BOOL(Destroyed);
				GETTER_SETTER_BOOL(CascadeOpacityChange);
				GETTER_SETTER_BOOL(CascadeColorChange);				
				GETTER_SETTER_BOOL(IgnoreCascadeColorChange);
				GETTER_SETTER_BOOL(Focused);
				GETTER_SETTER_BOOL(DebugDraw);				
				GETTER_SETTER_BOOL(WasFocusedOnMouseDown);
				GETTER_SETTER_BOOL(UseHotSpotInCalculations);
				GETTER_SETTER(SortModes, m_SortMode, SortMode);


				

				///If it's true, than object cannot be deleted.
				GETTER_SETTER_BOOL(Immortal);
				
				GETTER_SETTER_BOOL(BlockDelayedProcess);

				///For internal use only				
				GETTER_SETTER_BOOL(Root);				
				//only for internal use or if you know what you doing!
				GETTER_SETTER_BOOL(BlockUpdateAnchors);
				GETTER_SETTER_BOOL(BlockUpdateBoundPoints);
				///Size of an object
				Vector m_Size;
				///Size of objects container
				Vector m_ContentSize;
				///Hot spot of an object
				HotSpot m_HotSpot;
				Vector m_HotSpotOffset;
				Transform m_Transform;
				Transform m_SavedTransform;
				Transform * m_ParentTransform;
				Color m_Color;
				Color m_BaseColor;
				float m_Angle;
				int m_Layer;
				int m_TopLayer;

				HotSpot m_Anchor;
				std::vector<Vector> m_BoundPoints;		

				Matrix m_TempMatrix;
				Matrix3x3 m_TempMatrix3x3;
				Matrix m_LocalMatrix;

				///CHILD SYSTEM///
				bool m_ChildsLock;
				std::vector<CBaseWidget*> m_Childs;
				std::deque<CBaseWidget*>  m_ChildsQueue;
				CBaseWidget * m_Parent;				

				CBaseWidget_EventListener m_BaseEventListener;

				std::function<void()> m_OnDestroyCallback;
			};
		}
	}
}
#endif //OPTIMUS_UI_BASE_WIDGET