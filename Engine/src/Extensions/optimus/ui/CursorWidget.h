#ifndef OPTIMUS_UI_CURSOR_WIDGET
#define OPTIMUS_UI_CURSOR_WIDGET

#include <Framework/Utils/MathUtils.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Global/Resources/TextureResource.h>
#include <Framework/Helpers/AtlasHelper.h>

#include <../External/xml/pugixml.hpp>
#include <queue>

#include "ImageWidget.h"
#include "../optimus_macro.h"
#include "../optimus_enum.h"

using namespace NBG;
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{			
			class CCursorWidget : public CBaseWidget
			{
			public:
				OPTIMUS_CREATE(CCursorWidget);				
				virtual ~CCursorWidget();				

				virtual void Init();

				void SetTexture(const std::string &texturePath);
			protected:
				CCursorWidget();				
				CImageWidget * m_Cursor;				
			};
		}
	}
}
#endif //OPTIMUS_UI_CURSOR_WIDGET