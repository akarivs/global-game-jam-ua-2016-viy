#include "ClipWidget.h"

#include <Framework.h>
#include "../optimus.h"


namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
			//////////////////////////////////////////////////////////////////////////
			CClipWidget::CClipWidget():ui::CBaseWidget()
			{

			}
			
			//////////////////////////////////////////////////////////////////////////
			CClipWidget::~CClipWidget()
			{
				
			}

			//////////////////////////////////////////////////////////////////////////
			void CClipWidget::Init()
			{
				m_IsTopClip = false;
				m_Timer = 0.0f;
				m_IsUseCallback = false;
				m_Image = nullptr;
				m_SpeedMultiplier = 1.0f;				
				m_IsCascadeColorChange = true;

				m_FinishLUACallback = nullptr;

				SetUpdatable(true);
			}
						
			/// @name Загрузка объекта из XML ноды
			void CClipWidget::OnLoadFromXMLNode(const pugi::xml_node &node)
			{
				LoadFromXML(node.attribute("path").value(),
					node.attribute("animation").value());

				AnimationType animType = AnimationType::ForwardLoop;
				std::string animTypeS = node.attribute("anim_type").value();

				if (animTypeS == "forward_once")animType = AnimationType::ForwardOnce;
				else if (animTypeS == "backward_once")animType = AnimationType::BackwardOnce;
				else if (animTypeS == "forward_loop")animType = AnimationType::ForwardLoop;
				else if (animTypeS == "backward_loop")animType = AnimationType::BackwardLoop;
				else if (animTypeS == "ping_pong")animType = AnimationType::PingPong;

				SetAnimationType(animType);

				if (node.attribute("play_from_start").as_bool())
				{
					Play();
				}

			}

			//////////////////////////////////////////////////////////////////////////
			void CClipWidget::LoadFromXML(const std::string &path, const std::string &animation)
			{
				m_IsTopClip = true;
				std::string docPath = path;
				NBG::CXMLResource* xmlRes = (NBG::CXMLResource*)g_ResManager->GetResource(docPath);
				NBG_Assert(xmlRes, "Cannot load xml!");
				pugi::xml_document* doc_window = xmlRes->GetXML();



				m_CurrentFrame = 0;
				m_FrameInterval = 1.0f / 30.0f;
				m_Timer = 0.0f;
				m_IsStoped = true;
				m_IsPongForward = true;

				pugi::xml_node objects = doc_window->first_child().first_child();;


				pugi::xml_node childNode = objects.first_child();
				if (animation.empty() == false)
				{
					objects = doc_window->first_child().find_child_by_attribute("id", animation.c_str());
				}

				m_AllTime = objects.attribute("time").as_float();
				m_CurrentTime = 0.0f;


				std::string modifier = objects.attribute("modifier").value();

				m_AnimType = AnimationType::ForwardLoop;

				treeLevel = 0;


				for (pugi::xml_node obj = objects.first_child(); obj; obj = obj.next_sibling())
				{
					int m;
					LoadNode(&obj, this, m);
				}
				MoveToTime(0.0f);

				g_ResManager->ReleaseResource(xmlRes);
			}

			//////////////////////////////////////////////////////////////////////////
			void CClipWidget::SetAnimationType(const AnimationType type, VOID_CALLBACK finishCallback)
			{
				m_AnimType = type;
				m_FinishCallback = finishCallback;
				m_IsUseCallback = true;
			}

			//////////////////////////////////////////////////////////////////////////
			void CClipWidget::SetAnimationType(const AnimationType type)
			{
				m_AnimType = type;
			}

			//////////////////////////////////////////////////////////////////////////
			void CClipWidget::LoadNode(pugi::xml_node * object, CClipWidget * parent, int &maxFrame)
			{
				CClipWidget * clip = CClipWidget::Create();
				clip->SetBlockDelayedProcess(true);

				clip->treeLevel = parent->treeLevel + 1;

				if (clip->treeLevel > 1)
				{
					clip->SetAnchor(HotSpot::HS_UL);
				}

				Vector anchor;
				anchor.x = object->attribute("centerX").as_float();
				anchor.y = object->attribute("centerY").as_float();

				auto graph = CImageWidget::Create();
				graph->SetBlockDelayedProcess(true);
				graph->SetTag(333);
				std::string texturePath = object->attribute("texture").value();
				texturePath = StringUtils::StringReplace(texturePath, std::string("\\"), std::string("/"), true);
				graph->SetTexture(texturePath);			
				graph->SetBlockUpdateBoundPoints(true);
				clip->AddChild(graph);
				clip->SetImage(graph);
				clip->SetSize(graph->GetSize());


				if (std::string(object->attribute("moving_type").value()) == "line")
				{
					clip->m_TweenFunc = OPTIMUS_FUNC_CALLBACK_4(Tweens::easeNone);
				}
				else
				{
					clip->m_TweenFunc = OPTIMUS_FUNC_CALLBACK_4(Tweens::easeOutInSine);
				}

				parent->AddChild(clip);
				parent->m_ChildClips.push_back(clip);

				clip->SetLayer(object->attribute("order").as_int());

				for (pugi::xml_node obj = object->first_child(); obj; obj = obj.next_sibling())
				{
					if (std::string(obj.name()) == "movingPart")
					{
						LoadNode(&obj, clip, maxFrame);
						continue;
					}
					float time = obj.attribute("time").as_float();
					float x = obj.attribute("x").as_float();
					float y = obj.attribute("y").as_float();
					float scaleX = obj.attribute("scaleX").as_float();
					float scaleY = obj.attribute("scaleY").as_float();
					if (obj.attribute("scaleX").empty())
					{
						scaleX = 1.0f;
					}
					if (obj.attribute("scaleY").empty())
					{
						scaleY = 1.0f;
					}

					float angle = MathUtils::ToDegree(obj.attribute("angle").as_float());
					if ((scaleX < 0.0f && scaleY > 0.0f) || (scaleX > 0.0f && scaleY < 0.0f))
					{
						//angle *= -1.0f;
					}

					float alpha = obj.attribute("alpha").as_float()*255.0f;


					KeyFrameData kf;
					kf.pos.x = x;
					kf.pos.y = y;
					kf.anchor.x = anchor.x;
					kf.anchor.y = anchor.y;
					kf.scale.x = scaleX;
					kf.scale.y = scaleY;
					kf.rotation = angle;
					kf.opacity = alpha;
					kf.duration = time;
					clip->m_KeyFrames.push_back(kf);


				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CClipWidget::MoveToTime(const float time)
			{
				m_CurrentTime = time * m_AllTime;

				KeyFrameData * prevFrame = nullptr;
				KeyFrameData * nextFrame = nullptr;
				float prevTime = -1.0f;
				for (int i = m_KeyFrames.size() - 1; i >= 0; i--)
				{
					KeyFrameData * f = &m_KeyFrames[i];
					if (f->duration < time)
					{
						prevFrame = f;
						break;
					}
				}
				for (int i = 0; i < m_KeyFrames.size(); i++)
				{
					KeyFrameData * f = &m_KeyFrames[i];
					if (f->duration >= time)
					{
						nextFrame = f;
						break;
					}
				}

				if (m_KeyFrames.size() > 0)
				{
					if (prevFrame == nullptr)prevFrame = &m_KeyFrames[0];
					if (nextFrame == nullptr)nextFrame = &m_KeyFrames[m_KeyFrames.size() - 1];
				}

				SetBlockUpdateAnchors(true);
				SetBlockUpdateBoundPoints(true);

				if (prevFrame && nextFrame)
				{
					if (prevFrame == nextFrame)
					{
						SetPosition(prevFrame->pos);
						SetScale(prevFrame->scale);
						auto im = GetChildByTag(333);
						SetHotSpot(HotSpot::HS_CUSTOM, prevFrame->anchor);
						SetRotation(prevFrame->rotation);
						SetOpacity(prevFrame->opacity);
					}
					else
					{
						

						float t = time - prevFrame->duration;
						float d = nextFrame->duration - prevFrame->duration;
						float c = nextFrame->pos.x - prevFrame->pos.x;
						float b = prevFrame->pos.x;

						Vector pos;						
						pos.x = m_TweenFunc(t, b, c, d);
						c = nextFrame->pos.y - prevFrame->pos.y;
						b = prevFrame->pos.y;
						pos.y = m_TweenFunc(t, b, c, d);
						SetPosition(pos);


						Vector scale;
						c = nextFrame->scale.x - prevFrame->scale.x;
						b = prevFrame->scale.x;
						scale.x = m_TweenFunc(t, b, c, d);
						c = nextFrame->scale.y - prevFrame->scale.y;
						b = prevFrame->scale.y;
						scale.y = m_TweenFunc(t, b, c, d);
						SetScale(scale);

						SetHotSpot(HotSpot::HS_CUSTOM, prevFrame->anchor);					


						c = nextFrame->rotation - prevFrame->rotation;
						b = prevFrame->rotation;
						SetRotation(m_TweenFunc(t, b, c, d));

						c = nextFrame->opacity - prevFrame->opacity;
						b = prevFrame->opacity;
						SetOpacity(m_TweenFunc(t, b, c, d));
					}
				}
				
				SetBlockUpdateBoundPoints(false);
				SetBlockUpdateAnchors(false);
							
				for (int i = 0; i < m_ChildClips.size(); i++)
				{
					CClipWidget * clip = m_ChildClips[i];
					clip->MoveToTime(time);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CClipWidget::Play(const bool restart)
			{
				m_IsStoped = false;
				if (restart)
				{
					if (m_AnimType == AnimationType::BackwardLoop  || m_AnimType == AnimationType::BackwardOnce)
					{
						m_CurrentTime = m_AllTime;
					}
					else
					{
						m_CurrentTime = 0.0f;
					}
					MoveToTime(m_CurrentTime);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CClipWidget::Stop()
			{
				m_IsStoped = true;
				if (m_AnimType == AnimationType::BackwardLoop || m_AnimType == AnimationType::ForwardOnce)
				{
					m_CurrentTime = 1.0f;
				}
				else if (m_AnimType == AnimationType::BackwardOnce || m_AnimType == AnimationType::ForwardLoop)
				{
					m_CurrentTime = 0.0f;
				}
				else
				{
					m_CurrentTime = 0.0f;
				}				
				if (m_IsUseCallback)
				{
					m_FinishCallback();
				}
				if (m_FinishLUACallback != nullptr)
				{
					m_FinishLUACallback->Start();
				}
				MoveToTime(m_CurrentTime);				
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CClipWidget::Update(const float dt)
			{
				ui::CBaseWidget::Update(dt);

				if (m_AllTime == 0.0f)return ReturnCodes::Skip;

				if (m_IsTopClip && !m_IsStoped)
				{					
					m_Timer += dt * m_SpeedMultiplier;
					while (m_Timer > m_FrameInterval)
					{
						m_Timer -= m_FrameInterval;

						if (m_AnimType == AnimationType::ForwardLoop || m_AnimType == AnimationType::ForwardOnce)
						{
							m_CurrentTime += m_FrameInterval;
							if (m_CurrentTime >= m_AllTime)
							{
								if (m_AnimType == AnimationType::ForwardLoop)
								{
									m_CurrentTime = 0;
								}
								else if (m_AnimType == AnimationType::ForwardOnce )
								{
									Stop();
									UpdateAnchors();
									SetColor(GetColor(), false);
									return ReturnCodes::Skip;
								}
							}
						}
						else if (m_AnimType == AnimationType::PingPong)
						{
							if (m_IsPongForward)
							{
								m_CurrentTime += m_FrameInterval;
								if (m_CurrentTime >= m_AllTime)
								{
									m_CurrentTime = m_AllTime;
									m_IsPongForward = false;
								}
							}
							else
							{
								m_CurrentTime -= m_FrameInterval;
								if (m_CurrentTime < 0.0f)
								{
									m_CurrentTime = 0.0f;
									m_IsPongForward = true;
								}
							}
						}
						MoveToTime(m_CurrentTime / m_AllTime);
						UpdateAnchors();	
						SetColor(GetColor(), false);
					}
				}
				return ReturnCodes::Skip;
			}
		}
	}
}