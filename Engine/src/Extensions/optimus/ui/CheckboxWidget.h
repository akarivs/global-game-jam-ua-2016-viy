#ifndef OPTIMUS_UI_CHECKBOX_WIDGET
#define OPTIMUS_UI_CHECKBOX_WIDGET

#include <Framework/Utils/MathUtils.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Global/Resources/TextureResource.h>
#include <Framework/Helpers/AtlasHelper.h>

#include <../External/xml/pugixml.hpp>
#include <queue>

#include "ImageWidget.h"
#include "../optimus_macro.h"
#include "../optimus_enum.h"

using namespace NBG;
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
			enum class CCheckboxWidget_EventType
			{
				Event_Check,
				Event_UnCheck,
			};
			typedef std::function < void(CBaseWidget * sender, CCheckboxWidget_EventType type) > CCheckboxWidget_EventListener;
			class CCheckboxWidget : public CBaseWidget
			{
			public:
				OPTIMUS_CREATE(CCheckboxWidget);
				static CCheckboxWidget * Create(const std::string &textureFolderPath)
				{
					auto ret = Create();
					ret->SetTexture(textureFolderPath);
					return ret;
				}
				virtual ~CCheckboxWidget();

				virtual void OnLoadFromXMLNode(const pugi::xml_node &node);

				virtual void OnSetDisabled();
				virtual bool IsContains(const Vector &pos);

				inline virtual void OnSizeChanged()
				{
					if (m_Normal)
					{
						m_Normal->SetSize(GetSize());
						m_Over->SetSize(GetSize());
						m_Clicked->SetSize(GetSize());
						m_ClickedOver->SetSize(GetSize());
						m_Disabled->SetSize(GetSize());


						m_Size = m_Normal->GetSize();
						UpdateHotSpot();
						UpdateAnchors();
					}
				};

				void SetScale9Enabled(const bool enabled)
				{
					CBaseWidget::SetScale9Enabled(enabled);

					m_Normal->SetScale9Enabled(enabled);
					m_Over->SetScale9Enabled(enabled);
					m_Clicked->SetScale9Enabled(enabled);
					m_ClickedOver->SetScale9Enabled(enabled);
					m_Disabled->SetScale9Enabled(enabled);
				}

				virtual void Init();

				void SetButtonListener(CCheckboxWidget_EventListener callback);
				void SetTexture(const std::string &textureFolderPath);

				void SetChecked(const bool checked);
			protected:
				CCheckboxWidget();
				ReturnCodes EventListener(CBaseWidget * sender, CBaseWidget_EventType type, CBaseWidget_EventData data);

				GETTER_SETTER(std::string, m_Sound, Sound);
				GETTER_SETTER_BOOL(UseAlphaMask);

				CImageWidget * m_Normal;
				CImageWidget * m_Over;				
				CImageWidget * m_Clicked;
				CImageWidget * m_ClickedOver;
				CImageWidget * m_Disabled;

				CImageWidget * m_CurrentState;

				CCheckboxWidget_EventListener m_CheckboxEventListener;

				void SetState(CImageWidget * state);
			};
		}
	}
}
#endif //OPTIMUS_UI_CHECKBOX_WIDGET