#include "WidgetsFactory.h"

#include "../optimus.h"

namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
			CWidgetsFactory * CWidgetsFactory::self = nullptr;
			CWidgetsFactory::CWidgetsFactory()
			{
				Add<CBaseWidget>("container");				
				Add<CAreaWidget>("area");
				Add<CButtonWidget>("button");
				Add<CClipWidget>("clip");
				Add<CCheckboxWidget>("checkbox");
				Add<CImageWidget>("image");
				Add<CListViewWidget>("list");
				Add<CInputTextWidget>("input_text");
				Add<particles::CEmmiterWidget>("particle");
				Add<CSliderWidget>("slider");
				Add<CSpriteAnimationWidget>("sprite_animation");				
				Add<CTextButtonWidget>("text_button");
				Add<CTextWidget>("text");
				Add<CVideoWidget>("video");
			}
		}
	}
}