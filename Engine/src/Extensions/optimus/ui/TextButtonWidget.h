#ifndef OPTIMUS_UI_TEXT_BUTTON_WIDGET
#define OPTIMUS_UI_TEXT_BUTTON_WIDGET

#include <Framework/Utils/MathUtils.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Global/Resources/TextureResource.h>
#include <Framework/Helpers/AtlasHelper.h>

#include <../External/xml/pugixml.hpp>
#include <queue>

#include "TextWidget.h"
#include "../optimus_macro.h"
#include "../optimus_enum.h"

using namespace NBG;
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
			enum class CTextButtonWidget_EventType
			{
				Event_Click,
				Event_DisabledClick,				
			};
			typedef std::function < void(CBaseWidget * sender, CTextButtonWidget_EventType type) > CTextButtonWidget_EventListener;
			class CTextButtonWidget : public CBaseWidget
			{
			public:
				OPTIMUS_CREATE(CTextButtonWidget);
				static CTextButtonWidget * Create(const std::string &fontName, const std::string &localeId)
				{
					auto ret = Create();
					//ret->SetTexture(fontName, localeId);
					return ret;
				}
				virtual ~CTextButtonWidget();

				virtual bool IsContains(const Vector &pos);
				virtual void OnLoadFromXMLNode(const pugi::xml_node &node);

				virtual void OnSetDisabled();			

				virtual void Init();

				void SetText(const std::wstring &text);

				virtual ReturnCodes Draw(CRenderState & renderState);

				void SetTextButtonListener(CTextButtonWidget_EventListener callback);				
			protected:
				CTextButtonWidget();
				ReturnCodes EventListener(CBaseWidget * sender, CBaseWidget_EventType type, CBaseWidget_EventData data);				
				GETTER_SETTER(std::string, m_SoundName, SoundName);
				GETTER_SETTER(float, m_LineOffsetY, LineOffsetY);
				GETTER_SETTER_BOOL(DrawLine);

				CTextWidget * m_Normal;
				CTextWidget * m_Over;
				CTextWidget * m_Disabled;
				CTextWidget * m_Clicked;

				CTextWidget * m_CurrentState;

				CTextButtonWidget_EventListener m_TextButtonEventListener;

				void SetState(CTextWidget * state);
			};
		}
	}
}
#endif //OPTIMUS_UI_TEXT_BUTTON_WIDGET