#include "ListViewWidget.h"

#include "SliderWidget.h"

#include <Framework.h>
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{

			//////////////////////////////////////////////////////////////////////////
			CListViewWidget::CListViewWidget() :CBaseWidget()
			{				

			}

			//////////////////////////////////////////////////////////////////////////
			CListViewWidget::~CListViewWidget()
			{
				
			}

			//////////////////////////////////////////////////////////////////////////
			void CListViewWidget::Init()
			{
				m_ItemsDistance = 10.0f;

				m_IsCatchTouch = true;
				
				m_ListViewEventListener = nullptr;			

				m_Container = CBaseWidget::Create();
				m_Container->SetAnchor(HotSpot::HS_UL);
				m_Container->SetHotSpot(HotSpot::HS_UL);
				m_Container->SetCatchTouch(true);
				AddChild(m_Container);

				m_Direction = CListView_DirectionType::Vertical;

				m_IsDrag = false;
				m_IsMouseDown = false;

				m_Slider = nullptr;
				m_Inertion = 0.0f;
				m_InertionPower = 0.5f;
				m_InertionStopage = 100.0f;

				m_EventCatcher = CBaseWidget::Create();
				m_EventCatcher->SetEventListener(OPTIMUS_CALLBACK_3(&CListViewWidget::EventListener, this));
				m_EventCatcher->SetCatchTouch(true);
				AddChild(m_EventCatcher);
			}

			//////////////////////////////////////////////////////////////////////////
			void CListViewWidget::SetSlider(CSliderWidget * slider)
			{
#ifdef NBG_ANDROID
				return;
#endif
				m_Slider = slider;

				m_Slider->SetSliderListener(OPTIMUS_CALLBACK_3(&CListViewWidget::SliderListener, this));

				float percent = 0.0f;
				if (m_Direction == CListView_DirectionType::Horizontal)
				{
					percent = GetSize().x / m_Container->GetSize().x;
				}
				else
				{
					percent = GetSize().y / m_Container->GetSize().y;
				}
				if (percent > 1.0f)percent = 1.0f;				
				m_Slider->SetMode(CSliderWidget_Mode::List, percent);
			}

			//////////////////////////////////////////////////////////////////////////
			void CListViewWidget::OnLoadFromXMLNode(const pugi::xml_node &node)
			{

				if (node.attribute("inertion_stopage").empty() == false)
				{
					m_InertionStopage = node.attribute("inertion_stopage").as_float();
				}
				if (node.attribute("inertion_power").empty() == false)
				{
					m_InertionPower = node.attribute("inertion_power").as_float();
				}
				if (node.attribute("items_distance").empty() == false)
				{
					m_ItemsDistance = node.attribute("items_distance").as_float();
				}

				std::string mode = node.attribute("direction").value();
				if (mode == "horizontal")
				{
					m_Direction = CListView_DirectionType::Horizontal;
				}
				else
				{
					m_Direction = CListView_DirectionType::Vertical;
				}

				m_EventCatcher->SetSize(m_Size);

			}

			//////////////////////////////////////////////////////////////////////////
			void CListViewWidget::SetListViewListener(CListViewWidget_EventListener callback)
			{
				m_ListViewEventListener = callback;
			}

			//////////////////////////////////////////////////////////////////////////
			void CListViewWidget::SliderListener(CSliderWidget * sender, CSliderWidget_EventType type, const float value)
			{
				if (type == CSliderWidget_EventType::Event_ValueChange)
				{
					float pos = 0.0f;
					if (m_Direction == CListView_DirectionType::Horizontal)
					{
						pos = (GetSize().x - m_Container->GetSize().x) * value;									
					}
					else
					{
						pos = (GetSize().y - m_Container->GetSize().y) * value;
					}
					UpdatePosition(pos);

					m_Inertion = 0.0f;
				}				
			}
			
			ReturnCodes CListViewWidget::OnMouseWheel(const int delta)
			{
				CBaseWidget::OnMouseWheel(delta);
				if (!IsContains(g_MousePos))
				{
					return ReturnCodes::Skip;
				}
				float pos = 0.0f;
				if (m_Direction == CListView_DirectionType::Horizontal)
				{
					pos = m_Container->GetPositionX() + delta * (m_Size.x / 4.0f);
				}
				else
				{
					pos = m_Container->GetPositionY() + delta * (m_Size.y / 4.0f);
				}
				m_Inertion = 0.0f;
				UpdatePosition(pos);

				return ReturnCodes::Block;
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CListViewWidget::EventListener(CBaseWidget * sender, CBaseWidget_EventType type, CBaseWidget_EventData data)
			{
				if (type == CBaseWidget_EventType::Event_MouseWheel)
				{
					
				}		
				else if (type == CBaseWidget_EventType::Event_MouseDown)
				{
					m_IsMouseDown = true;
				
					m_DragStartPos = ConvertToLocalSpace(data.mousePos);
					m_DragContainerStartPos = m_Container->GetPosition();
					m_DragStartTime = g_GameApplication->GetAppTime();

					
				}
				else if (type == CBaseWidget_EventType::Event_MouseUp || type == CBaseWidget_EventType::Event_MouseUpOutside)
				{
					m_IsMouseDown = false;
					
					Vector diff = ConvertToLocalSpace(data.mousePos) - m_DragStartPos;
					if (m_Direction == CListView_DirectionType::Horizontal)
					{
						m_Inertion = (diff.x / ((g_GameApplication->GetAppTime() - m_DragStartTime)))*m_InertionPower;
					}
					else
					{
						m_Inertion = (diff.y / ((g_GameApplication->GetAppTime() - m_DragStartTime)))*m_InertionPower;
					}
					m_Inertion *= 0.05f;
					if (m_IsDrag)
					{
						m_IsDrag = false;
						return ReturnCodes::Block;
					}					
				}
				else if (type == CBaseWidget_EventType::Event_MouseMove || type == CBaseWidget_EventType::Event_MouseMoveOutside)
				{
					if (m_IsMouseDown)
					{
						Vector diff = ConvertToLocalSpace(data.mousePos) - m_DragStartPos;
						if (diff.GetLength() > 10.0f && !m_IsDrag)
						{
							m_IsDrag = true;

							m_DragStartPos = ConvertToLocalSpace(data.mousePos);
							m_DragContainerStartPos = m_Container->GetPosition();
							m_DragStartTime = g_GameApplication->GetAppTime();
							diff = Vector();
						}
						if (m_IsDrag)
						{
							if (m_Direction == CListView_DirectionType::Horizontal)
							{
								UpdatePosition(m_DragContainerStartPos.x + diff.x);
							}
							else
							{
								UpdatePosition(m_DragContainerStartPos.y + diff.y);
							}
						}
					}					
					if (type == CBaseWidget_EventType::Event_MouseMove)
					{
						if (m_IsDrag)
						{
							return ReturnCodes::Block;
						}							
					}					
				}
				return ReturnCodes::Skip;
			}

			//////////////////////////////////////////////////////////////////////////
			void CListViewWidget::UpdatePosition(float pos)
			{
				
				if (m_Direction == CListView_DirectionType::Horizontal)
				{
					if (m_Container->GetSize().x < GetSize().x)return;
					if (pos > 0.0f)pos = 0.0f;
					else if (pos < GetSize().x - m_Container->GetSize().x)pos = GetSize().x - m_Container->GetSize().x;
					m_Container->SetPositionX(pos);

					if (m_Slider)
					{
						float percent = fabs(pos) / fabs((GetSize().x - m_Container->GetSize().x));
						m_Slider->SetPercent(percent);
					}

				}
				else
				{
					if (m_Container->GetSize().y < GetSize().y)return;
					if (pos > 0.0f)pos = 0.0f;
					else if (pos < GetSize().y - m_Container->GetSize().y)pos = GetSize().y - m_Container->GetSize().y;
												
					m_Container->SetPositionY(pos);										
					

					if (m_Slider)
					{
						float percent = fabs(pos) / fabs((GetSize().y - m_Container->GetSize().y));
						m_Slider->SetPercent(percent);
					}
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CListViewWidget::AddWidget(CBaseWidget * widget)
			{
				widget->SetAnchor(HotSpot::HS_UL);
				widget->SetHotSpot(HotSpot::HS_UL);
				widget->UpdateBoundPoints();

				Vector newSize = m_Container->GetSize();
				if (m_Direction == CListView_DirectionType::Horizontal)
				{
					widget->SetPositionX(newSize.x);
					newSize += Vector(m_ItemsDistance + (widget->GetBounds()[1].x - widget->GetBounds()[0].x), 0.0f);
				}
				else
				{
					widget->SetPositionY(newSize.y);
					newSize += Vector(0.0f, m_ItemsDistance + (widget->GetBounds()[2].y - widget->GetBounds()[0].y));
				}                              

				m_Container->SetSize(newSize);
				m_Container->AddChild(widget);

				if (m_Slider)
				{
					float percent = GetSize().x / m_Container->GetSize().x;
					if (percent > 1.0f)percent = 1.0f;
					m_Slider->SetMode(CSliderWidget_Mode::List, percent);
				}				
			}

			//////////////////////////////////////////////////////////////////////////
			void CListViewWidget::Clear()
			{
				m_Container->SetSize(Vector());
				m_Container->RemoveAllChilds(true);
				m_Container->SetPosition(0.0f, 0.0f);
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CListViewWidget::Update(const float dt)
			{
				CBaseWidget::Update(dt);

				if (m_Inertion != 0.0f)
				{
					if (m_Inertion > 0.0f)
					{
						m_Inertion -= m_InertionStopage*dt;
						if (m_Inertion <= 0.0f)m_Inertion = 0.0f;
					}
					else if (m_Inertion < 0.0f)
					{
						m_Inertion += m_InertionStopage*dt;
						if (m_Inertion >= 0.0f)m_Inertion = 0.0f;
					}
					float pos = 0.0f;
					if (m_Direction == CListView_DirectionType::Horizontal)
					{
						pos = m_Container->GetPosition().x+m_Inertion*dt;
					}
					else
					{
						pos = m_Container->GetPosition().y + m_Inertion*dt;
					}
					UpdatePosition(pos);
				}

				return ReturnCodes::Skip;
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CListViewWidget::Draw(CRenderState & renderState)
			{
				auto stencil = g_StencilHelper;
				auto size = GetSize();
				stencil->Begin();										
				stencil->DrawRectangle(&m_Transform, size);
				stencil->Switch();
				return ReturnCodes::Skip;
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CListViewWidget::AfterDraw(CRenderState & renderState)
			{
				g_StencilHelper->End();
				return ReturnCodes::Skip;
			}
		}
	}
}