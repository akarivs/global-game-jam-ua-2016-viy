#ifndef OPTIMUS_UI_INPUT_TEXT_WIDGET
#define OPTIMUS_UI_INPUT_TEXT_WIDGET

#include "BaseWidget.h"
#include "ImageWidget.h"
#include "TextWidget.h"
#include "../optimus_macro.h"
#include "../optimus_enum.h"
#include <Framework/Structures.h>

using namespace NBG;
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
			/** @brief Класс, реализующий работу с вводом текста.
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			enum class CInputTextWidget_EventType
			{
				Event_TextChange,				
			};
			typedef std::function < void(CInputTextWidget_EventType type, void * userData) > CInputTextWidget_EventListener;
			class CInputTextWidget : public CBaseWidget
			{
			public:
				/// @name Конструктор/деструктор
				OPTIMUS_CREATE(CInputTextWidget);
				static CInputTextWidget* Create(FontDescription * fontDesc);
				virtual ~CInputTextWidget();

				virtual void OnLoadFromXMLNode(const pugi::xml_node &node);

				///Инициализация объекта строкой.
				virtual void Init();

				//////////////////////////////////////////////////////////////////////////
				void InitLabel(FontDescription * fontDesc);

				//////////////////////////////////////////////////////////////////////////
				virtual void OnHotSpotChanged();
				virtual void OnColorChanged();				

				//////////////////////////////////////////////////////////////////////////
				void SetInputTextListener(CInputTextWidget_EventListener callback);

				void SetTextFocused(const bool focused);

				/// Установить текст.		
				void SetText(const std::wstring& text);

				/// Получить текст
				const std::wstring & GetText(){ return m_Symbols; };

				/// Обновить.
				virtual ReturnCodes Update(const float dt);

				/// Переместить курсор в начало.
				void MoveCursorToStart();

				/// Переместить курсор в конец.
				void MoveCursorToEnd();

				/// Сбросить форму.
				void Reset(){ m_Symbols = m_DefaultSymbols; m_CursorPos = m_DefaultSymbols.size(); SetText(m_Symbols); UpdateCursor(); };
			private:
				CInputTextWidget();
				ReturnCodes EventListener(CBaseWidget * sender, CBaseWidget_EventType type, CBaseWidget_EventData data);
				

				/// Обновить курсор.
				void UpdateCursor();

				/// Позиция курсора.
				int m_CursorPos;

				/// Время текущего состояния курсора - вкл/выкл.
				float m_CursorAnimTime;

				/// Показуется ли в данный момент курсор.
				bool m_IsShowCursor;

				/// Строка
				CTextWidget * m_Label;

				/// Курсор
				CTextWidget * m_CursorLabel;

				/// Фон
				CImageWidget * m_Background;

				/// Символы введённые.
				std::wstring m_Symbols;

				/// Маска ввода
				GETTER_SETTER(std::wstring, m_Mask, Mask);				

				/// Символы по умолчанию.
				std::wstring m_DefaultSymbols;

				CInputTextWidget_EventListener m_InputTextEventListener;

				/// В фокусе ли.
				bool m_IsTextFocused;

				/// Максимальное количество символов.
				GETTER_SETTER(int, m_MaxSymbols, MaxSymbols);
			};
		}
	}
}
#endif //OPTIMUS_UI_INPUT_TEXT_WIDGET
