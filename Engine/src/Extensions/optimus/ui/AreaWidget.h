#ifndef OPTIMUS_UI_AREA_WIDGET
#define OPTIMUS_UI_AREA_WIDGET

#include <Framework/Utils/MathUtils.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Global/Resources/TextureResource.h>
#include <Framework/Helpers/AtlasHelper.h>

#include <../External/xml/pugixml.hpp>
#include <queue>

#include "ImageWidget.h"
#include "../optimus_macro.h"
#include "../optimus_enum.h"

using namespace NBG;
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{					
			enum class EAreaType
			{
				Rectangle,
				Circle,
				Polygon
			};
			class CAreaWidget : public CBaseWidget
			{
			public:
				OPTIMUS_CREATE(CAreaWidget);				
				virtual ~CAreaWidget();

				virtual bool IsContains(const Vector &pos);


				virtual void OnLoadFromXMLNode(const pugi::xml_node &node);
				virtual void Init();
				

				virtual ReturnCodes AfterDraw();
			protected:
				CAreaWidget();

				GETTER_SETTER(EAreaType, m_AreaType, AreaType);
				GETTER_SETTER(float, m_Radius, Radius);
				GETTER_SETTER(std::vector<Vector>, m_Polygon, Polygon);				
			};
		}
	}
}
#endif //OPTIMUS_UI_AREA_WIDGET