#include "ButtonWidget.h"

#include <Framework.h>
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{

			//////////////////////////////////////////////////////////////////////////
			CButtonWidget::CButtonWidget():CBaseWidget()
			{				
				m_IsTrustedDisabled = false;
			}

			//////////////////////////////////////////////////////////////////////////
			CButtonWidget::~CButtonWidget()
			{
				
			}

			//////////////////////////////////////////////////////////////////////////
			void CButtonWidget::Init()
			{
				m_IsUseAlphaMask = false;
				m_IsCatchTouch = true;
				SetEventListener(OPTIMUS_CALLBACK_3(&CButtonWidget::EventListener,this));
				m_ButtonEventListener = nullptr;

				m_Normal	= CImageWidget::Create();
				m_Over		= CImageWidget::Create();
				m_Clicked	= CImageWidget::Create();
				m_Disabled	= CImageWidget::Create();

				AddChild(m_Normal);
				AddChild(m_Over);
				AddChild(m_Clicked);
				AddChild(m_Disabled);

				m_SoundName = "button_click";
			}

			//////////////////////////////////////////////////////////////////////////
			void CButtonWidget::OnLoadFromXMLNode(const pugi::xml_node &node)
			{
				auto size = GetSize();
				if (m_IsScale9Enabled)
				{
					SetSize(size);
				}
				SetTexture(node.attribute("texture").value());
				m_IsUseAlphaMask = node.attribute("use_alpha").as_bool();
				if (node.attribute("sound").empty() == false)
				{
					m_SoundName = node.attribute("sound").value();
				}				
			}

			//////////////////////////////////////////////////////////////////////////
			bool CButtonWidget::IsContains(const Vector &pos)
			{
				if (m_IsUseAlphaMask)
				{
#ifdef NBG_ANDROID
					///TODO: �������� ��������� �����-����� ��� ��������
					return CBaseWidget::IsContains(pos);
#elif defined (NBG_IOS)
                    return CBaseWidget::IsContains(pos);
#endif
					m_CurrentState->SetUseAlphaMask(true);
					return m_CurrentState->IsContains(pos);
				}
				return CBaseWidget::IsContains(pos);
			}

			//////////////////////////////////////////////////////////////////////////
			void CButtonWidget::OnSetDisabled()
			{
				if (m_IsDisabled)
				{
					SetState(m_Disabled);
				}
				else
				{
					SetState(m_Normal);
				}
				m_IsDisabled = false;
			}

			//////////////////////////////////////////////////////////////////////////
			bool CButtonWidget::IsDisabled()
			{
				return false;
			}

			//////////////////////////////////////////////////////////////////////////
			void CButtonWidget::SetButtonListener(CButtonWidget_EventListener callback)
			{
				m_ButtonEventListener = callback;
			}


			//////////////////////////////////////////////////////////////////////////
			void CButtonWidget::SetTexture(const std::string &textureFolderPath)
			{
				std::string path = textureFolderPath;
				if (path.substr(path.length() - 1, 1) != "/")path += "/";

				
				m_Normal->SetTexture(path + "normal.png");
				if (!m_IsScale9Enabled)
				{
					SetSize(m_Normal->GetSize());
					m_Normal->SetTexture(path + "normal.png");
					
				}
				
				m_Over->SetTexture(path + "over.png");
			
				m_Clicked->SetTexture(path + "clicked.png");				
				if (g_FileSystem->IsFileExists(path + "disabled.png"))
				{
					m_Disabled->SetTexture(path + "disabled.png");
				}
				else if (g_AtlasHelper->GetTextureDescription(path + "disabled.png") != nullptr)
				{
					m_Disabled->SetTexture(path + "disabled.png");					
				}
				else
				{
					m_Disabled->SetTexture(path + "normal.png");
				}
				
				
				if (m_IsScale9Enabled)
				{
					OnSizeChanged();
				}

				m_Over->SetVisible(false);
				m_Clicked->SetVisible(false);
				m_Disabled->SetVisible(false);

				m_CurrentState = nullptr;
				SetState(m_Normal);
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CButtonWidget::EventListener(CBaseWidget * sender, CBaseWidget_EventType type, CBaseWidget_EventData data)
			{
				if (type == CBaseWidget_EventType::Event_MouseUp)
				{
					if (m_CurrentState == m_Disabled)
					{
						if (m_ButtonEventListener)
						{
							m_ButtonEventListener(this,CButtonWidget_EventType::Event_DisabledClick);							
						}
                        CEvent evt((int)CButtonWidget_EventType::Event_DisabledClick);
                        evt.target = this;
                        DispatchEvent(evt);
						return ReturnCodes::Block;
					}
					else if (m_CurrentState == m_Clicked && m_IsWasFocusedOnMouseDown)
					{
						SetState(m_Over);
						g_SoundManager->Play(m_SoundName);

						if (m_ButtonEventListener)
						{							
							m_ButtonEventListener(this,CButtonWidget_EventType::Event_Click);
							if (m_CurrentState != m_Disabled)
							{
								SetState(m_Normal);
							}                            
						}
                        
                        CEvent evt((int)CButtonWidget_EventType::Event_Click);
                        evt.target = this;
                        DispatchEvent(evt);

						return ReturnCodes::Block;
					}					
				}		
				else if (type == CBaseWidget_EventType::Event_Over)
				{
					if (m_CurrentState == m_Normal)
					{
						SetState(m_Over);						
					}
					return ReturnCodes::Block;
				}
				else if (type == CBaseWidget_EventType::Event_Out)
				{
					if (m_CurrentState != m_Disabled)
					{
						if (m_IsWasFocusedOnMouseDown == false)
						{
							SetState(m_Normal);
						}
					}
				}
				else if (type == CBaseWidget_EventType::Event_MouseDown)
				{
					if (m_CurrentState != m_Disabled)
					{
						SetState(m_Clicked);
					}
					return ReturnCodes::Block;
				}
				else if (type == CBaseWidget_EventType::Event_MouseUpOutside)
				{
					if (m_CurrentState != m_Disabled)
					{
						SetState(m_Normal);
					}
				}
				return ReturnCodes::Skip;
			}

			//////////////////////////////////////////////////////////////////////////
			void CButtonWidget::SetState(CImageWidget * state)
			{
				if (m_CurrentState == state)return;
				if (m_CurrentState)
				{
					m_CurrentState->SetVisible(false);
				}								
				m_CurrentState = state;
				m_CurrentState->SetVisible(true);

				

				if (m_CurrentState == m_Over)
				{
					if (g_System->IsMobileSystem())
					{
						m_Over->SetVisible(false);
						m_Normal->SetVisible(true);
					}
				}
			}

		}
	}
}