#ifndef OPTIMUS_UI_SLIDER_WIDGET
#define OPTIMUS_UI_SLIDER_WIDGET

#include <Framework/Utils/MathUtils.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Global/Resources/TextureResource.h>
#include <Framework/Helpers/AtlasHelper.h>

#include <../External/xml/pugixml.hpp>
#include <queue>

#include "ImageWidget.h"
#include "ButtonWidget.h"
#include "../optimus_macro.h"
#include "../optimus_enum.h"

namespace NBG
{
	namespace optimus
	{
		namespace ui
		{			
			class CSliderWidget;
			enum class CSliderWidget_EventType
			{
				Event_ValueChange,
				Event_DragComplete,
			};			

			enum class CSliderWidget_Mode
			{
				List,
				Slider,
			};
			typedef std::function < void(CSliderWidget * sender, CSliderWidget_EventType type, const float value) > CSliderWidget_EventListener;
			class CSliderWidget : public CBaseWidget
			{
			public:
				OPTIMUS_CREATE(CSliderWidget);
				static CSliderWidget * Create(const std::string &baseTexture, const std::string &fillTexture, const std::string buttonPath = "")
				{
					auto ret = Create();
					ret->Init(baseTexture, fillTexture, buttonPath);
					return ret;
				}
				virtual ~CSliderWidget();

				virtual ReturnCodes OnMouseMove(const Vector &mousePos);
				virtual ReturnCodes OnMouseUp(const Vector &mousePos, const int mouseButton);
				virtual ReturnCodes OnMouseDown(const Vector &mousePos, const int mouseButton);

				void SetSliderListener(CSliderWidget_EventListener listener);

				void OnLoadFromXMLNode(const pugi::xml_node &node);
				virtual void Init();				
				void Init(const std::string &baseTexture, const std::string &fillTexture, const std::string buttonPath = "");

				void SetPercent(const float percent, bool callListener = false);				
				void SetMode(CSliderWidget_Mode mode, float sliderPercent = 0.0f);
			protected:
				CSliderWidget();

				CImageWidget * m_Base;
				CImageWidget * m_Fill;
				CButtonWidget * m_Button;

				bool m_IsDrag;
				Vector m_DragStartPos;
				float m_DragPercent;

				CSliderWidget_EventListener m_SliderListener;

				float m_Percent;

				CSliderWidget_Mode m_Mode;
			};
		}
	}
}
#endif //OPTIMUS_UI_SLIDER_WIDGET