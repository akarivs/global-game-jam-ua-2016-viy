#ifndef OPTIMUS_UI_SPRITE_ANIMATION_WIDGET
#define OPTIMUS_UI_SPRITE_ANIMATION_WIDGET

#include <Framework/Datatypes.h>

#include "ImageWidget.h"
#include "../Helpers/actions/CallbackLUAAction.h"

namespace NBG
{
	namespace optimus
	{
		namespace ui
		{			
			/** @brief Класс, реализующий работу с анимированным 2D объектом.
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CSpriteAnimationWidget : public CBaseWidget
			{
			public:
				OPTIMUS_CREATE(CSpriteAnimationWidget);								
				virtual ~CSpriteAnimationWidget();

				/// @name Загрузка объекта из XML ноды
				virtual void OnLoadFromXMLNode(const pugi::xml_node &node);

				virtual void OnBlendModeChanged();

				virtual void Init();
				/// Инициализация объекта
				/// @param texture		- задаёт анимированную текстуру. Текстуры должны лежать в одной папке и быть последовательно названы. Если путь к папке images/anim/test, то нужно передать images/anim/test.png.
				/// @param framesCount	- количество кадров в анимации.
				/// @param interval		- интервал между кадрами.
				/// @param animType		- тип анимации.
				/// @param startFrame	- номер стартового кадра анимации.								
				void InitAnimation(const std::string &texture, const int framesCount, const float interval, AnimationType animType, const int startFrame);

				/// Инициализация объекта
				/// @param texture		- задаёт анимированную текстуру. Текстуры должны лежать в одной папке и быть последовательно названы. Если путь к папке images/anim/test, то нужно передать images/anim/test.png.
				/// @param framesMap	- карта фреймов
				/// @param interval		- интервал между кадрами.
				/// @param animType		- тип анимации.				
				void InitAnimation(const std::string &texture, const std::vector<int> framesMap, const float interval, AnimationType animType);				
				

				///LUA_HELPERS
				static CSpriteAnimationWidget * __LuaCreate(const std::string &texture, const int framesCount, const float interval, const int animType, const int startFrame)
				{
					auto wdg = Create();
					wdg->InitAnimation(texture, framesCount, interval, (AnimationType)animType, startFrame);
					return wdg;
				}

				void __LuaSetFramesMap(const std::string &map);


				/// Установка текстуры.
				void SetTexture(const std::string &texture);
				void SetTexture(CTextureResource * texture);

				/// Переключиться на следующий кадр анимации.
				void NextFrame();

				/// Получить текущий кадр анимации.
				int GetCurrentFrame()
				{
					return m_CurrentFrame;
				};

				/// Приостановить анимацию.
				void Pause();

				/// Остановить анимацию.
				void Stop();

				/// Проиграть анимацию.
				void Play();

				/// Установить случайный кадр.
				void RandomFrame();

				/// Остановлена ли анимация.
				bool IsStoped()
				{
					return m_IsStoped;
				};

				/// Обновить анимацию.
				virtual ReturnCodes Update(const float dt);

				///Установить повтор текстур
				void SetRepeatedTexture();

				///Установить коллбек на кадр
				void SetCallback(const int frame, std::function<void()> callback);

				///Установить коллбек из LUA на кадр
				void SetCallbackLua(const int frame, helpers::CCallbackLUAAction * callback);


				//////////////////////////////////////////////////////////////////////////
				void SetUV(FRect uv)
				{
					m_UV = uv;
					m_Image->SetUV(uv);
				}

	
				

				///Свойство: множитель скорости анимации
				GETTER_SETTER(float, m_SpeedMultiplier, SpeedMultiplier);

			private:
				CSpriteAnimationWidget();
				/// Объект, в который рисуется графика.
				CImageWidget * m_Image;

				/// Текущий кадр анимации.
				int m_CurrentFrame;

				/// Количество кадров анимации.
				GETTER_SETTER(int, m_FramesCount, FramesCount);

				/// Время между показом кадра.
				GETTER_SETTER(float, m_FramesInterval, m_FramesInterval);

				/// Текущее время.вто
				float m_CurrentTime;

				/// Стартовый кадр.
				GETTER_SETTER(int, m_StartFrame, StartFrame);

				/// Стоит ли анимация на пауза.
				bool m_IsPaused;

				/// Название анимации, без номера кадра.
				std::string m_AnimName;

				/// Расширение файла.
				std::string m_AnimExtension;

				/// Тип анимации.
				AnimationType m_AnimType;

				/// Остановлена ли анимация.
				bool m_IsStoped;

				/// Если режим PingPong - тогда смотрим в какую сторону воспроизводить.
				bool m_IsPongForward;

				///Карта с коллбеками
				std::map<int, std::function<void()>>m_Callbacks;

				///Карта с коллбеками LUA
				std::map<int, helpers::CCallbackLUAAction *>m_CallbacksLua;

				///Карта кадров
				std::vector<int> m_FramesMap;
				std::vector<CTextureResource*> m_FramesResourceMap;

				FRect m_UV;
			};
		}
	}
}
#endif //OPTIMUS_UI_SPRITE_ANIMATION_WIDGET

