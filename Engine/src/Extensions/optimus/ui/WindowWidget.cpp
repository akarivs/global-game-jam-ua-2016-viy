#include "WindowWidget.h"

#include "../optimus.h"
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
			//////////////////////////////////////////////////////////////////////////
			CWindowWidget::CWindowWidget() :CBaseWidget()
			{


			}

			//////////////////////////////////////////////////////////////////////////
			CWindowWidget::~CWindowWidget()
			{

			}

			//////////////////////////////////////////////////////////////////////////
			void CWindowWidget::Init()
			{				
				SetCatchKeyboard(true);
				SetCatchTouch(true);
				SetUpdatable(true);		

				SetDisabled(true);
				SetOpacity(0.0f);

				m_IsShowing = false;
				m_IsModal = false;
				m_IsDarker = true;

				m_OpenSound = "open_window";
				m_CloseSound = "close_window";

				m_IsCloseOnEsc = true;
			}

			//////////////////////////////////////////////////////////////////////////
			void CWindowWidget::OnLoadFromXMLNode(const pugi::xml_node &node)
			{
				SetModal(node.attribute("is_modal").as_bool());
				if (node.attribute("is_darker").empty() == false)
				{
					SetDarker(node.attribute("is_darker").as_bool());
				}				
				if (node.attribute("esc").empty() == false)
				{
					SetCloseOnEsc(node.attribute("esc").as_bool());
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CWindowWidget::SetModal(bool isModal)
			{
				if (m_IsModal == isModal)return;
				m_IsModal = isModal;				
			}


			//////////////////////////////////////////////////////////////////////////
			void CWindowWidget::Show()
			{
				m_IsShowing = true;
				SetDisabled(false);

				OnShowStart();
				ShowFunction();

				if (m_OpenSound.size() > 0)
				{
					g_SoundManager->Play(m_OpenSound);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CWindowWidget::Hide()
			{
				m_IsShowing = false;
				SetDisabled(true);
							

				OnHideStart();
				HideFunction();

				if (m_OpenSound.size() > 0)
				{
					g_SoundManager->Play(m_CloseSound);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CWindowWidget::ShowFunction()
			{
				SetOpacity(0.0f);
				SetScale(0.4f);
				auto queue = helpers::CActionQueue::Create();
				queue->AddAction(helpers::CShowAction::Create(this, 0.2f, optimus::Tween::EASE_IN_SIN, 0.0f));
				queue->AddAction(helpers::CScaleToAction::Create(this, 0.2f, Vector(1.0f, 1.0f, 1.0f), optimus::Tween::EASE_IN_SIN));
				queue->AddAction(helpers::CCallbackAction::Create(OPTIMUS_CALLBACK_0(&CWindowWidget::OnShowEnd,this)));
				AddChild(queue);
			}
			
			//////////////////////////////////////////////////////////////////////////
			void CWindowWidget::HideFunction()
			{
				auto queue = helpers::CActionQueue::Create();
				queue->AddAction(helpers::CHideAction::Create(this, 0.2f, optimus::Tween::EASE_OUT_SIN, 0.0f));
				queue->AddAction(helpers::CScaleToAction::Create(this, 0.2f, Vector(1.3f, 1.3f, 1.3f), optimus::Tween::EASE_OUT_SIN));
				queue->AddAction(helpers::CCallbackAction::Create(OPTIMUS_CALLBACK_0(&CWindowWidget::OnHideEnd, this)));
				AddChild(queue);
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CWindowWidget::OnKeyUp(const int key)
			{
				auto res = CBaseWidget::OnKeyUp(key); 

				if (m_IsCloseOnEsc)
				{
					if (key == KEY_ESCAPE && res != ReturnCodes::Block)
					{
						Hide();
						return ReturnCodes::Block;
					}
				}

				if (m_IsModal)
				{ 
					return ReturnCodes::Block; 
				} 
				return res;
			}

			
			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CWindowWidget::Update(const float dt)
			{
				CBaseWidget::Update(dt);
				if (m_IsModal)
				{
					if (m_IsShowing)
					{
						return ReturnCodes::Block;
					}
				}
				return ReturnCodes::Skip;
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CWindowWidget::Draw(CRenderState &rs)
			{				
				if (m_IsDarker && m_IsVisible)
				{
					if (m_IsShowing)
					{
						g_Render->DrawLine(Vector(-1000, 0), Vector(1000, 0), 600, Color(m_Color.a/2.0f,0.0f,0.0f,0.0f));
					}
				}
				return ReturnCodes::Skip;
			}
		}
	}
}
