#include "ImageWidget.h"
#include "BaseWidget.h"

#include <Framework.h>

namespace NBG
{
	namespace optimus
	{
		namespace ui
		{		
			
			//////////////////////////////////////////////////////////////////////////
			CImageWidget::CImageWidget() :CBaseWidget()
			{

			}

			//////////////////////////////////////////////////////////////////////////
			CImageWidget::~CImageWidget()
			{
				if (m_Resource)
				{
					g_ResManager->ReleaseResource(m_Resource);
					m_Resource = nullptr;
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CImageWidget::Init()
			{
				m_IsNeedToUpdateMesh = false;
				m_BlockMeshUpdate = false;
				m_Mesh.Init(4);
				m_MeshType = MeshType::Mesh_Rectangle;
				m_LockUpdateScale9 = false;


				m_IsCheckOutOfScreen = false;

				m_CutXPercent = 1.0f;
				m_CutYPercent = 1.0f;

				m_UV.left = 0.0f;
				m_UV.top = 0.0f;
				m_UV.right = 1.0f;
				m_UV.bottom = 1.0f;

				m_Resource = nullptr;
				m_IsTileTexture = false;
				m_IsRepeatedTexture = false;

				m_IsUseAlphaMask = false;
                
                m_AlphaMask = nullptr;

				m_Shader = nullptr;
				m_Desc = nullptr;

				m_Scale9Container = CBaseWidget::Create();
				m_Scale9Container->SetLayer(-10000);
				//AddChild(m_Scale9Container);
			}

			//////////////////////////////////////////////////////////////////////////
			void CImageWidget::OnLoadFromXMLNode(const pugi::xml_node &node)
			{
				auto size = GetSize();
				if (node.attribute("texture").empty() == false)
				{
					SetTexture(node.attribute("texture").value());
				}				
				if (m_IsScale9Enabled)
				{
					SetSize(size);		
					m_ImageSize = size; 
					AddChild(m_Scale9Container);
				}
				if (node.attribute("mesh_warp").empty() == false)
				{
					std::string meshWarp = node.attribute("mesh_warp").value();
					auto strList = StringUtils::ExplodeString(meshWarp, ';');
					for (int i = 0; i < strList.size(); i++)
					{
						if (i == 4)break;
						auto coords = StringUtils::ExplodeString(strList[i], ',');
						if (coords.size() == 2)
						{
							m_Mesh.rectangleOffsets[i].x = StringUtils::ToFloat(coords[0]);
							m_Mesh.rectangleOffsets[i].y = StringUtils::ToFloat(coords[1]);
						}
					}
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CImageWidget::SetTexture(const std::string &texturePath)
			{
				m_TexturePath = texturePath;

				//����������� ���������� ��������.
				if (m_Resource != nullptr)
				{
					g_ResManager->ReleaseResource(m_Resource);
					m_Resource = nullptr;
				}

				std::string path = texturePath;
				path = StringUtils::StringReplace(path, std::string("%LOCALE%"), g_LocaleManager->GetLocale());

				m_Desc = g_AtlasHelper->GetTextureDescription(g_EditionHelper->ConvertPath(path));


				///���� ����� �������� � ������
				if (m_Desc != nullptr)
				{
					m_Offset = m_Desc->offset;
					path = g_EditionHelper->ConvertPath(m_Desc->atlasID);
					m_Resource = CAST(CTextureResource*, g_ResManager->GetResource(path));
					SetSize(m_Desc->baseSize.x, m_Desc->baseSize.y);
					m_ImageSize = m_Desc->size;
					m_UV = m_Desc->uv;
					m_UseAtlas = true;
					UpdateOffset();
				}
				///����� ������� ��������� �������� � �����
				else
				{
					m_Offset = Vector();
					m_Resource = CAST(CTextureResource*, g_ResManager->GetResource(path.c_str()));
					if (m_Resource)
					{
						SetSize(m_Resource->GetTextureWidth(), m_Resource->GetTextureHeight());
						m_ImageSize = m_Size;
						m_UseAtlas = false;
                                                
						auto texture = m_Resource->GetTexture();
						if (texture->GetSize().x == m_Resource->GetTextureWidth() &&
							texture->GetSize().y == m_Resource->GetTextureHeight())
						{
							m_UV.left = 0.0f;
							m_UV.top = 0.0f;
							m_UV.right = 1.0f;
							m_UV.bottom = 1.0f;
						}
						else
						{
							m_UV.left = 0.0f;
							m_UV.top = 0.0f;
							m_UV.right = texture->GetSize().x / (float)m_Resource->GetTextureWidth();
							m_UV.bottom = texture->GetSize().y / (float)m_Resource->GetTextureHeight();
							SetSize(texture->GetSize());
						}	
						UpdateScale9Sprite();
					}
					else
					{
						SetTexture(NBG::CTextureResource::GetDefaultTexture());
					}
				}
				m_IsNeedToUpdateMesh = true;
			}

			//////////////////////////////////////////////////////////////////////////
			void CImageWidget::SetTexture(NBG::CTextureResource * textureRes)
			{
				if (textureRes)
				{
					///����������� ���������� ��������.
					if (m_Resource != nullptr)
					{
						g_ResManager->ReleaseResource(m_Resource);
						m_Resource = nullptr;
					}
					m_Resource = textureRes;
					m_TexturePath = m_Resource->GetPath();
					textureRes->IncreaseRefsCount();
					SetSize(m_Resource->GetTextureWidth(), m_Resource->GetTextureHeight());
					m_ImageSize = m_Size; 
					m_UseAtlas = false;
					m_IsNeedToUpdateMesh = true;
					UpdateScale9Sprite();
				}
				else
				{
					///����������� ���������� ��������.
					if (m_Resource != nullptr)
					{
						g_ResManager->ReleaseResource(m_Resource);
						m_Resource = nullptr;
					}					
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CImageWidget::SetRepeatedTexture(const bool isRepeated)
			{
				m_IsRepeatedTexture = isRepeated;
				if (m_IsRepeatedTexture && m_Resource)
				{
					g_GameApplication->GetTextureManager()->SetRepeatTexture(m_Resource->GetTexture());
				}
			}


			//////////////////////////////////////////////////////////////////////////
			char ** CImageWidget::GetAlphaMask()
			{
				if (m_Resource)
				{
					NBG::FRect rectangle;
					rectangle.left = 0;
					rectangle.right = GetSize().x;
					rectangle.top = 0;
					rectangle.bottom = GetSize().y;

					m_MaskSize = GetSize();

					if (m_UseAtlas)
					{
						Vector atlasSize(m_Resource->GetTextureWidth(), m_Resource->GetTextureHeight());
						rectangle.left = m_Desc->uv.left * atlasSize.x;
						rectangle.right = m_Desc->uv.right * atlasSize.x;
						rectangle.top = m_Desc->uv.top * atlasSize.y;
						rectangle.bottom = m_Desc->uv.bottom * atlasSize.y;

						m_MaskSize.x = rectangle.right - rectangle.left;
						m_MaskSize.y = rectangle.bottom - rectangle.top;
					}
					return g_Render->GetAlphaFromRegion(m_Resource->GetTexture(), rectangle);
				}
				return nullptr;
			}


			//////////////////////////////////////////////////////////////////////////
			bool CImageWidget::IsContains(const Vector &pos)
			{
				if (m_IsUseAlphaMask)
				{
					if (CBaseWidget::IsContains(pos))
					{
						Vector convertedPos = ConvertToLocalSpace(pos);
						if (m_UseAtlas)
						{
							convertedPos.x -= m_Desc->offset.x;
							convertedPos.y -= m_Desc->offset.y;
						}

						if (convertedPos.x < 0 || convertedPos.y < 0)return false;
						if (convertedPos.x >= m_MaskSize.x || convertedPos.y >= m_MaskSize.y)return false;
						if (m_AlphaMask[(int)convertedPos.x][(int)convertedPos.y] == 1)
						{
							return true;
						}
					}
					return false;
				}
				else
				{
					return CBaseWidget::IsContains(pos);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CImageWidget::UpdateMesh(CRenderState & renderState)
			{
				if (m_BlockMeshUpdate)return;
				Texture * texture = nullptr;
				if (m_Resource)
				{
					texture = m_Resource->GetTexture();
				}
				if (m_MeshType == MeshType::Mesh_Rectangle)
				{
					tempMatrix.CreateFromTransform(m_Transform, true);
					tempMatrix.Multiply(renderState.GetMatrixPointer());
					m_Mesh.SetRectData(&m_UV, &tempMatrix, m_ImageSize, m_Color, texture, m_IsTileTexture, m_Offset);
				}
				else if (m_MeshType == MeshType::Mesh_Custom)
				{
					m_Mesh.UpdateVertexes(GetTransform(), texture, m_IsTileTexture);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CImageWidget::OnHotSpotChanged()
			{
				//m_IsNeedToUpdateMesh = true;
				UpdateOffset();
			}

			//////////////////////////////////////////////////////////////////////////
			void CImageWidget::UpdateOffset()
			{				
				if (m_Desc)
				{
					m_Offset = m_Desc->offset;
				}
			}

			//////////////////////////////////////////////////////////////////////////
			Vector CImageWidget::GetSizeForAnchor()
			{
				if (m_Desc)
				{
					if (m_IsScale9Enabled)
					{
						return m_Size * Vector(m_CutXPercent, m_CutYPercent);
					}
					return m_Desc->baseSize * Vector(m_CutXPercent, m_CutYPercent);
				}
				return CBaseWidget::GetSizeForAnchor();			
			}

			//////////////////////////////////////////////////////////////////////////
			void CImageWidget::Cut(const float xPercent, const float yPercent, bool xFlip, bool yFlip)
			{
				m_CutXPercent = xPercent;
				m_CutYPercent = yPercent;
				if (m_UseAtlas)
				{
					float width = m_Desc->uv.right - m_Desc->uv.left;
					float height = m_Desc->uv.bottom - m_Desc->uv.top;
					width *= xPercent;
					height *= yPercent;

					if (xFlip)
					{
						m_UV.left = m_Desc->uv.right - width;
						m_UV.right = m_Desc->uv.right;
					}
					else
					{
						m_UV.left = m_Desc->uv.left;
						m_UV.right = m_Desc->uv.left + width;
					}

					if (yFlip)
					{
						m_UV.top = m_Desc->uv.bottom - height;
						m_UV.bottom = m_Desc->uv.bottom;
					}
					else
					{
						m_UV.top = m_Desc->uv.top;
						m_UV.bottom = m_Desc->uv.top + height;
					}
					SetSize(m_Desc->baseSize.x*xPercent, m_Desc->baseSize.y*yPercent);
					m_ImageSize.x = m_Desc->size.x*xPercent;
					m_ImageSize.y = m_Desc->size.y*yPercent;					
				}
				else
				{
					float atlasHalfWidthTexel = 0.5f / (float)m_Resource->GetTextureWidth();
					float atlasHalfHeightTexel = 0.5f / (float)m_Resource->GetTextureHeight();

					if (xFlip)
					{
						m_UV.left = 1.0f - xPercent;
						m_UV.right = 1.0f;
					}
					else
					{
						m_UV.left = 0.0f;
						m_UV.right = xPercent;
					}


					if (yFlip)
					{
						m_UV.top = 1.0f - yPercent;
						m_UV.bottom = 1.0f;
					}
					else
					{
						m_UV.top = 0.0f;
						m_UV.bottom = yPercent;
					}


					m_UV.left += atlasHalfWidthTexel;
					m_UV.right -= atlasHalfWidthTexel;
					m_UV.top += atlasHalfHeightTexel;
					m_UV.bottom -= atlasHalfHeightTexel;

					Vector size = m_Resource->GetTexture()->GetSize();
					SetSize(size.x*xPercent, size.y*yPercent);
					m_ImageSize = m_Size;
				}
				m_IsNeedToUpdateMesh = true;
			}

			//////////////////////////////////////////////////////////////////////////
			void CImageWidget::UpdateScale9Sprite()
			{
				if (m_LockUpdateScale9)return;
				if (m_IsScale9Enabled == false)return;
				if (!m_Resource)return;

				m_LockUpdateScale9 = true;

				if (m_Scale9Container->GetChilds().size() == 0)
				{
					auto ul = CImageWidget::Create();
					auto mu = CImageWidget::Create();
					auto ur = CImageWidget::Create();
					auto ml = CImageWidget::Create();
					auto mid = CImageWidget::Create();
					auto mr = CImageWidget::Create();
					auto dl = CImageWidget::Create();
					auto md = CImageWidget::Create();
					auto dr = CImageWidget::Create();


					m_Scale9Container->SetSize(GetSize());

					m_Scale9Container->AddChild(ul);
					m_Scale9Container->AddChild(mu);
					m_Scale9Container->AddChild(ur);
					m_Scale9Container->AddChild(ml);
					m_Scale9Container->AddChild(mid);
					m_Scale9Container->AddChild(mr);
					m_Scale9Container->AddChild(dl);
					m_Scale9Container->AddChild(md);
					m_Scale9Container->AddChild(dr);


					ul->SetTag((int)HotSpot::HS_UL + (int)_ServiceTags::Slice9Items);
					mu->SetTag((int)HotSpot::HS_MU + (int)_ServiceTags::Slice9Items);
					ur->SetTag((int)HotSpot::HS_UR + (int)_ServiceTags::Slice9Items);
					ml->SetTag((int)HotSpot::HS_ML + (int)_ServiceTags::Slice9Items);
					mid->SetTag((int)HotSpot::HS_MID + (int)_ServiceTags::Slice9Items);
					mr->SetTag((int)HotSpot::HS_MR + (int)_ServiceTags::Slice9Items);
					dl->SetTag((int)HotSpot::HS_DL + (int)_ServiceTags::Slice9Items);
					md->SetTag((int)HotSpot::HS_MD + (int)_ServiceTags::Slice9Items);
					dr->SetTag((int)HotSpot::HS_DR + (int)_ServiceTags::Slice9Items);



					ul->SetAnchor(HotSpot::HS_UL); ul->SetHotSpot(HotSpot::HS_UL);
					mu->SetAnchor(HotSpot::HS_UL); mu->SetHotSpot(HotSpot::HS_UL);
					ur->SetAnchor(HotSpot::HS_UR); ur->SetHotSpot(HotSpot::HS_UR);
					ml->SetAnchor(HotSpot::HS_UL); ml->SetHotSpot(HotSpot::HS_UL);
					mid->SetAnchor(HotSpot::HS_UL); mid->SetHotSpot(HotSpot::HS_UL);
					mr->SetAnchor(HotSpot::HS_UR); mr->SetHotSpot(HotSpot::HS_UR);
					dl->SetAnchor(HotSpot::HS_DL); dl->SetHotSpot(HotSpot::HS_DL);
					md->SetAnchor(HotSpot::HS_DL); md->SetHotSpot(HotSpot::HS_DL);
					dr->SetAnchor(HotSpot::HS_DR); dr->SetHotSpot(HotSpot::HS_DR);
				}

				auto textureSize = m_Resource->GetTexture()->GetSize();
				auto resSize = GetSize();



				auto ul = m_Scale9Container->GetChildByTag<CImageWidget>((int)HotSpot::HS_UL + (int)_ServiceTags::Slice9Items);
				auto mu = m_Scale9Container->GetChildByTag<CImageWidget>((int)HotSpot::HS_MU + (int)_ServiceTags::Slice9Items);
				auto ur = m_Scale9Container->GetChildByTag<CImageWidget>((int)HotSpot::HS_UR + (int)_ServiceTags::Slice9Items);
				auto ml = m_Scale9Container->GetChildByTag<CImageWidget>((int)HotSpot::HS_ML + (int)_ServiceTags::Slice9Items);
				auto mid = m_Scale9Container->GetChildByTag<CImageWidget>((int)HotSpot::HS_MID + (int)_ServiceTags::Slice9Items);
				auto mr = m_Scale9Container->GetChildByTag<CImageWidget>((int)HotSpot::HS_MR + (int)_ServiceTags::Slice9Items);
				auto dl = m_Scale9Container->GetChildByTag<CImageWidget>((int)HotSpot::HS_DL + (int)_ServiceTags::Slice9Items);
				auto md = m_Scale9Container->GetChildByTag<CImageWidget>((int)HotSpot::HS_MD + (int)_ServiceTags::Slice9Items);
				auto dr = m_Scale9Container->GetChildByTag<CImageWidget>((int)HotSpot::HS_DR + (int)_ServiceTags::Slice9Items);

				ul->SetTexture(m_Resource);
				mu->SetTexture(m_Resource);
				ur->SetTexture(m_Resource);
				ml->SetTexture(m_Resource);
				mid->SetTexture(m_Resource);
				mr->SetTexture(m_Resource);
				dl->SetTexture(m_Resource);
				md->SetTexture(m_Resource);
				dr->SetTexture(m_Resource);

				
				float texelX = 0.5f / textureSize.x;
				float texelY = 0.5f / textureSize.y;			

				float upCoord = texelX;
				float leftCoord = texelY;
				float rightCoord = 1.0f - texelX;
				float downCoord = 1.0f - texelY;
				float pixelSizeX = 1.0f / textureSize.x;
				float pixelSizeY = 1.0f / textureSize.y;
				
				if (m_Desc)
				{
					leftCoord = m_Desc->uv.left;
					upCoord = m_Desc->uv.top;
					rightCoord = m_Desc->uv.right;
					downCoord = m_Desc->uv.bottom;
					
					textureSize.x = (rightCoord - leftCoord)*textureSize.x;
					textureSize.y = (downCoord - upCoord)*textureSize.y;				
				}				

				float lessCoordX = leftCoord + (((rightCoord - leftCoord) / 2.0f) - pixelSizeX);
				float moreCoordX = leftCoord + ((rightCoord - leftCoord) / 2.0f);

				float lessCoordY = upCoord + (((downCoord - upCoord) / 2.0f) - pixelSizeY);
				float moreCoordY = upCoord + ((downCoord - upCoord) / 2.0f);

				Vector lessSize = textureSize / 2.0f - Vector(1.0f, 1.0f);
				Vector moreSize = textureSize / 2.0f;

				

				if (textureSize.x >= resSize.x)
				{
					mu->SetVisible(false);
					md->SetVisible(false);
					SetSizeX(textureSize.x);

					resSize.x = textureSize.x;
					lessSize.x = textureSize.x / 2.0f;
							
				}
				else
				{
					mu->SetVisible(true);
					md->SetVisible(true);

					mu->SetUV(FRect(lessCoordX, upCoord, moreCoordX, lessCoordY));
					md->SetUV(FRect(lessCoordX, moreCoordY, moreCoordX, downCoord));				
					


					mu->SetPositionX(lessSize.x);					
					mu->SetSizeX(resSize.x - textureSize.x+ 1);
					mu->SetSizeY(lessSize.y);
					
					md->SetPositionX(lessSize.x);
					md->SetSizeX(resSize.x - textureSize.x + 1);
					md->SetSizeY(moreSize.y);					
				}

				if (textureSize.y >= resSize.y)
				{
					ml->SetVisible(false);
					mr->SetVisible(false);
					SetSizeY(textureSize.y);	
					resSize.y = textureSize.y;
					lessSize.y = textureSize.y / 2.0f;
				
				}
				else
				{
					ml->SetVisible(true);
					mr->SetVisible(true);

					ml->SetUV(FRect(leftCoord, lessCoordY, lessCoordX, moreCoordY));
					mr->SetUV(FRect(moreCoordX, lessCoordY, rightCoord, moreCoordY));


					
					ml->SetPositionY(lessSize.y);
					ml->SetSizeX(lessSize.x);
					ml->SetSizeY(resSize.y - textureSize.y + 1);
					
					
					mr->SetPositionY(lessSize.y);
					mr->SetSizeX(moreSize.x);
					mr->SetSizeY(resSize.y - textureSize.y + 1);
				}

				

				ul->SetUV(FRect(leftCoord, upCoord, lessCoordX, lessCoordY));
				ur->SetUV(FRect(moreCoordX, upCoord, rightCoord, lessCoordY));
				dl->SetUV(FRect(leftCoord, moreCoordY, lessCoordX, downCoord));
				dr->SetUV(FRect(moreCoordX, moreCoordY, rightCoord, downCoord));

				ul->SetSize(lessSize.x, lessSize.y);
				ur->SetSize(moreSize.x, lessSize.y);

				dl->SetSize(lessSize.x, moreSize.y);
				dr->SetSize(moreSize.x, moreSize.y);


				if (textureSize.x < resSize.x || textureSize.y < resSize.y)
				{
					mid->SetVisible(true);					
					mid->SetUV(FRect(lessCoordX, lessCoordY, moreCoordX, moreCoordY));


					mid->SetPositionX(lessSize.x - (textureSize.x >= resSize.x ? 1 : 0));
					mid->SetPositionY(lessSize.y - (textureSize.y >= resSize.y ? 1 : 0));

					mid->SetSizeX(resSize.x - textureSize.x + (textureSize.x >= resSize.x ? 2 : 1));
					mid->SetSizeY(resSize.y - textureSize.y + (textureSize.y >= resSize.y ? 1: 1));					
				}
				else
				{
					mid->SetVisible(false);
				}

				
				m_Scale9Container->SetSize(GetSize());
				m_LockUpdateScale9 = false;

			}

			//////////////////////////////////////////////////////////////////////////
			void CImageWidget::SetScale9Enabled(const bool enabled)
			{
				CBaseWidget::SetScale9Enabled(enabled);

				if (enabled)
				{				
					UpdateScale9Sprite();
					m_Scale9Container->SetVisible(true);
					AddChild(m_Scale9Container);
				}
				else
				{
					m_Scale9Container->SetVisible(false);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CImageWidget::Draw(CRenderState & renderState)
			{
				g_Render->SetBlendMode(m_BlendMode);
#ifdef NBG_WIN32
				g_Render->SetTextureRepeat(m_IsRepeatedTexture);
#endif
				if (m_IsScale9Enabled == false)
				{
					Texture * texture = nullptr;
					if (m_Resource)
					{
						texture = m_Resource->GetTexture();
					}
					if (m_MeshType == MeshType::Mesh_Rectangle)
					{					
											
						bool matrixEquals = false;

						tempMatrix.CreateFromTransform(m_Transform, true);
						
											
						if (prevMatrix.Equals(&tempMatrix) && prevGlobalMatrix.Equals(renderState.GetMatrixPointer()))
						{
							matrixEquals = true;
						}
						else
						{
							prevMatrix = tempMatrix;
							tempMatrix.Multiply(renderState.GetMatrixPointer());
							prevMatrixTransformed = tempMatrix;
						}
						prevGlobalMatrix = *renderState.GetMatrixPointer();

						if (m_IsCheckOutOfScreen)
						{
							auto bounds = g_System->GetGameBounds();							
							bounds *= 0.5f;							

							Vector p;
							Vector pSizeMin;
							Vector pSizeMax = m_Size;							
							IntVector pSize;
							
							pSizeMin = tempMatrix.TransformPoint(pSizeMin);
							pSizeMax = tempMatrix.TransformPoint(pSizeMax);		

							int diffX = pSizeMax.x - pSizeMin.x;
							int diffY = pSizeMax.y - pSizeMin.y;
							
							pSize.x = abs(diffX);
							pSize.y = abs(diffY);

							p.x = pSizeMin.x + diffX;
							p.y = pSizeMin.y + diffY;

							float maxSize = 0.0f;
							pSize.x > pSize.y ? maxSize = pSize.x : maxSize = pSize.y;

							if (p.x + maxSize < -bounds.x)return ReturnCodes::Skip;
							if (p.x - maxSize > bounds.x)return ReturnCodes::Skip;
							if (p.y + maxSize < -bounds.y - maxSize)return ReturnCodes::Skip;
							if (p.y - maxSize > bounds.y)return ReturnCodes::Skip;
						}
						if (matrixEquals)
						{
							if (m_IsNeedToUpdateMesh)
							{						
								m_Mesh.SetRectData(&m_UV, &prevMatrixTransformed, m_ImageSize, m_Color, texture, m_IsTileTexture, m_Offset);
								m_IsNeedToUpdateMesh = false;
							}							
						}	
						else
						{
							m_Mesh.SetRectData(&m_UV, &tempMatrix, m_ImageSize, m_Color, texture, m_IsTileTexture, m_Offset);
							m_IsNeedToUpdateMesh = false;
						}
					}
					else if (m_MeshType == MeshType::Mesh_Custom)
					{
						m_Mesh.UpdateVertexes(GetTransform(), texture, m_IsTileTexture);
					}
					

					if (m_Resource)
					{
						if (m_Shader)
						{
							helpers::CShaderHelper::GetInstance()->UseShader(m_Shader, m_Resource);
						}
						g_Render->DrawMesh(m_Resource->GetTexture(), &m_Mesh);
						if (m_Shader)
						{
							helpers::CShaderHelper::GetInstance()->EndShader(m_Shader);
						}
					}
					else
					{
						g_Render->DrawMesh(nullptr, &m_Mesh);
					}
				}				
				return ReturnCodes::Skip;
			}

		}
	}
}