#ifndef OPTIMUS_UI_TEXT_WIDGET
#define OPTIMUS_UI_TEXT_WIDGET

#include <Framework/Datatypes/FRect.h>
#include <Framework/Global/Resources/TextureResource.h>
#include <Framework/Structures.h>

#include "BaseWidget.h"

namespace NBG
{
	namespace optimus
	{
		namespace ui
		{

			/** @brief Класс, реализующий вывод форматированной строки.
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CTextWidget : public CBaseWidget
			{
			public:
				/// @name Конструктор/деструктор
				OPTIMUS_CREATE(CTextWidget);
				static CTextWidget * Create(NBG::FontDescription * fontDesc, const std::wstring str = L"")
				{
					auto ret = Create();
					ret->Init(fontDesc, str);
					return ret;
				}
				virtual ~CTextWidget();

				/// Виртуальные наследуемые функции
				virtual void OnLoadFromXMLNode(const pugi::xml_node &node);
				virtual void OnScaleChanged();
				virtual void OnHotSpotChanged();
				virtual void OnColorChanged();

				virtual void Init();

				///Инициализация лейбла шрифтом и строкой
				void Init(NBG::FontDescription * fontDesc, const std::wstring str = L"");

				///Установка текста
				void SetText(const std::wstring text);

				///Установка шрифта
				void SetFont(NBG::FontDescription * fontDesc);
				

				///Получение текущего текста
				inline std::wstring GetText()
				{
					return m_Text;
				};

				///Получение текста, установленного при создании объекта
				///Нужно для случаев, когда текст передаётся в виде: "Coins: %s"
				inline std::wstring GetBaseText()
				{
					return m_BaseText;
				};

				CBaseWidget * GetTextContainer(){ return m_TextContainer; }

				///Методы получения ширины строки, можно получить либо по индексам, либо полную строку.
				///WARNING: работает только для строк без переносов
				float GetStringWidth(const int &sIndex, const int &eIndex);
				float GetStringWidth();

				/// Установка межсимвольного расстояния
				inline void SetLetterSpacing(float letterSpacing)
				{
					m_BaseCharacterPadding = letterSpacing;
					m_CharacterPadding = m_BaseCharacterPadding;
					SetText(m_Text);
				};

				/// Установка высоты строки.
				inline void SetLineHeight(float height)
				{
					m_LineHeight = height;
					SetText(m_Text);
				};

				/// Получение высоты строки.
				inline int GetLineHeight()
				{
					return m_LineHeight;
				};

				/// Установить базовое положение строки.
				void SetBasePosition(const float &x, const float &y)
				{
					m_BasePosition.x = x;
					m_BasePosition.y = y;
				}

				/// Установить ограничивающий прямоугольник
				void SetBoundingBox(const FRect &boundingBox)
				{
					m_BoundingBox = boundingBox;
					SetText(m_Text);
				}

			private:
				CTextWidget();

				void PreprocessString();

				/// Текущий индекс спрайта буквы
				int m_CurrentSprite;
				std::vector<CBaseWidget*> * m_Sprites;

				/// Текст выводимый.
				std::wstring m_Text;

				/// Базовый текст.
				std::wstring m_BaseText;

				/// Описание шрифта.
				NBG::FontDescription * m_FontDesc;

				/// Смещение текста.
				Vector m_Offset;

				/// Базовое положение текста.
				Vector m_BasePosition;

				/// Контейнер, который хранит текст.
				CBaseWidget * m_TextContainer;

				/// @name Константы для спец. символов.
				static const int SPACE_ID = 10000001;
				static const int NEW_LINE_ID = 10000002;
				static const int SERVICE_ID = 10000003;

				/// Символы.
				std::vector<CharOffsets> m_Chars;

				/// Количество символов.
				int m_CharsCount;

				/// Пары кернинга.
				NBG::FontKernPair * m_Kernings;

				/// Карта пар кернинга
				std::map<int, std::vector<NBG::FontKernPairV2>> m_KerningsMap;

				/// Ограниченивающий прямоугольник
				NBG::FRect m_BoundingBox;

				/// Количество пар.
				int m_KerningsCount;

				///Методы получения ширины строки, можно получить либо по индексам, либо полную строку.
				///WARNING: работает только для строк без переносов
				float _GetStringWidth(const int &sIndex, const int &eIndex);
				float _GetStringWidth();


				/// Ограничение по ширине строки
				int m_MaxWidth;

				/// Функция быстрого поиска по сортированному массиву
				NBG::CharOffsets* GetCharById(const unsigned short &id);

				/// Закешированные символы, чтобы не нужно было искать каждый раз ID символов в массиве.
				std::vector<int> m_CachedSymbols;

				/// ID текстуры шрифта.
				std::string	m_TextureId;

				/// Ресурс текстуры шрифта.
				CTextureResource * m_Texture;

				/// Межсимвольное расстояние.
				float m_CharacterPadding;

				/// Начальное межсимвольное расстояние.
				float m_BaseCharacterPadding;

				/// Точка привязки символа.
				int m_BaseLine;

				/// Размер пробела.
				int m_SpaceSize;

				/// Высота строки.
				int m_LineHeight;

				/// Закеширован ли рендер (?)
				bool m_RenderCache;

				/// Для оптимизации, чтобы лишний раз не перестраивать символы.
				HotSpot m_PrevHotSpot;

				///Отрисовать линию символов (в текущей реализации - один раз создаёт массив спрайтов и больше к этой функции не возвращается)
				void DrawLine(int x, int y, const int &sIndex, const int &eIndex);

				///TODO: Неоптимизированный вариант поиска кернинг пар. Сделать поиск более быстрым.
				int GetKerningAmount(const unsigned short first, const unsigned short second);

				std::map<int,Color> m_Colors;
			};
		}
	}
}
#endif
