#ifndef OPTIMUS_UI_CLIP_WIDGET
#define OPTIMUS_UI_CLIP_WIDGET

#include <Framework/Datatypes.h>
#include <Framework/Datatypes/Vector.h>

#include "BaseWidget.h"
#include "ImageWidget.h"

#include "../optimus_enum.h"
#include <queue>


namespace NBG
{
	namespace optimus
	{
		namespace helpers
		{
			class CCallbackLUAAction;
		}
	}
}

namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
			class CClipWidget : public ui::CBaseWidget
			{
			public:
				OPTIMUS_CREATE(CClipWidget);
				virtual ~CClipWidget();

				virtual void Init();

				
				virtual void OnLoadFromXMLNode(const pugi::xml_node &node);
				void LoadFromXML(const std::string &path, const std::string &animation = "");				

				void LoadNode(pugi::xml_node * object, CClipWidget * parent, int &maxFrame);
				void MoveToTime(const float time);
				void Play(const bool restart = false);
				void Stop();	

				
				void SetAnimationType(const AnimationType type, VOID_CALLBACK finishCallback);
				void SetAnimationType(const AnimationType type);

				//////////////////////////////////////////////////////////////////////////
				void __LuaSetAnimationType(const int type, helpers::CCallbackLUAAction * callback )
				{
					m_FinishLUACallback = callback;
					SetAnimationType(AnimationType(type));
				}


				struct KeyFrameData
				{
					Vector anchor;
					Vector pos;
					Vector scale;
					float rotation;
					float opacity;
					float duration;
				};

				std::vector<KeyFrameData>m_KeyFrames;
				std::vector<CClipWidget*>m_ChildClips;

				


				void SetImage(ui::CImageWidget * im)
				{
					m_Image = im;
				}

				GETTER_SETTER(float, m_SpeedMultiplier, SpeedMultiplier);

				float GetClipTime()
				{
					return m_AllTime;
				}

				int treeLevel;
				std::function<float(float, float, float, float)> m_TweenFunc;

				virtual ReturnCodes Update(const float dt);
			private:
				bool m_IsTopClip;
				bool m_IsStoped;
				int m_CurrentFrame;
				float m_FrameInterval;
				float m_Timer;

				ui::CImageWidget * m_Image;

				float m_AllTime;
				float m_CurrentTime;

				bool m_IsPongForward;

				


				AnimationType m_AnimType;
				VOID_CALLBACK m_FinishCallback;
				helpers::CCallbackLUAAction * m_FinishLUACallback;
				bool m_IsUseCallback;

			protected:
				CClipWidget();
			};
		}
	}
}
#endif //NBG_FRAMEWORK_ANIMATION_CLIP

