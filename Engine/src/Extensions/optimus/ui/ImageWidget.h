#ifndef OPTIMUS_UI_IMAGE_WIDGET
#define OPTIMUS_UI_IMAGE_WIDGET

#include <Framework/Utils/MathUtils.h>

#include <Framework/Datatypes/Matrix.h>
#include <Framework/Datatypes/Mesh.h>
#include <Framework/Datatypes/Color.h>
#include <Framework/Datatypes/Event.h>
#include <Framework/Datatypes/IntVector.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/Transform.h>

#include <Framework/Datatypes/Transform.h>
#include <Framework/Global/Resources/TextureResource.h>
#include <Framework/Helpers/AtlasHelper.h>

#include <../External/xml/pugixml.hpp>
#include <queue>

#include "../Helpers/ShaderHelper.h"


#include "BaseWidget.h"
#include "../optimus_macro.h"
#include "../optimus_enum.h"

using namespace NBG;
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{			
			class CImageWidget : public CBaseWidget
			{
			public:		
				OPTIMUS_CREATE(CImageWidget);				
				static CImageWidget * Create(const std::string &texturePath)
				{
					auto ret = Create();					
					ret->SetTexture(texturePath);
					return ret;
				}
				static CImageWidget * Create(NBG::CTextureResource * textureRes)
				{
					auto ret = Create();
					ret->SetTexture(textureRes);
					return ret;
				}
				

				const std::string & GetTexturePath()
				{
					return m_TexturePath;
				}

				virtual ~CImageWidget();

				virtual void Init();

				enum class MeshType
				{
					Mesh_Rectangle,
					Mesh_Custom
				};

				virtual Vector GetSize()
				{					
					return CBaseWidget::GetSize();
				}

				virtual Vector GetSizeForAnchor();
			

				virtual void OnLoadFromXMLNode(const pugi::xml_node &node);

				void SetTexture(const std::string &texturePath);
				void SetTexture(NBG::CTextureResource * textureRes);

				/// @name ��������������� ������� �������� �������.
				inline virtual void OnPositionChanged()
				{
					//m_IsNeedToUpdateMesh = true;
				};
				inline virtual void OnScaleChanged()
				{
					//m_IsNeedToUpdateMesh = true;
				};
				inline virtual void OnRotationChanged()
				{
					//m_IsNeedToUpdateMesh = true;
				};
				inline virtual void OnColorChanged()
				{
					m_IsNeedToUpdateMesh = true;
				};
				inline virtual void OnSizeChanged()
				{
					m_IsNeedToUpdateMesh = true;
					if (m_Desc == nullptr)
					{
						m_ImageSize = m_Size;
					}
					else
					{
						m_ImageSize = m_Size;
					}
					UpdateScale9Sprite();
				};
				virtual void OnHotSpotChanged();
				inline virtual void OnParentTransformChanged()
				{
					//m_IsNeedToUpdateMesh = true;
				};				


				virtual bool IsContains(const Vector &pos);

				void SetUV(FRect uv)
				{
					m_UV = uv;
					m_IsNeedToUpdateMesh = true;
				}

				void Cut(const float xPercent, const float yPercent, bool xFlip = false, bool yFlip = false);
				void SetScale9Enabled(const bool enabled);

				virtual ReturnCodes Draw(CRenderState & renderState);

				Mesh & GetMesh()
				{
					return m_Mesh;
				}

				

				/// ���������� ����� �������� �������� - �����������/�������.
				void SetTextureTiling(bool tile)
				{
					m_IsTileTexture = tile;
					m_IsNeedToUpdateMesh = true;
				};

				void SetRepeatedTexture(const bool isRepeated);

				char ** GetAlphaMask();


				///LUA HELPERS
				static CImageWidget * __LuaCreate(const std::string &texturePath)
				{
					return Create(texturePath);
				}

				void BlockMeshUpdate()
				{
					m_BlockMeshUpdate = true;
				}


				void SetUseAlphaMask(bool use)
				{
					if (use && m_IsUseAlphaMask == false)
					{
                        if (m_AlphaMask == nullptr)
                        {
                            m_AlphaMask = GetAlphaMask();
                        }
                        if (m_AlphaMask == nullptr)
                        {
                            m_IsUseAlphaMask = false;
                            return;
                        }
					}
					m_IsUseAlphaMask = use;
				}

				bool IsUseAlphaMask()
				{
					return m_IsUseAlphaMask;
				}
				CImageWidget();
			protected:				
				
				GETTER_SETTER(helpers::CShader*,m_Shader,Shader);
				GETTER_SETTER_BOOL(NeedToUpdateMesh);
				GETTER_SETTER_BOOL(CheckOutOfScreen);
				bool m_IsUseAlphaMask;
				void UpdateMesh(CRenderState & renderState);
				void UpdateOffset();
				void UpdateScale9Sprite();

				Vector m_ImageSize;

				std::string m_TexturePath;
				CTextureResource * m_Resource;
				AtlasTextureDescription * m_Desc;				

				CBaseWidget * m_Scale9Container;
				NBG::FRect m_UV;
				bool m_UseAtlas;
				bool m_IsTileTexture;
				bool m_IsRepeatedTexture;

				bool m_BlockMeshUpdate;
				bool m_LockUpdateScale9;

				Matrix tempMatrix;
				Matrix prevMatrix;
				Matrix prevMatrixTransformed;
				Matrix prevGlobalMatrix;

				Vector m_Offset;

				Mesh m_Mesh;
				MeshType m_MeshType;				

				char ** m_AlphaMask;
				IntVector m_MaskSize;

				Transform m_TempTransform;

				float m_CutXPercent;
				float m_CutYPercent;
			};
		}
	}
}
#endif //OPTIMUS_UI_IMAGE_WIDGET