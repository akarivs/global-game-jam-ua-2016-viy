#include "TextButtonWidget.h"

#include <Framework.h>
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{

			//////////////////////////////////////////////////////////////////////////
			CTextButtonWidget::CTextButtonWidget() :CBaseWidget()
			{				

			}

			//////////////////////////////////////////////////////////////////////////
			CTextButtonWidget::~CTextButtonWidget()
			{
				
			}

			//////////////////////////////////////////////////////////////////////////
			void CTextButtonWidget::Init()
			{				
				m_IsCatchTouch = true;
				SetEventListener(OPTIMUS_CALLBACK_3(&CTextButtonWidget::EventListener, this));
				m_TextButtonEventListener = nullptr;

				m_Normal	= CTextWidget::Create();
				m_Over		= CTextWidget::Create();
				m_Clicked	= CTextWidget::Create();
				m_Disabled	= CTextWidget::Create();

				AddChild(m_Normal);
				AddChild(m_Over);
				AddChild(m_Clicked);
				AddChild(m_Disabled);

				m_SoundName = "button_click";

				m_IsDrawLine = true;
				m_LineOffsetY = 8.0f;
			}

			//////////////////////////////////////////////////////////////////////////
			void CTextButtonWidget::OnLoadFromXMLNode(const pugi::xml_node &node)
			{
				if (node.attribute("sound").empty() == false)
				{
					m_SoundName = node.attribute("sound").value();
				}				
				if (node.attribute("line_y").empty() == false)
				{
					m_LineOffsetY = node.attribute("line_y").as_float();
				}			


				auto normalColor = Color(node.attribute("color_normal").value());
				auto overColor = Color(node.attribute("color_over").value());
				auto clickedColor = Color(node.attribute("color_clicked").value());
				auto disabledColor = Color(node.attribute("color_disabled").value());

				auto text = g_LocaleManager->GetText(node.attribute("text").value());
				auto font = g_FontsManager->GetFont(node.attribute("font").value());


				m_Normal->Init(font, text);
				m_Over->Init(font, text);
				m_Clicked->Init(font, text);
				m_Disabled->Init(font, text);

				m_Normal->SetColor(normalColor);
				m_Over->SetColor(overColor);
				m_Clicked->SetColor(clickedColor);
				m_Disabled->SetColor(disabledColor);

				



				

				m_Over->SetVisible(false);
				m_Clicked->SetVisible(false);
				m_Disabled->SetVisible(false);

				m_CurrentState = NULL;
				SetState(m_Normal);

				//SetSize(m_Normal->GetSize());				
			}

			//////////////////////////////////////////////////////////////////////////
			bool CTextButtonWidget::IsContains(const Vector &pos)
			{
				return CBaseWidget::IsContains(pos);
			}

			//////////////////////////////////////////////////////////////////////////
			void CTextButtonWidget::OnSetDisabled()
			{
				if (m_IsDisabled)
				{
					SetState(m_Disabled);
				}
				else
				{
					SetState(m_Normal);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CTextButtonWidget::SetTextButtonListener(CTextButtonWidget_EventListener callback)
			{
				m_TextButtonEventListener = callback;
			}



			//////////////////////////////////////////////////////////////////////////
			void CTextButtonWidget::SetText(const std::wstring &text)
			{
				m_Normal->SetText(text);
				m_Over->SetText(text);
				m_Clicked->SetText(text);
				m_Disabled->SetText(text);
			}

			
			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CTextButtonWidget::EventListener(CBaseWidget * sender, CBaseWidget_EventType type, CBaseWidget_EventData data)
			{
				if (type == CBaseWidget_EventType::Event_MouseUp)
				{
					if (m_CurrentState == m_Disabled)
					{
						if (m_TextButtonEventListener)
						{
							m_TextButtonEventListener(this,CTextButtonWidget_EventType::Event_DisabledClick);
						}
						return ReturnCodes::Block;
					}
					else if (m_CurrentState == m_Clicked && m_IsWasFocusedOnMouseDown)
					{
						SetState(m_Over);
						if (m_TextButtonEventListener)
						{
							g_SoundManager->Play(m_SoundName);
							m_TextButtonEventListener(this, CTextButtonWidget_EventType::Event_Click);
						}				

						CEvent evt((int)CTextButtonWidget_EventType::Event_Click);
						evt.target = this;						
						DispatchEvent(evt);

						return ReturnCodes::Block;
					}					
				}		
				else if (type == CBaseWidget_EventType::Event_Over)
				{
					if (m_CurrentState == m_Normal)
					{
						SetState(m_Over);						
					}
					return ReturnCodes::Block;
				}
				else if (type == CBaseWidget_EventType::Event_Out)
				{
					if (m_CurrentState != m_Disabled)
					{
						if (m_IsWasFocusedOnMouseDown == false)
						{
							SetState(m_Normal);
						}
					}
				}
				else if (type == CBaseWidget_EventType::Event_MouseDown)
				{
					if (m_CurrentState != m_Disabled)
					{
						SetState(m_Clicked);
					}
					return ReturnCodes::Block;
				}
				else if (type == CBaseWidget_EventType::Event_MouseUpOutside)
				{
					if (m_CurrentState != m_Disabled)
					{
						SetState(m_Normal);
					}
				}
				return ReturnCodes::Skip;
			}

			//////////////////////////////////////////////////////////////////////////
			void CTextButtonWidget::SetState(CTextWidget * state)
			{
				if (m_CurrentState == state)return;
				if (m_CurrentState)
				{
					m_CurrentState->SetVisible(false);
				}								
				m_CurrentState = state;
				m_CurrentState->SetVisible(true);				

				if (m_CurrentState == m_Over)
				{
					if (g_System->IsMobileSystem())
					{
						m_Over->SetVisible(false);
						m_Normal->SetVisible(true);
					}
				}
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CTextButtonWidget::Draw(CRenderState & renderState)
			{
				if (m_IsDrawLine)
				{
					if (m_CurrentState != m_Normal)
					{
						auto size = m_Normal->GetSize();
						auto trans = m_Normal->GetTransform()->GetAbsoluteTransform();

						Matrix m;
						m.CreateFromTransform(trans,true);						

						Vector pos, pos2;
						pos2.x = size.x;
						

						size.y += m_LineOffsetY * trans.scaley;
						pos2.y = size.y;
						pos.y = size.y;				
						

						m.TransformPoint(pos.x, pos.y);
						m.TransformPoint(pos2.x, pos2.y);

						g_Render->DrawLine(pos, pos2, 2.0f * trans.scalex, m_CurrentState->GetColor());
					}					
				}
				return ReturnCodes::Skip;
			}
		}
	}
}