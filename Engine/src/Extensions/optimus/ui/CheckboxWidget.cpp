#include "CheckboxWidget.h"

#include <Framework.h>
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{

			//////////////////////////////////////////////////////////////////////////
			CCheckboxWidget::CCheckboxWidget() :CBaseWidget()
			{

			}

			//////////////////////////////////////////////////////////////////////////
			CCheckboxWidget::~CCheckboxWidget()
			{

			}

			//////////////////////////////////////////////////////////////////////////
			void CCheckboxWidget::Init()
			{
				m_IsCatchTouch = true;
				SetEventListener(OPTIMUS_CALLBACK_3(&CCheckboxWidget::EventListener, this));
				m_CheckboxEventListener = nullptr;

				m_Normal = CImageWidget::Create();
				m_Over = CImageWidget::Create();
				m_Clicked = CImageWidget::Create();
				m_ClickedOver = CImageWidget::Create();
				m_Disabled = CImageWidget::Create();

				AddChild(m_Normal);
				AddChild(m_Over);
				AddChild(m_Clicked);
				AddChild(m_ClickedOver);
				AddChild(m_Disabled);

				m_Sound = "checkbox_click";
			}

			//////////////////////////////////////////////////////////////////////////
			void CCheckboxWidget::OnLoadFromXMLNode(const pugi::xml_node &node)
			{
				auto size = GetSize();
				if (m_IsScale9Enabled)
				{
					SetSize(size);
				}
				if (node.attribute("sound").empty() == false)
				{
					m_Sound = node.attribute("sound").value();
				}
				SetTexture(node.attribute("texture").value());
				m_IsUseAlphaMask = node.attribute("use_alpha").as_bool();

			}

			//////////////////////////////////////////////////////////////////////////
			void CCheckboxWidget::OnSetDisabled()
			{
				if (m_IsDisabled)
				{
					SetState(m_Disabled);
				}
				else
				{
					SetState(m_Normal);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CCheckboxWidget::SetButtonListener(CCheckboxWidget_EventListener callback)
			{
				m_CheckboxEventListener = callback;
			}

			//////////////////////////////////////////////////////////////////////////
			bool CCheckboxWidget::IsContains(const Vector &pos)
			{
				if (m_IsUseAlphaMask)
				{
#ifdef NBG_ANDROID
					///TODO: �������� ��������� �����-����� ��� ��������
					return CBaseWidget::IsContains(pos);
#endif
					m_CurrentState->SetUseAlphaMask(true);
					return m_CurrentState->IsContains(pos);
				}
				return CBaseWidget::IsContains(pos);
			}

			//////////////////////////////////////////////////////////////////////////
			void CCheckboxWidget::SetTexture(const std::string &textureFolderPath)
			{
				std::string path = textureFolderPath;
				if (path.substr(path.length() - 1, 1) != "/")path += "/";


				m_Normal->SetTexture(path + "normal.png");
				m_Over->SetTexture(path + "over.png");
				m_Clicked->SetTexture(path + "clicked.png");
				m_ClickedOver->SetTexture(path + "clicked_over.png");


				if (g_FileSystem->IsFileExists(path + "disabled.png"))
				{
					m_Disabled->SetTexture(path + "disabled.png");
				}
				else
				{
					m_Disabled->SetTexture(path + "normal.png");
				}
				if (m_IsScale9Enabled)
				{
					OnSizeChanged();
				}
				else
				{
					SetSize(m_Normal->GetSize());
				}



				m_Over->SetVisible(false);
				m_Clicked->SetVisible(false);
				m_ClickedOver->SetVisible(false);
				m_Disabled->SetVisible(false);

				m_CurrentState = NULL;
				SetState(m_Normal);
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CCheckboxWidget::EventListener(CBaseWidget * sender, CBaseWidget_EventType type, CBaseWidget_EventData data)
			{
				if (type == CBaseWidget_EventType::Event_MouseUp)
				{
					if (m_CurrentState == m_Disabled)
					{
						return ReturnCodes::Block;
					}
					else if (m_CurrentState == m_ClickedOver && m_IsWasFocusedOnMouseDown)
					{
						SetState(m_Over);
						if (m_CheckboxEventListener)
						{
							m_CheckboxEventListener(this, CCheckboxWidget_EventType::Event_UnCheck);
							g_SoundManager->Play(m_Sound);
						}
						return ReturnCodes::Block;
					}
					else if (m_CurrentState == m_Over && m_IsWasFocusedOnMouseDown)
					{
						SetState(m_ClickedOver);
						if (m_CheckboxEventListener)
						{
							m_CheckboxEventListener(this, CCheckboxWidget_EventType::Event_Check);
							g_SoundManager->Play(m_Sound);
						}
						return ReturnCodes::Block;
					}
				}
				else if (type == CBaseWidget_EventType::Event_Over)
				{
					if (m_CurrentState == m_Normal)
					{
						SetState(m_Over);
					}
					else if (m_CurrentState == m_Clicked)
					{
						SetState(m_ClickedOver);
					}
					return ReturnCodes::Block;
				}
				else if (type == CBaseWidget_EventType::Event_Out)
				{
					if (m_CurrentState != m_Disabled)
					{
						if (m_IsWasFocusedOnMouseDown == false)
						{
							if (m_CurrentState == m_ClickedOver || m_CurrentState == m_Clicked)
							{
								SetState(m_Clicked);
							}
							else
							{								
								SetState(m_Normal);
							}								
						}
					}
				}
				else if (type == CBaseWidget_EventType::Event_MouseDown)
				{
					if (m_CurrentState != m_Disabled)
					{
						//SetState(m_Clicked);
					}
					return ReturnCodes::Block;
				}
				else if (type == CBaseWidget_EventType::Event_MouseUpOutside)
				{
					if (m_CurrentState != m_Disabled)
					{
						if (m_CurrentState == m_ClickedOver || m_CurrentState == m_Clicked)
						{
							SetState(m_Clicked);
						}
						else
						{							
							SetState(m_Normal);
						}
					}
				}
				return ReturnCodes::Skip;
			}

			//////////////////////////////////////////////////////////////////////////
			void CCheckboxWidget::SetChecked(const bool checked)
			{
				if (checked)
				{
					SetState(m_Clicked);
				}
				else
				{
					SetState(m_Normal);					
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CCheckboxWidget::SetState(CImageWidget * state)
			{
				if (m_CurrentState == state)return;
				if (m_CurrentState)
				{
					m_CurrentState->SetVisible(false);
				}
				m_CurrentState = state;
				m_CurrentState->SetVisible(true);

				if (m_CurrentState == m_Over)
				{
					if (g_System->IsMobileSystem())
					{
						m_Over->SetVisible(false);
						m_Normal->SetVisible(true);
					}
				}
				else if (m_CurrentState == m_ClickedOver)
				{
					if (g_System->IsMobileSystem())
					{
						m_ClickedOver->SetVisible(false);
						m_Clicked->SetVisible(true);
					}
				}
			}

		}
	}
}