#include "AreaWidget.h"

#include <Framework.h>
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{

			//////////////////////////////////////////////////////////////////////////
			CAreaWidget::CAreaWidget() :CBaseWidget()
			{				
				
			}

			//////////////////////////////////////////////////////////////////////////
			CAreaWidget::~CAreaWidget()
			{
				
			}

			//////////////////////////////////////////////////////////////////////////
			void CAreaWidget::Init()
			{
				m_IsCatchTouch = true;			

				m_AreaType = EAreaType::Rectangle;
				m_Radius = 0.0f;								
			}

			//////////////////////////////////////////////////////////////////////////
			void CAreaWidget::OnLoadFromXMLNode(const pugi::xml_node &node)
			{
				std::string type = node.attribute("area_type").value();
				if (type == "rectangle")
				{
					m_AreaType = EAreaType::Rectangle;
				}
				else if (type == "circle")
				{
					m_AreaType = EAreaType::Circle;
					m_Radius = node.attribute("radius").as_float();
				}
				else if (type == "polygon")
				{
					m_AreaType = EAreaType::Polygon;
					std::string vertexesS = node.attribute("polygon").value();
					auto vertexesArray = StringUtils::ExplodeString(vertexesS, ';');
					for (int i = 0; i < vertexesArray.size(); i++)
					{
						auto vectorS = StringUtils::ExplodeString(vertexesArray[i], ',');
						NBG_Assert(vectorS.size() == 2, "Polygon vector invalid format!");

						Vector vec;
						vec.x = StringUtils::ToFloat(vectorS[0]);
						vec.y = StringUtils::ToFloat(vectorS[1]);
						m_Polygon.push_back(vec);
					}
				}
			}

			//////////////////////////////////////////////////////////////////////////
			bool CAreaWidget::IsContains(const Vector &pos)
			{
				if (m_AreaType == EAreaType::Circle)
				{
					auto trans = m_Transform.GetAbsoluteTransform();
					Vector myPos = Vector(trans.x, trans.y);
					return MathUtils::IsPointInsideCircle(pos, myPos, m_Radius);
				}
				else if (m_AreaType == EAreaType::Rectangle)
				{
					return CBaseWidget::IsContains(pos);
				}
				else if (m_AreaType == EAreaType::Polygon)
				{
					auto trans = m_Transform.GetAbsoluteTransform();
					Vector myPos = Vector(trans.x, trans.y);
					Vector transPos = pos;
					transPos -= myPos;					

					return MathUtils::IsPointInsidePoly(transPos, m_Polygon);
				}
				return false;
			}

			//////////////////////////////////////////////////////////////////////////
			ReturnCodes CAreaWidget::AfterDraw()
			{
				if (m_IsDebugDraw)
				{
					if (m_AreaType == EAreaType::Circle)
					{
						auto trans = GetAbsoluteTransform();
						Vector myPos = Vector(trans.x, trans.y);

						int segments = 36; 
						float step = 6.28f / (float)segments;
						float angle = 0.0f;

						for (int i = 0; i < segments; i++)
						{
							g_Render->DrawLine(myPos + Vector(cos(angle), -sin(angle))*m_Radius, myPos + Vector(cos(angle + step), -sin(angle + step))*m_Radius, 1.0f, Color(255, 255, 255, 255));
							angle += step;
						}
					}
					else if (m_AreaType == EAreaType::Polygon)
					{
						auto trans = GetAbsoluteTransform();
						Vector myPos = Vector(trans.x, trans.y);

						for (int i = 0; i < m_Polygon.size()-1; i++)
						{
							auto vec = m_Polygon[i] + myPos;
							auto vec2 = m_Polygon[i + 1] + myPos;
							g_Render->DrawLine(vec,vec2, 1.0f, Color(255, 255, 255, 255));						
						}
						g_Render->DrawLine(m_Polygon[0] + myPos, m_Polygon[m_Polygon.size() - 1] + myPos, 1.0f, Color(255, 255, 255, 255));
					}
				}
				return ReturnCodes::Skip;
			}
		}
	}
}