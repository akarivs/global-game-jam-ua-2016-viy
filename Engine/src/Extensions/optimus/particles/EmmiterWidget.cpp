#include "EmmiterWidget.h"


#include <Framework.h>

using namespace NBG;
namespace NBG
{
	namespace optimus
	{
		namespace particles
		{
			//==============================================================================
			CEmmiterWidget::CEmmiterWidget() :ui::CBaseWidget()
			{

			}

			//==============================================================================
			CEmmiterWidget::~CEmmiterWidget()
			{
				delete[] m_Particles;
				if (m_AlphaMask)
				{
					for (int x = 0; x < m_AlphaMaskSize.x; x++)
					{
						delete[] m_AlphaMask[x];
					}
					delete[] m_AlphaMask;
					m_AlphaMask = nullptr;
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CEmmiterWidget::Init(const std::string &xmlPath)
			{
				xml_document doc;
				xml_node node = doc.append_child("test");
				node.append_attribute("path").set_value(xmlPath.c_str());
				OnLoadFromXMLNode(node);

				m_ParticlesToEmit = 0.0f;
			}

			//////////////////////////////////////////////////////////////////////////
			CEmmiterWidget * CEmmiterWidget::Create(const std::string &path)
			{
				auto ret = CEmmiterWidget::Create();
				ret->Init(path);
				return ret;
			}



			//////////////////////////////////////////////////////////////////////////
			void CEmmiterWidget::OnLoadFromXMLNode(const pugi::xml_node &node)
			{
				std::string path = node.attribute("path").value();
				bool playFromStart = node.attribute("play_from_start").as_bool();
				float playTime = node.attribute("play_time").as_float();

				NBG::CXMLResource * xml = CAST(NBG::CXMLResource*, NBG::g_ResManager->GetResource(path));
				pugi::xml_document * doc = xml->GetXML();
				pugi::xml_node level_node = doc->first_child();
				int count;
				std::string texture = level_node.attribute("texture").value();

				count = level_node.attribute("count").as_int();

				
				Init(count, texture);
				pugi::xml_node object = level_node.find_child_by_attribute("id", "velocity");
				if (object.empty())
				{
					CONSOLE_ERROR("velocity param is not exist");
				}
				else
				{
					SetVelocity(object.attribute("velocity").as_float(),
						object.attribute("max_velocity").as_float());
				}
				object = level_node.find_child_by_attribute("id", "angle");
				if (object.empty())
				{
					CONSOLE_ERROR("angle param is not exist");
				}
				else
				{
					SetAngle(object.attribute("angle").as_float(),
						object.attribute("max_angle").as_float());
				}
				object = level_node.find_child_by_attribute("id", "size");
				if (object.empty())
				{
					CONSOLE_ERROR("size param is not exist");
				}
				else
				{
					Vector size = Vector(object.attribute("sizex").as_float(),
						object.attribute("sizey").as_float(), object.attribute("sizez").as_float());
					Vector maxSize = Vector(object.attribute("maxsizex").as_float(),
						object.attribute("maxsizey").as_float(), object.attribute("maxsizez").as_float());
					SetSize(size, maxSize);
				}
				object = level_node.find_child_by_attribute("id", "color");
				if (object.empty())
				{
					CONSOLE_ERROR("color param is not exist");
				}
				else
				{
					pugi::xml_node color = object.find_child_by_attribute("id", "start");
					Color start = Color(color.attribute("a").as_int(),
						color.attribute("r").as_int(),
						color.attribute("g").as_int(),
						color.attribute("b").as_int());
					color = object.find_child_by_attribute("id", "mid");
					Color mid = Color(color.attribute("a").as_int(),
						color.attribute("r").as_int(),
						color.attribute("g").as_int(),
						color.attribute("b").as_int());
					color = object.find_child_by_attribute("id", "end");
					Color end = Color(color.attribute("a").as_int(),
						color.attribute("r").as_int(),
						color.attribute("g").as_int(),
						color.attribute("b").as_int());
					SetEmmiterColor(start, mid, end);
				}
				object = level_node.find_child_by_attribute("id", "life_time");
				if (object.empty())
				{
					CONSOLE_ERROR("life_time param is not exist");
				}
				else
				{
					SetLifeTime(object.attribute("time").as_float(),
						object.attribute("max_time").as_float());
				}
				object = level_node.find_child_by_attribute("id", "angular_velocity");
				if (object.empty())
				{
					CONSOLE_ERROR("angular_velocity param is not exist");
				}
				else
				{
					SetAngularVelocity(object.attribute("velocity").as_float(),
						object.attribute("max_velocity").as_float());
				}
				object = level_node.find_child_by_attribute("id", "speed");
				if (object.empty())
				{
					CONSOLE_ERROR("speed param is not exist");
				}
				else
				{
					SetSpeed(object.attribute("speed").as_float(),
						object.attribute("max_speed").as_float());
				}
				object = level_node.find_child_by_attribute("id", "emission_rate");
				if (object.empty())
				{
					CONSOLE_ERROR("emission_rate param is not exist");
				}
				else
				{

					SetEmmisionRate(object.attribute("rate").as_float());
				}
				object = level_node.find_child_by_attribute("id", "additive");
				if (object.empty())
				{
					CONSOLE_ERROR("additive param is not exist");
				}
				else
				{
					if (object.attribute("additive").as_bool())
					{
						SetBlendMode(BlendMode::ADD);
					}					
				}

				object = level_node.find_child_by_attribute("id", "zone");
				if (object.empty())
				{
					//CONSOLE_ERROR("zone param is not exist");
				}
				else
				{
					std::string type = object.attribute("type").value();
					if (type == "rectangle")
					{
						SetRectangleZone(object.attribute("p1").as_float(), object.attribute("p2").as_float());
					}
					else if (type == "circle")
					{
						SetCircleZone(object.attribute("p1").as_float(), object.attribute("p2").as_float());
					}
					else if (type == "oval")
					{
						SetOvalZone(object.attribute("p1").as_float(), object.attribute("p2").as_float(), object.attribute("p3").as_float(), object.attribute("p4").as_float());
					}
					else if (type == "rectangle_border")
					{
						SetRectangleBorderZone(object.attribute("p1").as_float(), object.attribute("p2").as_float(), object.attribute("p3").as_float(), object.attribute("p4").as_float());
					}
				}
				NBG::g_ResManager->ReleaseResource(xml);
				if (playFromStart)
				{
					Play(playTime);
					object = level_node.find_child_by_attribute("id","pre_run");
					float counter = object.attribute("rate").as_float() / 0.033f;
					for (int i = 0; i <counter; i++)
					{
						Update(0.033f);
					}				
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CEmmiterWidget::Init()
			{
				m_Particles = nullptr;
				m_ZoneType = ZONE_Point;
				m_IsStoped = true;
				m_AdditiveMode = true;
				m_AlphaMask = nullptr;

				m_Speed = m_SpeedMax = 0.0f;
				m_AngularVelocity = m_AngularVelocityMax = 0.0f;
				m_Velocity = m_VelocityMax = 0.0f;
				m_Angle = m_AngleMax = 0.0f;
				m_LifeTime = m_LifeTimeMax = 0.0f;
				m_TimerStop = 0.0f;

				m_LiveParticles = 0;

				SetUpdatable(true);
			}

			//==============================================================================
			void CEmmiterWidget::Init(const int maxParticles, const std::string &texture)
			{
				CTextureResource * res = CAST(CTextureResource*, g_ResManager->GetResource(texture));
				m_ParticlesCount = maxParticles;
				m_Particles = new CParticle[m_ParticlesCount];
				for (int i = 0; i < m_ParticlesCount; i++)
				{
					CParticle * particle = &m_Particles[i];
					particle->SetTexture(res);
					particle->SetPosition(Vector(0.0f, 0.0f));
					particle->GetTransform()->SetParent(GetTransform());
					particle->m_Parent = this;
				}
			}

		

			//==============================================================================
			void CEmmiterWidget::SetSpeed(const float speed, const float maxspeed)
			{
				m_Speed = speed;
				m_SpeedMax = maxspeed;
			}

			//==============================================================================
			void CEmmiterWidget::SetVelocity(const float velocity, float maxvelocity)
			{
				m_Velocity = velocity;
				m_VelocityMax = maxvelocity;
			}

			//==============================================================================
			void CEmmiterWidget::SetAngularVelocity(const float velocity, float maxvelocity)
			{
				m_AngularVelocity = velocity;
				m_AngularVelocityMax = maxvelocity;
			}

			//==============================================================================
			void CEmmiterWidget::SetAngle(const float angle, float maxangle)
			{
				m_Angle = MathUtils::ToRadian(angle);
				m_AngleMax = MathUtils::ToRadian(maxangle);
			}

			//==============================================================================
			void CEmmiterWidget::SetLifeTime(const float time, const float maxtime)
			{
				m_LifeTime = time;
				m_LifeTimeMax = maxtime;
			}

			//==============================================================================
			void CEmmiterWidget::SetSize(const Vector &size, const Vector &maxsize)
			{
				m_Size = size;
				m_SizeMax = maxsize;
			}

			//==============================================================================
			void CEmmiterWidget::SetCircleZone(const float minRadius, const float maxRadius)
			{
				m_RadiusMin = minRadius;
				m_RadiusMax = maxRadius;
				m_ZoneType = ZONE_Circle;
			}

			//////////////////////////////////////////////////////////////////////////
			void CEmmiterWidget::SetOvalZone(const float minHorRadius, const float maxHorRadius, const float minVertRadius, const float maxVertRadius)
			{
				m_RadiusMin = minHorRadius;
				m_RadiusMax = maxHorRadius;
				m_VerticalRadiusMin = minVertRadius;
				m_VerticalRadiusMax = maxVertRadius;
				m_ZoneType = ZONE_Oval;
			}

			//==============================================================================
			void CEmmiterWidget::SetRectangleZone(const float width, const float height)
			{
				m_ZoneWidth = width / 2.0f;
				m_ZoneHeight = height / 2.0f;
				m_ZoneType = ZONE_Rectangle;
			}

			//==============================================================================
			void CEmmiterWidget::SetRectangleBorderZone(const float width, const float height, const float maxWidth, const float maxHeight)
			{
				m_ZoneWidth = width / 2.0f;
				m_ZoneHeight = height / 2.0f;

				m_ZoneWidthMax = maxWidth / 2.0f;
				m_ZoneHeightMax = maxHeight / 2.0f;
				m_ZoneType = ZONE_RectangleBorder;
			}

			//==============================================================================
			void CEmmiterWidget::SetMaskZone(const std::vector<Vector> &mask)
			{
				m_Mask = mask;
				m_CurrentMaskPoint = 0;
				m_ZoneType = ZONE_Mask;
			}

			//==============================================================================
			void CEmmiterWidget::SetMaskZone(const std::string &maskPath)
			{
				std::vector<Vector>mask;


				FILE * op = fopen(maskPath.c_str(), "rb");


				fseek(op, 0, SEEK_END);
				int size = ftell(op);
				fseek(op, 0, SEEK_SET);

				char * buffer = new char[size];
				fread(buffer, size, 1, op);
				fclose(op);

				int offset = 0;

				bool isOptimizedVersion = false;
				char identifier[10];
				memcpy(&identifier, &buffer[0], 10);
				std::string identifierString = identifier;
				if (identifierString == "OPTIMIZED")
				{
					offset += 10;
					isOptimizedVersion = true;
				}
				else
				{
					offset += sizeof(int);
					offset += sizeof(int);
				}
				while (offset < size)
				{
					Vector  pos;
					if (isOptimizedVersion)
					{
						int point;
						memcpy(&point, &buffer[offset], sizeof(int)); offset += sizeof(int);
						pos.x = point / 1024;
						pos.y = point % 1024;
					}
					else
					{
						int x, y;
						memcpy(&x, &buffer[offset], sizeof(int));
						offset += sizeof(int);
						memcpy(&y, &buffer[offset], sizeof(int));
						offset += sizeof(int);
						pos.x = x;
						pos.y = y;
					}
					mask.push_back(pos);
				}
				SetMaskZone(mask);
				delete[] buffer;
			}

			//==============================================================================
			void CEmmiterWidget::SetAlphaMaskZone(char ** alphaMask, const Vector size)
			{
				std::vector<Vector>mask;
				m_AlphaMask = alphaMask;
				m_AlphaMaskSize = size;
				m_ZoneType = ZONE_AlphaMask;
			}

			//==============================================================================
			void CEmmiterWidget::SetEmmisionRate(const int rate)
			{
				m_EmmisionRate = rate;
			}

			//==============================================================================
			void CEmmiterWidget::SetEmmiterColor(Color start, Color mid, Color end)
			{
				m_StartColor.SetColor(start.GetPackedColor());
				m_MidColor.SetColor(mid.GetPackedColor());
				m_EndColor.SetColor(end.GetPackedColor());
			}

			//==============================================================================
			void CEmmiterWidget::SetEmmiterColor(const FloatColor &start, const FloatColor &mid, const FloatColor &end)
			{
				m_StartColor = start;
				m_MidColor = mid;
				m_EndColor = end;
			}

			//==============================================================================
			void CEmmiterWidget::SetAdditive(const bool additive)
			{
				m_AdditiveMode = additive;
			}

			//==============================================================================
			void CEmmiterWidget::SetParticleOffset(const Vector &offset)
			{
				m_ParticlesOffset = offset;
				m_ParticlesOffset /= GetScale();
			}

			//==============================================================================
			void CEmmiterWidget::Play(const float timer)
			{
				m_TimerStop = timer;
				m_IsStoped = false;				
				m_ParticlesToEmit = 0.0f;
			}

			//==============================================================================
			void CEmmiterWidget::Stop()
			{
				m_IsStoped = true;
				m_ParticlesToEmit = 0.0f;
			}

			//==============================================================================
			void CEmmiterWidget::Shoot(const int count)
			{
				Emit(count);
			}

			//==============================================================================
			Vector CEmmiterWidget::GetAlphaMaskPos()
			{
#if defined(NBG_ANDROID) || defined(NBG_IOS)
				return Vector();
#endif
				Vector res = m_ParticlesOffset;
                
				while (true)
				{
					int xR = g_Random->RandI(0, m_AlphaMaskSize.x - 1);
					int yR = g_Random->RandI(0, m_AlphaMaskSize.y - 1);

					int val = m_AlphaMask[xR][yR];

					if (val == 1)
					{
						res.x += xR;
						res.y += yR;
						break;
					}
				}
				return res;
			}

			//==============================================================================
			void CEmmiterWidget::Emit(const int count, const float dt)
			{
				if (m_IsStoped && count == -1)return;
				float radius, vertRadius;
				int x, y;
				Vector size;
				
				if (count != -1)
				{
					m_ParticlesToEmit = count;
				}
				else
				{
					m_ParticlesToEmit += dt / 1.0f * m_EmmisionRate;
				}

				if (m_ParticlesToEmit < 1.0f)return;
				int rnd = 0;
				for (int i = 0; i < m_ParticlesCount; i++)
				{
					CParticle * part = &m_Particles[i];
					
					if (!part->IsCreated())
					{
						m_LiveParticles++;
						float angle = g_Random->RandF(m_Angle, m_AngleMax);
						switch (m_ZoneType)
						{
						case ZONE_Point:
							part->SetPosition(m_ParticlesOffset);
							break;
						case ZONE_Circle:
							radius = g_Random->RandF(m_RadiusMin, m_RadiusMax);
							part->SetPosition(Vector(cos(angle)*radius + m_ParticlesOffset.x, -sin(angle)*radius + m_ParticlesOffset.y));
							break;
						case ZONE_Oval:
							radius = g_Random->RandF(m_RadiusMin, m_RadiusMax);
							vertRadius = g_Random->RandF(m_VerticalRadiusMin, m_VerticalRadiusMax);
							part->SetPosition(Vector(cos(angle)*vertRadius + m_ParticlesOffset.x, -sin(angle)*radius + m_ParticlesOffset.y));
							break;
						case ZONE_Rectangle:							
							x = g_Random->RandF(-m_ZoneWidth, m_ZoneWidth) + m_ParticlesOffset.x;
							y = g_Random->RandF(-m_ZoneHeight, m_ZoneHeight) + m_ParticlesOffset.y;
							part->SetPosition(Vector(x,y));
							break;
						case ZONE_RectangleBorder:
							rnd = g_Random->RandI(1, 4);
							if (rnd == 1)
							{
								x = (g_Random->RandF(-m_ZoneWidthMax, m_ZoneWidthMax)) + m_ParticlesOffset.x;
								y = (g_Random->RandF(-m_ZoneHeightMax, -m_ZoneHeight)) + m_ParticlesOffset.y;
							}
							else if (rnd == 2)
							{
								x = (g_Random->RandF(-m_ZoneWidthMax, m_ZoneWidthMax)) + m_ParticlesOffset.x;
								y = (g_Random->RandF(m_ZoneHeight, m_ZoneHeightMax)) + m_ParticlesOffset.y;
							}
							else if (rnd == 3)
							{
								x = (g_Random->RandF(-m_ZoneWidthMax, -m_ZoneWidth)) + m_ParticlesOffset.x;
								y = (g_Random->RandF(-m_ZoneHeightMax, m_ZoneHeightMax)) + m_ParticlesOffset.y;
							}
							else if (rnd == 4)
							{
								x = (g_Random->RandF(m_ZoneWidth, m_ZoneWidthMax)) + m_ParticlesOffset.x;
								y = (g_Random->RandF(-m_ZoneHeightMax, m_ZoneHeightMax)) + m_ParticlesOffset.y;
							}							
								
							
							part->SetPosition(Vector(x, y));
							break;
						case ZONE_Mask:
							if (m_CurrentMaskPoint >= m_Mask.size())
							{
								Stop();
								return;
							}
							Vector *point;
							point = &m_Mask[m_CurrentMaskPoint];
							part->SetPosition(*point + m_ParticlesOffset);
							m_CurrentMaskPoint++;
							break;
						case ZONE_AlphaMask:
							part->SetPosition(GetAlphaMaskPos() + m_ParticlesOffset);
							break;
						}
						size.x = g_Random->RandF(m_Size.x, m_SizeMax.x);
						size.y = g_Random->RandF(m_Size.y, m_SizeMax.y);
						size.z = g_Random->RandF(m_Size.z, m_SizeMax.z);

						part->Create(g_Random->RandF(m_LifeTime, m_LifeTimeMax), m_StartColor, m_MidColor, m_EndColor, angle, g_Random->RandF(m_Speed, m_SpeedMax), g_Random->RandF(m_Velocity, m_VelocityMax), size, g_Random->RandF(m_AngularVelocity, m_AngularVelocityMax));
						m_ParticlesToEmit--;
					}
					if (m_ParticlesToEmit <= 0)
					{
						m_ParticlesToEmit = 0.0f;
						break;
					}
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CEmmiterWidget::InstantStop()
			{
				for (int i = 0; i < m_ParticlesCount; i++)
				{
					auto &part = m_Particles[i];
					part.m_IsCreated = false;					
				}
				m_LiveParticles = 0;
			}

			//==============================================================================
			ui::ReturnCodes CEmmiterWidget::Update(const float dt)
			{
				if (m_TimerStop > 0.0f)
				{
					m_TimerStop -= dt;
					if (m_TimerStop <= 0.0f)
					{
						m_TimerStop = 0.0f;
						Stop();
					};
				}

				if (m_LiveParticles > 0)
				{
					m_Global = m_Transform.GetAbsoluteTransform();
					for (int i = 0; i < m_ParticlesCount; i++)
					{
						auto &part = m_Particles[i];
						if (part.m_IsCreated == false)continue;
						if (part.Update(dt))
						{
							m_LiveParticles--;
						}
					}
				}				
				Emit(-1,dt);
				return ui::ReturnCodes::Skip;
			}

			//==============================================================================
			ui::ReturnCodes CEmmiterWidget::Draw(CRenderState &renderState)
			{				
				if (m_LiveParticles == 0)return ui::ReturnCodes::Skip;
				g_Render->SetBlendMode(m_BlendMode);							
				for (int i = 0; i < m_ParticlesCount; i++)
				{
					m_Particles[i].Draw(renderState);
				}				
				return ui::ReturnCodes::Skip;
			}
		}
	}
}