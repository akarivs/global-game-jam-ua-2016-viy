#include "Particle.h"
#include <Framework.h>
#include "../optimus.h"

using namespace NBG;
namespace NBG
{
	namespace optimus
	{
		namespace particles
		{
			//==============================================================================
			CParticle::CParticle()
			{
				m_IsCreated = false;
				m_IsKilled = false;
				m_TextureResource = NULL;

				m_Mesh.Init(4);
				m_UV = NBG::FRect(0, 0, 1, 1);
				m_Size = Vector(8, 8);
				m_Mesh.SetRectData(&m_UV, &m_Transform, m_Size, Color(), NULL, false);
				m_Vertexes = m_Mesh.GetVertexes();
			}

			//==============================================================================
			CParticle::~CParticle()
			{

			}

			//================================================================================
			bool CParticle::Create(const float lifeTime, const FloatColor &startColor, const FloatColor &midColor, const FloatColor &endColor, const float angle, const float speed, const float velocity, const Vector &size, const float angular)
			{
				if (m_IsCreated)return false;
				m_CurrentTime = 0.0f;
				m_LifeTime = lifeTime;
				m_StartColor = startColor;
				m_MidColor = midColor;
				m_EndColor = endColor;
				m_Angle = angle;
				m_Dir = Vector(cos(angle), -sin(angle));
				m_Speed = speed;
				m_Velocity = velocity;
				m_Angular = angular;
				m_IsKilled = false;
				m_Size = size;
				m_IsCreated = true;
				float time = 0.0f;
				Update(time);
				return true;
			}

			

			//==============================================================================
			bool CParticle::Update(const float &dt)
			{				
				m_CurrentTime += dt;
				float percent = m_CurrentTime / m_LifeTime;
				if (percent >= 1.0f)
				{
					percent = 1.0f;
					m_IsCreated = false;
					m_IsKilled = true;
				}
				m_Speed += m_Velocity*dt;

				if (m_Angular != 0.0f)
				{
					m_Angle += m_Angular*dt;
					m_Dir = Vector(MathUtils::FastCos(m_Angle), -MathUtils::FastSin(m_Angle));
					m_Transform.rz = m_Angle;
				}

				float sp = m_Speed*dt;
				m_Transform.x += m_Dir.x*sp;
				m_Transform.y += m_Dir.y*sp;

				/* Обновление цвета*/
				if (percent > 0.5f)
				{
					float p = ((percent - 0.5f) / 0.5f);
					floatClr.a = (float)m_MidColor.a + ((float)(m_EndColor.a - m_MidColor.a) * p);
					floatClr.r = (float)m_MidColor.r + ((float)(m_EndColor.r - m_MidColor.r) * p);
					floatClr.g = (float)m_MidColor.g + ((float)(m_EndColor.g - m_MidColor.g) * p);
					floatClr.b = (float)m_MidColor.b + ((float)(m_EndColor.b - m_MidColor.b) * p);

					size = m_Size.y + ((m_Size.z - m_Size.y) * p);
				}
				else
				{
					float p = (percent / 0.5f);
					floatClr.a = (float)m_StartColor.a + ((float)(m_MidColor.a - m_StartColor.a) *  p);
					floatClr.r = (float)m_StartColor.r + ((float)(m_MidColor.r - m_StartColor.r) *  p);
					floatClr.g = (float)m_StartColor.g + ((float)(m_MidColor.g - m_StartColor.g) *  p);
					floatClr.b = (float)m_StartColor.b + ((float)(m_MidColor.b - m_StartColor.b) *  p);

					size = m_Size.x + ((m_Size.y - m_Size.x) * p);
				}
				
				clrI.a = floatClr.a;
				clrI.r = floatClr.r;
				clrI.g = floatClr.g;
				clrI.b = floatClr.b;
				clrI.Normalize();


				m_Transform.hx = size / 2.0f;
				m_Transform.hy = m_Transform.hx;


				m_Vertexes[0].x = 0;
				m_Vertexes[0].y = 0;
			

				m_Vertexes[1].x = size;
				m_Vertexes[1].y = m_Vertexes[0].y;
				

				m_Vertexes[2].x = m_Vertexes[0].x;
				m_Vertexes[2].y = size;
				

				m_Vertexes[3].x = m_Vertexes[1].x;
				m_Vertexes[3].y = m_Vertexes[2].y;

				__g_PartVertexes[0] = m_Vertexes[0];
				__g_PartVertexes[1] = m_Vertexes[1];
				__g_PartVertexes[2] = m_Vertexes[2];
				__g_PartVertexes[3] = m_Vertexes[3];

				global = m_Transform;
				global.AddTransform(m_Parent->m_Global);				 
				matrix.CreateFromTransform(global,true);

				bool flipU = false;
				bool flipV = false;
				if (global.scalex < 0.0f)flipU = true;
				if (global.scaley < 0.0f)flipV = true;

				for (int i = 0; i < 4; i++)
				{
					int index = i;
					if (flipU && !flipV)
					{
						if (i == 0)index = 1;
						else if (i == 1)index = 0;
						else if (i == 2)index = 3;
						else if (i == 3)index = 2;
					}
					else if (flipV && !flipU)
					{
						if (i == 0)index = 2;
						else if (i == 1)index = 3;
						else if (i == 2)index = 0;
						else if (i == 3)index = 1;
					}
					else if (flipV && flipU)
					{
						if (i == 0)index = 3;
						else if (i == 1)index = 2;
						else if (i == 2)index = 1;
						else if (i == 3)index = 0;
					}

					Vector res = matrix.TransformPoint(Vector(__g_PartVertexes[i].x, __g_PartVertexes[i].y));
					m_Vertexes[index].x = res.x;
					m_Vertexes[index].y = res.y;
					m_Vertexes[index].color = clrI;
				}
				return m_IsKilled;
			}

			//==============================================================================
			void CParticle::Draw(CRenderState &renderState)
			{
				if (!m_IsCreated)return;
				if (m_TextureResource)
				{
					g_Render->DrawMesh(m_TextureResource->GetTexture(), &m_Mesh);
				}
				else
				{
					g_Render->DrawMesh(NULL, &m_Mesh);
				}
			}
		}
	}
}