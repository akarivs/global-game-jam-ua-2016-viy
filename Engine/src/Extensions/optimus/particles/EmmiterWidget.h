#ifndef OPTIMUS_PARTICLES_EMMITER
#define OPTIMUS_PARTICLES_EMMITER


#include "../ui/BaseWidget.h"
#include "../ui/ImageWidget.h"

#include <Framework/Datatypes/Color.h>
#include <Framework/Datatypes/Event.h>
#include <Framework/Datatypes/Vector.h>
#include <Framework/Datatypes/Vertex.h>
#include <Framework/Datatypes/Transform.h>

#include "Particle.h"

using namespace NBG;
namespace NBG
{
	namespace optimus
	{
		namespace particles
		{
			/** @brief Класс, реализующий работу эммитера
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CEmmiterWidget : public ui::CBaseWidget
			{
			public:
				/// @name Конструктор/деструктор
				OPTIMUS_CREATE(CEmmiterWidget);
				static CEmmiterWidget * Create(const int maxParticles, const std::string &texture)
				{
					auto ret = Create();
					ret->Init(maxParticles, texture);
					return ret;

				}
				static CEmmiterWidget * Create(const std::string &path);

				virtual ~CEmmiterWidget();


				static CEmmiterWidget * __luaCreate(const int maxParticles, const std::string &texture)
				{
					return Create(maxParticles,texture);
				}


				virtual void OnLoadFromXMLNode(const pugi::xml_node &node);
				

				enum CreateZoneType
				{
					ZONE_Point,		///создание из точки
					ZONE_Circle,	///создание в круге
					ZONE_Oval, ///создание в овале
					ZONE_Rectangle, ///создание в прямоугольнике
					ZONE_RectangleBorder, ///создание в прямоугольнике с отступом от центра
					ZONE_Mask,		///создание по маске точек
					ZONE_AlphaMask,	///создание по альфа-каналу
				};

				virtual void Init();
				void Init(const int maxParticles, const std::string &texture);				
				void Init(const std::string &xmlPath);

				void SetSpeed(const float speed, const float maxspeed);
				void SetAngle(const float angle, const float maxangle);
				void SetVelocity(const float velocity, float maxvelocity);
				void SetAngularVelocity(const float velocity, float maxvelocity);
				void SetLifeTime(const float time, const float maxtime);
				void SetSize(const Vector &size, const Vector &maxsize);
				void SetEmmisionRate(const int rate);
				void SetEmmiterColor(Color start, Color mid, Color end);
				void SetEmmiterColor(const FloatColor &start, const FloatColor &mid, const FloatColor &end);
				void SetAdditive(const bool additive);
				void SetParticleOffset(const Vector &offset);
				Vector & GetParticleOffset(){ return m_ParticlesOffset; };

				///Установка зоны создания частиц
				void SetCircleZone(const float minRadius, const float maxRadius);
				void SetOvalZone(const float minHorRadius, const float maxHorRadius, const float minVertRadius, const float maxVertRadius);
				void SetRectangleZone(const float width, const float height);
				void SetRectangleBorderZone(const float width, const float height, const float maxWidth, const float maxHeight);
				void SetMaskZone(const std::vector<Vector> &mask);
				void SetMaskZone(const std::string &maskPath);
				void SetAlphaMaskZone(char ** alphaMask, const Vector size);

				///Возвращает количество живых частиц.
				int GetLiveParticlesCount()
				{
					return m_LiveParticles;
				}

				//////////////////////////////////////////////////////////////////////////
				bool IsStoped()
				{
					return m_IsStoped;
				}

				void Play(const float time = 0.0f);
				void Stop();

				/// Остановить 
				void InstantStop();
				/// Выстрелить указанным количеством частиц
				void Shoot(const int count);

				Transform m_Global;

				virtual ui::ReturnCodes Update(const float dt);
				virtual ui::ReturnCodes Draw(CRenderState &renderState);
			protected:
				CEmmiterWidget();

				Vector GetAlphaMaskPos();
				CParticle * m_Particles;
				GETTER_SETTER(int, m_ParticlesCount, ParticlesCount);
				bool m_IsStoped;

				float m_TimerStop;
				float m_ParticlesToEmit;

				void Emit(const int count = -1, const float dt = 0.0f);

				///Количество активных частиц
				int m_LiveParticles;


				///Скорость
				float m_Speed;
				float m_SpeedMax;

				///Ускорение
				float m_Velocity;
				float m_VelocityMax;

				///Угловое ускорение
				float m_AngularVelocity;
				float m_AngularVelocityMax;

				///Время жизни
				float m_LifeTime;
				float m_LifeTimeMax;

				//Цвет
				FloatColor m_StartColor;
				FloatColor m_MidColor;
				FloatColor m_EndColor;

				///Частота эммисии (частиц в секунду)
				float m_EmmisionRate;

				///Размер
				Vector m_Size; ///используются x,y,z: x - стартовый, y - в средине жизни, z - в конце
				Vector m_SizeMax; ///используются x,y,z: x - стартовый, y - в средине жизни, z - в конце

				///Угол
				float m_Angle;
				float m_AngleMax;

				///Тип блендинга
				bool m_AdditiveMode;

				///Создание зон
				CreateZoneType m_ZoneType;
				float m_RadiusMin;
				float m_RadiusMax;
				float m_VerticalRadiusMin;
				float m_VerticalRadiusMax;
				float m_ZoneWidth;
				float m_ZoneHeight;
				float m_ZoneWidthMax;
				float m_ZoneHeightMax;

				Vector m_ParticlesOffset;
				std::vector<Vector>m_Mask;
				int m_CurrentMaskPoint;

				char ** m_AlphaMask;
				Vector m_AlphaMaskSize;			
			};
		}
	}
}
#endif //OPTIMUS_PARTICLES_EMMITER
