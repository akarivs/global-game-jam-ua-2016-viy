#include "UIPool.h"

#include "../ui/BaseWidget.h"

namespace NBG
{
	namespace optimus
	{
		namespace pool
		{

			CUIPool * CUIPool::self = nullptr;
			
			
			//////////////////////////////////////////////////////////////////////////
			CUIPool::CUIPool()
			{
				m_CanDelete = false;	
			}

			//////////////////////////////////////////////////////////////////////////
			CUIPool::~CUIPool()
			{
				
			}

			//////////////////////////////////////////////////////////////////////////
			void CUIPool::DeleteObjects()
			{
				while (true)
				{
					if (m_CanDelete)
					{
						while (m_ObjectsToDelete.empty() == false)
						{							
							auto obj = CAST(ui::CBaseWidget*, m_ObjectsToDelete.front());							
							if (obj->IsImmortal() == false)
							{								
								delete obj;							
							}							
							m_ObjectsToDelete.pop_front();							
						}
						m_CanDelete = false;
						CScenesManager::GetInstance()->QueueTask(EScenesManagerTasks::RemoveUnusedResources);
					}	
					std::this_thread::sleep_for(std::chrono::milliseconds(33));
				}				
			}

			//////////////////////////////////////////////////////////////////////////
			void CUIPool::PushObject(void * object)
			{	
				m_Objects.push_back(object);								
			}

			//////////////////////////////////////////////////////////////////////////
			void CUIPool::PopObject(void * object)
			{				
				auto iter = std::find(m_Objects.begin(), m_Objects.end(), object);
				if (iter != m_Objects.end())
				{								
					m_ObjectsToDelete.push_back(object);										
					m_Objects.erase(iter);					
				}								
			}

			//////////////////////////////////////////////////////////////////////////
			void CUIPool::UpdatePool()
			{				
				if (m_ObjectsToDelete.empty())return;
				
				while (m_ObjectsToDelete.empty() == false)
				{
					auto obj = CAST(ui::CBaseWidget*, m_ObjectsToDelete.front());
					if (obj->IsImmortal() == false)
					{
						delete obj;
					}
					m_ObjectsToDelete.pop_front();
				}				
				CScenesManager::GetInstance()->QueueTask(EScenesManagerTasks::RemoveUnusedResources);								
			}			
		}
	}
}