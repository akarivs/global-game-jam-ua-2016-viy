#ifndef OPTIMUS_POOL_UI
#define OPTIMUS_POOL_UI

#include <vector>
#include <deque>

namespace NBG
{
	namespace optimus
	{

		namespace pool
		{
			/** @brief Пул хранящий ссылки на все созданные объекты, которые унаследованы от BaseWidget

			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/
			class CUIPool
			{
			public:
				/// @name Конструктор деструктор
				static CUIPool * GetInstance()
				{
					if (self == nullptr)
					{
						self = new CUIPool();
					}
					return self;
				}
				~CUIPool();

				void PushObject(void * object);
				void PopObject(void * object);
				void UpdatePool();				
			private:			
				CUIPool();
				void DeleteObjects();
				bool m_CanDelete;
				static CUIPool * self;	
				std::mutex m_RMutex;				
				std::vector<void*> m_Objects;
				std::deque<void*> m_ObjectsToDelete;				
				std::deque<void*> m_ObjectsToDeleteDelayed;
			};
		}
	}	
}

#endif ///OPTIMUS_POOL_UI
