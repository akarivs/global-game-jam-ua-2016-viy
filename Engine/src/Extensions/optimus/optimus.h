#ifndef OPTIMUS_HEADERS
#define OPTIMUS_HEADERS

#include "ui/BaseWidget.h"
#include "ui/AreaWidget.h"
#include "ui/ButtonWidget.h"
#include "ui/CheckboxWidget.h"
#include "ui/CursorWidget.h"
#include "ui/ClipWidget.h"
#include "ui/ImageWidget.h"
#include "ui/InputTextWidget.h"
#include "ui/ListViewWidget.h"
#include "ui/TextButtonWidget.h"
#include "ui/TextWidget.h"
#include "ui/SliderWidget.h"
#include "ui/SpriteAnimationWidget.h"
#include "ui/VideoWidget.h"
#include "ui/WindowWidget.h"
#include "ui/WidgetsFactory.h"
#include "helpers/ActionHelper.h"
#include "helpers/LuaHelper.h"
#include "helpers/RateMyAppHelper.h"
#include "helpers/ResourcePreloaderHelper.h"
#include "helpers/ShaderHelper.h"
#include "managers/ScenesManager.h"
#include "particles/EmmiterWidget.h"
#include "optimus_macro.h"
#include "optimus_enum.h"
#include "optimus_tween.h"
#endif

using namespace optimus;
using namespace helpers;
using namespace managers;
using namespace ui;
using namespace particles;
using namespace pool;