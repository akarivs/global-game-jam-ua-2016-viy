#include "pool/UIPool.h"
#include <functional>



#define OPTIMUS_CREATE(__TYPE__) \
static __TYPE__* Create() \
{ \
    __TYPE__ *pRet =  new __TYPE__(); \
	pRet->Init(); \
	NBG::optimus::pool::CUIPool::GetInstance()->PushObject(pRet); \
    return pRet; \
} \
void Destroy() \
{ \
	if (IsImmortal() == false) \
	    SetDestroyed(true); \
		else \
		SetParent(nullptr); \
	NBG::optimus::pool::CUIPool::GetInstance()->PopObject(this); \
	if (IsImmortal() == false){	\
	for (int i = 0; i < m_Childs.size(); i++)m_Childs[i]->Destroy(); \
	m_Childs.clear(); \
	} \
}

#define OPTIMUS_CALLBACK_0(callback,obj,...) std::bind(callback,obj, ## __VA_ARGS__)
#define OPTIMUS_CALLBACK_1(callback,obj,...) std::bind(callback,obj, std::placeholders::_1,## __VA_ARGS__)
#define OPTIMUS_CALLBACK_2(callback,obj,...) std::bind(callback,obj, std::placeholders::_1, std::placeholders::_2,##__VA_ARGS__)
#define OPTIMUS_CALLBACK_3(callback,obj,...) std::bind(callback,obj, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3,##__VA_ARGS__)
#define OPTIMUS_CALLBACK_4(callback,obj,...) std::bind(callback,obj, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4,##__VA_ARGS__)

#define OPTIMUS_FUNC_CALLBACK_0(callback,...) std::bind(callback,##__VA_ARGS__)
#define OPTIMUS_FUNC_CALLBACK_3(callback,...) std::bind(callback,std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, ##__VA_ARGS__)
#define OPTIMUS_FUNC_CALLBACK_4(callback,...) std::bind(callback,std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4,##__VA_ARGS__)

#define OPTIMUS_TIME_CATCH_START double _________time = g_TimeManager->ReadTimer()
#define OPTIMUS_TIME_CATCH_END CONSOLE("Catch time: %dms",int(g_TimeManager->ReadTimer() - _________time))