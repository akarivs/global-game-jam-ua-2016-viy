#ifndef OPTIMUS_ENUM_H
#define OPTIMUS_ENUM_H
namespace NBG
{
	namespace optimus
	{
		namespace ui
		{
			enum class ReturnCodes
			{
				Skip,
				SkipDontProcess,
				Block,
				BlockDontDispatch
			};
			enum class SortModes
			{
				Manual,
				ByLayer
			};
		}

		enum class Tween
		{
			EASE_LINEAR = 0,
			EASE_IN_QUAD = 1,
			EASE_OUT_QUAD = 2,
			EASE_IN_OUT_QUAD = 3,
			EASE_OUT_IN_QUAD = 4,
			EASE_IN_CUBIC = 5,
			EASE_OUT_CUBIC = 6,
			EASE_IN_OUT_CUBIC = 7,
			EASE_OUT_IN_CUBIC = 8,
			EASE_IN_ELASTIC = 9,
			EASE_OUT_ELASTIC = 10,
			EASE_IN_OUT_ELASTIC = 11,
			EASE_OUT_IN_ELASTIC = 12,
			EASE_IN_CIRCULAR = 13,
			EASE_OUT_CIRCULAR = 14,
			EASE_IN_OUT_CIRCULAR = 15,
			EASE_OUT_IN_CIRCULAR = 16,
			EASE_IN_SIN = 17,
			EASE_OUT_SIN = 18,
			EASE_IN_OUT_SIN = 19,
			EASE_OUT_IN_SIN = 20
		};

		enum class AnimationType
		{
			ForwardOnce,
			BackwardOnce,
			ForwardLoop,
			BackwardLoop,
			PingPong
		};

		enum class _ServiceTags
		{
			TextSymbols = -10000,
			Slice9Items = -20000
		};
	}
}
#endif