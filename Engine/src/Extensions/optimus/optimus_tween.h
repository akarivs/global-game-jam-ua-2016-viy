#ifndef OPTIMUS_TWEENER_H
#define OPTIMUS_TWEENER_H

#include <math.h>
namespace NBG
{
	namespace optimus
	{
		#define A_PI 3.14159265359
		class Tweens
		{
		public:
			static float easeNone(float t, float b, float c, float d)
			{
				return c*t / d + b;
			}
			/**
			* Easing equation function for a quadratic (t^2) easing in: accelerating from zero velocity.
			*/
			static float easeInQuad(float t, float b, float c, float d)
			{
				return c*(t /= d)*t + b;
			}
			/**
			* Easing equation function for a quadratic (t^2) easing out: decelerating to zero velocity.
			*/
			static float easeOutQuad(float t, float b, float c, float d)
			{
				return -c *(t /= d)*(t - 2) + b;
			}
			/**
			* Easing equation function for a quadratic (t^2) easing in/out: acceleration until halfway, then deceleration.
			*/
			static float easeInOutQuad(float t, float b, float c, float d)
			{
				if ((t /= d / 2) < 1) return c / 2 * t*t + b;
				return -c / 2 * ((--t)*(t - 2) - 1) + b;
			}
			/**
			* Easing equation function for a quadratic (t^2) easing out/in: deceleration until halfway, then acceleration.
			*/
			static float easeOutInQuad(float t, float b, float c, float d)
			{
				if (t < d / 2) return easeOutQuad(t * 2, b, c / 2, d);
				return easeInQuad((t * 2) - d, b + c / 2, c / 2, d);
			}
			/**
			* Easing equation function for a cubic (t^3) easing in: accelerating from zero velocity.
			*/
			static float easeInCubic(float t, float b, float c, float d)
			{
				return c*(t /= d)*t*t + b;
			}
			/**
			* Easing equation function for a cubic (t^3) easing out: decelerating to zero velocity.
			*/
			static float easeOutCubic(float t, float b, float c, float d)
			{
				return c*((t = t / d - 1)*t*t + 1) + b;
			}
			/**
			* Easing equation function for a cubic (t^3) easing in/out: acceleration until halfway, then deceleration.
			*/
			static float easeInOutCubic(float t, float b, float c, float d)
			{
				if ((t /= d / 2) < 1) return c / 2 * t*t*t + b;
				return c / 2 * ((t -= 2)*t*t + 2) + b;
			}
			/**
			* Easing equation function for a cubic (t^3) easing out/in: deceleration until halfway, then acceleration.
			*/
			static float easeOutInCubic(float t, float b, float c, float d)
			{
				if (t < d / 2) return easeOutCubic(t * 2, b, c / 2, d);
				return easeInCubic((t * 2) - d, b + c / 2, c / 2, d);
			}

			/* PLACE FOR QUAD */

			/* PLACE FOR QUINT */

			/* PLACE FOR CIRCLE */
			/**
			* Easing equation function for a circular (sqrt(1-t^2)) easing in: accelerating from zero velocity.
			*/
			static float easeInCircular(float t, float b, float c, float d)
			{
				return -c * (sqrt(1 - (t /= d)*t) - 1) + b;
			}
			/**
			* Easing equation function for a circular (sqrt(1-t^2)) easing out: decelerating from zero velocity.
			*/
			static float easeOutCircular(float t, float b, float c, float d)
			{
				return c * sqrt(1 - (t = t / d - 1)*t) + b;
			}
			/**
			* Easing equation function for a circular (sqrt(1-t^2)) easing in/out: acceleration until halfway, then deceleration.
			*/
			static float easeInOutCircular(float t, float b, float c, float d)
			{
				if ((t /= d / 2) < 1) return -c / 2 * (sqrt(1 - t*t) - 1) + b;
				return c / 2 * (sqrt(1 - (t -= 2)*t) + 1) + b;
			}
			/**
			* Easing equation function for a circular (sqrt(1-t^2)) easing out/in: deceleration until halfway, then acceleration.
			*/
			static float easeOutInCircular(float t, float b, float c, float d)
			{
				if (t < d / 2) return easeOutCircular(t * 2, b, c / 2, d);
				return easeInCircular((t * 2) - d, b + c / 2, c / 2, d);
			}


			/* PLACE FOR SIN */

			/**
			* Easing equation function for a sinusoidal (sin(t)) easing in: accelerating from zero velocity.
			*/
			static float easeInSine(float t, float b, float c, float d)
			{
				return -c * MathUtils::FastCos(t / d * ((float)A_PI / 2)) + c + b;
			}

			/**
			* Easing equation function for a sinusoidal (sin(t)) easing out: decelerating from zero velocity.
			*/
			static float easeOutSine(float t, float b, float c, float d)
			{
				return c * MathUtils::FastSin(t / d * ((float)A_PI / 2)) + b;
			}

			/**
			* Easing equation function for a sinusoidal (sin(t)) easing out/in: deceleration until halfway, then acceleration.
			*/
			static float easeInOutSine(float t, float b, float c, float d)
			{
				if (t < d / 2) return easeOutSine(t * 2, b, c / 2, d);
				return easeInSine((t * 2) - d, b + c / 2, c / 2, d);
			}

			/**
			* Easing equation function for a sinusoidal (sin(t)) easing in/out: acceleration until halfway, then deceleration.
			*/
			static float easeOutInSine(float t, float b, float c, float d)
			{
				return -c / 2 * (MathUtils::FastCos((float)A_PI*t / d) - 1) + b;
			}

			/* PLACE FOR EXPOT */


			/**
			* Easing equation function for an elastic (exponentially decaying sine wave) easing in: accelerating from zero velocity.
			*/
			static float easeInElastic(float t, float b, float c, float d)
			{
				if (t == 0) return b;
				if ((t /= d) == 1) return b + c;
				float p = d*0.3f;
				float s = 0;
				float a = 0;
				if (a < fabs(c)) {
					a = c;
					s = p / 4;
				}
				else {
					s = p / (2 * (float)A_PI) * asin(c / a);
				}
				return -(a*pow(2, 10 * (t -= 1)) * MathUtils::FastSin((t*d - s)*(2 * (float)A_PI) / p)) + b;
			}

			/**
			* Easing equation function for an elastic (exponentially decaying sine wave) easing out: decelerating from zero velocity.
			*/
			static float easeOutElastic(float t, float b, float c, float d)
			{

				if (t == 0)return b;
				if ((t /= d) == 1) return b + c;
				float p = d*0.3f;
				float s = 0.0f;
				float a = 0.0f;
				if (a < fabs(c)) { a = c; s = p / 4.0f; }
				else s = p / (2.0f*(float)A_PI)*asin(c / a);
				return (a*(float)pow(2, -10.0f*t) * MathUtils::FastSin((t*d - s)*(2.0f*(float)A_PI) / p) + c + b);
			}

			/**
			* Easing equation function for an elastic (exponentially decaying sine wave) easing in/out: acceleration until halfway, then deceleration.
			*/
			static float easeInOutElastic(float t, float b, float c, float d)
			{
				if (t == 0) return b;
				if ((t /= d / 2) == 2) return b + c;
				float p = 0.7f;
				float s = 0.8f;
				float  a = 1.5f;
				if (a < fabs(c)) {
					a = c;
					s = p / 4;
				}
				else {
					s = p / (2 * (float)A_PI) * asin(c / a);
				}
				if (t < 1) return -0.5f*(a*pow(2, 10 * (t -= 1)) * MathUtils::FastSin((t*d - s)*(2 * (float)A_PI) / p)) + b;
				return a*pow(2, -10 * (t -= 1)) * MathUtils::FastSin((t*d - s)*(2.0f*(float)A_PI) / p)*0.5f + c + b;
			}
		};
	}
}
#endif //OPTIMUS_TWEENER_H