#include "ScenesManager.h"

#include "../optimus.h"
#include <Framework.h>

namespace NBG
{
	namespace optimus
	{
		namespace managers
		{

			CScenesManager * CScenesManager::self = nullptr;

			//////////////////////////////////////////////////////////////////////////
			CScenesManager::CScenesManager()
			{
				m_Container = nullptr;
				m_CurrentScene = nullptr;
				m_FramesCount = 0;
				m_FpsTime = 0.0f;
				m_Fps = 0.0f;

				m_AverageUpdateTime = m_AverageDrawTime = m_DrawTime = m_UpdateTime = 0.0f;
				m_UpdatesCount = m_DrawCount = 0;

				m_Cursor = ui::CCursorWidget::Create();
				m_Cursor->SetRoot(true);
				m_Cursor->SetHotSpot(HotSpot::HS_UL);

				m_NeedToHardRestart = false;

				m_ReplayHelper = CReplayHelper::GetInstance();
				m_ReplayHelper->SetOnMouseUpCallback(OPTIMUS_CALLBACK_2(&CScenesManager::OnMouseUp, this));
				m_ReplayHelper->SetOnMouseDownCallback(OPTIMUS_CALLBACK_2(&CScenesManager::OnMouseDown, this));
				m_ReplayHelper->SetOnMouseMoveCallback(OPTIMUS_CALLBACK_1(&CScenesManager::OnMouseMove, this));				
			}

			//////////////////////////////////////////////////////////////////////////
			CScenesManager::~CScenesManager()
			{
				m_ReplayHelper->StopRecord("test.rec");
				delete m_Container;
			}

			//////////////////////////////////////////////////////////////////////////
			void CScenesManager::Init()
			{
				m_Container = ui::CBaseWidget::Create();
				m_Container->SetRoot(true);
				m_Container->SetSize(g_System->GetGameBounds());
				m_Container->SetUpdatable(true);
				m_Container->SetCatchTouch(true);
				m_Container->SetCatchKeyboard(true);

				m_DebugLabel = ui::CTextWidget::Create(g_FontsManager->GetFirstFont(), L"Draw Calls: %s\nPoly Count: %s\nFPS: %s\nMemory: %smb\nUpdate: %s\nDraw: %s");
				m_DebugLabel->SetVisible(false);

				m_DebugLabel->SetAnchor(HotSpot::HS_UL);
				m_DebugLabel->SetHotSpot(HotSpot::HS_UL);
				m_DebugLabel->SetScale(0.75f);
				m_Container->AddChild(m_DebugLabel);
			}

			//////////////////////////////////////////////////////////////////////////
			void CScenesManager::SetScene(ui::CBaseWidget * scene)
			{
				if (m_CurrentScene)
				{
					m_Container->RemoveChild(m_CurrentScene);
					m_CurrentScene->Destroy();
					m_CurrentScene->SetOnDestroyCallback([&]{
						g_ResManager->ReleaseUnusedResources();
					});
				}
				m_CurrentScene = scene;
				if (scene == nullptr)return;
				scene->SetUpdatable(true);
				scene->SetCatchTouch(true);
				scene->SetCatchKeyboard(true);
				m_Container->AddChild(scene);
				helpers::CLuaHelper::GetInstance()->OnSceneChange(scene);
				m_DebugLabel->BringForward();
			}

			//////////////////////////////////////////////////////////////////////////
			void CScenesManager::SetScene(std::string  scene)
			{
				auto sceneP = Get(scene);
				if (sceneP)
				{
					SetScene(sceneP);
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CScenesManager::QueueTask(EScenesManagerTasks type)
			{
				g_GlobalMutex->lock();
				m_Tasks.push(type);
				g_GlobalMutex->unlock();
			}


			//////////////////////////////////////////////////////////////////////////
			void CScenesManager::OnRenderChange(RenderChangeType type)
			{
				if (!m_Container)return;
				if (type == RenderChangeType::After)
				{
					m_Container->SetSize(g_System->GetGameBounds());
				}
				m_Container->OnRenderChange(type);
			}

			//////////////////////////////////////////////////////////////////////////
			void CScenesManager::OnFocusChange(FocusChangeType type)
			{
				if (!m_Container)return;
				m_Container->OnFocusChange(type);
			}

			//////////////////////////////////////////////////////////////////////////
			void CScenesManager::OnMouseMove(const Vector &mousePos)
			{
				m_ReplayHelper->OnMouseMove(mousePos);
				m_Container->OnMouseMove(mousePos);				
			}

			//////////////////////////////////////////////////////////////////////////
			void CScenesManager::OnMouseWheel(const int delta)
			{
				m_Container->OnMouseWheel(delta);
			}

			//////////////////////////////////////////////////////////////////////////
			void CScenesManager::OnMouseDown(const Vector &mousePos, const int mouseButton)
			{
				m_ReplayHelper->OnMouseDown(mousePos, mouseButton);
				m_Container->OnMouseDown(mousePos, mouseButton);
			}

			//////////////////////////////////////////////////////////////////////////
			void CScenesManager::OnMouseUp(const Vector &mousePos, const int mouseButton)
			{
				m_ReplayHelper->OnMouseUp(mousePos, mouseButton);
				m_Container->OnMouseUp(mousePos, mouseButton);
			}

			//////////////////////////////////////////////////////////////////////////
			void CScenesManager::OnKeyDown(const int key)
			{
				m_Container->OnKeyDown(key);
			}

			//////////////////////////////////////////////////////////////////////////
			void CScenesManager::OnKeyUp(const int key)
			{
				m_Container->OnKeyUp(key);
				if (g_GameApplication->IsCheatsEnabled())
				{
					if (g_Input->IsKeyDown(KEY_SHIFT) && key == KEY_R)
					{
						if (m_CurrentScene)
						{
							m_NeedToHardRestart = true;
						}
					}
					else if (g_Input->IsKeyDown(KEY_SHIFT) && key == KEY_L)
					{
						g_ResManager->DumpResources();
					}
					else if (g_Input->IsKeyDown(KEY_SHIFT) && key == KEY_F)
					{
						g_ResManager->FreeAllResources();
					}
					else if (g_Input->IsKeyDown(KEY_SHIFT) && key == KEY_C)
					{
#ifdef NBG_WIN32
						if (AllocConsole())
						{
							freopen("CONIN$", "r", stdin);
							freopen("CONOUT$", "w", stdout);
							freopen("CONOUT$", "w", stderr);
						}
#endif
					}
					else if (g_Input->IsKeyDown(KEY_SHIFT) && key == KEY_V)
					{
						if (g_GameApplication->GetWindowedSize().x != 1024)
						{
							g_GameApplication->SetWindowedSize(1024, 768);
						}
						else
						{
							g_GameApplication->SetWindowedSize(1366, 768);
						}
						g_GameApplication->SetFullscreen(false);
					}
					else if (key == KEY_OEM_3)
					{
						m_DebugLabel->SetVisible(!m_DebugLabel->IsVisible());
					}
					else if (g_Input->IsKeyDown(KEY_SHIFT) && key == KEY_K)
					{
						g_PlayersManager->GetCurrentPlayer()->SetCustomCursor(!g_PlayersManager->GetCurrentPlayer()->IsCustomCursor());

						g_System->ShowCursor(!g_PlayersManager->GetCurrentPlayer()->IsCustomCursor());
						m_Cursor->SetVisible(g_PlayersManager->GetCurrentPlayer()->IsCustomCursor());
						m_Cursor->SetPosition(g_MousePos);
					}
				}
			}

			//////////////////////////////////////////////////////////////////////////
			void CScenesManager::Update(const float dt)
			{
				m_ReplayHelper->Update(dt);
				auto time = g_TimeManager->ReadTimer();
				m_Container->Update(dt);

				m_FpsTime += dt;
				m_UpdatesCount++;

				if (m_FpsTime >= 1.0f)
				{
					m_Fps = m_FramesCount;
					m_FpsTime = 0.0f;
					m_FramesCount = 0;

					m_AverageUpdateTime = m_UpdateTime / m_UpdatesCount;
					m_AverageDrawTime = m_DrawTime / m_DrawCount;

					m_UpdateTime = m_DrawTime = 0.0f;
					m_DrawCount = m_UpdatesCount = 0;

					if (m_DebugLabel->IsVisible())
					{
						std::wstring text = m_DebugLabel->GetBaseText();
						text = StringUtils::StringReplace(text, std::wstring(L"%s"), StringUtils::ToWString(g_Render->GetDrawCalls()));
						text = StringUtils::StringReplace(text, std::wstring(L"%s"), StringUtils::ToWString(g_Render->GetPolyCount()));
						text = StringUtils::StringReplace(text, std::wstring(L"%s"), StringUtils::ToWString(m_Fps));
						text = StringUtils::StringReplace(text, std::wstring(L"%s"), StringUtils::ToWString(g_System->GetMemoryUsage()));
						text = StringUtils::StringReplace(text, std::wstring(L"%s"), StringUtils::ToWString(m_AverageUpdateTime));
						text = StringUtils::StringReplace(text, std::wstring(L"%s"), StringUtils::ToWString(m_AverageDrawTime));
						m_DebugLabel->SetText(text);
					}
				}
				m_UpdateTime += g_TimeManager->ReadTimer() - time;				
			}

			//////////////////////////////////////////////////////////////////////////
			void CScenesManager::Draw(CRenderState & state)
			{
				m_FramesCount++;

				m_DrawBeginTime = g_TimeManager->ReadTimer();
				m_Container->___Draw(state);
				if (g_System->IsMobileSystem() == false && g_PlayersManager->GetCurrentPlayer()->IsCustomCursor())
				{
					state.Reset();
					m_Cursor->SetPosition(g_MousePos);
					m_Cursor->___Draw(state);
				}				
				m_DrawCount++;
				m_DrawTime += g_TimeManager->ReadTimer() - m_DrawBeginTime;
			}

			//////////////////////////////////////////////////////////////////////////
			void CScenesManager::AfterLoop()
			{	
				m_Container->ProcessDelayed();
				if (m_NeedToHardRestart)
				{
					g_ResManager->ReleaseUnusedResources();
					g_GameApplication->GetConfig()->LoadConfig("xml/config.xml");
					g_LocaleManager->Init(g_GameApplication->GetStringsPath());


					m_CurrentScene->RemoveAllChilds(true);
					g_ResManager->ReleaseUnusedResources();
					m_CurrentScene->Init();
					m_CurrentScene->OnHardRestart();
					m_CurrentScene->SetUpdatable(true);
					m_CurrentScene->SetCatchTouch(true);
					m_CurrentScene->SetCatchKeyboard(true);

					m_NeedToHardRestart = false;
				}


				if (m_Tasks.size() > 0)
				{
					g_GlobalMutex->lock();
					while (m_Tasks.empty() == false)
					{
						auto task = m_Tasks.front();
						m_Tasks.pop();

						if (task == EScenesManagerTasks::RemoveUnusedResources)
						{
							g_ResManager->ReleaseUnusedResources();
						}
					}
					g_GlobalMutex->unlock();
				}					
			}
		}
	}
}