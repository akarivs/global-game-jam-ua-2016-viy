#ifndef OPTIMUS_MANAGERS_SCENES
#define OPTIMUS_MANAGERS_SCENES

#include "../ui/BaseWidget.h"
#include "../ui/TextWidget.h"
#include "../ui/CursorWidget.h"
#include <Framework/Global/Render.h>
#include <Framework/Global/OperatingSystem.h>

#include "../helpers/ReplayHelper.h"

namespace NBG
{
	namespace optimus
	{
		namespace managers
		{
			/** @brief Менеджер игровых сцен 
			*
			* @author Vadim Simonov <akari.vs@gmail.com>
			* @copyright 2013-2014 New Bridge Games
			*
			*/

			enum class EScenesManagerTasks
			{
				RemoveUnusedResources
			};

			class CScenesManager
			{
			public:
				/// @name Конструктор деструктор
				static CScenesManager * GetInstance()
				{
					if (self == nullptr)
					{
						self = new CScenesManager();
					}
					return self;
				}
				~CScenesManager();

				template <class T>
				void Register(const std::string &name)
				{
					m_ScenesMap[name] = std::bind([&](){return T::Create(); });
				}

				ui::CBaseWidget * Get(std::string name)
				{
					return m_ScenesMap[name]();
				}

				void Init();
				void SetScene(ui::CBaseWidget * scene);				
				void SetScene(std::string  scene);

				void QueueTask(EScenesManagerTasks type);

				ui::CTextWidget * GetDebugLabel()
				{
					return m_DebugLabel;
				}

				ui::CBaseWidget * GetCurrentScene()
				{
					return m_CurrentScene;
				}

				void OnMouseMove(const Vector &mousePos);
				void OnMouseDown(const Vector &mousePos, const int mouseButton);
				void OnMouseUp(const Vector &mousePos, const int mouseButton);
				void OnMouseWheel(const int delta);

				void OnKeyDown(const int key);
				void OnKeyUp(const int key);

				void OnRenderChange(RenderChangeType type);
				void OnFocusChange(FocusChangeType type);

				void Update(const float dt);
				void Draw(CRenderState & state);
				void AfterLoop();
			private:			
				CScenesManager();
				static CScenesManager * self;
				ui::CBaseWidget * m_Container;
				ui::CTextWidget * m_DebugLabel;

				ui::CBaseWidget * m_CurrentScene;

				std::queue<EScenesManagerTasks> m_Tasks;

				float m_Fps;
				float m_FpsTime;
				float m_UpdateTime;
				float m_UpdatesCount;
				float m_DrawTime;
				float m_DrawCount;
				float m_AverageUpdateTime;
				float m_AverageDrawTime;
				int m_FramesCount;
				double m_DrawBeginTime;

				bool m_NeedToHardRestart;

				GETTER_SETTER(ui::CCursorWidget*, m_Cursor, Cursor);

				helpers::CReplayHelper * m_ReplayHelper;

				std::map < std::string, std::function<ui::CBaseWidget*()> > m_ScenesMap;
			};
		}
	}	
}

#endif ///OPTIMUS_MANAGERS_SCENES
