LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)


######### PNG ##############
include $(CLEAR_VARS)
LOCAL_MODULE := png
LOCAL_MODULE_FILENAME := libpng
LOCAL_SRC_FILES := ../../lib/Android/libpng.a 
include $(PREBUILT_STATIC_LIBRARY)

######### THEORA ##############
include $(CLEAR_VARS)
LOCAL_MODULE := theora
LOCAL_MODULE_FILENAME := libtheora
LOCAL_SRC_FILES := ../../lib/Android/libtheora.a 
include $(PREBUILT_STATIC_LIBRARY)

######### THEORA PLAYER ##############
include $(CLEAR_VARS)
LOCAL_MODULE := theoraplayer
LOCAL_MODULE_FILENAME := libtheoraplayer
LOCAL_SRC_FILES := ../../lib/Android/libtheoraplayer.a 
include $(PREBUILT_STATIC_LIBRARY)

######### OGG ##############
include $(CLEAR_VARS)
LOCAL_MODULE := ogg
LOCAL_MODULE_FILENAME := libogg
LOCAL_SRC_FILES := ../../lib/Android/libogg.a 
include $(PREBUILT_STATIC_LIBRARY)

######### OPENAL ##############
include $(CLEAR_VARS)
LOCAL_MODULE := openal
LOCAL_MODULE_FILENAME := libopenal
LOCAL_SRC_FILES := ../../lib/Android/libopenal.a
include $(PREBUILT_STATIC_LIBRARY)

######### VORBIS ##############
include $(CLEAR_VARS)
LOCAL_MODULE := vorbis
LOCAL_MODULE_FILENAME := libvorbis
LOCAL_SRC_FILES := ../../lib/Android/libvorbis.a 
include $(PREBUILT_STATIC_LIBRARY)

######### ZLIB ##############
include $(CLEAR_VARS)
LOCAL_MODULE := zlib
LOCAL_MODULE_FILENAME := libzlib
LOCAL_SRC_FILES := ../../lib/Android/libzlib.a 
include $(PREBUILT_STATIC_LIBRARY)

######### LUA ##############
include $(CLEAR_VARS)
LOCAL_MODULE := lua
LOCAL_MODULE_FILENAME := liblua
LOCAL_SRC_FILES := ../../lib/Android/lua_static.a 
include $(PREBUILT_STATIC_LIBRARY)

######### ENGINE ##############
include $(CLEAR_VARS)
LOCAL_MODULE := NBG_Engine
LOCAL_MODULE_FILENAME := libnbgengine

#libjpeg
#LOCAL_SRC_FILES += ../../external/libjpeg/ansi2knr.c
#LOCAL_SRC_FILES += ../../external/libjpeg/cdjpeg.c
#LOCAL_SRC_FILES += ../../external/libjpeg/cjpeg.c
#LOCAL_SRC_FILES += ../../external/libjpeg/ckconfig.c
#LOCAL_SRC_FILES += ../../external/libjpeg/djpeg.c
#LOCAL_SRC_FILES += ../../external/libjpeg/example.c
LOCAL_SRC_FILES += ../../external/libjpeg/jcapimin.c
LOCAL_SRC_FILES += ../../external/libjpeg/jcapistd.c
LOCAL_SRC_FILES += ../../external/libjpeg/jccoefct.c
LOCAL_SRC_FILES += ../../external/libjpeg/jccolor.c
LOCAL_SRC_FILES += ../../external/libjpeg/jcdctmgr.c
LOCAL_SRC_FILES += ../../external/libjpeg/jchuff.c
LOCAL_SRC_FILES += ../../external/libjpeg/jcinit.c
LOCAL_SRC_FILES += ../../external/libjpeg/jcmainct.c
LOCAL_SRC_FILES += ../../external/libjpeg/jcmarker.c
LOCAL_SRC_FILES += ../../external/libjpeg/jcmaster.c
LOCAL_SRC_FILES += ../../external/libjpeg/jcomapi.c
LOCAL_SRC_FILES += ../../external/libjpeg/jcparam.c
LOCAL_SRC_FILES += ../../external/libjpeg/jcphuff.c
LOCAL_SRC_FILES += ../../external/libjpeg/jcprepct.c
LOCAL_SRC_FILES += ../../external/libjpeg/jcsample.c
LOCAL_SRC_FILES += ../../external/libjpeg/jctrans.c
LOCAL_SRC_FILES += ../../external/libjpeg/jdapimin.c
LOCAL_SRC_FILES += ../../external/libjpeg/jdapistd.c
LOCAL_SRC_FILES += ../../external/libjpeg/jdatadst.c
LOCAL_SRC_FILES += ../../external/libjpeg/jdatasrc.c
LOCAL_SRC_FILES += ../../external/libjpeg/jdcoefct.c
LOCAL_SRC_FILES += ../../external/libjpeg/jdcolor.c
LOCAL_SRC_FILES += ../../external/libjpeg/jddctmgr.c
LOCAL_SRC_FILES += ../../external/libjpeg/jdhuff.c
LOCAL_SRC_FILES += ../../external/libjpeg/jdinput.c
LOCAL_SRC_FILES += ../../external/libjpeg/jdmainct.c
LOCAL_SRC_FILES += ../../external/libjpeg/jdmarker.c
LOCAL_SRC_FILES += ../../external/libjpeg/jdmaster.c
LOCAL_SRC_FILES += ../../external/libjpeg/jdmerge.c
LOCAL_SRC_FILES += ../../external/libjpeg/jdphuff.c
LOCAL_SRC_FILES += ../../external/libjpeg/jdpostct.c
LOCAL_SRC_FILES += ../../external/libjpeg/jdsample.c
LOCAL_SRC_FILES += ../../external/libjpeg/jdtrans.c
LOCAL_SRC_FILES += ../../external/libjpeg/jerror.c
LOCAL_SRC_FILES += ../../external/libjpeg/jfdctflt.c
LOCAL_SRC_FILES += ../../external/libjpeg/jfdctfst.c
LOCAL_SRC_FILES += ../../external/libjpeg/jfdctint.c
LOCAL_SRC_FILES += ../../external/libjpeg/jidctflt.c
LOCAL_SRC_FILES += ../../external/libjpeg/jidctfst.c
LOCAL_SRC_FILES += ../../external/libjpeg/jidctint.c
LOCAL_SRC_FILES += ../../external/libjpeg/jidctred.c
LOCAL_SRC_FILES += ../../external/libjpeg/jmemansi.c
LOCAL_SRC_FILES += ../../external/libjpeg/jmemmgr.c
LOCAL_SRC_FILES += ../../external/libjpeg/jmemname.c
LOCAL_SRC_FILES += ../../external/libjpeg/jmemnobs.c
LOCAL_SRC_FILES += ../../external/libjpeg/jpegtran.c
LOCAL_SRC_FILES += ../../external/libjpeg/jquant1.c
LOCAL_SRC_FILES += ../../external/libjpeg/jquant2.c
LOCAL_SRC_FILES += ../../external/libjpeg/jutils.c
#LOCAL_SRC_FILES += ../../external/libjpeg/rdbmp.c
#LOCAL_SRC_FILES += ../../external/libjpeg/rdcolmap.c
#LOCAL_SRC_FILES += ../../external/libjpeg/rdgif.c
#LOCAL_SRC_FILES += ../../external/libjpeg/rdjpgcom.c
#LOCAL_SRC_FILES += ../../external/libjpeg/rdppm.c
#LOCAL_SRC_FILES += ../../external/libjpeg/rdrle.c
#LOCAL_SRC_FILES += ../../external/libjpeg/rdswitch.c
#LOCAL_SRC_FILES += ../../external/libjpeg/rdtarga.c
#LOCAL_SRC_FILES += ../../external/libjpeg/transupp.c
#LOCAL_SRC_FILES += ../../external/libjpeg/wrbmp.c
#LOCAL_SRC_FILES += ../../external/libjpeg/wrgif.c
#LOCAL_SRC_FILES += ../../external/libjpeg/wrjpgcom.c
#LOCAL_SRC_FILES += ../../external/libjpeg/wrppm.c
#LOCAL_SRC_FILES += ../../external/libjpeg/wrrle.c
#LOCAL_SRC_FILES += ../../external/libjpeg/wrtarga.c




#DATATYPES
LOCAL_SRC_FILES += ../../src/Framework/Datatypes/Color.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Datatypes/Event.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Datatypes/FloatColor.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Datatypes/FRect.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Datatypes/IntVector.cpp
LOCAL_SRC_FILES += ../../src/Framework/Datatypes/Matrix.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Datatypes/Matrix3x3.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Datatypes/Mesh.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Datatypes/Texture.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Datatypes/Transform.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Datatypes/Vector.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Datatypes/Vertex.cpp 
#GLOBAL
LOCAL_SRC_FILES += ../../src/Framework/Global/FileSystem/android/FileSystem.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Global/OperatingSystem/android/OperatingSystem.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Global/Render/android/Render.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Global/Render/RenderState.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Global/Resources/IResource.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Global/Resources/OGGResource.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Global/Resources/TextureResource.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Global/Resources/XMLResource.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Global/System/android/System.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Global/TextureManager/BaseTextureManager.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Global/TextureManager/android/TextureManager.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Global/TimeManager/android/TimeManager.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Global/Input.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Global/Random.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Global/ResourcesManager.cpp 
#HELPERS
LOCAL_SRC_FILES += ../../src/Framework/Helpers/AtlasHelper.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Helpers/BinaryHelper.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Helpers/EditionHelper.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Helpers/StencilHelper.cpp 
#MANAGERS
LOCAL_SRC_FILES += ../../src/Framework/Managers/FontsManager.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Managers/LocalizationManager.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Managers/PlayersManager.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Managers/SoundManager.cpp 
#UTILS
LOCAL_SRC_FILES += ../../src/Framework/Utils/Config.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Utils/Log.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Utils/MathUtils.cpp 
LOCAL_SRC_FILES += ../../src/Framework/Utils/StringUtils.cpp 
#FRAMEWORK
LOCAL_SRC_FILES += ../../src/Framework/GameApplication.cpp 
###***EXTENSIONS***###
#OPTIMUS
LOCAL_SRC_FILES += ../../src/Extensions/optimus/helpers/ReplayHelper.cpp 
LOCAL_SRC_FILES += ../../src/Extensions/optimus/optimus.cpp 

#INAPP
LOCAL_SRC_FILES += ../../src/Extensions/inapp/InAppManager.cpp 
LOCAL_SRC_FILES += ../../src/Extensions/inapp/delegates/InAppWindowsDummyDelegate.cpp 

###***EXTERNAL***###
#XML
LOCAL_SRC_FILES += ../../external/xml/pugixml.cpp 
#SLB
LOCAL_SRC_FILES += ../../external/slb/SLB.cpp 
#THEORA
LOCAL_SRC_FILES += ../../external/theora_player/OpenAL_AudioInterface.cpp 

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../src \
		    $(LOCAL_PATH)/../../external/xml \
            $(LOCAL_PATH)/../../Engine \
		    $(LOCAL_PATH)/../../include/theora_player \
		    $(LOCAL_PATH)/../../include/png \
		    $(LOCAL_PATH)/../../include/lua  \
			$(LOCAL_PATH)/../../include  \
            $(LOCAL_PATH)/../../src/Extensions 
			
LOCAL_STATIC_LIBRARIES := theoraplayer theora vorbis ogg png zlib lua openal			
LOCAL_LDLIBS := -llog \
                -landroid
				
include $(BUILD_STATIC_LIBRARY)
