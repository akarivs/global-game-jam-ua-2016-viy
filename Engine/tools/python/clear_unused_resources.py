import os      
import shutil
from xml.dom import minidom

print("Start clear unused resources")

working_dir = "Resources"
atlases_dir = working_dir+"/atlases"
     

def list_files(dir):     
    r = []
    subdirs = [x[0] for x in os.walk(dir)]
    for subdir in subdirs:
        files = os.walk(subdir).next()[2]
        if (len(files) > 0):
            for file in files:
                r.append(subdir + "/" + file)
    return r 

#remove editor directory
if (os.path.isdir("Resources/images/editor")):
	shutil.rmtree("Resources/images/editor")  
            
#remove all images that packed in atlases			
atlases = list_files(atlases_dir)
atlases = [ fi for fi in atlases if fi.endswith(".xml") ]
  
for atlas in atlases: 
	xmldoc = minidom.parse(atlas)
	itemlist = xmldoc.getElementsByTagName('image') 
	for s in itemlist :
		file_to_remove = s.attributes['id'].value 
		if os.path.exists(working_dir + "/" + file_to_remove):
			os.remove(working_dir + "/" + file_to_remove)


print("End clear unused resources: Success")