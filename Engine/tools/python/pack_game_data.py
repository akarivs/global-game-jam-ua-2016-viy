import os      
import shutil
from xml.dom import minidom
import struct
import binascii
from struct import *
import time
import sys
import winsound

import PackRules

working_dir = "Resources"
atlases_dir = working_dir+"/atlases"

print("Start packing game data")

directoryXML = ""  

def list_files(dir):     
	r = []
	directoryXML = "<?xml version='1.0' encoding='utf-8'?>\n"
	directoryXML += "<directories>\n"
	subdirs = [x[0] for x in os.walk(dir)]
	for subdir in subdirs:	
		if (PackRules.CheckSubdir(subdir) == True):
			continue
		t = ""
		level = subdir.count('\\')
		for i in range(0,level):
			t += "\t"
		tChild = t+"\t"
		tChild2	= tChild+"\t"
		if (level == 0):	
			directoryXML += t+"<directory name='ROOT'>\n"	
		else:
			directoryXML += t+"<directory name='"+subdir[10:]+"'>\n"	
		directoryXML += tChild+"<files>\n"	
		files = os.walk(subdir).next()[2]
		if (len(files) > 0):
			for file in files:				
				if (PackRules.CheckSubdirAndFile(subdir, file)) == True:
					continue

				size = os.path.getsize(subdir + "/" + file)
				r.append([subdir + "/" + file, size])
				directoryXML += tChild2+"<file name='"+file+"' />\n"	
		directoryXML += tChild+"</files>\n"	
		directoryXML += t+"</directory>\n"	
	directoryXML += "</directories>\n"
	return [r,directoryXML] 

#pack game data into game.pack

res = list_files(working_dir)
resources = res[0]
directoryXML = res[1]


filesCount = len(resources)
counter = 0 

output = pack("7s","NBGPACK") 
output += pack('c', '\n')
output += pack("i",1) #version

offset = 12
#header with info about files and its offsets
headerSize = filesCount * 256 + filesCount * 4 + filesCount * 4;		
offset += headerSize;
offset += len(directoryXML)
output += pack("i",headerSize) #header size
offset += 8
for resource in resources: 
	stringR = resource[0][10:]
	str_len = len(stringR)
	for i in range(0, 256): 
		if (i<str_len):
			output += pack("c",stringR[i:i+1])
		elif (i==str_len):
			output += pack("c","\n")
		else:
			output += pack("c", "\0")
	output += pack("i",resource[1])
	output += pack("i",offset)
	offset += resource[1]
output += pack("i",len(directoryXML)); 		#DIRECTORY XML SIZE
output += directoryXML;	
f = open('game.pack', 'wb')
f.write(output)
f.close()

fOut = open('game.pack', 'ab')
for resource in resources:
	counter = counter + 1 
	percent = float(counter)/float(filesCount)
	if percent > 1:
		percent = 1
	
	f = open(resource[0],"rb")	
	fOut.write(f.read())
	f.close()
	os.remove(resource[0])
	sys.stdout.write("\r"+str(int(percent*100))+"%")
	sys.stdout.flush()
fOut.close()

Freq = 2500 # Set Frequency To 2500 Hertz
Dur = 1000 # Set Duration To 1000 ms == 1 second
winsound.Beep(Freq,Dur)
print("\nEnd packing game data")