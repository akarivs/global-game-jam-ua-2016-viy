#!C:/Python27/
import sys
import os
import inspect
import subprocess
from contextlib import contextmanager
import shutil
import distutils.core
from winsound import Beep

def BeepMessage():
	start = 10
	end = 18

	mid = start + (end-start)/2

	for i in range(start,end):	
		code = i
		if i > mid:
			code = start + end-i
		Beep(code*150,100);



NBG_TOOLS_VERSION = '0.1'

def removeEmptyFolders(path):
  if not os.path.isdir(path):
    return

  # remove empty subfolders
  files = os.listdir(path)
  if len(files):
    for f in files:
      fullpath = os.path.join(path, f)
      if os.path.isdir(fullpath):
        removeEmptyFolders(fullpath)

  # if folder empty, delete it
  files = os.listdir(path)
  if len(files) == 0:    
    os.rmdir(path)

if len(sys.argv) == 2 and sys.argv[1] in ('-v', '--version'):
    print("%s" % NBG_TOOLS_VERSION)
    exit(0)

if len(sys.argv) == 4 and sys.argv[1] == 'create':
	name = sys.argv[2]
	package = sys.argv[3]
	callPath = os.path.abspath(os.getcwd())
	script_dir = os.path.dirname(os.path.realpath(__file__)) + os.sep
	dir = callPath + "/" + name
	engineDir = dir + "/Engine"
	try:
	    os.stat(dir)
	except:
	    os.mkdir(dir) 

	try:
	    os.stat(engineDir)
	except:
	    os.mkdir(engineDir) 

	distutils.dir_util.copy_tree(script_dir+"../templates/default", dir)

	for dname, dirs, files in os.walk(dir):
	    for fname in files:
	        fpath = os.path.join(dname, fname)
		if (fname == 'resource.h' or fname == 'NBG_HelloWorld.rc' or fname == 'icon1.ico' or fname == 'icon2.ico' or "png" in fname or "ogg" in fname or "jpg" in fname):
			fname = ''	
		else:
	        	with open(fpath) as f:
				s = f.read()
			 	s = s.replace("HelloWorld", name)
			 	s = s.replace("com.nbg.hello_world", package)
		        with open(fpath, "w") as f:
	        	    f.write(s)

	os.rename(dir+'/Classes/Modules/HelloWorld',dir+'/Classes/Modules/'+name)
	os.rename(dir+'/Classes/Modules/'+name+'/SceneHelloWorld.cpp',dir+'/Classes/Modules/'+name+'/Scene'+name+".cpp")
	os.rename(dir+'/Classes/Modules/'+name+'/SceneHelloWorld.h',dir+'/Classes/Modules/'+name+'/Scene'+name+".h")
	os.rename(dir+'/Project.Win32/NBG_HelloWorld.rc',dir+'/Project.Win32/NBG_'+name+'.rc')
	os.rename(dir+'/Project.Win32/NBG_HelloWorld.sln',dir+'/Project.Win32/NBG_'+name+'.sln')
	os.rename(dir+'/Project.Win32/NBG_HelloWorld.vcxproj',dir+'/Project.Win32/NBG_'+name+'.vcxproj')
	os.rename(dir+'/Project.Win32/NBG_HelloWorld.vcxproj.filters',dir+'/Project.Win32/NBG_'+name+'.vcxproj.filters')
	os.rename(dir+'/Project.Win32/NBG_HelloWorld.vcxproj.user',dir+'/Project.Win32/NBG_'+name+'.vcxproj.user')

	distutils.dir_util.copy_tree(script_dir+"../../dist", engineDir+os.sep+"dist")
	distutils.dir_util.copy_tree(script_dir+"../../external", engineDir+os.sep+"external")
	distutils.dir_util.copy_tree(script_dir+"../../include", engineDir+os.sep+"include")
	distutils.dir_util.copy_tree(script_dir+"../../lib", engineDir+os.sep+"lib")
	distutils.dir_util.copy_tree(script_dir+"../../solution/android", engineDir+os.sep+"solution/android")
	distutils.dir_util.copy_tree(script_dir+"../../solution/ios_osx", engineDir+os.sep+"solution/ios_osx")
	os.mkdir(engineDir + os.sep + "solution" + os.sep + "win32")
	shutil.copyfile(script_dir+"../.."+ os.sep + "solution" + os.sep + "win32" + os.sep + "NBG_Engine.vcxproj.filters",engineDir + os.sep + "solution" + os.sep + "win32" + os.sep + "NBG_Engine.vcxproj.filters")
	shutil.copyfile(script_dir+"../.."+ os.sep + "solution" + os.sep + "win32" + os.sep + "NBG_Engine.vcxproj.user",engineDir + os.sep + "solution" + os.sep + "win32" + os.sep + "NBG_Engine.vcxproj.user")
	shutil.copyfile(script_dir+"../.."+ os.sep + "solution" + os.sep + "win32" + os.sep + "NBG_Engine.vcxproj",engineDir + os.sep + "solution" + os.sep + "win32" + os.sep + "NBG_Engine.vcxproj")
	distutils.dir_util.copy_tree(script_dir+"../../src", engineDir+os.sep+"src")
	shutil.copyfile(script_dir+"../../Readme.txt",engineDir + os.sep + "Readme.txt")
	BeepMessage()
	exit(0)
if len(sys.argv) >= 2 and sys.argv[1] == 'publish':
	callPath = os.path.abspath(os.getcwd())

	canLoad = os.path.isdir(callPath+os.sep+"Engine") and os.path.isdir(callPath+os.sep+"Resources") and os.path.isdir(callPath+os.sep+"Branch") 
	if canLoad == False:
		print("Not a project root dir!")
		exit(0)

	
	publishPath = callPath + os.sep + "_PUBLISH"
	branchPath = callPath + os.sep + "Branch"
	resourcesPath = callPath + os.sep + "Resources"
	releaseDistPath = callPath + os.sep + "Engine" + os.sep + "Dist" + os.sep + "dll" + os.sep + "release"
	releasePath = callPath + os.sep + "Project.Win32" + os.sep + "Release" + os.sep
	pythonPath = callPath + os.sep + "Tools" + os.sep + "Python" + os.sep
	
	shutil.rmtree(publishPath,ignore_errors=True)
	os.mkdir(publishPath)
	print(publishPath)
	
	distutils.dir_util.copy_tree(branchPath, publishPath + os.sep + "Branch")
	distutils.dir_util.copy_tree(resourcesPath, publishPath + os.sep + "Resources")
	distutils.dir_util.copy_tree(releaseDistPath, publishPath)
	
	
	
	for name in os.listdir(releasePath):
		if name.lower().endswith('.exe'):
			print(publishPath + os.sep + name)
			shutil.copyfile(releasePath+name,publishPath + os.sep + name)
			
	if len(sys.argv) == 4 and sys.argv[3] == "demo":
		shutil.copyfile(pythonPath+"DemoRules.py",publishPath + os.sep + "DemoRules.py")
		script_dir = os.path.dirname(os.path.realpath(__file__)) + os.sep		
		shutil.copyfile(script_dir+"clear_demo_resources.py",publishPath + os.sep + "clear_demo.py")		
		shutil.copyfile(script_dir+"clear_unused_resources.py",publishPath + os.sep + "clear.py")
		os.chdir(publishPath)
		subprocess.call(["python", publishPath + os.sep + "clear.py"])
		subprocess.call(["python", publishPath + os.sep + "clear_demo.py"])	
		os.remove(publishPath + os.sep + "clear.py")		
		os.remove(publishPath + os.sep + "clear_demo.py")		
		os.remove(publishPath + os.sep + "DemoRules.py")
		os.remove(publishPath + os.sep + "DemoRules.pyc")

	if len(sys.argv) == 4 and sys.argv[3] == "se":
		shutil.copyfile(pythonPath+"SERules.py",publishPath + os.sep + "SERules.py")
		script_dir = os.path.dirname(os.path.realpath(__file__)) + os.sep		
		shutil.copyfile(script_dir+"clear_se_resources.py",publishPath + os.sep + "clear_se.py")		
		shutil.copyfile(script_dir+"clear_unused_resources.py",publishPath + os.sep + "clear.py")
		os.chdir(publishPath)
		subprocess.call(["python", publishPath + os.sep + "clear.py"])
		subprocess.call(["python", publishPath + os.sep + "clear_se.py"])	
		os.remove(publishPath + os.sep + "clear.py")		
		os.remove(publishPath + os.sep + "clear_se.py")		
		os.remove(publishPath + os.sep + "SERules.py")
		os.remove(publishPath + os.sep + "SERules.pyc")
	
	if len(sys.argv) >= 3 and sys.argv[2] == "packed":
		shutil.copyfile(pythonPath+"PackRules.py",publishPath + os.sep + "PackRules.py")
		script_dir = os.path.dirname(os.path.realpath(__file__)) + os.sep		
		shutil.copyfile(script_dir+"clear_unused_resources.py",publishPath + os.sep + "clear.py")
		shutil.copyfile(script_dir+"pack_game_data.py",publishPath + os.sep + "pack.py")		
		os.chdir(publishPath)
		subprocess.call(["python", publishPath + os.sep + "clear.py"])
		subprocess.call(["python", publishPath + os.sep + "pack.py"])
		os.remove(publishPath + os.sep + "clear.py")
		os.remove(publishPath + os.sep + "pack.py")
		os.remove(publishPath + os.sep + "PackRules.py")
		os.remove(publishPath + os.sep + "PackRules.pyc")

	removeEmptyFolders(publishPath + os.sep + "Resources")
	BeepMessage()	
	exit(0)
if len(sys.argv) == 2 and sys.argv[1] == 'update-engine':
	callPath = os.path.abspath(os.getcwd())
	script_dir = os.path.dirname(os.path.realpath(__file__)) + os.sep

	canLoad = os.path.isdir(callPath+os.sep+"Engine") 
	if canLoad == False:
		print("Not a project root dir!")
		exit(0)

	engineDir = callPath + os.sep + "Engine"
	
	shutil.rmtree(engineDir,ignore_errors=True)
	os.mkdir(engineDir)
	
	distutils.dir_util.copy_tree(script_dir+"../../dist", engineDir+os.sep+"dist")
	distutils.dir_util.copy_tree(script_dir+"../../external", engineDir+os.sep+"external")
	distutils.dir_util.copy_tree(script_dir+"../../include", engineDir+os.sep+"include")
	distutils.dir_util.copy_tree(script_dir+"../../lib", engineDir+os.sep+"lib")
	distutils.dir_util.copy_tree(script_dir+"../../solution/android", engineDir+os.sep+"solution/android")
	distutils.dir_util.copy_tree(script_dir+"../../solution/ios_osx", engineDir+os.sep+"solution/ios_osx")
	os.mkdir(engineDir + os.sep + "solution" + os.sep + "win32")
	shutil.copyfile(script_dir+"../.."+ os.sep + "solution" + os.sep + "win32" + os.sep + "NBG_Engine.vcxproj.filters",engineDir + os.sep + "solution" + os.sep + "win32" + os.sep + "NBG_Engine.vcxproj.filters")
	shutil.copyfile(script_dir+"../.."+ os.sep + "solution" + os.sep + "win32" + os.sep + "NBG_Engine.vcxproj.user",engineDir + os.sep + "solution" + os.sep + "win32" + os.sep + "NBG_Engine.vcxproj.user")
	shutil.copyfile(script_dir+"../.."+ os.sep + "solution" + os.sep + "win32" + os.sep + "NBG_Engine.vcxproj",engineDir + os.sep + "solution" + os.sep + "win32" + os.sep + "NBG_Engine.vcxproj")
	distutils.dir_util.copy_tree(script_dir+"../../src", engineDir+os.sep+"src")
	shutil.copyfile(script_dir+"../../Readme.txt",engineDir + os.sep + "Readme.txt")

        BeepMessage()
	exit(0)