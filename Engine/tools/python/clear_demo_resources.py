import os      
import shutil
import DemoRules
from xml.dom import minidom

print("Start clear demo resources")

working_dir = "Resources"              

def clear_non_demo_files(dir):     	
	filesToDelete = []
	dirsToDelete = []

	subdirs = [x[0] for x in os.walk(dir)]
	for subdir in subdirs:	
		if (DemoRules.CheckSubdir(subdir) == True):
			if os.path.isdir(subdir):
				dirsToDelete.append(subdir)				
			continue
		files = os.walk(subdir).next()[2]
		if (len(files) > 0):
			for file in files:				
				if (DemoRules.CheckSubdirAndFile(subdir, file)) == True:
					pathToFile = os.path.join(subdir, file)
					if os.path.exists(pathToFile):
						filesToDelete.append(pathToFile)										
					continue				
						
	for file in filesToDelete: 
		os.remove(file)  
	
	
	for dir in dirsToDelete: 
		if os.path.isdir(dir):
			shutil.rmtree(dir)  		
	

#pack game data into game.pack
clear_non_demo_files(working_dir)           
print("End clear demo resources: Success")