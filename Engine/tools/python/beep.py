from winsound import Beep
import math

start = 10
end = 18

mid = start + (end-start)/2

for i in range(start,end):	
	code = i
	if i > mid:
		code = start + end-i
	Beep(code*150,100);
